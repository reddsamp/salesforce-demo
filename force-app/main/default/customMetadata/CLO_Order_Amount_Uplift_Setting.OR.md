<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Standard Order</label>
    <protected>false</protected>
    <values>
        <field>CLO_Order_Or_Work_Order_Type__c</field>
        <value xsi:type="xsd:string">OR</value>
    </values>
    <values>
        <field>CLO_Uplift_Multiplication_Factor__c</field>
        <value xsi:type="xsd:double">0.4</value>
    </values>
</CustomMetadata>

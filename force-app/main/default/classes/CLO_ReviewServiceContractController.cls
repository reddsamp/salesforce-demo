public class CLO_ReviewServiceContractController {
    @AuraEnabled
    public static ServiceContract getServiceContract(String serviceContractId)
    {
            ServiceContract serviceCon = [select Id, Name, Account.AccountNumber, SAP_Order_Id__c, SBQQSC__Quote__r.Id, CLO_SAP_Sync_Error__c, CLO_Paymetric_Error__c FROM ServiceContract where id =: serviceContractId];
            List<CLO_Paymetric_Payment__c> lstPayments = new List<CLO_Paymetric_Payment__c>();
            Set<Id> quoteIds =new Set<Id>();
            /*if(serviceCon.SBQQSC__Quote__r.Id != null)
            {
                update new ServiceContract(Id = serviceCon.Id, CLO_SAP_Sync_Error__c = '', CLO_Paymetric_Error__c = '');
            }*/
            if(serviceCon.SBQQSC__Quote__r.Id != null && serviceCon.CLO_SAP_Sync_Error__c != '' && serviceCon.CLO_SAP_Sync_Error__c != null) {
                quoteIds.add(serviceCon.SBQQSC__Quote__r.Id);
                lstPayments = [SELECT id, CLO_Authorization_Code__c, CLO_Authorization_Reference_Code__c, CLO_Credit_Card_Authorized_Date__c, CreatedDate, CLO_Quote__c, CLO_TransactionID__c, CLO_StatusCode__c, CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c, CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c, CLO_Message__c,CLO_Credit_Card_Authorized_Time__c FROM CLO_Paymetric_Payment__c WHERE CLO_Quote__c = :quoteIds AND CLO_StatusCode__c = '100' ORDER BY CreatedDate DESC LIMIT 1 ];
                
                if(lstPayments != null && lstPayments.size() > 0)
                {
                    update new ServiceContract(Id = serviceCon.Id, CLO_SAP_Sync_Error__c = '', CLO_Paymetric_Error__c = '');
                }
            }
        
        return [select Id, Name, Account.AccountNumber, SAP_Order_Id__c, CLO_SAP_Sync_Error__c, CLO_Paymetric_Error__c, (Select Id, LineItemNumber, SBQQSC__PackageProductDescription__c, CLO_Total_Discount_Amount__c, CLO_List_Total__c,UnitPrice, TotalPrice, CLO_Tax__c, SBQQSC__Quantity__c FROM ContractLineItems) FROM ServiceContract where id =: serviceContractId];
    }
    
    @AuraEnabled
    public static List<ContractLineItem> getContractLineItems(String serviceContractId)
    {
        return [Select Id, LineItemNumber, SBQQSC__PackageProductDescription__c, CLO_Total_Discount_Amount__c, CLO_List_Total__c, UnitPrice, TotalPrice, CLO_Tax__c, SBQQSC__Product__r.Primary_Unit_of_Measure__c, SBQQSC__Quantity__c FROM ContractLineItem WHERE ServiceContractId =: serviceContractId];
    }
    
    @AuraEnabled
    public static ServiceContract syncWithSAP(String serviceContractId, Boolean isSimulateContract)
    {
        System.debug('isSimulateContract :: '+isSimulateContract);
        List<String> productNames = new List<String>();
        List<ContractLineItem> contractLineItems = new List<ContractLineItem>();
        for(ServiceContract serContract :[select Id, CLO_SAP_Sync_Error__c, Status, SAP_Order_Id__c, CLO_Ship_To__c, ContractNumber, CLO_PONumber__c, 
                                         CLO_Sales_Org__c, CLO_TransactionType__c, Account.AccountNumber, CLO_Payment_Terms__c, CLO_Incoterm_1__c,
                                         Account.CLO_Sales_Org__c, Account.CLO_Customer_Number__c, CreatedBy.Email, Owner.Email,CLO_Incoterm_2__c, CLO_Order_Reason__c,
                                         StartDate, EndDate, CLO_Shipping_Condition__c, Description, CLO_Asset_Number__c, CLO_Asset_Material_Number__c,
                                         Name, CLO_NetAmount__c, Account.CLO_Incoterm_1__c, Account.CLO_Incoterm_2__c, Account.CLO_Shipping_Condition__c,
                                         CurrencyIsoCode, CreatedDate, CLO_Opportunity_Type__c, CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c,
                                         CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c, CLO_Authorization_Amount__c,
                                         CLO_Payment_Card__r.CLO_CC_Expiry_Date__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c, CLO_Paymetric_Error__c, SBQQSC__Quote__r.Id, CLO_Payment_Transction_ID__c,
                                         (Select Id, LineItemNumber, SBQQSC__PackageProductDescription__c, CLO_Total_Discount_Amount__c, CLO_List_Total__c, UnitPrice, TotalPrice, 
                                                 CLO_Tax__c, CLO_Asset_Serial_Number__c, SBQQSC__Product__r.ProductCode, SBQQSC__Product__r.Primary_Unit_of_Measure__c, SBQQSC__Quantity__c, CLO_End_Date__c, EndDate, CLO_Start_Date__c, StartDate FROM ContractLineItems) FROM ServiceContract where id =: serviceContractId]){
                                       
            CLO_SAP_ServiceContract.invokeCreateServiceContract(serContract, isSimulateContract);    
        }
                
        return [select Id, CLO_SAP_Sync_Error__c, SAP_Order_Id__c, CLO_Paymetric_Error__c,
                (Select Id, LineItemNumber, SBQQSC__PackageProductDescription__c, CLO_Total_Discount_Amount__c, CLO_List_Total__c, UnitPrice, CLO_Tax__c, SBQQSC__Quantity__c, TotalPrice FROM ContractLineItems) from ServiceContract where id =: serviceContractId];
    }
}
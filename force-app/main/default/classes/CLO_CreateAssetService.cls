global class CLO_CreateAssetService {
    webservice static void CreateAsset(String CLO_SAP_External_Id, String CLO_SAP_Customer_Number, String CLO_Asset_Name, String CLO_Asset_Number, String CLO_SAP_Equipment_Number, String CLO_SAP_Equipment_Status, String CLO_SAP_Product_code, String InsertOrUpdate) {
        try
        {
            List<Account> lstAccount = [SELECT Id FROM Account WHERE AccountNumber =: CLO_SAP_Customer_Number];
            List<Product2> lstProduct = [SELECT Id from Product2 WHERE ProductCode =: CLO_SAP_Product_code];
            List<Asset> lstExistingAsset = [SELECT Id FROM Asset WHERE CLO_SAP_External_Id__c =: CLO_SAP_External_Id];

            Asset asset = new Asset();            
            
            asset.Name = CLO_Asset_Name;
            asset.accountId = lstAccount[0].Id;
            asset.SerialNumber = CLO_Asset_Number;
            asset.status = CLO_SAP_Equipment_Status;
            asset.CLO_SAP_Equipment_Number__c = CLO_SAP_Equipment_Number;
            asset.CLO_SAP_Equipment_Status__c = CLO_SAP_Equipment_Status;
            asset.CLO_SAP_External_Id__c = CLO_SAP_External_Id;
            asset.Product2Id = lstProduct[0].Id;
            
            if (lstExistingAsset.size() > 0){
                asset.Id = lstExistingAsset[0].Id;
            }
            
            Database.UpsertResult ur = Database.upsert(asset, false);
        }
        
        Catch(Exception ex)
        {
            throw new CLO_CreateAsset_Exception('FAILED TO CREATE/UPDATE THE ASSET'+ '\n' + ex.getMessage());
        }
                
    }
    
    public class CLO_CreateAsset_Exception extends Exception {}

}
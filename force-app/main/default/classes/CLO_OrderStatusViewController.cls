public class CLO_OrderStatusViewController
{
    
    @AuraEnabled
    public static List<OrderStatusWrapper> fetchOrderStatus()
    {
        List<OrderStatusWrapper> statusList = new List<OrderStatusWrapper>();
        
        String objectName = 'Order';
        String fieldName ='Status';
          
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            statusList.add(new OrderStatusWrapper(pickListVal.getLabel(), 'slds-path__item slds-is-incomplete ' + pickListVal.getValue()));
        }    
        
        return statusList;
    }
    
    public class OrderStatusWrapper
    {
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String cls {get;set;}
        
        public OrderStatusWrapper(String status, String cls)
        {
            this.status = status;
            this.cls = cls;
        }
    }
    
    public class OrderWrapper
    {
        @AuraEnabled public Order order {get;set;}
        @AuraEnabled public List<boolean> statusList {get;set;}
        
        public OrderWrapper(Order order, List<boolean> statusList)
        {
            this.order = order;
            this.statusList = statusList;
        }
    }
    
    @AuraEnabled
    public static List<OrderWrapper> fetchOrderList()
    {
        List<OrderWrapper> orderWList = new List<OrderWrapper>();
        List<OrderStatusWrapper> sList = fetchOrderStatus();
        
        for(Order o : [select id, OrderNumber, toLabel(Status) from Order where Status != null])
        {
            List<boolean> statusList = new List<boolean>();
            
            for(OrderStatusWrapper s : sList)
            {
                if(s.status == o.Status)
                {
                    statusList.add(true);
                }
                else
                {
                    statusList.add(false);
                }
            }
            
            orderWList.add(new OrderWrapper(o, statusList));    
        }
        
        return orderWList;
    }
}
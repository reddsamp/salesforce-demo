global class CLO_ScheduledWarrantyEntitlements implements Schedulable {
   global void execute(SchedulableContext sc) {
      CLO_WarrantyEntitlementBatch objBatch = new CLO_WarrantyEntitlementBatch(); 
      database.executebatch(objBatch);
   }
}
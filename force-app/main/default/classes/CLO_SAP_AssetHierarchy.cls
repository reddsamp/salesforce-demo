Public Class CLO_SAP_AssetHierarchy
{
    Public Static void CreateAssetHierarchy(Asset assetObj)
    {
        /*Authentication using bearer token*/
        SAPAssetHierarchy.ZSF_ASSET_HIERARCHY assetHierarchyObj = new SAPAssetHierarchy.ZSF_ASSET_HIERARCHY();
        assetHierarchyObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSF_ASSET_HIERARCHY');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        assetHierarchyObj.inputHttpHeaders_x = new Map<String,String>();
        assetHierarchyObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        assetHierarchyObj.timeout_x = 120000;
        
        String EQUI_NO = assetObj.CLO_SAP_Equipment_Number__c;
        SAPAssetHierarchy.TABLE_OF_V_EQUI EQUI_TAB = NULL;
        
        SAPAssetHierarchy.TABLE_OF_V_EQUI responseObj = assetHierarchyObj.ZSF_ASSET_HIERARCHY(EQUI_NO, EQUI_TAB);
        
        if(responseObj != NULL){            
            if(responseObj.item != NULL && responseObj.item.size() > 0){
                List<Asset> lstChildAssets = new List<Asset>();
                for(SAPAssetHierarchy.V_EQUI sapChildAsset : responseObj.item){
                    Asset childAsset = new Asset();
                    childAsset.ParentId = assetObj.Id;
                    childAsset.CLO_SAP_Equipment_Number__c = sapChildAsset.EQUNR;
                    //for testing - remove later
                    Product2 product = [Select Id from Product2 where ProductCode =: sapChildAsset.MATNR];
                    childAsset.Product2Id = product.Id;
                    //childAsset.Product2.ProductCode = sapChildAsset.MATNR;
                    childAsset.Name = sapChildAsset.MATNR + ' | ' + sapChildAsset.SERNR; 
                    childAsset.SerialNumber = sapChildAsset.SERNR;
                    childAsset.AccountId = assetObj.AccountId;
                    lstChildAssets.add(childAsset);
                }
                
                if(lstChildAssets.size() > 0){
                    insert lstChildAssets;
                }
            }
        }
    }

}
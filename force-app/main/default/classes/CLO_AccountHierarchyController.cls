public class CLO_AccountHierarchyController
{
    @AuraEnabled
    public static List<Account> getAccounts(String accountId) {
        List<Account> accounts=[SELECT Id, Name FROM Account where id=:accountId];
        return accounts;
    }

    @AuraEnabled
    public static List<Account> getChildAccounts(String ParentAccountid) {
        List<Account> accounts=[SELECT Id, Name FROM Account where parentid=:ParentAccountid limit 20];
        return accounts;
    }
    
    @AuraEnabled
    public static List<Account> findHierarchyData(string recId){
        List<Account> accList = new List<Account>();
        string queryString = 'select id,name,type,industry,parentId from Account ';
        
        //Section to get all child account details from ultimate parent starts-------------------------
        List<String> currentParent      = new List<String>{};
        Integer level               = 0;
        Boolean endOfStructure      = false;
        
        //method to find ultimate parent of account
        string topMostparent = GetUltimateParentId(recId );
        system.debug('*******topMostparent:'+topMostparent);
        currentParent.add(topMostparent);
        
        //Loop though all children
        string finalQueryString = '';
        List<Account>  queryOutput = new List<Account> ();
        while ( !endOfStructure ){  
            if( level == 0 ){
                finalQueryString = queryString + ' where id IN : CurrentParent ORDER BY ParentId  Limit 1000';
            } 
            else {
                finalQueryString = queryString + ' where ParentID IN : CurrentParent ORDER BY ParentId Limit 1000';
            }
            
            if(finalQueryString != null && finalQueryString !=''){
                try{
                    if(Limits.getLimitQueries()-Limits.getQueries()>0){
                        queryOutput = database.query(finalQueryString);
                    }else{
                        endOfStructure = true;
                    }
                }catch(exception ex){ 
                    endOfStructure = true;
                }
            }
            
            if( queryOutput.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                //iterating through query output
                for ( Integer i = 0 ; i < queryOutput.size(); i++ ){
                    currentParent.add(queryOutput[i].Id);
                    accList.add(queryOutput[i]);
                 }
            }
            level++;
        }
        return accList;
    }
    
    @AuraEnabled
    public static List<AccountHierarchyDataWrapper> fetchHierarchyData(string recId){
        List<AccountHierarchyDataWrapper> dataList = new List<AccountHierarchyDataWrapper>();
        Map<String, List<Account>> accountMap = new Map<String, List<Account>>();
        
        string topMostparent = GetUltimateParentId(recId);
        string queryString = 'Select Id,Name,Type,Industry,ParentId,(Select Id, FirstName, LastName, Phone, Email from Contacts), (Select Id, Name from Opportunities) from Account ';
        Integer level               = 0;
        Boolean endOfStructure      = false;
        List<String> currentParent      = new List<String>{};
        currentParent.add(topMostparent);
        
        string finalQueryString = '';
        List<Account>  queryOutput = new List<Account> ();
        Account parentAccount = new Account();
        
        while ( !endOfStructure ){  
            if( level == 0 ){
                finalQueryString = queryString + ' where id IN : CurrentParent ORDER BY ParentId  Limit 1000';
            } 
            else {
                finalQueryString = queryString + ' where ParentID IN : CurrentParent ORDER BY ParentId Limit 1000';
            }
            
            if(finalQueryString != null && finalQueryString !=''){
                try{
                    if(Limits.getLimitQueries()-Limits.getQueries()>0){
                        queryOutput = database.query(finalQueryString);
                        
                        if(level == 0)
                            parentAccount = queryOutput[0];
                    }else{
                        endOfStructure = true;
                    }
                }catch(exception ex){ 
                    endOfStructure = true;
                }
            }
            
            if( queryOutput.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                //iterating through query output
                for ( Integer i = 0 ; i < queryOutput.size(); i++ ){
                    currentParent.add(queryOutput[i].Id);
                    
                    List<Account> tempList = accountMap.get(queryOutput[i].ParentId);
                    
                    if(tempList == null) tempList = new List<Account>();
                    
                    tempList.add(queryOutput[i]);
                    
                    accountMap.put(queryOutput[i].ParentId, tempList);
                }
            }
            level++;
        }
        
        system.debug('accountMap :::::' + accountMap);
        
        List<Account> startList = accountMap.get(topMostparent);
        
        integer marginLeft = 0;
        
        dataList.add(new AccountHierarchyDataWrapper(parentAccount, true, marginLeft));
        
        if(startList != null && startList.size() > 0){
            for(Account acc: startList)
            {
                marginLeft = 30;
                
                dataList.add(new AccountHierarchyDataWrapper(acc, true, marginLeft));
                
                List<Account> tempList = accountMap.get(acc.Id);
                
                if(tempList != null && tempList.size() > 0){
                    for(Account acc1: tempList)
                    {
                        marginLeft = 60;
                        
                        dataList.add(new AccountHierarchyDataWrapper(acc1, true, marginLeft));
                        
                        List<Account> tempList2 = accountMap.get(acc1.Id);
                        
                        if(tempList2 != null && tempList2.size() > 0){
                            for(Account acc2: tempList2)
                            {
                                marginLeft = 90;
                                
                                dataList.add(new AccountHierarchyDataWrapper(acc2, true, marginLeft));
                                
                                List<Account> tempList3 = accountMap.get(acc2.Id);
                                
                                if(tempList3 != null && tempList3.size() > 0){
                                    for(Account acc3: tempList3)
                                    {
                                        marginLeft = 120;
                                        
                                        dataList.add(new AccountHierarchyDataWrapper(acc3, true, marginLeft));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return dataList;
    }
    
    @AuraEnabled
    public static Account fetchTopmostAccount(string recId){
        Account account = new Account();
        string topMostparent = GetUltimateParentId(recId);
        string queryString = 'Select Id,Name,Type,Industry,ParentId,(Select Id, FirstName, Phone, Email from Contacts), (Select Id, Name, StageName from Opportunities) from Account ';
        Integer level               = 0;
        Boolean endOfStructure      = false;
        List<String> currentParent      = new List<String>{};
        currentParent.add(topMostparent);
        
        string finalQueryString = '';
        List<Account>  queryOutput = new List<Account> ();
        
        finalQueryString = queryString + ' where id IN : CurrentParent ORDER BY ParentId  Limit 1000';
        queryOutput = database.query(finalQueryString);
        
        return queryOutput[0];
    }
    
    public class AccountHierarchyWrapper{
        @AuraEnabled public List<AccountHierarchyDataWrapper> accList {get;set;}
        
        public AccountHierarchyWrapper(List<AccountHierarchyDataWrapper> accList)
        {
            this.accList = accList;
        }
    }
    
    public class AccountHierarchyDataWrapper{
        @AuraEnabled public Account acc {get;set;}
        @AuraEnabled public boolean showIcon {get;set;}
        @AuraEnabled public Integer marginLeft {get;set;}
        
        public AccountHierarchyDataWrapper(Account acc, boolean showIcon, integer marginLeft)
        {
            this.acc = acc;
            this.showIcon = showIcon;
            this.marginLeft = marginLeft;
        }
    }
    
    // Find the tom most element in Heirarchy  
    // @return objId
    public static String GetUltimateParentId( string recId ){
        Boolean top = false;
        while ( !top ) {
            string queryString = 'select id ,name, ParentId from Account where Id =:recId LIMIT 1';
            Account acc = database.query(queryString);
            if ( acc.parentId != null ) {
                recId = acc.parentId;
            }else {
                top = true;
            }
        }
        return recId ;
    }
}
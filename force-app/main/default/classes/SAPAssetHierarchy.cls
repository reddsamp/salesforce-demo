//Generated by wsdl2apex

public class SAPAssetHierarchy {
    public class V_EQUI {
        public String MANDT;
        public String EQUNR;
        public String ERDAT;
        public String ERNAM;
        public String EQASP;
        public String AEDAT;
        public String AENAM;
        public String BEGRU;
        public String EQTYP;
        public String EQART;
        public String LVORM;
        public String INVNR;
        public String GROES;
        public String BRGEW;
        public String GEWEI;
        public String ANSDT;
        public String ANSWT;
        public String WAERS;
        public String ELIEF;
        public String GWLEN;
        public String GWLDT;
        public String WDBWT;
        public String HERST;
        public String HERLD;
        public String HZEIN;
        public String SERGE;
        public String TYPBZ;
        public String BAUJJ;
        public String BAUMM;
        public String APLKZ;
        public String AULDT;
        public String INBDT;
        public String GERNR;
        public String GWLDV;
        public String EQDAT;
        public String EQBER;
        public String EQNUM;
        public String OBJNR;
        public String EQSNR;
        public String CUOBJ;
        public String KRFKZ;
        public String KMATN;
        public String MATNR;
        public String SERNR;
        public String WERK;
        public String LAGER;
        public String CHARGE;
        public String KUNDE;
        public String WARPL;
        public String IMRC_POINT;
        public String REVLV;
        public String MGANR;
        public String BEGRUI;
        public String S_EQUI;
        public String S_SERIAL;
        public String S_KONFI;
        public String S_SALE;
        public String S_FHM;
        public String S_ELSE;
        public String S_ISU;
        public String S_EQBS;
        public String S_FLEET;
        public String BSTVP;
        public String SPARTE;
        public String HANDLE;
        public String TSEGTP;
        public String EMATN;
        public String ACT_CHANGE_AA;
        public String S_CC;
        public String DATLWB;
        public String UII;
        public String IUID_TYPE;
        public String UII_PLANT;
        public String CHANGEDDATETIME;
        public String DUMMY;
        public String ZZITO_UDI;
        public String EQEXT_ACTIVE;
        public String EQUI_SRTYPE;
        public String EQUI_SNTYPE;
        public String EQLB_DUTY;
        public String EQLB_HIDE;
        public String J_3GDISPO;
        public String J_3GZDEQUI;
        public String J_3GEQART;
        public String J_3GKZMENG;
        public String J_3GKONDE;
        public String J_3GFIKTIV;
        public String J_3GBELTYP;
        public String MEINS;
        public String J_3GKZLADG;
        public String J_3GKZBERG;
        public String J_3GEIFR;
        public String J_3GVERMEIN;
        public String J_3GZULNR;
        public String x_xSAPCEM_xABRECHVH;
        public String x_xSAPCEM_xABRECHLG;
        public String x_xSAPCEM_xDISPOGR;
        public String DUMMY1;
        public String DATBI;
        public String EQLFN;
        public String EQUZN;
        public String TIMBI;
        public String ESTAI;
        public String ESTAE;
        public String STNAM;
        public String DATAB;
        public String IWERK;
        public String IWERKI;
        public String SUBMT;
        public String MAPAR;
        public String HEQUI;
        public String HEQNR;
        public String INGRP;
        public String INGRPI;
        public String PM_OBJTY;
        public String GEWRK;
        public String GEWRKI;
        public String TIDNR;
        public String TIDNRI;
        public String ILOAN;
        public String KUND1;
        public String KUND2;
        public String KUND3;
        public String LIZNR;
        public String RBNR;
        public String EZDAT;
        public String EZBER;
        public String EZNUM;
        public String RBNR_I;
        public String IBLNR;
        public String BLDAT;
        public String PVS_FOCUS;
        public String PPEGUID;
        public String TECHS;
        public String FUNCID;
        public String FRCFIT;
        public String FRCRMV;
        public String SEGCHANGEDDATETIME;
        public String PVS_NODE;
        public String PVS_OTYPE;
        public String PVS_APPLOBJ_TYPE;
        public String TOP_EQUI;
        public String TOP_EQUI_FLAG;
        public String J_3GEIGNER;
        public String J_3GVERWAL;
        public String J_3GPMAUFE;
        public String J_3GPMAUFV;
        public String J_3GPACHT;
        public String SPRAS;
        public String EQKTX;
        public String KZLTX;
        public String TXASP;
        public String EQKTU;
        public String TEXTCHANGEDDATETIME;
        public String TPLNR;
        public String ABCKZ;
        public String ABCKZI;
        public String EQFNR;
        public String EQFNRI;
        public String SWERK;
        public String SWERKI;
        public String STORT;
        public String STORTI;
        public String MSGRP;
        public String MSGRPI;
        public String BEBER;
        public String BEBERI;
        public String CR_OBJTY;
        public String PPSID;
        public String PPSIDI;
        public String GSBER;
        public String GSBERI;
        public String KOKRS;
        public String KOKRSI;
        public String KOSTL;
        public String KOSTLI;
        public String PROID;
        public String PROIDI;
        public String BUKRS;
        public String BUKRSI;
        public String ANLNR;
        public String ANLNRI;
        public String ANLUN;
        public String ANLUNI;
        public String DAUFN;
        public String DAUFNI;
        public String AUFNR;
        public String AUFNRI;
        public String TPLNRI;
        public String VKORG;
        public String VKORGI;
        public String VTWEG;
        public String VTWEGI;
        public String SPART;
        public String SPARTI;
        public String ADRNR;
        public String ADRNRI;
        public String OWNER;
        public String VKBUR;
        public String VKGRP;
        public String LVORZ;
        public String ERNAZ;
        public String ERDAZ;
        public String AENAZ;
        public String AEDAZ;
        private String[] MANDT_type_info = new String[]{'MANDT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQUNR_type_info = new String[]{'EQUNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ERDAT_type_info = new String[]{'ERDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ERNAM_type_info = new String[]{'ERNAM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQASP_type_info = new String[]{'EQASP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AEDAT_type_info = new String[]{'AEDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AENAM_type_info = new String[]{'AENAM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BEGRU_type_info = new String[]{'BEGRU','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQTYP_type_info = new String[]{'EQTYP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQART_type_info = new String[]{'EQART','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LVORM_type_info = new String[]{'LVORM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INVNR_type_info = new String[]{'INVNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GROES_type_info = new String[]{'GROES','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BRGEW_type_info = new String[]{'BRGEW','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GEWEI_type_info = new String[]{'GEWEI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ANSDT_type_info = new String[]{'ANSDT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ANSWT_type_info = new String[]{'ANSWT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WAERS_type_info = new String[]{'WAERS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ELIEF_type_info = new String[]{'ELIEF','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GWLEN_type_info = new String[]{'GWLEN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GWLDT_type_info = new String[]{'GWLDT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WDBWT_type_info = new String[]{'WDBWT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] HERST_type_info = new String[]{'HERST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] HERLD_type_info = new String[]{'HERLD','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] HZEIN_type_info = new String[]{'HZEIN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SERGE_type_info = new String[]{'SERGE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TYPBZ_type_info = new String[]{'TYPBZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BAUJJ_type_info = new String[]{'BAUJJ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BAUMM_type_info = new String[]{'BAUMM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] APLKZ_type_info = new String[]{'APLKZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AULDT_type_info = new String[]{'AULDT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INBDT_type_info = new String[]{'INBDT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GERNR_type_info = new String[]{'GERNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GWLDV_type_info = new String[]{'GWLDV','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQDAT_type_info = new String[]{'EQDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQBER_type_info = new String[]{'EQBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQNUM_type_info = new String[]{'EQNUM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] OBJNR_type_info = new String[]{'OBJNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQSNR_type_info = new String[]{'EQSNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CUOBJ_type_info = new String[]{'CUOBJ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KRFKZ_type_info = new String[]{'KRFKZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KMATN_type_info = new String[]{'KMATN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MATNR_type_info = new String[]{'MATNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SERNR_type_info = new String[]{'SERNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WERK_type_info = new String[]{'WERK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LAGER_type_info = new String[]{'LAGER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CHARGE_type_info = new String[]{'CHARGE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUNDE_type_info = new String[]{'KUNDE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WARPL_type_info = new String[]{'WARPL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IMRC_POINT_type_info = new String[]{'IMRC_POINT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] REVLV_type_info = new String[]{'REVLV','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MGANR_type_info = new String[]{'MGANR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BEGRUI_type_info = new String[]{'BEGRUI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_EQUI_type_info = new String[]{'S_EQUI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_SERIAL_type_info = new String[]{'S_SERIAL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_KONFI_type_info = new String[]{'S_KONFI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_SALE_type_info = new String[]{'S_SALE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_FHM_type_info = new String[]{'S_FHM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_ELSE_type_info = new String[]{'S_ELSE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_ISU_type_info = new String[]{'S_ISU','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_EQBS_type_info = new String[]{'S_EQBS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_FLEET_type_info = new String[]{'S_FLEET','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BSTVP_type_info = new String[]{'BSTVP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SPARTE_type_info = new String[]{'SPARTE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] HANDLE_type_info = new String[]{'HANDLE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TSEGTP_type_info = new String[]{'TSEGTP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EMATN_type_info = new String[]{'EMATN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ACT_CHANGE_AA_type_info = new String[]{'ACT_CHANGE_AA','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_CC_type_info = new String[]{'S_CC','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DATLWB_type_info = new String[]{'DATLWB','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] UII_type_info = new String[]{'UII','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IUID_TYPE_type_info = new String[]{'IUID_TYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] UII_PLANT_type_info = new String[]{'UII_PLANT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CHANGEDDATETIME_type_info = new String[]{'CHANGEDDATETIME','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DUMMY_type_info = new String[]{'DUMMY','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ZZITO_UDI_type_info = new String[]{'ZZITO_UDI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQEXT_ACTIVE_type_info = new String[]{'EQEXT_ACTIVE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQUI_SRTYPE_type_info = new String[]{'EQUI_SRTYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQUI_SNTYPE_type_info = new String[]{'EQUI_SNTYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQLB_DUTY_type_info = new String[]{'EQLB_DUTY','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQLB_HIDE_type_info = new String[]{'EQLB_HIDE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GDISPO_type_info = new String[]{'J_3GDISPO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GZDEQUI_type_info = new String[]{'J_3GZDEQUI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GEQART_type_info = new String[]{'J_3GEQART','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GKZMENG_type_info = new String[]{'J_3GKZMENG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GKONDE_type_info = new String[]{'J_3GKONDE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GFIKTIV_type_info = new String[]{'J_3GFIKTIV','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GBELTYP_type_info = new String[]{'J_3GBELTYP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MEINS_type_info = new String[]{'MEINS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GKZLADG_type_info = new String[]{'J_3GKZLADG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GKZBERG_type_info = new String[]{'J_3GKZBERG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GEIFR_type_info = new String[]{'J_3GEIFR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GVERMEIN_type_info = new String[]{'J_3GVERMEIN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GZULNR_type_info = new String[]{'J_3GZULNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] x_xSAPCEM_xABRECHVH_type_info = new String[]{'_-SAPCEM_-ABRECHVH','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] x_xSAPCEM_xABRECHLG_type_info = new String[]{'_-SAPCEM_-ABRECHLG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] x_xSAPCEM_xDISPOGR_type_info = new String[]{'_-SAPCEM_-DISPOGR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DUMMY1_type_info = new String[]{'DUMMY1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DATBI_type_info = new String[]{'DATBI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQLFN_type_info = new String[]{'EQLFN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQUZN_type_info = new String[]{'EQUZN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TIMBI_type_info = new String[]{'TIMBI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ESTAI_type_info = new String[]{'ESTAI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ESTAE_type_info = new String[]{'ESTAE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STNAM_type_info = new String[]{'STNAM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DATAB_type_info = new String[]{'DATAB','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IWERK_type_info = new String[]{'IWERK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IWERKI_type_info = new String[]{'IWERKI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SUBMT_type_info = new String[]{'SUBMT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MAPAR_type_info = new String[]{'MAPAR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] HEQUI_type_info = new String[]{'HEQUI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] HEQNR_type_info = new String[]{'HEQNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INGRP_type_info = new String[]{'INGRP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INGRPI_type_info = new String[]{'INGRPI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PM_OBJTY_type_info = new String[]{'PM_OBJTY','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GEWRK_type_info = new String[]{'GEWRK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GEWRKI_type_info = new String[]{'GEWRKI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TIDNR_type_info = new String[]{'TIDNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TIDNRI_type_info = new String[]{'TIDNRI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ILOAN_type_info = new String[]{'ILOAN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUND1_type_info = new String[]{'KUND1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUND2_type_info = new String[]{'KUND2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUND3_type_info = new String[]{'KUND3','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LIZNR_type_info = new String[]{'LIZNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] RBNR_type_info = new String[]{'RBNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EZDAT_type_info = new String[]{'EZDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EZBER_type_info = new String[]{'EZBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EZNUM_type_info = new String[]{'EZNUM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] RBNR_I_type_info = new String[]{'RBNR_I','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IBLNR_type_info = new String[]{'IBLNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BLDAT_type_info = new String[]{'BLDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PVS_FOCUS_type_info = new String[]{'PVS_FOCUS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PPEGUID_type_info = new String[]{'PPEGUID','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TECHS_type_info = new String[]{'TECHS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] FUNCID_type_info = new String[]{'FUNCID','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] FRCFIT_type_info = new String[]{'FRCFIT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] FRCRMV_type_info = new String[]{'FRCRMV','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SEGCHANGEDDATETIME_type_info = new String[]{'SEGCHANGEDDATETIME','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PVS_NODE_type_info = new String[]{'PVS_NODE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PVS_OTYPE_type_info = new String[]{'PVS_OTYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PVS_APPLOBJ_TYPE_type_info = new String[]{'PVS_APPLOBJ_TYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TOP_EQUI_type_info = new String[]{'TOP_EQUI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TOP_EQUI_FLAG_type_info = new String[]{'TOP_EQUI_FLAG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GEIGNER_type_info = new String[]{'J_3GEIGNER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GVERWAL_type_info = new String[]{'J_3GVERWAL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GPMAUFE_type_info = new String[]{'J_3GPMAUFE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GPMAUFV_type_info = new String[]{'J_3GPMAUFV','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_3GPACHT_type_info = new String[]{'J_3GPACHT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SPRAS_type_info = new String[]{'SPRAS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQKTX_type_info = new String[]{'EQKTX','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KZLTX_type_info = new String[]{'KZLTX','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TXASP_type_info = new String[]{'TXASP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQKTU_type_info = new String[]{'EQKTU','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TEXTCHANGEDDATETIME_type_info = new String[]{'TEXTCHANGEDDATETIME','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TPLNR_type_info = new String[]{'TPLNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ABCKZ_type_info = new String[]{'ABCKZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ABCKZI_type_info = new String[]{'ABCKZI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQFNR_type_info = new String[]{'EQFNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQFNRI_type_info = new String[]{'EQFNRI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SWERK_type_info = new String[]{'SWERK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SWERKI_type_info = new String[]{'SWERKI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STORT_type_info = new String[]{'STORT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STORTI_type_info = new String[]{'STORTI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MSGRP_type_info = new String[]{'MSGRP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MSGRPI_type_info = new String[]{'MSGRPI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BEBER_type_info = new String[]{'BEBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BEBERI_type_info = new String[]{'BEBERI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CR_OBJTY_type_info = new String[]{'CR_OBJTY','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PPSID_type_info = new String[]{'PPSID','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PPSIDI_type_info = new String[]{'PPSIDI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GSBER_type_info = new String[]{'GSBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GSBERI_type_info = new String[]{'GSBERI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KOKRS_type_info = new String[]{'KOKRS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KOKRSI_type_info = new String[]{'KOKRSI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KOSTL_type_info = new String[]{'KOSTL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KOSTLI_type_info = new String[]{'KOSTLI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PROID_type_info = new String[]{'PROID','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PROIDI_type_info = new String[]{'PROIDI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BUKRS_type_info = new String[]{'BUKRS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BUKRSI_type_info = new String[]{'BUKRSI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ANLNR_type_info = new String[]{'ANLNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ANLNRI_type_info = new String[]{'ANLNRI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ANLUN_type_info = new String[]{'ANLUN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ANLUNI_type_info = new String[]{'ANLUNI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DAUFN_type_info = new String[]{'DAUFN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DAUFNI_type_info = new String[]{'DAUFNI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AUFNR_type_info = new String[]{'AUFNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AUFNRI_type_info = new String[]{'AUFNRI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TPLNRI_type_info = new String[]{'TPLNRI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKORG_type_info = new String[]{'VKORG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKORGI_type_info = new String[]{'VKORGI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VTWEG_type_info = new String[]{'VTWEG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VTWEGI_type_info = new String[]{'VTWEGI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SPART_type_info = new String[]{'SPART','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SPARTI_type_info = new String[]{'SPARTI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ADRNR_type_info = new String[]{'ADRNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ADRNRI_type_info = new String[]{'ADRNRI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] OWNER_type_info = new String[]{'OWNER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKBUR_type_info = new String[]{'VKBUR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKGRP_type_info = new String[]{'VKGRP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LVORZ_type_info = new String[]{'LVORZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ERNAZ_type_info = new String[]{'ERNAZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ERDAZ_type_info = new String[]{'ERDAZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AENAZ_type_info = new String[]{'AENAZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AEDAZ_type_info = new String[]{'AEDAZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'MANDT','EQUNR','ERDAT','ERNAM','EQASP','AEDAT','AENAM','BEGRU','EQTYP','EQART','LVORM','INVNR','GROES','BRGEW','GEWEI','ANSDT','ANSWT','WAERS','ELIEF','GWLEN','GWLDT','WDBWT','HERST','HERLD','HZEIN','SERGE','TYPBZ','BAUJJ','BAUMM','APLKZ','AULDT','INBDT','GERNR','GWLDV','EQDAT','EQBER','EQNUM','OBJNR','EQSNR','CUOBJ','KRFKZ','KMATN','MATNR','SERNR','WERK','LAGER','CHARGE','KUNDE','WARPL','IMRC_POINT','REVLV','MGANR','BEGRUI','S_EQUI','S_SERIAL','S_KONFI','S_SALE','S_FHM','S_ELSE','S_ISU','S_EQBS','S_FLEET','BSTVP','SPARTE','HANDLE','TSEGTP','EMATN','ACT_CHANGE_AA','S_CC','DATLWB','UII','IUID_TYPE','UII_PLANT','CHANGEDDATETIME','DUMMY','ZZITO_UDI','EQEXT_ACTIVE','EQUI_SRTYPE','EQUI_SNTYPE','EQLB_DUTY','EQLB_HIDE','J_3GDISPO','J_3GZDEQUI','J_3GEQART','J_3GKZMENG','J_3GKONDE','J_3GFIKTIV','J_3GBELTYP','MEINS','J_3GKZLADG','J_3GKZBERG','J_3GEIFR','J_3GVERMEIN','J_3GZULNR','x_xSAPCEM_xABRECHVH','x_xSAPCEM_xABRECHLG','x_xSAPCEM_xDISPOGR','DUMMY1','DATBI','EQLFN','EQUZN','TIMBI','ESTAI','ESTAE','STNAM','DATAB','IWERK','IWERKI','SUBMT','MAPAR','HEQUI','HEQNR','INGRP','INGRPI','PM_OBJTY','GEWRK','GEWRKI','TIDNR','TIDNRI','ILOAN','KUND1','KUND2','KUND3','LIZNR','RBNR','EZDAT','EZBER','EZNUM','RBNR_I','IBLNR','BLDAT','PVS_FOCUS','PPEGUID','TECHS','FUNCID','FRCFIT','FRCRMV','SEGCHANGEDDATETIME','PVS_NODE','PVS_OTYPE','PVS_APPLOBJ_TYPE','TOP_EQUI','TOP_EQUI_FLAG','J_3GEIGNER','J_3GVERWAL','J_3GPMAUFE','J_3GPMAUFV','J_3GPACHT','SPRAS','EQKTX','KZLTX','TXASP','EQKTU','TEXTCHANGEDDATETIME','TPLNR','ABCKZ','ABCKZI','EQFNR','EQFNRI','SWERK','SWERKI','STORT','STORTI','MSGRP','MSGRPI','BEBER','BEBERI','CR_OBJTY','PPSID','PPSIDI','GSBER','GSBERI','KOKRS','KOKRSI','KOSTL','KOSTLI','PROID','PROIDI','BUKRS','BUKRSI','ANLNR','ANLNRI','ANLUN','ANLUNI','DAUFN','DAUFNI','AUFNR','AUFNRI','TPLNRI','VKORG','VKORGI','VTWEG','VTWEGI','SPART','SPARTI','ADRNR','ADRNRI','OWNER','VKBUR','VKGRP','LVORZ','ERNAZ','ERDAZ','AENAZ','AEDAZ'};
    }
    public class TABLE_OF_V_EQUI {
        public SAPAssetHierarchy.V_EQUI[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZSF_ASSET_HIERARCHYResponse_element {
        public SAPAssetHierarchy.TABLE_OF_V_EQUI EQUI_TAB;
        private String[] EQUI_TAB_type_info = new String[]{'EQUI_TAB','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'EQUI_TAB'};
    }
    public class ZSF_ASSET_HIERARCHY_element {
        public String EQUI_NO;
        public SAPAssetHierarchy.TABLE_OF_V_EQUI EQUI_TAB;
        private String[] EQUI_NO_type_info = new String[]{'EQUI_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] EQUI_TAB_type_info = new String[]{'EQUI_TAB','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'EQUI_NO','EQUI_TAB'};
    }
    public class ZSF_ASSET_HIERARCHY {
        public String endpoint_x = 'https://l800134-iflmap.hcisbp.us4.hana.ondemand.com/cxf/ReadAssetHierarchy_CEQ';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'SAPAssetHierarchy'};
        public SAPAssetHierarchy.TABLE_OF_V_EQUI ZSF_ASSET_HIERARCHY(String EQUI_NO,SAPAssetHierarchy.TABLE_OF_V_EQUI EQUI_TAB) {
            SAPAssetHierarchy.ZSF_ASSET_HIERARCHY_element request_x = new SAPAssetHierarchy.ZSF_ASSET_HIERARCHY_element();
            request_x.EQUI_NO = EQUI_NO;
            request_x.EQUI_TAB = EQUI_TAB;
            SAPAssetHierarchy.ZSF_ASSET_HIERARCHYResponse_element response_x;
            Map<String, SAPAssetHierarchy.ZSF_ASSET_HIERARCHYResponse_element> response_map_x = new Map<String, SAPAssetHierarchy.ZSF_ASSET_HIERARCHYResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:ZSF_ASSET_HIERARCHY:ZSF_ASSET_HIERARCHYRequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSF_ASSET_HIERARCHY',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSF_ASSET_HIERARCHY.Response',
              'SAPAssetHierarchy.ZSF_ASSET_HIERARCHYResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.EQUI_TAB;
        }
    }
}
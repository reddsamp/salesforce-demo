public with sharing class CLO_AccountTriggerHelper {
     @AuraEnabled(cacheable=true)
    public static List<Account> getAccountList() {
      System.debug('The account list from apex-->'+ [SELECT Id, Name, Net_Promoter_Score__c FROM Account]);
        return [SELECT Id, Name, Net_Promoter_Score__c FROM Account];
    }
    
      
    public static void calAccountScore(List<Account> obj2IDs){
           
        for(Account acc: obj2IDs){
            if(acc.CLO_ProductFamily__c != null && acc.Revenueamount__c != null){
            if((acc.CLO_ProductFamily__c.containsIgnoreCase('SculpSure')&&(acc.Revenueamount__c > -1 && acc.Revenueamount__c < 10500))||
               (acc.CLO_ProductFamily__c.containsIgnoreCase('PicoSure')&&(acc.Revenueamount__c > -1 && acc.Revenueamount__c < 2751))||
               (acc.CLO_ProductFamily__c.containsIgnoreCase('SmartLipo')&&(acc.Revenueamount__c > -1 && acc.Revenueamount__c < 4501))||
               (acc.CLO_ProductFamily__c.containsIgnoreCase('Pelleve')&&(acc.Revenueamount__c > -1 && acc.Revenueamount__c < 1001))
              ){
                acc.Net_Promoter_Score__c = 25;
              }
            else if((acc.CLO_ProductFamily__c.containsIgnoreCase('SculpSure')&&(acc.Revenueamount__c > 10499 && acc.Revenueamount__c < 17501))||
                    (acc.CLO_ProductFamily__c.containsIgnoreCase('PicoSure')&&(acc.Revenueamount__c > 2750 && acc.Revenueamount__c < 5001))||
                    (acc.CLO_ProductFamily__c.containsIgnoreCase('SmartLipo')&&(acc.Revenueamount__c > 4500 && acc.Revenueamount__c < 7501))||
                    (acc.CLO_ProductFamily__c.containsIgnoreCase('Pelleve')&&(acc.Revenueamount__c > 1000 && acc.Revenueamount__c < 3001))
                   ){
                       acc.Net_Promoter_Score__c = 35;
                   }
            else if((acc.CLO_ProductFamily__c.containsIgnoreCase('SculpSure')&&(acc.Revenueamount__c > 17500))||
                    (acc.CLO_ProductFamily__c.containsIgnoreCase('PicoSure')&&(acc.Revenueamount__c > 5000))||
                    (acc.CLO_ProductFamily__c.containsIgnoreCase('SmartLipo')&&(acc.Revenueamount__c > 7500))||
                    (acc.CLO_ProductFamily__c.containsIgnoreCase('Pelleve')&&(acc.Revenueamount__c > 3000))
                   ){
                      acc.Net_Promoter_Score__c =  45;
                   }
        }
          }      
}

}
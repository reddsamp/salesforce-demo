/**
  * @ClassName      : CaseTriggerHandler 
  * @Description    : Handler class for Case trigger 
  * @Author         : Markandeyulu K
  * @CreatedDate    : 25th aug 2020
  */


public class CLO_CaseTriggerHandler implements CLO_ITriggerHandler{
    
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
    public static String strNotnumbr=''; 
    public static Map<Id,String> mapCasIdNbr = new Map<Id,String>();
    public static List<Case> lstCastoUpd = new List<Case>();
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle beforeInsert business logic for Case Trigger
    * @Parameters  : List<sObject> newList 
    * @Return      : void 
    */ 
    public void beforeInsert(List<sObject> newList) {
        
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle afterInsert business logic for Case Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
    * @Return      : void 
    */

    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
        List<Id> lstCaseIds = new List<Id>();
        lstCaseIds.addAll(newMap.keySet());
        Boolean isPatImpact=false;
        String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseId(lstCaseIds);
        for(Case cas : (List<Case>) CLO_DBManager.getSObjectList('Id,CaseNumber,Status,Type,CLO_Bill_To_Customer__r.AccountNumber,Product.ProductCode,CLO_Serial_Number__c,Contact.Name,ContactEmail,CLO_Patient_User_Impact__c,Owner.Email,Description,CLO_Awareness_Date__c,Account.CLO_Sales_Org__c ', ' Case ', whereClause)){
            if(cas.CLO_Patient_User_Impact__c == CLO_Constants.YES){
                    isPatImpact = true;
            }
            if(cas.Type == CLO_Constants.COMPLAINT){               
                String  strAwarenessDate = cas.CLO_Awareness_Date__c.format('yyyy-MM-dd');         
                sapNotification(false, cas.CaseNumber, cas.CLO_Bill_To_Customer__r.AccountNumber, cas.Product.ProductCode, cas.CLO_Serial_Number__c, cas.Contact.Name, cas.ContactEmail, isPatImpact, false, cas.Account.CLO_Sales_Org__c, cas.Owner.Email, cas.Description,cas.Id,strAwarenessDate);
            }
        }
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle beforeUpdate business logic for Case Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
    * @Return      : void 
    */
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
        
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle afterUpdate business logic for Case Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap,List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */

    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
        if(CLO_CommonUtil.runOnce()){
            Map<id,Case> mapOldCase = (Map<id,Case>) oldMap;
            List<Id> lstCaseIds = new List<Id>();
            lstCaseIds.addAll(newMap.keySet());
            Boolean isPatImpact=false;
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseId(lstCaseIds);
            for(Case cas : (List<Case>) CLO_DBManager.getSObjectList('Id,CaseNumber,Status,Type,CLO_Bill_To_Customer__r.AccountNumber,Product.ProductCode,CLO_Serial_Number__c,Contact.Name,ContactEmail,CLO_Patient_User_Impact__c,Owner.Email,Description,CLO_Awareness_Date__c,Account.CLO_Sales_Org__c ', ' Case ', whereClause)){
                if(cas.CLO_Patient_User_Impact__c == CLO_Constants.YES){
                    isPatImpact = true;
                }
                if(mapOldCase.get(cas.id).Type != cas.Type && cas.Type == CLO_Constants.COMPLAINT){                                      
                    String  strAwarenessDate = cas.CLO_Awareness_Date__c.format('yyyy-MM-dd');
                    sapNotification(false, cas.CaseNumber, cas.CLO_Bill_To_Customer__r.AccountNumber, cas.Product.ProductCode, cas.CLO_Serial_Number__c, cas.Contact.Name, cas.ContactEmail, isPatImpact, false, cas.Account.CLO_Sales_Org__c, cas.Owner.Email, cas.Description,cas.Id,strAwarenessDate);
                }
            }
        }
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle beforeDelete business logic for Case Trigger
    * @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */ 
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        String strProfileId = userinfo.getProfileId();
        String whereClause = ' Id = ' + CLO_StringUtil.inquoteString(strProfileId);
        Profile objProfile = (Profile)CLO_DBManager.getSObject(' id, Name ',' Profile ', whereClause);
        List<Case> lstOldCase = (List<Case>) oldList;
        for(Case objCase : lstOldCase){
            if(objCase.Status != CLO_Constants.STATUS_OPEN && objProfile.Name == CLO_Constants.SERVICE_OPERATIONS){
                objCase.addError(Label.CLO_Case_Deletion_Validation);
            }                            
        }
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle afterDelete business logic for Case Trigger
    * @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */ 
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 25th aug 2020
    * @Description : This method id used to handle afterUnDelete business logic for Case Trigger
    * @Parameters  : List<sObject> newList, Map<Id, sObject> newMap
    * @Return      : void 
    */ 
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 16th nov 2020
    * @Description : This method is used to get and update the SAP Notification Number on Case.
    * @Parameters  : Boolean isTestRun, String caseNumber, String soldTo, String materialNumber, String serialNumber, String contactName, String contactEmail, Boolean patientImpact, Boolean potentialComplaint, String plant, String personResponsible, String caseDescription,String casId
    * @Return      : void 
    */
    @future(callout=true)
    public static void sapNotification(Boolean isTestRun, String caseNumber, String soldTo, String materialNumber, String serialNumber, String contactName, String contactEmail, Boolean patientImpact, Boolean potentialComplaint, String plant, String personResponsible, String caseDescription,String casId,String awarenessDate){
        
        strNotnumbr = CLO_SAP_CreateNotificationn.CreateCaseNotification(isTestRun, caseNumber, soldTo, materialNumber, serialNumber, contactName, contactEmail, patientImpact,false, plant, personResponsible, caseDescription,awarenessDate);
        mapCasIdNbr.put(casId,strNotnumbr);  
        
        if(mapCasIdNbr.values() != Null && mapCasIdNbr.values().size() > 0){
             for(id caseId : mapCasIdNbr.keyset()){
                 If(mapCasIdNbr.containsKey(caseId)){
                     Case objCas = new Case(ID=caseId);
                     objCas.CLO_SAP_Notification_Number__c = mapCasIdNbr.get(caseId);
                     lstCastoUpd.add(objCas);
                 }
             } 
             
             If(lstCastoUpd.size() > 0){
                 try{
                     CLO_DBManager.updateSObjects(lstCastoUpd);
                 }Catch(DMLException de){
                     System.debug('DML lstCastoUpd : '+ lstCastoUpd);
                 }Catch(Exception e){
                     System.debug('EX lstCastoUpd : '+lstCastoUpd);
                 }
             }       
        }          
    }  
}
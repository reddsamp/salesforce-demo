public class CLO_SAPWorkOrderEstimateController {
    @AuraEnabled
    public static WorkOrder getWorkOrder(String workOrderId)
    {
        return [select Id, Account.AccountNumber, CLO_SAP_Sync_Error__c,
        (Select Id, Description, LineItemNumber, UnitPrice, CLO_Non_Billable__c, ListPrice, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM WorkOrderLineItems) from WorkOrder where id =: workOrderId];
    }
    
    @AuraEnabled
    public static WorkOrder syncWithSAP(String workOrderId, boolean isSimulateOrder)
    {
        List<String> productNames = new List<String>();
        List<WorkOrderLineItem> orderItems = new List<WorkOrderLineItem>();
        for(WorkOrder wo:[SELECT Id,Status,CreatedDate,Account.AccountNumber,Account.CLO_Sales_Org__c,Account.CLO_Delivering_Plant__c, account.CLO_Customer_Number__c,
                              CreatedBy.Email,WorkOrderNumber,CLO_PO_Number__c,Asset.SerialNumber,Asset.ProductCode,
                              account.CLO_Incoterm_1__c,account.CLO_Incoterm_2__c,CLO_Billing_Reason__c, 
                              EndDate,Account.CLO_Shipping_Condition__c, CLO_Ship_To__c,
                              CLO_Ship_To__r.AccountNumber,CLO_Payment_Terms__c,CurrencyIsoCode,
                              CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c,
                              CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c,
                              CLO_Payment_Card__r.CLO_CC_Expiry_Date__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c,
                              (select Id,
                               LineItemNumber,
                               ListPrice,
                               Product__r.Name,
                               Product__r.ProductCode,
                               Product__r.Primary_Unit_of_Measure__c,
                               Quantity,
                               UnitPrice,
                               CLO_Non_Billable__c,
                               Product__r.QuantityUnitOfMeasure,
                               CurrencyIsoCode 
                               FROM WorkOrderLineItems),
                               (select Id,
                               ProductRequiredNumber,
                               CLO_Description__c,
                               QuantityRequired,
                               Product2.Name,
                               Product2.ProductCode,
                               Product2.Primary_Unit_of_Measure__c,
                               QuantityUnitOfMeasure,
                               CLO_Non_Billable__c,
                               CurrencyIsoCode 
                               FROM ProductsRequired),
                               (select Id,
                               ProductConsumedNumber,
                               CLO_Description__c,
                               QuantityConsumed,
                               Product2.Name,
                               Product2.ProductCode,
                               Product2.Primary_Unit_of_Measure__c,
                               QuantityUnitOfMeasure,
                               CLO_Non_Billable__c,
                               CurrencyIsoCode 
                               FROM ProductsConsumed) FROM WorkOrder where id=: workOrderId]){
                                       
                CLO_WorkOrderController.simulateWOEstimateSAPService(wo);          
        }
                
        return [select Id, Account.AccountNumber, CLO_SAP_Sync_Error__c, 
                (Select Id, Description, LineItemNumber, UnitPrice, CLO_Non_Billable__c, ListPrice, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM WorkOrderLineItems) from WorkOrder where id =: workOrderId];
    }
    
    @AuraEnabled
    public static List<wrpWoPrItem> getWoPrItems(String workOrderId)
    {

        List<WorkOrder> lstWorkOrder = [SELECT Id, Status FROM WorkOrder WHERE id =: workOrderId];
        List<ProductRequired> lstPrItems = new List<ProductRequired>();
        List<ProductConsumed> lstPcItems = new List<ProductConsumed>();

        if(lstWorkOrder[0].Status == 'Completed')
        {
            lstPcItems = [Select Id, ProductConsumedNumber, CLO_Description__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c,CreatedDate  FROM ProductConsumed WHERE WorkOrderId =: workOrderId Order By CreatedDate];
        }
        else {
            lstPrItems = [Select Id, ProductRequiredNumber, CLO_Description__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c,CreatedDate  FROM ProductRequired WHERE ParentRecordId =: workOrderId  Order By CreatedDate];
        }
        
        System.debug('CLO_SAPWorkOrderEstimateCtrl :: lstWorkOrder :: '+lstWorkOrder);
        List<wrpWoPrItem> lstWoPrItem = new List<wrpWoPrItem>();
        wrpWoPrItem objWoPrItem; 
        
         for(WorkOrderLineItem woItem :[Select Id, Description, UnitPrice, CreatedDate, LineItemNumber, ListPrice, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM WorkOrderLineItem WHERE WorkOrderId =: workOrderId Order By CreatedDate])
        {
            objWoPrItem = new wrpWoPrItem();
            objWoPrItem.itemNum    = woItem.LineItemNumber;
            objWoPrItem.descrption    = woItem.Description;
            objWoPrItem.grossVal    = woItem.CLO_Gross_Value__c;
            objWoPrItem.discount   = woItem.CLO_Discount__c;
            objWoPrItem.netVal    = woItem.CLO_Net_Value__c;
            objWoPrItem.tax   = woItem.CLO_Tax__c;
            System.debug('woItem :: '+objWoPrItem);
            lstWoPrItem.add(objWoPrItem);
        }

        for(ProductRequired prItem : lstPrItems)
        {
            objWoPrItem = new wrpWoPrItem();
            objWoPrItem.itemNum    = prItem.ProductRequiredNumber;
            objWoPrItem.descrption    = prItem.CLO_Description__c;
            objWoPrItem.grossVal    = prItem.CLO_Gross_Value__c;
            objWoPrItem.discount   = prItem.CLO_Discount__c;
            objWoPrItem.netVal    = prItem.CLO_Net_Value__c;
            objWoPrItem.tax   = prItem.CLO_Tax__c;
            System.debug('prItem :: '+objWoPrItem);
            lstWoPrItem.add(objWoPrItem);
        }
        for(ProductConsumed pcItem : lstPcItems)
        {
            objWoPrItem = new wrpWoPrItem();
            objWoPrItem.itemNum    = pcItem.ProductConsumedNumber;
            objWoPrItem.descrption    = pcItem.CLO_Description__c;
            objWoPrItem.grossVal    = pcItem.CLO_Gross_Value__c;
            objWoPrItem.discount   = pcItem.CLO_Discount__c;
            objWoPrItem.netVal    = pcItem.CLO_Net_Value__c;
            objWoPrItem.tax   = pcItem.CLO_Tax__c;
            System.debug('pcItem :: '+objWoPrItem);
            lstWoPrItem.add(objWoPrItem);
        }
        return lstWoPrItem;
    }
    
    public class wrpWoPrItem
    {
        @AuraEnabled
        public String itemNum {get;set;}
        @AuraEnabled
        public String descrption {get;set;}
        @AuraEnabled
        public Decimal grossVal {get;set;}
        @AuraEnabled
        public Decimal discount   {get;set;}
        @AuraEnabled
        public Decimal netVal   {get;set;}
        @AuraEnabled
        public Decimal tax {get;set;}
    }
}
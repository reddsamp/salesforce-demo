/**
  * @ClassName      : CLO_CloseCaseController 
  * @Description    : Controller for Case Close VF Page. 
  * @Author         : Markandeyulu K
  * @CreatedDate    : 05th Oct 2020
  */

public class CLO_CloseCaseController {

    public Case objCas;
    public CLO_CloseCaseController(ApexPages.StandardController controller) {
        objCas  = (Case)controller.getRecord();
        objCas.Status = CLO_Constants.STATUS_CLOSED;
    }
}
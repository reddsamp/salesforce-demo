global class CLO_WarrantyEntitlementBatch implements Database.batchable<sObject>{ 
   global Database.querylocator start(Database.BatchableContext info){ 
       return Database.getQueryLocator('SELECT Id,Name,AssetId,Status FROM Entitlement WHERE AssetId != Null'); 
   }     
   global void execute(Database.BatchableContext info, List<Entitlement> scope){
       Map<Id,List<Entitlement>> mapAstEnt = new Map<Id,List<Entitlement>>();
       List<Asset> lstAsset = new List<Asset>();
       Set<Id> setAsset = new Set<Id>();
       for(Entitlement objEnt : scope){
           If(objEnt.Status == 'Active' && objEnt.Name.containsIgnoreCase('WARRANTY')){ 
               If(mapAstEnt != Null && mapAstEnt.containsKey(objEnt.AssetId)){
                   mapAstEnt.get(objEnt.AssetId).add(objEnt);
               } else {
                   mapAstEnt.put(objEnt.AssetId,new List<Entitlement>{objEnt});
               }    
           } else {
               setAsset.add(objEnt.AssetId);    
           }
       } 
       
       if(setAsset.size() > 0 && mapAstEnt.keySet().size() > 0){
           setAsset.removeAll(mapAstEnt.keySet());
       }
       
       if(mapAstEnt != Null && mapAstEnt.keyset() != Null && mapAstEnt.keyset().size() > 0){
           for(Id astId : mapAstEnt.keyset()){
               Asset astObj = new Asset(ID = astId);
               astObj.CLO_Entitlement_Count_Total__c = mapAstEnt.get(astId).Size();               
               lstAsset.add(astObj);              
           }
       }
       
       If(setAsset.size() > 0){
           for(Id asstId : setAsset){
                Asset asstObj = new Asset(ID = asstId);
                asstObj.CLO_Entitlement_Count_Total__c = 0;               
                lstAsset.add(asstObj);  
           }
       }
       
       if(lstAsset.size() > 0){           
            try{
                       
                CLO_DBManager.updateSObjects(lstAsset); 
            } Catch(DMLException de) {
                System.debug('dmlExcep :: '+de.getMessage());
            } Catch(Exception ex) {
                System.debug('Exception :' +ex.getMessage());
            }   
       }
        
   }     
   global void finish(Database.BatchableContext info){     
   } 
}
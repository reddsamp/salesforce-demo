/**
  * @ClassName      : CLO_CloseCaseControllerTest 
  * @Description    : This class is used to Cover the Test Coverage for CLO_CloseCaseController Class.
  * @Author         : Markandeyulu K
  * @CreatedDate    : 25th Aug 2020
  */

@isTest
public class CLO_CloseCaseControllerTest{

    static testMethod void testExtnConstructor(){
        Test.startTest();
        // Create test Cases
        List<Case> lstCas = new List<Case>();
        for(Integer i=0;i<5;i++) {
            lstCas.add(new Case(Status = 'Open'));
        }
        CLO_DBManager.insertSObjects(lstCas); 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(lstCas[0]);
        CLO_CloseCaseController ctr = new CLO_CloseCaseController (sc);
        PageReference ref = new PageReference('/apex/Case_Close'); 
        Test.setCurrentPage(ref);
        Test.stopTest();
    }
}
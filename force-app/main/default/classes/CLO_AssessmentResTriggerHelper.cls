public class CLO_AssessmentResTriggerHelper {
    
    public static void calAccPromoterScore(list<FCXM__Assessment_Response__c> newAssessmentList){
        set<id> setAccIds = new set<id>();
        list<id> listAccIds;
        map<id, list<FCXM__Assessment_Response__c>> mapAssessment =new map<id,list<FCXM__Assessment_Response__c>>();
        
        for(FCXM__Assessment_Response__c eachAssessment: newAssessmentList){
            if(eachAssessment.FCXM__Account__c != null)
                setAccIds.add(eachAssessment.FCXM__Account__c);
        }
        if(CLO_CommonUtil.isNullOrEmpty(setAccIds)) return;
        listAccIds = new list<id>(setAccIds);
        
         String whereClause1 = ' Id IN ' + CLO_StringUtil.getInClauseId(listAccIds);
        List<Account> accountsToUpdate= (List<Account>)CLO_DBManager.getSObjectList
            ('CLO_ProductFamily__c, Revenueamount__c, Id', 'account',whereClause1);
        System.debug('accountsToUpdate '+accountsToUpdate);
        String whereClause2 = ' FCXM__Account__c IN ' + CLO_StringUtil.getInClauseId(listAccIds)+ ' and FCXM__Answer_Points__c > 0';
        list<FCXM__Assessment_Response__c> assessmentList = (List<FCXM__Assessment_Response__c>)CLO_DBManager.getSObjectList
            ('FCXM__Account__c,FCXM__Answer_Points__c,CreatedDate', 'FCXM__Assessment_Response__c', whereClause2);
        System.debug('assessmentList '+assessmentList);
        
        for(FCXM__Assessment_Response__c assessment: assessmentList) {
            if(mapAssessment.containsKey(assessment.FCXM__Account__c)) {
                mapAssessment.get(assessment.FCXM__Account__c).add(assessment);
            }   
            else {
                mapAssessment.put(assessment.FCXM__Account__c, new List<FCXM__Assessment_Response__c> {assessment});
            }
        }
        
        for(account acc: accountsToUpdate){
            date latestCreatedDate;
            Decimal totalAnswerPoints=0;
            
            if(mapAssessment.containsKey(acc.Id) ){
                for(FCXM__Assessment_Response__c assessment: mapAssessment.get(acc.Id)){
                    {
                        if(latestCreatedDate == null){latestCreatedDate =  assessment.CreatedDate.date();}
                        if( assessment.CreatedDate.date() > latestCreatedDate){
                            latestCreatedDate = assessment.CreatedDate.date();  
                        }}
                }
                for(FCXM__Assessment_Response__c assessment: mapAssessment.get(acc.Id)){
                    {
                        if( latestCreatedDate <= assessment.CreatedDate.date()){
                            totalAnswerPoints = totalAnswerPoints + assessment.FCXM__Answer_Points__c;  
                        }}
                }}
            acc.Net_Promoter_Score__c = totalAnswerPoints + calAccountScore(acc.CLO_ProductFamily__c, acc.Revenueamount__c); 
            
        }
        If(!CLO_CommonUtil.isNullOrEmpty(accountsToUpdate)){
            try{
                CLO_DBManager.updateSObjects(accountsToUpdate);
            } Catch(DMLException de){
                System.debug('accountsToUpdate DML @@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('accountsToUpdate EX @@@ : '+e.getMessage());
            }   
        }
        
    }
    
    
    public static integer calAccountScore(string prodFamily, Decimal revenueAmount){
        
        if(prodFamily != null && revenueAmount != null){
            if((prodFamily.containsIgnoreCase('SculpSure')&&(revenueAmount > -1 && revenueAmount < 10500))||
               (prodFamily.containsIgnoreCase('PicoSure')&&(revenueAmount > -1 && revenueAmount < 2751))||
               (prodFamily.containsIgnoreCase('SmartLipo')&&(revenueAmount > -1 && revenueAmount < 4501))||
               (prodFamily.containsIgnoreCase('Pelleve')&&(revenueAmount > -1 && revenueAmount < 1001))
              ){
                  return 25;
              }
            else if((prodFamily.containsIgnoreCase('SculpSure')&&(revenueAmount > 10499 && revenueAmount < 17501))||
                    (prodFamily.containsIgnoreCase('PicoSure')&&(revenueAmount > 2750 && revenueAmount < 5001))||
                    (prodFamily.containsIgnoreCase('SmartLipo')&&(revenueAmount > 4500 && revenueAmount < 7501))||
                    (prodFamily.containsIgnoreCase('Pelleve')&&(revenueAmount > 1000 && revenueAmount < 3001))
                   ){
                       return 35;
                   }
            else if((prodFamily.containsIgnoreCase('SculpSure')&&(revenueAmount > 17500))||
                    (prodFamily.containsIgnoreCase('PicoSure')&&(revenueAmount > 5000))||
                    (prodFamily.containsIgnoreCase('SmartLipo')&&(revenueAmount > 7500))||
                    (prodFamily.containsIgnoreCase('Pelleve')&&(revenueAmount > 3000))
                   ){
                       return 45;
                   }else
                       return 0;
        }else
            return 0;  
    }

}
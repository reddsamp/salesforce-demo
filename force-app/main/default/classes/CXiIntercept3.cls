public without sharing class CXiIntercept3 {
    private static String m_strURL;
    private static String m_strGUID;
    private static String m_strPSK;
    private static String rfc_endpoint;
    private static String rfc_unpw;
    private static String AuthorizationCode;
    private static String tokenizeCardNumber;
    private static String strTransactionID;
    private static String strStatusCode;
    private static String strStatusTXN;
    private static String strAuthorizationDate;
    private static String strAuthorizationTime;
    private static String strMessage;
    public static String woId {get; set;}
    public static String qtId {get; set;}
    public static String ordId {get; set;}
    public static String currencyIsoCode {get; set;}

    public String getDigitalSignature {get; set;}
    public String iFrameUrl {get; set;}
    public String accessToken {get; set;}
    public static String accountId {get; set;}
    public static String custNumber {get; set;}
    public static boolean isWorkOrder {get; set;}
    public static String custName {get; set;}
    public static boolean missingCredentials {get; set;}
    public static boolean IsCreditCardSave{get; set;}
    public static String selectedCC{get; set;}
    public static Map<String, String> mapResult;
    public static boolean isSumbit{get; set;}
    public String tokenCardNumber {get;set;}
    
    public CLO_Credit_Card_Details__c ccDetail {get;set;}
    public List<SelectOption> CCOptions {get;set;}

    static {
        missingCredentials = false;
        List<CLO_Paymetrics_Endpoints__c> pes = [SELECT CLO_GUID__c, CLO_iFrameURLRoot__c, CLO_psk__c, CLO_rfc_endpoint__c, CLO_rfc_unpw__c FROM CLO_Paymetrics_Endpoints__c limit 1];
        if (pes.size() == 0 || pes[0].CLO_GUID__c == null || pes[0].CLO_iFrameURLRoot__c == null || pes[0].CLO_psk__c == null || pes[0].CLO_rfc_endpoint__c == null || pes[0].CLO_rfc_unpw__c == null) {
            missingCredentials = true;
        } else {
            CLO_Paymetrics_Endpoints__c pe = pes[0];
            m_strURL = pe.CLO_iFrameURLRoot__c;
            m_strGUID = pe.CLO_GUID__c;
            m_strPSK = pe.CLO_psk__c;
            rfc_endpoint = pe.CLO_rfc_endpoint__c;
            rfc_unpw = pe.CLO_rfc_unpw__c;
            system.debug('rfc_endpoint ::'+rfc_endpoint);
        }
    }


    public double amount;
    public CXiIntercept3(ApexPages.StandardController controller) {
        System.debug('CXiIntercept3 :: Constructor :: '+DateTime.now()); 
        IsCreditCardSave = true;
        accountId = ApexPages.currentPage().getParameters().get('id');
        String quoteId = ApexPages.currentPage().getParameters().get('quoteId');
        String qId = ApexPages.currentPage().getParameters().get('qId');
        String orderId = ApexPages.currentPage().getParameters().get('orderId');
        String workOrderId = ApexPages.currentPage().getParameters().get('woId');
        String workOrderNum = ApexPages.currentPage().getParameters().get('workOrderId');
        String oId = ApexPages.currentPage().getParameters().get('oId');
        String Currencyode = ApexPages.currentPage().getParameters().get('currencyIsoCode');
        String orderTyp = ApexPages.currentPage().getParameters().get('orderType');
        isWorkOrder = false;
        isSumbit = false;
        if(workOrderId != null){
            woId = workOrderId;
            //isWorkOrder = true;
        }
        if(qId != null){
            qtId = qId;
        }
        if(oId != null){
            ordId = oId;
        }

        if(Currencyode != null){
            currencyIsoCode = Currencyode;
        }
        System.debug('orderId :: '+orderId);
        String strReferenceNum = '';
        if(quoteId != null)
            strReferenceNum = quoteId;
        else if(orderId != null)
            strReferenceNum = orderId;
        else if(workOrderId != null)
            strReferenceNum = workOrderNum;
          
        if(orderTyp != null && orderTyp == 'ZDEO')
        {
            isSumbit = true;
        }
        else if(orderTyp != null && orderTyp == 'ZRP')
        {
            isSumbit = true;
        }

        if(isSumbit && orderTyp != null && orderTyp == 'ZDEO')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Payment is not required for demo Orders.'));
        } 
        if(isSumbit && orderTyp != null && orderTyp == 'ZRP')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Payment is not required for Advance Exchange Orders.'));
        }  
        ccDetail = new CLO_Credit_Card_Details__c(Account__c = accountId, CLO_Reference_Number__c = strReferenceNum);
        CCOptions = new List<SelectOption>();
        
        CCOptions = queryCCOptions();
        
        if (!missingCredentials) {
            accessToken = getIFrameAccessToken();

            iFrameUrl = m_strURL + '/view/iframe/' + m_strGUID + '/' + accessToken + '/true';
            
            Account acct = [SELECT Id, AccountNumber, RecordTypeId, Parent.AccountNumber, Name, Parent.Name FROM Account WHERE Id = :accountId];
            custNumber = acct.AccountNumber;//acct.RecordTypeId == shipToRT ? acct.Parent.AccountNumber : acct.AccountNumber;
            custName =  acct.Name;//acct.RecordTypeId == shipToRT ? acct.Parent.Name : acct.Name;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Missing Paymetrics Credentials. Please contact your Administrator.'));
        }
        ccDetail.CLO_CC_Amount__c = 0;
        /*
        String strPrice = ApexPages.currentPage().getParameters().get('authAmt');
        if(String.isNotEmpty(strPrice)){
            Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
            Matcher matcher = nonAlphanumeric.matcher(strPrice);
            strPrice = matcher.replaceAll(''); 
            
            Decimal d = Decimal.valueOf(!String.isEmpty(strPrice) ? strPrice : '0');
            d = d.setScale(8, RoundingMode.HALF_EVEN);
            ccDetail.CLO_CC_Amount__c = d;
        }
        */
        String authAmt = ApexPages.currentPage().getParameters().get('authAmt');
        if(String.isNotEmpty(authAmt)){
            Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
            Matcher matcher = nonAlphanumeric.matcher(authAmt);
            authAmt = matcher.replaceAll(''); 
            
            Decimal d = Decimal.valueOf(!String.isEmpty(authAmt) ? authAmt : '0');
            d = d.setScale(8, RoundingMode.HALF_EVEN);
            ccDetail.CLO_CC_Amount__c = d;
            amount = d;
        }
        String depositAmt = ApexPages.currentPage().getParameters().get('depositAmt');
        if(String.isNotEmpty(depositAmt)){
            Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
            Matcher matcher = nonAlphanumeric.matcher(depositAmt);
            depositAmt = matcher.replaceAll(''); 
            
            Decimal d = Decimal.valueOf(!String.isEmpty(depositAmt) ? depositAmt : '0');
            d = d.setScale(8, RoundingMode.HALF_EVEN);
            ccDetail.CLO_CC_Amount__c = d;
            amount = d;
        }

        String ordAmount = ApexPages.currentPage().getParameters().get('ordAmount');
        if(String.isNotEmpty(ordAmount)){
            Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
            Matcher matcher = nonAlphanumeric.matcher(ordAmount);
            ordAmount = matcher.replaceAll(''); 
            
            Decimal d = Decimal.valueOf(!String.isEmpty(ordAmount) ? ordAmount : '0');
            d = d.setScale(8, RoundingMode.HALF_EVEN);
            ccDetail.CLO_CC_Amount__c = d;
            amount = d;
        }
    }

    @RemoteAction
    public static Map<String, String> authorizePayment(String aTok, String amountStr, String aid, String custNum, String strToken, String cardType,
                                                       String expMonth, String expYear, String woId,String qtId, String ordId, String currencyIsoCode, String ccNum) {
        List<WorkOrder> lstWo = new List<WorkOrder>();
        Decimal amount = Decimal.valueOf(amountStr.replaceAll(',', ''));
        String strWhere = 'getResponsePacket';
        String signature = retrieveDigitalSignature(aTok);
        String strUrlParams = m_strURL + '/ResponsePacket';
        String strWhereClause = '';                                                   
        Dom.Document hDoc = null;
        Dom.XmlNode hRoot = null;
        Dom.XmlNode hNode = null;

        strUrlParams += '?MerchantGUID=' + m_strGUID;
        strUrlParams += '&Signature=' + EncodingUtil.urlEncode(signature, 'UTF-8');
        strUrlParams += '&AccessToken=' + aTok;
        System.debug('ccNum ::: '+ccNum);                                                
        hDoc = executeHttpRequest(strUrlParams, '', 'GET');
        System.debug(hDoc);
        hRoot = hDoc.getRootElement();
        String exp = expMonth +'/' + expYear;
        String credCardNum = ccNum;                                                   
        if(ccNum == null || ccNum == '')                                                   
            credCardNum = strToken;   
        
        Map<String, String> result = callRFC(credCardNum, exp, amount, cardType, custNum, currencyIsoCode);
        if(strToken.contains('*'))
        {
            strWhereClause = 'CLO_Credit_Card_Last_4__c = ' +CLO_StringUtil.inquoteString(strToken) +' LIMIT 1';
        }
        else if(result.get('status') == 'SUCCESS')
        {
            saveCC(strToken, cardType, aid);                                                   
            strWhereClause = 'CLO_CC_Card_Number__c = ' +CLO_StringUtil.inquoteString(strToken) +' LIMIT 1'; 
        }  
        else if(result.get('status') == 'FAILED')
        {
            return new Map<String, String> {
            'status' => result.get('status'),
            'StatusTXN' => result.get('StatusTXN'),
            'tokenizeCardNumber' => result.get('tokenizeCardNumber'),
            'AuthorizationCode' => result.get('AuthorizationCode'),
            'Message' => result.get('Message'),
            'TransactionID' => result.get('TransactionID')
            };
        }                                                        
        List<CLO_Credit_Card_Details__c> lstCCDetails= (List<CLO_Credit_Card_Details__c>) CLO_DBManager.getSObjectList('Id, Name, CLO_Credit_Card_Last_4__c, CLO_CC_Card_Number__c', 'CLO_Credit_Card_Details__c', strWhereClause);                                                
        if(!CLO_CommonUtil.isNullOrEmpty(lstCCDetails)) {
            string ccId = lstCCDetails[0].Id;
            String strDate = String.ValueOf(result.get('AuthorizationDate')).split('T')[0];
            String[] strTimeSplit = result.get('AuthorizationTime').split(':');
            Time timeChange = Time.newInstance( Integer.valueOf(strTimeSplit[0]), //hour
                                                Integer.valueOf(strTimeSplit[1]), //min
                                                Integer.valueOf(strTimeSplit[2]),
                                                0);//sec                              
            CLO_Paymetric_Payment__c payment = new CLO_Paymetric_Payment__c(CLO_Payment_Status__c= result.get('status'),
                                                                            CLO_Amount__c = amount,
                                                                            CLO_Account__c = aid,
                                                                            //CLO_Confirmation_Number__c = result.get('token'),
                                                                            CLO_SAP_Response_raw_data__c = result.get('rawResponse'),
                                                                            CLO_TransactionID__c = result.get('TransactionID'),
                                                                            CLO_Authorization_Code__c = result.get('AuthorizationCode'),
                                                                            CLO_Tokenize_Card_Number__c = result.get('tokenizeCardNumber'),
                                                                            CLO_StatusCode__c = result.get('StatusCode'),
                                                                            CLO_Message__c = result.get('Message'),
                                                                            CLO_Credit_Card_Authorized_Date__c = Date.valueOf(strDate),
                                                                            CLO_Credit_Card_Authorized_Time__c = timeChange,
                                                                            CLO_Work_Order__c = (woId != '' && woId != null)?Id.valueOf(woId):null,
                                                                            CLO_Quote__c = (qtId != '' && qtId != null)?Id.valueOf(qtId):null,
                                                                            CLO_Order__c = (ordId != '' && ordId != null)?Id.valueOf(ordId):null,
                                                                            CLO_Payment_Card__c = (ccId != '' && ccId != null)?Id.valueOf(ccId):null,
                                                                            CurrencyIsoCode = (currencyIsoCode != '' && currencyIsoCode != null)?currencyIsoCode:null,
                                                                            CLO_SAP_Error_Message__c = result.get('StatusTXN'));
                                                                            
            insert payment;
            if(woId != '' && woId != null)
            {
                update new WorkOrder(Id = woId, CLO_Pre_authorized__c = mapResult.get('status'), CLO_Pre_authorized_Reason__c = result.get('Message'), CLO_Payment_Transction_ID__c = result.get('TransactionID'));
            }
        }
        /*
        ID workOrdId;
        If(woId != '' && woId != null){
            workOrdId = Id.valueOf(woId);       
            for(CLO_Paymetric_Payment__c pp : [Select Id,CLO_Payment_Status__c,CLO_Work_Order__c From CLO_Paymetric_Payment__c Where CLO_Payment_Status__c = 'SUCCESS' AND CLO_Work_Order__c =: workOrdId Limit 1]){
                WorkOrder objWo = new WorkOrder(ID=pp.CLO_Work_Order__c);
                objWo.CLO_Payment_Received__c = true;
                lstWo.add(objWo);
            }
        }
        
        If(lstWo.size() > 0){
            Update lstWo;     
        }
        */
        if(qtId != '' && qtId != null && result.get('TransactionID') != '' && result.get('TransactionID') != null)
        {
            update new SBQQ__Quote__c(Id = qtId, CLO_Payment_Transction_ID__c = result.get('TransactionID'));
        }
        if(ordId != '' && ordId != null && result.get('TransactionID') != '' && result.get('TransactionID') != null)
        {
            update new Order(Id = ordId, CLO_Payment_Transction_ID__c = result.get('TransactionID'));
        }

        return new Map<String, String> {
            'status' => result.get('status'),
            'StatusTXN' => result.get('StatusTXN'),
            'tokenizeCardNumber' => result.get('tokenizeCardNumber'),
            'AuthorizationCode' => result.get('AuthorizationCode'),
            'Message' => result.get('Message'),
            'TransactionID' => result.get('TransactionID')
        };
    }

    public class PMAuthException extends Exception {}

    public String getIFrameAccessToken() {
        String strAT = '';
        String strResponse = '';
        String strURL = m_strURL + '/AccessToken';
        String strWhere = 'getSignedXML';
        String strXml = buildXML();
        String strSignature = '';

        Dom.Document hDoc = null;
        Dom.XmlNode hRoot = null;
        Dom.XmlNode hResponse = null;
        Dom.XmlNode hAT = null;

        String strPostXml = 'MerchantGuid=' + m_strGUID + '&' +
                            'SessionRequestType=1&' +
                            'Packet=' + EncodingUtil.urlEncode(strXml, 'UTF-8') + '&' +
                            'Signature=' + EncodingUtil.urlEncode(retrieveDigitalSignature(strXml), 'UTF-8') + '&' +
                            'MerchantDevelopmentEnvironment=SalesForce';
        if (!Test.isRunningTest()) {
            hDoc = executeHttpRequest(strURL, strPostXml, 'POST');
            hRoot = hDoc.getRootElement();
        }
        if (hRoot != null) {
            hResponse = hRoot.getChildElement('ResponsePacket', null);
            if (hResponse != null) {
                hAT = hResponse.getChildElement('AccessToken', null);
                if (hAT != null)
                    strAT = hAT.getText();
            }
        }
        return strAT;
    }

    private static String retrieveDigitalSignature(String strData) {
        String strWhere = 'getDigitalSignature';
        return EncodingUtil.base64Encode(
                   Crypto.generateMac(
                       'hmacSHA256',
                       Blob.valueOf(strData),
                       Blob.valueOf(m_strPSK)
                   )
               );
    }

    public CXiIntercept3() {
        //proposalId = ApexPages.currentPage().getParameters().get('pid');
    }

    public static void xiResponse() {

    }

    private static Dom.Document executeHttpRequest(String targetURL, String urlParameters, String requestType) {
        String strWhere = 'executeHttpRequest';
        Dom.Document hDoc = null;
        Http myHttp = new Http();
        HttpRequest httpRQ = new HttpRequest();
        HttpResponse httpRS = null;

        httpRQ.setEndpoint(targetURL);
        httpRQ.setMethod(requestType);
        if (requestType.equalsIgnoreCase('POST')) {
            httpRQ.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            httpRQ.setHeader('Content-Length', String.valueOf(urlParameters.Length()));
            httpRQ.setHeader('Content-Language', 'en-US');
            httpRQ.setHeader('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate');
            httpRQ.setHeader('Pragma', 'no-cache');
            httpRQ.setBody(urlParameters);
        }

        httpRS = myHttp.send(httpRQ);
        if (httpRS != null) {
            hDoc = httpRS.getBodyDocument();
        } else {
            hDoc = new Dom.Document();
        }

        return hDoc;
    }

    private String buildXML() {
        String baseUrl = String.valueOf(System.URL.getSalesforceBaseUrl().toExternalForm());
        String xmlString = '';
        xmlString += '<?xml version="1.0" encoding="UTF-8"?>';
        xmlString += '<merchantHtmlPacketModel xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="Paymetric:XiIntercept:MerchantHtmlPacketModel">';
        xmlString +=    '<iFramePacket>';
        xmlString +=        '<hostUri>' + baseUrl + '/' + '</hostUri>';
        xmlString +=        '<cssUri>https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css</cssUri>';
        xmlString +=    '</iFramePacket>';
        xmlString += '<merchantHtml>';
        xmlString += '<htmlSection class="merchant_paycontent">';
        xmlString +=    '<cardDropdownSection>';
        // xmlString +=      '<tag name="div" class="PaymentDetailHeader">Enter your credit card information</tag>';
        xmlString +=        '<tag name="div">';
        xmlString +=            '<label for="cardType" text="card type"/>';
        xmlString +=            '<ddlCardType id="cd">';
        xmlString +=                '<items>';
        xmlString +=                    '<item for="american express"/>';
        xmlString +=                    '<item for="mastercard"/>';
        xmlString +=                    '<item for="visa"/>';
        xmlString +=                    '<item for="discover"/>';
        xmlString +=                '</items>';
        xmlString +=            '</ddlCardType>';
        xmlString +=        '</tag>';
        xmlString +=        '<tag name="div">';
        xmlString +=    '<label for="cardNumber" text="card number"/>';
        xmlString +=    '<tboxCardNumber tokenize="true" min="15" min-msg="Card number not long enough" />';
        xmlString +=    '<validationMsg for="cardNumber" class="valmsg text-danger"/>';
        xmlString +=    '</tag>';
        xmlString +=    '<tag name="div">';
        xmlString +=    '<label for="startMonth" text="start date"/>';
        xmlString +=    '<ddlStartMonth default-text="month" display-format="MMM" class="merchant_combos" required="false"/>';
        xmlString +=    '<ddlStartYear default-text="year" class="merchant_combos" years-to-display="10" required="false" start-date="true"/>';
        xmlString +=    '<validationMsg for="startYear" class="valmsg text-danger"/>';
        xmlString +=    '</tag>';
        xmlString +=    '<tag name="div">';
        xmlString +=    '<label for="expMonth" text="exp date"/>';
        xmlString +=    '<ddlExpMonth default-text="month" class="merchant_combos" required="false"/>';
        xmlString +=    '<ddlExpYear default-text="year" class="merchant_combos" years-to-display="10" required="false" exp-date="true"/>';
        xmlString +=    '<validationMsg for="expYear" class="valmsg text-danger"/>';
        xmlString +=    '</tag>';
        xmlString +=    '</cardDropdownSection>';
        xmlString +=    '</htmlSection>';
        xmlString +=    '</merchantHtml>';
        xmlString +=    '</merchantHtmlPacketModel>';

        return xmlString;
    }


    public static Map<String, String> callRFC(String tokenizedCardNum,
            String exp,
            Decimal amount,
            String cardType,
            String customerNum, 
            String currencyIsoCode) {
        System.debug('callRFC ::'+DateTime.now());
        System.debug('currencyIsoCode ::'+currencyIsoCode);        
            String m_strURL;
            String m_strUser;
            String m_strPassword;
            String m_strMerchantID;
            
        System.debug('DATA: ' + tokenizedCardNum + ' ' + exp + ' ' + String.valueOf(amount) + ' ' + cardType + ' ' + customerNum);
       /*
            m_strURL = pe.CLO_iFrameURLRoot__c;
            m_strGUID = pe.CLO_GUID__c;
            m_strPSK = pe.CLO_psk__c;
            rfc_endpoint = pe.CLO_rfc_endpoint__c;
            rfc_unpw = pe.CLO_rfc_unpw__c;
       */
        List<CLO_Paymetrics_Endpoints__c> lstPayEndpoints = [SELECT Id, Name, CLO_User_Name__c, CLO_Paymetric_Password__c,CLO_Paymetric_Endpoint__c,CLO_Merchant_ID__c, CurrencyIsoCode FROM CLO_Paymetrics_Endpoints__c WHERE CurrencyIsoCode =:currencyIsoCode LIMIT 1];
        System.debug('lstPayEndpoints ::'+lstPayEndpoints);           
        m_strURL = lstPayEndpoints[0].CLO_Paymetric_Endpoint__c;//'https://cert-xipayapi.paymetric.com/PMXIGGE/XiPay30WS.asmx';
        m_strUser = lstPayEndpoints[0].CLO_User_Name__c;//'PAYMETRIC\\WSACynsureLLCDV';
        m_strPassword = lstPayEndpoints[0].CLO_Paymetric_Password__c;//'Cynsurellc106*';
        m_strMerchantID = lstPayEndpoints[0].CLO_Merchant_ID__c;//'c0dfe064-64a8-4599-bc71-c75fcd2419d3';
        System.debug('Paymetric Endpoints : ' + m_strURL + ' ' + m_strUser + ' ' + m_strPassword + ' ' + m_strMerchantID + ' ' + lstPayEndpoints[0].CurrencyIsoCode);
        System.debug('exp :: '+exp);
        System.debug('cardType :: '+cardType);
        System.debug('pAYMETRIC :: currencyIsoCode :: '+currencyIsoCode);

        XiPay xpay = new XiPay(m_strURL, m_strUser,m_strPassword,m_strMerchantID);   
        string rawResp = xpay.getFullAuthorization(tokenizedCardNum, exp, amount, cardType, currencyIsoCode);
        
        DOM.Document doc=new DOM.Document();
        try{
            doc.load(rawResp);
            DOM.XmlNode rootNode=doc.getRootElement();
            mapResult = parseXML(rootNode);
            mapResult.put('rawResponse',rawResp);

            
        }catch(exception e){
            system.debug(e.getMessage());
        }
        
        String status;
        if (mapResult.get('StatusCode') != null && mapResult.get('StatusCode') != '' && Integer.ValueOf(mapResult.get('StatusCode')) >= 0) {
            status = 'SUCCESS';            
        } else {
            status = 'FAILED';
        }

        mapResult.put('status',status);
        System.debug('CallRFC :: mapResult :: '+mapResult);
        return mapResult;
    }

    @future(callout = true)
    public static void callRFCAsync(String tokenizedCardNum,
            String exp,
            Decimal amount,
            String cardType,
            String customerNum,
            String woId,
            String ccId,
            String ccName) {
        System.debug('callRFC ::'+DateTime.now());

            String m_strURL;
            String m_strUser;
            String m_strPassword;
            String m_strMerchantID;
            
        System.debug('DATA: ' + tokenizedCardNum + ' ' + exp + ' ' + String.valueOf(amount) + ' ' + cardType + ' ' + customerNum);
       /*
            m_strURL = pe.CLO_iFrameURLRoot__c;
            m_strGUID = pe.CLO_GUID__c;
            m_strPSK = pe.CLO_psk__c;
            rfc_endpoint = pe.CLO_rfc_endpoint__c;
            rfc_unpw = pe.CLO_rfc_unpw__c;
       */
        m_strURL = 'https://cert-xipayapi.paymetric.com/PMXIGGE/XiPay30WS.asmx';
        m_strUser = 'PAYMETRIC\\WSACynsureLLCDV';
        m_strPassword = 'Cynsurellc106*';
        m_strMerchantID = 'c0dfe064-64a8-4599-bc71-c75fcd2419d3';
        
        System.debug('exp :: '+exp);
        System.debug('cardType :: '+cardType);
        
        XiPay xpay = new XiPay(m_strURL, m_strUser,m_strPassword,m_strMerchantID);   
        string rawResp = xpay.getFullAuthorization(tokenizedCardNum, exp, amount, cardType, currencyIsoCode);
        
        DOM.Document doc=new DOM.Document();
        try{
            doc.load(rawResp);
            DOM.XmlNode rootNode=doc.getRootElement();
            mapResult = parseXML(rootNode);
            mapResult.put('rawResponse',rawResp);

            
        }catch(exception e){
            system.debug(e.getMessage());
        }
        
        String status;
        if (mapResult.get('StatusCode') != null && mapResult.get('StatusCode') != '' && Integer.ValueOf(mapResult.get('StatusCode')) >= 0) {
            status = 'SUCCESS';            
        } else {
            status = 'FAILED';
        }

        mapResult.put('status',status);
        System.debug('CallRFC :: mapResult :: '+mapResult);
        try{
            if(woId != null) {
                WorkOrder worder = new WorkOrder(Id=woId,CLO_Pre_authorized__c = mapResult.get('status'),CLO_Pre_authorized_Reason__c = mapResult.get('StatusCode') + ' and ' + mapResult.get('StatusTXN'));
                //update worder;
            }

            if(ccId != null){
                String strNameOnCard = '';
                if(ccName != null)
                    strNameOnCard = ccName;
                    
                if(ccName != null && tokenizedCardNum != null)
                    strNameOnCard += ' - ' + tokenizedCardNum.right(4);
                
                CLO_Credit_Card_Details__c ccDetails = new CLO_Credit_Card_Details__c(Id=ccId, Name = strNameOnCard, CLO_Tokenize_Card_Number__c = mapResult.get('tokenizeCardNumber'), CLO_Credit_Card_Status__c = 'Active');
                update ccDetails;
            }


        }catch(DMLException e){
            //Handle DML Exception here
        }

        return ;
    }
    
    private static Map<String, String> parseXML(DOM.XMLNode node) {
        
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            if(node.getName() !=null && node.getName()=='AuthorizationCode'){
                AuthorizationCode = node.getText().trim();
                System.debug('AuthorizationCode :: '+AuthorizationCode );
            }
            if(node.getName() !=null && node.getName()=='CardNumber'){
                tokenizeCardNumber = node.getText().trim();
                System.debug('tokenizeCardNumber :: '+tokenizeCardNumber);
            }    
            if(node.getName() !=null && node.getName()=='TransactionID'){
                strTransactionID = node.getText().trim();
                System.debug('strTransactionID :: '+strTransactionID);
            } 
            if(node.getName() !=null && node.getName()=='StatusCode'){
                strStatusCode = node.getText().trim();
                System.debug('strStatusCode :: '+strStatusCode);
            }
            if(node.getName() !=null && node.getName()=='StatusTXN'){
                strStatusTXN = node.getText().trim();
                System.debug('strStatusTXN :: '+strStatusTXN);
            }
            if(node.getName() !=null && node.getName()=='AuthorizationDate'){
                strAuthorizationDate = node.getText().trim();
                System.debug('strAuthorizationDate :: '+strAuthorizationDate);
            }
            if(node.getName() !=null && node.getName()=='AuthorizationTime'){
                strAuthorizationTime = node.getText().trim();
                System.debug('strAuthorizationTime :: '+strAuthorizationTime);
            }
            if(node.getName() !=null && node.getName()=='Message'){
                strMessage = node.getText().trim();
                System.debug('strMessage :: '+strMessage);
            }
            
        }
        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child);
        }
        
        return new Map<String, String> {
            'tokenizeCardNumber' => tokenizeCardNumber,
            'AuthorizationCode' => AuthorizationCode,
            'TransactionID' => strTransactionID,
            'StatusCode' => strStatusCode,
            'StatusTXN' => strStatusTXN,
            'Message' => strMessage,
            'AuthorizationTime' => strAuthorizationTime,
            'AuthorizationDate' => strAuthorizationDate
        };
    }
    
    public void saveCC()
    {
        System.debug('saveCC ::'+DateTime.now());
        if(ccDetail.CLO_CC_Default__c)
        {
            ccDetail.CLO_CC_Default__c = false;
            
            if(ccDetail.CLO_Credit_Card_Name_on_Card__c != null)
                ccDetail.Name = ccDetail.CLO_Credit_Card_Name_on_Card__c;
                    
            if(ccDetail.Name != null && ccDetail.CLO_CC_Card_Number__c != null)
                ccDetail.Name += ' - ' + ccDetail.CLO_CC_Card_Number__c.right(4);
            
            ccDetail.CLO_Credit_Card_Status__c = 'Active';
            system.debug('accountId :::::' + ApexPages.currentPage().getParameters().get('id'));
            ccDetail.Account__c = ApexPages.currentPage().getParameters().get('id');
            ccDetail.CLO_Tokenize_Card_Number__c = tokenCardNumber;
            
            upsert ccDetail CLO_CC_Card_Number__c;
        }
        
        List<Account> accList = [select id, Name, AccountNumber, CLO_Sales_Org__c, CurrencyIsoCode from Account
                                 where id =: ApexPages.currentPage().getParameters().get('id')];
        
        Map<String, String> ccTypeMap = new Map<String, String>{'AX' => 'AMEX',
                                                                'MC' => 'MC',
                                                                'VI' => 'Visa',
                                                                'DI' => 'DISC'};
        
        if(accList.size() > 0)
        {
            String quoteId = ApexPages.currentPage().getParameters().get('quoteId');
            String orderId = ApexPages.currentPage().getParameters().get('orderId');
            System.debug('orderId :: '+orderId);
            System.debug('orderId :: '+orderId);
            String strQuoteNumber;
            String strOrderNumber;
            String strWhereClause;
            if(quoteId != null) {
                strQuoteNumber = quoteId;
                //strWhereClause = ' CLO_Quote__c = '+quoteId;
            }
            if(orderId != null) {
                strOrderNumber = orderId;
                //strWhereClause = ' Order__c = '+orderId;
            }
            /*system.debug('strWhereClause :: '+strWhereClause);
            List<CLO_Paymetric_Payment__c>  lstPayments = (List<CLO_Paymetric_Payment__c>)CLO_DBManager.getSObjectList('CLO_StatusCode__c, CLO_Payment_Card__r.CLO_Reference_Number__c', 'CLO_Paymetric_Payment__c', strWhereClause);
            system.debug('lstPayments :: '+lstPayments);
            if (lstPayments[0].CLO_StatusCode__c != null && lstPayments[0].CLO_StatusCode__c != '' && Integer.ValueOf(lstPayments[0].CLO_StatusCode__c) >= 0) {
            */  
            String depositAmt = ApexPages.currentPage().getParameters().get('depositAmt');
            if(depositAmt != null)
            {
                CLO_PaymetricServiceInvoker.invokeCreditCardPaymentService(accList[0].AccountNumber, 
                                                                        strQuoteNumber,
                                                                        strOrderNumber,
                                                                        ccDetail.CLO_CC_Amount__c+'',
                                                                        ccTypeMap.get(ccDetail.CLO_CC_Type__c),
                                                                        '20'+ccDetail.CLO_CC_Expiry_Year__c+'-'+ccDetail.CLO_CC_Expiry_Month__c+'-01',
                                                                        ccDetail.CLO_Tokenize_Card_Number__c,
                                                                        accList[0].CurrencyIsoCode,
                                                                         accList[0].CLO_Sales_Org__c);
            }                                                                            
                                                                            
            /*}
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, lstPayments[0].CLO_StatusCode__c));
            }*/
        }
    }
    
    public List<SelectOption> queryCCOptions() {
        List<SelectOption> ccOptions= new List<SelectOption>();
        
        ccOptions.add(new SelectOption('', '-Select Credit Card-'));
        
        for(CLO_Credit_Card_Details__c ccDetails: [select Id,CLO_Reference_Number__c, CLO_Tokenize_Card_Number__c, Name, CLO_CC_Default__c, CLO_CC_Card_Number__c, CLO_Credit_Card_Last_4__c from CLO_Credit_Card_Details__c where Account__c =: accountId]) {
            if(ccDetails != null && ccDetails.CLO_Credit_Card_Last_4__c != null) {
                ccOptions.add(new SelectOption(ccDetails.Id, ccDetails.CLO_Credit_Card_Last_4__c ));
                
                if(ccDetails.CLO_CC_Default__c)
                {
                    selectedCC = ccDetails.Id;
                    querySelectedCard();
                }
            }
        }
        return ccOptions;      
    }
    
    public void querySelectedCard()
    {
        system.debug('selectedCC :::::' + selectedCC);
        if(selectedCC != null && selectedCC.trim().length() > 0)
        {
            ccDetail = [select Id, Name, CLO_CC_Amount__c, CLO_Tokenize_Card_Number__c, CLO_CC_Default__c, CLO_Credit_Card_Name_on_Card__c, 
                        CLO_Reference_Number__c, CLO_Credit_Card_Last_4__c,
                        CLO_CC_Expiry_Month__c, CLO_CC_Expiry_Year__c, CLO_CC_Type__c, CLO_CC_Card_Number__c 
                        from CLO_Credit_Card_Details__c where Id =: selectedCC];
                        
            ccDetail.CLO_Reference_Number__c = null;
            //ccDetail.CLO_CC_Amount__c = 0;
            /*
            String strPrice = ApexPages.currentPage().getParameters().get('totprice');
            if(String.isNotEmpty(strPrice)){
                Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
                Matcher matcher = nonAlphanumeric.matcher(strPrice);
                strPrice = matcher.replaceAll(''); 
                
                Decimal d = Decimal.valueOf(!String.isEmpty(strPrice) ? strPrice : '0');
                d = d.setScale(8, RoundingMode.HALF_EVEN);
                ccDetail.CLO_CC_Amount__c = d;
            }*/
            String authAmt = ApexPages.currentPage().getParameters().get('authAmt');
            if(String.isNotEmpty(authAmt)){
                Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
                Matcher matcher = nonAlphanumeric.matcher(authAmt);
                authAmt = matcher.replaceAll(''); 
                
                Decimal d = Decimal.valueOf(!String.isEmpty(authAmt) ? authAmt : '0');
                d = d.setScale(8, RoundingMode.HALF_EVEN);
                ccDetail.CLO_CC_Amount__c = d;
            }

            String depositAmt = ApexPages.currentPage().getParameters().get('depositAmt');
            if(String.isNotEmpty(depositAmt)){
                Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
                Matcher matcher = nonAlphanumeric.matcher(depositAmt);
                depositAmt = matcher.replaceAll(''); 
                
                Decimal d = Decimal.valueOf(!String.isEmpty(depositAmt) ? depositAmt : '0');
                d = d.setScale(8, RoundingMode.HALF_EVEN);
                ccDetail.CLO_CC_Amount__c = d;
            }
            
            String ordAmount = ApexPages.currentPage().getParameters().get('ordAmount');
            if(String.isNotEmpty(ordAmount)){
                Pattern nonAlphanumeric = Pattern.compile('[^.0-9]');
                Matcher matcher = nonAlphanumeric.matcher(ordAmount);
                ordAmount = matcher.replaceAll(''); 
                
                Decimal d = Decimal.valueOf(!String.isEmpty(ordAmount) ? ordAmount : '0');
                d = d.setScale(8, RoundingMode.HALF_EVEN);
                ccDetail.CLO_CC_Amount__c = d;
            }
            String quoteId = ApexPages.currentPage().getParameters().get('quoteId');
            String orderId = ApexPages.currentPage().getParameters().get('orderId');
            String workOrderId = ApexPages.currentPage().getParameters().get('workOrderId');
            System.debug('orderId :: '+orderId);
            String strReferenceNum = '';
            if(quoteId != null)
                strReferenceNum = quoteId;
            else if(orderId != null)
                strReferenceNum = orderId;
            else if(workOrderId != null)
                strReferenceNum = workOrderId;
                
            ccDetail.CLO_Reference_Number__c = strReferenceNum;
            ccDetail.CLO_CC_Default__c = false;
        }
        else
        {
            String quoteId = ApexPages.currentPage().getParameters().get('quoteId');
            String orderId = ApexPages.currentPage().getParameters().get('orderId');
            String workOrderId = ApexPages.currentPage().getParameters().get('workOrderId');
            String strReferenceNum = '';           
            if(quoteId != null)
                strReferenceNum = quoteId;
            else if(orderId != null)
                strReferenceNum = orderId;
            else if(workOrderId != null)
                strReferenceNum = workOrderId;
            
            ccDetail = new CLO_Credit_Card_Details__c(Account__c = accountId, CLO_Reference_Number__c = strReferenceNum, CLO_CC_Amount__c = amount);
        }
    }
    
   public static void saveCC(String ccNumber, String cardType, String aid) {
            upsert new CLO_Credit_Card_Details__c(CLO_CC_Card_Number__c = '', 
                                                  CLO_Tokenize_Card_Number__c= '',
                                                  CLO_CC_Type__c = cardType,
                                                  CLO_Credit_Card_Name_on_Card__c = 'test');
    }
}
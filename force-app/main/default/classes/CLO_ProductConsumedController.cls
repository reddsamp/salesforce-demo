public class CLO_ProductConsumedController {

    public List<ProductConsumed> productsCList {get;set;}
    public List<List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>> dataList {get;set;}
    public List<SelectOption> woliList {get;set;}
    public String isError {get;set;}
    
    public String worderId;
    
    public CLO_ProductConsumedController() {
        init();
    }


    public CLO_ProductConsumedController(ApexPages.StandardController controller) {
        init();
    }
    
    public void init()
    {
        worderId = ApexPages.currentPage().getParameters().get('id');
        productsCList = new List<ProductConsumed>();
        dataList = new List<List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>>();
        woliList = new List<SelectOption>();
        
        woliList.add(new SelectOption('', '-Select Workorder Line Item'));
        
        for(WorkorderLineItem rec : [select id, LineItemNumber from WorkorderLineItem where WorkorderId =: worderId])
        {
            woliList.add(new SelectOption(rec.Id, rec.LineItemNumber));
        }
        
        productsCList = [select id, QuantityConsumed, WorkorderLineItemId, ProductItemId, ProductItem.Product2.ProductCode, ProductItem.LocationId, ProductItem.QuantityOnHand
                         from ProductConsumed
                         where WorkorderId =: worderId];
    }
    
    public void callMaterialService()
    {
        dataList = new List<List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>>();
        
        List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> tempList = fetchFromOrderItem(worderId);
        List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> rowList =  new List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>();
        
        Integer count = 0;
        for(CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper s : tempList)
        {
            count++;
            rowList.add(s);
            
            if(count == 999)
            {
                count = 0;
                dataList.add(rowList);
                rowList = new List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>();
                break;
            }
        }
        
        if(rowList.size() > 0)
            dataList.add(rowList);
        
        Map<String, ProductConsumed> qMap = new Map<String, ProductConsumed>();
        
        for(ProductConsumed p : productsCList){
            qMap.put(p.ProductItem.Product2.ProductCode, p);
        }
        
        for(List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> data : dataList){
            for(CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper d : data){
                if(qMap.containskey(d.MATNR))
                {
                    d.quantity = (Integer) qMap.get(d.MATNR).QuantityConsumed;
                    d.customerName = qMap.get(d.MATNR).WorkorderLineItemId;
                    d.UoM = qMap.get(d.MATNR).Id;
                }
                else
                {
                    d.quantity = null;
                    d.UoM = null;
                }
            }
        }
    }
    
    
    public static List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> fetchFromOrderItem(String worderId)
    {
        List<Workorder> woList = [select id, AccountId, Account.CLO_Sales_Org__c
                                  from Workorder
                                  where Id =: worderId];
                                 
        return CLO_MaterialAviabilityController.fetchMaterialAvaliablity(woList[0].Account.CLO_Sales_Org__c, 
                                                                         '', 
                                                                         '', 
                                                                         UserInfo.getUserEmail(),//'joshi.ambadas@focalcxm.com', 
                                                                         '', 
                                                                         'X');
    }
    
     public void saveProductsConsumed()
    {
        try
        {
            isError = 'No';
            
            List<ProductItem> piInsertList = new List<ProductItem>();
            List<ProductConsumed> pcInsertList = new List<ProductConsumed>();
            List<ProductConsumed> pc2InsertList = new List<ProductConsumed>();
            
            Map<String, String> productCodes = new Map<String, String>();
            Map<String, String> locationCodes = new Map<String, String>();
            
            for(List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> data : dataList){
                for(CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper d : data){
                    if(d.quantity != null)
                    {
                        productCodes.put(d.MATNR, null);
                        locationCodes.put(d.WERKS, null);
                    }
                }
            }
            
            for(Product2 p : [select id, ProductCode from Product2 where ProductCode in : productCodes.keyset()])
            {
                productCodes.put(p.ProductCode, p.id);
            }
            
            for(sObject p : [select id, Name from Location where Name in : locationCodes.keyset()])
            {
                locationCodes.put((String) p.get('Name'), p.id);
            }
            
            Map<String, String> productItemMap = new Map<String, String>();
            for(ProductItem p : [select id, LocationId, Product2Id, SerialNumber 
                                 from ProductItem 
                                 where Product2Id in : productCodes.values()])
            {
                productItemMap.put(p.LocationId+''+p.Product2Id, p.Id);
            }
            
            for(List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> data : dataList){
                for(CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper d : data){
                    if(d.quantity != null && d.quantity > 0)
                    {
                        if(productItemMap.containskey(locationCodes.get(d.WERKS)+''+productCodes.get(d.MATNR)))
                        {
                            piInsertList.add(new ProductItem(Id = productItemMap.get(locationCodes.get(d.WERKS)+''+productCodes.get(d.MATNR)),
                                                             QuantityOnHand = integer.valueOf(d.LABST)));
                            pcInsertList.add(new ProductConsumed(WorkorderId = worderId,
                                                                 QuantityConsumed = d.quantity,
                                                                 WorkOrderLineItemId = d.customerName,
                                                                 ProductItemId = productItemMap.get(locationCodes.get(d.WERKS)+''+productCodes.get(d.MATNR)),
                                                                 Id = (d.UoM != null && d.UoM.trim().length() > 0) ? d.UoM : null));
                        }
                        else
                        {
                            piInsertList.add(new ProductItem(LocationId = locationCodes.get(d.WERKS),
                                                             Product2Id = productCodes.get(d.MATNR),
                                                             QuantityOnHand = integer.valueOf(d.LABST),
                                                             QuantityUnitOfMeasure = 'Each',
                                                             SerialNumber = d.SERNR));
                        
                            pc2InsertList.add(new ProductConsumed(WorkorderId = worderId,
                                                                 QuantityConsumed = d.quantity,
                                                                 WorkOrderLineItemId = d.customerName,
                                                                 Id = (d.UoM != null && d.UoM.trim().length() > 0) ? d.UoM : null));
                        }
                    }
                }
            }
            
            system.debug(':::::'+piInsertList);
            if(piInsertList.size() > 0)
                upsert piInsertList;
                
            for(integer i=0; i < pc2InsertList.size(); i++)
            {
                pc2InsertList[i].ProductItemId = piInsertList[i].Id;
            }
                
            if(pcInsertList.size() > 0)
                upsert pcInsertList;
        
            if(pc2InsertList.size() > 0)
                upsert pc2InsertList;
        }catch(Exception ex){
            system.debug(':::::' + ex.getMessage());
            isError = 'Yes';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
}
public class CLO_OrderItemController {
    
    public List<OrderItem> orderItems {get;set;}
    public List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> dataList {get;set;}
    public String orderId;
    public String orderItemId {get;set;}
    public String locationString {get;set;}
    
    public CLO_OrderItemController() {
        init();
    }


    public CLO_OrderItemController(ApexPages.StandardController controller) {
        init();
    }
    
    public void init()
    {
        orderId = ApexPages.currentPage().getParameters().get('id');
        orderItems = new List<OrderItem>();
        dataList = new List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>();
        
        orderItems = [select id, Product2.Name, Quantity, CLO_Batch__c, Product2.ProductCode, Location__c, Location__r.Name from OrderItem
                      where orderid =: orderId];
    }
    
    
    public void callMaterialService()
    {
        dataList = new List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>();
        dataList = fetchFromOrderItem(orderItemId);
    }
    
    
    public PageReference saveLocations()
    {
        try
        {
            if(locationString != null)
            {
                List<String> locOIList = locationString.split('~~~');
                if(locOIList.size() > 0)
                {
                    for(String s : locOIList)
                    {
                        List<String> splitedList = s.split('``````');
                        
                        if(splitedList.size() == 2)
                        {
                            List<sObject> lList = [select id from Location where Name =: splitedList[1]];
                            
                            if(lList.size() > 0)
                            {
                                update new OrderItem(Id = splitedList[0],
                                                     Location__c = lList[0].Id);
                            }
                            else
                            {
                                sObject loc = Schema.getGlobalDescribe().get('Location').newSObject() ;
                                loc.put('Name' , splitedList[1]);
                                loc.put('LocationType' , 'Plant');
                                insert loc;
                                
                                update new OrderItem(Id = splitedList[0],
                                                     Location__c = loc.Id);
                            }
                        }
                    }
                }
            }
        }catch(Exception ex){}
        
        return new PageReference('/'+orderId).setRedirect(true);
    }
    
    
    public static List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> fetchFromOrderItem(String orderItemId)
    {
        List<OrderItem> ordeItemList = [select id, Order.AccountId, Order.Account.CLO_Sales_Org__c, Product2.Name, Quantity, Product2.ProductCode, Location__c from OrderItem
                                        where Id =: orderItemId];
                                 
        return CLO_MaterialAviabilityController.fetchMaterialAvaliablity(ordeItemList[0].Order.Account.CLO_Sales_Org__c, 
                                                                         '', 
                                                                         ordeItemList[0].Product2.ProductCode, 
                                                                         '', 
                                                                         '', 
                                                                         '');
    }
}
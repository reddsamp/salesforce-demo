public class CLO_SAPOrderSyncController
{
    
    @AuraEnabled
    public static Order getOrder(String orderId)
    {
        List<Order> listOrd = [select Id, CLO_SAP_Sync_Error__c,SAP_Order_Id__c, CLO_Paymetric_Error__c, (SELECT id, OrderItemNumber FROM OrderItems) FROM Order WHERE id =: orderId];
        List<CLO_Paymetric_Payment__c> lstPayments = new List<CLO_Paymetric_Payment__c>();
        List<OrderItem> lstOrderItems = new List<OrderItem>();
        
        For(Order ord:listOrd) {
            lstOrderItems = ord.OrderItems;
            System.debug('lstOrderItems :: Size :: '+lstOrderItems.size());
        }
        /*
        if(lstOrderItems.isEmpty())
        {
            update new Order(Id = listOrd[0].Id, CLO_SAP_Sync_Error__c = 'Please Add Order Products.', CLO_Paymetric_Error__c = '');
        }*/
        Set<Id> ordIds =new Set<Id>();
        if(listOrd[0].CLO_Paymetric_Error__c != '' && listOrd[0].CLO_Paymetric_Error__c != null){
            ordIds.add(listOrd[0].Id);
            lstPayments = [SELECT id, CLO_Authorization_Code__c, CLO_Authorization_Reference_Code__c, CLO_Credit_Card_Authorized_Date__c, CreatedDate, CLO_Quote__c, CLO_TransactionID__c, CLO_StatusCode__c, CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c, CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c, CLO_Message__c,CLO_Credit_Card_Authorized_Time__c FROM CLO_Paymetric_Payment__c WHERE CLO_Order__c = :ordIds AND CLO_StatusCode__c = '100' ORDER BY CreatedDate DESC LIMIT 1 ];
            System.debug('lstPayments :::: '+lstPayments);
        }
        if((lstPayments != null && lstPayments.size() > 0) || lstOrderItems.isEmpty()) {
            System.debug('CLO_Paymetric_Error__c :::: '+listOrd[0].CLO_Paymetric_Error__c);
            update new Order(Id = listOrd[0].Id, CLO_Paymetric_Error__c = '');
        }
        return [select Id, SAP_Order_Id__c, Account.AccountNumber, CLO_SAP_Sync_Error__c, CLO_Paymetric_Error__c, 
        (Select Id, Description, OrderItemNumber, CLO_Primary_Line_Number__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM OrderItems) from Order where id =: orderId];
    }
    
    @AuraEnabled
    public static List<OrderItem> getOrderItems(String orderId)
    {
        return [Select Id, Quantity, Description, OrderItemNumber, CLO_Primary_Line_Number__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM OrderItem WHERE orderid =: orderId];
    }
    
    @AuraEnabled
    public static Order syncWithSAP(String orderId, boolean isSimulateOrder)
    {
        List<String> productNames = new List<String>();
        List<OrderItem> orderItems = new List<OrderItem>();
        for(order ord:[SELECT Id,Status,Type,CreatedDate,Account.AccountNumber,Account.CLO_Sales_Org__c,account.CLO_Customer_Number__c,
                              CreatedBy.Email,OrderNumber,PoNumber,PoDate,CLO_Requested_Delivery_Date__c, CLO_Paymetric_Error__c,
                              account.CLO_Incoterm_1__c,account.CLO_Incoterm_2__c,CLO_Order_Reason__c,EffectiveDate,
                              EndDate,Account.CLO_Shipping_Condition__c,CurrencyIsoCode,CLO_OrderNotes_H__c,
                              CLO_DeliveryNotes_H__c,CLO_Invoice_Notes_H__c,CLO_Sales_Org__c,CLO_Ship_To__c,
                              CLO_Ship_To__r.AccountNumber,CLO_Incoterm_1__c,CLO_Incoterm_2__c,
                              CLO_Shipping_Condition__c,CLO_Payment_Terms__c, CLO_Opportunity_Type__c,
                              CLO_Initial_Customer__c,CLO_New_Customer__c,CLO_Spec_Stock_Partner__c,
                              CLO_Goods_Issue_Date__c,CLO_Doc_Header_Text__c,CLO_Sales_Rep_Email__c,
                              ShippingStreet,ShippingCity,ShippingPostalCode,ShippingStateCode,ShippingCountryCode,
                              CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c,Owner.Email,
                              CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c,
                              CLO_Payment_Card__r.CLO_CC_Expiry_Date__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c,CLO_On_behalf_of__c, CLO_Pre_authorized__c,
                              CLO_Order_Amount__c,CLO_Authorization_Amount__c,CLO_Asset_Material_Number__c,CLO_Asset_Number__c,CLO_Payment_Transction_ID__c,
                                                                              (select Id,
                                                                               OrderItemNumber,   
                                                                               CLO_Parent_Bundle_Number__c,
                                                                               Product2.Name,
                                                                               Product2.ProductCode,
                                                                               quantity,
                                                                               UnitPrice,
                                                                               Product2.QuantityUnitOfMeasure,
                                                                               CLO_Price_Included_Value__c,
                                                                               CLO_Requested_Date__c,
                                                                               CLO_Primary_Line_Number__c,
                                                                               CurrencyIsoCode,
                                                                               CLO_Actual_quantity_delivered_sales_unit__c,
                                                                               CLO_Batch_Number__c,
                                                                               CLO_Valuation_Type__c,
                                                                               CLO_Asset_Serial_Number__c,
                                                                               CLO_Fee_Amount__c,
                                                                               CLO_Referral_and_Finder_Fee_Notes__c,
                                                                               CLO_Non_Billable__c,
                                                                               CLO_Price_Override__c
                                                                               FROM OrderItems
                                                                               Order By CLO_Parent_Bundle_Number__c NULLS FIRST) FROM order where id=: orderId]){
                                       
                System.debug('ord.OrderItems :::::' + ord.OrderItems);
                
                CLO_SAP_ServiceInvoker.invokeCreateOrderService(ord, isSimulateOrder);          
                                       
               
        }
                
        return [select Id, SAP_Order_Id__c, Account.AccountNumber, CLO_SAP_Sync_Error__c, CLO_Paymetric_Error__c,
                (Select Id, Description, OrderItemNumber, CLO_Primary_Line_Number__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM OrderItems) from Order where id =: orderId];
    }
}
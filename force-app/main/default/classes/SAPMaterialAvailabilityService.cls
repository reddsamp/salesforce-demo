//Generated by wsdl2apex

public class SAPMaterialAvailabilityService {
    public class ZSF_MATERIAL_AVAILABILITY_element {
        public String I_CONS_STOCK;
        public String I_CUSTOMER;
        public String I_MATERIAL;
        public String I_PLANT;
        public String I_SALES_ORG;
        public String I_SALES_REP_EMAIL;
        public SAPMaterialAvailabilityService.TABLE_OF_ZMAT_STOCK T_MAT_STOCK;
        private String[] I_CONS_STOCK_type_info = new String[]{'I_CONS_STOCK','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] I_CUSTOMER_type_info = new String[]{'I_CUSTOMER','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] I_MATERIAL_type_info = new String[]{'I_MATERIAL','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] I_PLANT_type_info = new String[]{'I_PLANT','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] I_SALES_ORG_type_info = new String[]{'I_SALES_ORG','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] I_SALES_REP_EMAIL_type_info = new String[]{'I_SALES_REP_EMAIL','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] T_MAT_STOCK_type_info = new String[]{'T_MAT_STOCK','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'I_CONS_STOCK','I_CUSTOMER','I_MATERIAL','I_PLANT','I_SALES_ORG','I_SALES_REP_EMAIL','T_MAT_STOCK'};
    }
    public class TABLE_OF_ZMAT_STOCK {
        public SAPMaterialAvailabilityService.ZMAT_STOCK[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZSF_MATERIAL_AVAILABILITYResponse_element {
        public SAPMaterialAvailabilityService.BAPIRETURN E_RETURN;
        public SAPMaterialAvailabilityService.TABLE_OF_ZMAT_STOCK T_MAT_STOCK;
        private String[] E_RETURN_type_info = new String[]{'E_RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] T_MAT_STOCK_type_info = new String[]{'T_MAT_STOCK','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'E_RETURN','T_MAT_STOCK'};
    }
    public class ZMAT_STOCK {
        public String MATNR;
        public String WERKS;
        public String KUNNR;
        public String CHARG;
        public String LABST;
        public String SERNR;
        private String[] MATNR_type_info = new String[]{'MATNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WERKS_type_info = new String[]{'WERKS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUNNR_type_info = new String[]{'KUNNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CHARG_type_info = new String[]{'CHARG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LABST_type_info = new String[]{'LABST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SERNR_type_info = new String[]{'SERNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'MATNR','WERKS','KUNNR','CHARG','LABST','SERNR'};
    }
    public class BAPIRETURN {
        public String TYPE_x;
        public String CODE;
        public String MESSAGE;
        public String LOG_NO;
        public String LOG_MSG_NO;
        public String MESSAGE_V1;
        public String MESSAGE_V2;
        public String MESSAGE_V3;
        public String MESSAGE_V4;
        private String[] TYPE_x_type_info = new String[]{'TYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CODE_type_info = new String[]{'CODE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_type_info = new String[]{'MESSAGE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOG_NO_type_info = new String[]{'LOG_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOG_MSG_NO_type_info = new String[]{'LOG_MSG_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V1_type_info = new String[]{'MESSAGE_V1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V2_type_info = new String[]{'MESSAGE_V2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V3_type_info = new String[]{'MESSAGE_V3','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V4_type_info = new String[]{'MESSAGE_V4','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'TYPE_x','CODE','MESSAGE','LOG_NO','LOG_MSG_NO','MESSAGE_V1','MESSAGE_V2','MESSAGE_V3','MESSAGE_V4'};
    }
    public class ZSF_MATERIAL_AVAILABILITY {
        public String endpoint_x = 'https://l800134-iflmap.hcisbp.us4.hana.ondemand.com/cxf/MaterialAvailability_CEQ';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'SAPMaterialAvailabilityService'};
        public SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITYResponse_element ZSF_MATERIAL_AVAILABILITY(String I_CONS_STOCK,String I_CUSTOMER,String I_MATERIAL,String I_PLANT,String I_SALES_ORG,String I_SALES_REP_EMAIL,SAPMaterialAvailabilityService.TABLE_OF_ZMAT_STOCK T_MAT_STOCK) {
            SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITY_element request_x = new SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITY_element();
            request_x.I_CONS_STOCK = I_CONS_STOCK;
            request_x.I_CUSTOMER = I_CUSTOMER;
            request_x.I_MATERIAL = I_MATERIAL;
            request_x.I_PLANT = I_PLANT;
            request_x.I_SALES_ORG = I_SALES_ORG;
            request_x.I_SALES_REP_EMAIL = I_SALES_REP_EMAIL;
            request_x.T_MAT_STOCK = T_MAT_STOCK;
            SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITYResponse_element response_x;
            Map<String, SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITYResponse_element> response_map_x = new Map<String, SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITYResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:ZSF_MATERIAL_AVAILABILITY:ZSF_MATERIAL_AVAILABILITYRequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSF_MATERIAL_AVAILABILITY',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSF_MATERIAL_AVAILABILITY.Response',
              'SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITYResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}
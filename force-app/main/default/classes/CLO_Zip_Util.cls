global class CLO_Zip_Util {

  /**
   * Receive Attachments info from Attachment ParentId
   */
  webService static String getAttachmentByParentId( String sfdcId ){

    if( String.isEmpty( sfdcId ) ) return CLO_Productbundle_Util.errorJson('Parameter sfdcId is required.');
    
    List<Attachment> attachmentList =  [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :sfdcId];
    if( attachmentList == null || attachmentList.size() == 0 ) return CLO_Productbundle_Util.errorJson('Attachment not found.');

    return wrapAttachmentList( attachmentList );
  }

  /**
   * Save Zip file to Document
   */
    
    webService static String sendReport(String memIds ) {
		System.debug(memIds);
        Map<String,PageReference> reportPagesIndex;
         String regex = '\\[|\\]';
 		memIds = memIds.replaceAll(regex, '');
        memIds= memIds.replaceAll( '\\s+', '');
        List<String> lstAlpha = memIds.split(',');
        List<String> optProdIds;
        // Create PDF
        List<Object> pdflist= new List<Object>();
          System.debug('Apex enter');
         PageReference reportPage  = Page.CLO_Product_Template;
        for ( String mem : lstAlpha ){ 
            System.debug(mem);
            String i = String.valueOf(mem);
            System.debug('Id value');
            System.debug(i);
        	reportPage.getParameters().put('id', i);
         System.debug(reportPage);
        Blob reportPdf;
          
        String pdfName;
        try {
            reportPdf = reportPage.getContent();
            Product2 record = [SELECT Id,Description FROM Product2 WHERE Id = :i];
            pdfName = record.Description;
        }
        catch (Exception e) {
            reportPdf = Blob.valueOf(e.getMessage());
        }
            Map<String, String> atMap = new Map<String, String>();
      //atMap.put( 'Id', at.Id );
      atMap.put( 'Name', pdfName );
      atMap.put( 'Body', EncodingUtil.base64Encode( reportPdf ) );
      //atMap.put( 'ContentType', at.ContentType );
     
        pdflist.add(atMap);
            }
        System.debug(pdflist);

        return CLO_Productbundle_Util.normalJson(pdflist); 
    }
  webService static String saveToDocument( String zipFileData, String fileName ){

    try{
      String userId = UserInfo.getUserId();
      List<Document> docList = [SELECT Id, Name, FolderId, Body FROM Document WHERE Name = :fileName AND FolderId = :userId LIMIT 1 ];
      Document doc = new Document();
      if( docList == null || docList.size() == 0 ) {
        doc.Name = fileName;
        doc.FolderId = UserInfo.getUserId();
        doc.Body = EncodingUtil.base64Decode( zipFileData );
        insert doc;
      } else {
        doc = docList.get(0);
        doc.Body = EncodingUtil.base64Decode( zipFileData );
        update doc;
      }
      return CLO_Productbundle_Util.normalJson( doc.Id );
    } catch ( Exception ex ) {
      return CLO_Productbundle_Util.errorJson( ex.getMessage() );
    }
  }

  //Format JSON String from AttachmentList
  private static String wrapAttachmentList( List<Attachment> attachmentList ){

    List<Object> dataList = new List<Object>();
    for( Attachment at : attachmentList ){
      Map<String, String> atMap = new Map<String, String>();
      //atMap.put( 'Id', at.Id );
      atMap.put( 'Name', at.Name );
      atMap.put( 'Body', EncodingUtil.base64Encode( at.Body ) );
      //atMap.put( 'ContentType', at.ContentType );
      dataList.add( atMap );
    }
    return CLO_Productbundle_Util.normalJson( dataList );
  }


}
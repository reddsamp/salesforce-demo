public class CLO_AssessmentResTriggerHandler  implements CLO_ITriggerHandler{
    
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
    public void beforeInsert(List<sObject> newList) {
    }
     
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
		CLO_AssessmentResTriggerHelper.calAccPromoterScore(newList);
    }
     
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
		CLO_AssessmentResTriggerHelper.calAccPromoterScore(newList);
    }
     
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
		
    }
     
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
		CLO_AssessmentResTriggerHelper.calAccPromoterScore(oldList);
    }
     
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
}
public class CLO_ServiceContractTriggerHandler implements CLO_ITriggerHandler{
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
    public static Set<Id> serviceContractIds = new Set<Id>();
    
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
    
    public void beforeInsert(List<sObject> newList) {
        
        
        List<String> customerIds = new List<String>();
        Map<Id, Account> customerMap = new Map<Id, Account>();
        List<ServiceContract> ServiceContractList = new List<ServiceContract>();
        List<ServiceContract> lstServiceContract = (List<ServiceContract>) newList;
        for(ServiceContract AI : lstServiceContract){
            if(AI.SBQQSC__Opportunity__c != null && AI.AccountId != null)
            {
                ServiceContractList.add(AI); 
            }
            If(!customerIds.contains(AI.AccountId) && AI.AccountId != Null){
                customerIds.add(AI.AccountId);
            }
        }
        If(customerIds.size() > 0){
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(customerIds);        
            customerMap = new Map<Id, Account>((List<Account>)CLO_DBManager.getSObjectList(' Id, CLO_Sales_Org__c, BillingCountry, BillingCountryCode, CLO_Payment_Terms__c, CLO_Incoterm_1__c, CLO_Incoterm_2__c  ',' Account ', whereClause)); 
        }
        
        if(ServiceContractList.size() > 0)
        {   
            for(ServiceContract At : ServiceContractList){
                Account acc = customerMap.get(At.AccountId);
                At.CLO_Sales_Org__c = acc.CLO_Sales_Org__c;
                At.CLO_Payment_Terms__c = acc.CLO_Payment_Terms__c;
                At.CLO_Incoterm_1__c = acc.CLO_Incoterm_1__c;
                At.CLO_Incoterm_2__c = acc.CLO_Incoterm_2__c;
                if(At.SBQQSC__Quote__c != null){
                    System.debug('entered2');
                    SBQQ__Quote__c q = [select Id,SBQQ__Type__c, Name,SBQQ__SalesRep__c, SBQQ__BillingFrequency__c from SBQQ__Quote__c where Id = :At.SBQQSC__Quote__c];
                    if (q.SBQQ__SalesRep__c != null)
                        At.OwnerId = q.SBQQ__SalesRep__c;
                    if (q.SBQQ__Type__c =='Service Agreement'){
                        if (q.SBQQ__BillingFrequency__c == 'Annual'){
                            At.CLO_Order_Reason__c = 'ZWY';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Semiannual'){
                            At.CLO_Order_Reason__c = 'ZWS';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Quarterly'){
                            At.CLO_Order_Reason__c = 'ZWQ';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Monthly'){
                            At.CLO_Order_Reason__c = 'ZWM';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Invoice Plan'){
                            At.CLO_Order_Reason__c = 'ZWZ';
                        }
                        //update objOrd;
                    }
                }
            }
        }
    }
    
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
        for(ServiceContract servcontract :  (List<ServiceContract>) newList){
            if(servcontract.SBQQSC__Opportunity__c != null){
                serviceContractIds.add(servcontract.SBQQSC__Opportunity__c);
            }
        }
       // CLO_ServiceContractTriggerHandler.updServContractPartsAmt();    
    }
    
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
        
    }
    
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
        CLO_ServiceContractTriggerHandler.CreateRenewalOpportunity((List<ServiceContract>) newList, (Map<Id, ServiceContract>)oldMap);
        for(ServiceContract servcontract :  (List<ServiceContract>) newList){
            if(servcontract.SBQQSC__Opportunity__c != null){
                serviceContractIds.add(servcontract.SBQQSC__Opportunity__c);
            }
        } 
        
        for(ServiceContract servcontract : (List<ServiceContract>) oldList){
            if(servcontract.SBQQSC__Opportunity__c != null){
                serviceContractIds.add(servcontract.SBQQSC__Opportunity__c);
            }
        }
        CLO_ServiceContractTriggerHandler.updServContractPartsAmt();   
    }
    
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        
    }
    
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        for(ServiceContract servcontract : (List<ServiceContract>) oldList){
            if(servcontract.SBQQSC__Opportunity__c != null){
                serviceContractIds.add(servcontract.SBQQSC__Opportunity__c);
            }
        }
        CLO_ServiceContractTriggerHandler.updServContractPartsAmt();    
    }
    
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
        for(ServiceContract servcontract :  (List<ServiceContract>) newList){
            if(servcontract.SBQQSC__Opportunity__c != null){
                serviceContractIds.add(servcontract.SBQQSC__Opportunity__c);
            }
        }
        CLO_ServiceContractTriggerHandler.updServContractPartsAmt();    
    }
    
    public static void CreateserviceContract(List<ServiceContract> ServiceContractlis, Map<Id, ServiceContract> oldmap){
        
        List<String> customerIds = new List<String>();
        Map<Id, Account> customerMap = new Map<Id, Account>();
        List<ServiceContract> ServiceContractList = new List<ServiceContract>();
        List<ServiceContract> lstServiceContract = ServiceContractlis;
        for(ServiceContract AI : lstServiceContract){
            if(AI.SBQQSC__Opportunity__c != null && AI.AccountId != null)
            {
                ServiceContractList.add(AI); 
            }
            If(!customerIds.contains(AI.AccountId) && AI.AccountId != Null){
                customerIds.add(AI.AccountId);
                System.debug('entered1');
            }
        }
        If(customerIds.size() > 0){
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(customerIds);        
            customerMap = new Map<Id, Account>((List<Account>)CLO_DBManager.getSObjectList(' Id, CLO_Sales_Org__c, BillingCountry, BillingCountryCode, CLO_Payment_Terms__c, CLO_Incoterm_1__c, CLO_Incoterm_2__c  ',' Account ', whereClause)); 
        }
        
        if(ServiceContractList.size() > 0)
        {   
            for(ServiceContract At : ServiceContractList){
                Account acc = customerMap.get(At.AccountId);
                At.CLO_Sales_Org__c = acc.CLO_Sales_Org__c;
                At.CLO_Payment_Terms__c = acc.CLO_Payment_Terms__c;
                At.CLO_Incoterm_1__c = acc.CLO_Incoterm_1__c;
                At.CLO_Incoterm_2__c = acc.CLO_Incoterm_2__c;
                if(At.SBQQSC__Quote__c != null){
                    //System.debug('entered2');
                    SBQQ__Quote__c q = [select Id,SBQQ__Type__c, Name, SBQQ__BillingFrequency__c from SBQQ__Quote__c where Id = :At.SBQQSC__Quote__c];
                    //Order ord = '
                    if (q.SBQQ__Type__c =='Service Agreement'){
                        if (q.SBQQ__BillingFrequency__c == 'Annual'){
                            At.CLO_Order_Reason__c = 'ZWY';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Semiannual'){
                            At.CLO_Order_Reason__c = 'ZWS';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Quarterly'){
                            At.CLO_Order_Reason__c = 'ZWQ';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Monthly'){
                            At.CLO_Order_Reason__c = 'ZWM';
                        }
                        else if (q.SBQQ__BillingFrequency__c == 'Invoice Plan'){
                            At.CLO_Order_Reason__c = 'ZWZ';
                        }
                        //update objOrd;
                    }
                }
            }
        }
    }
    
    public static void CreateRenewalOpportunity(List<ServiceContract> newList, Map<Id, ServiceContract> oldmap)
    {
        Set<String> oppIdSet = new Set<String>();
        Map<String, String> oppIdMap = new Map<String, String>();
        Set<String> quoteIdSet = new Set<String>();
        
        if(oldmap != null)
        {
            for(ServiceContract s : newList){
                if(s.EndDate < Date.today() && s.EndDate > date.today().addDays(-2) && s.EndDate != oldmap.get(s.Id).EndDate)
                {
                    system.debug('in :::::');
                    if(s.SBQQSC__Opportunity__c != null)
                    {
                        oppIdSet.add(s.SBQQSC__Opportunity__c);
                        oppIdMap.put(s.SBQQSC__Opportunity__c, s.Id);
                    }
                    if(s.SBQQSC__Quote__c != null)
                        quoteIdSet.add(s.SBQQSC__Quote__c);
                }
            }
        }
        
        Map<String, Opportunity> oppInsertMap = new Map<String, Opportunity>();
        Map<String, SBQQ__Quote__c> quoteInsertMap = new Map<String, SBQQ__Quote__c>();
        Map<String, List<SBQQ__QuoteLine__c>> quoteLinesInsertMap = new Map<String, List<SBQQ__QuoteLine__c>>();
        
        system.debug('oppIdSet :::::' + oppIdSet);
        system.debug('quoteIdSet :::::' + quoteIdSet);
        
        if(oppIdSet.size() > 0)
        {
            List<Opportunity> oppList = [select id, AccountId, Type, Name, Probability, CLO_Opportunity_Score__c, RecordTypeId, SBQQ__Contracted__c,
                                         CloseDate, StageName, Amount, NextStep, Pricebook2Id, CLO_Asset__c, OwnerId
                                         from Opportunity
                                         where id in : oppIdSet];
            
            for(Opportunity o : oppList)
            {
                Opportunity opp = o.clone(false, false);
                opp.Name = o.Name + ' - Renewal Opportunity';
                opp.SBQQ__Renewal__c = true;
                opp.StageName = 'Prospecting';
                opp.CloseDate = o.CloseDate.addDays(1);
                opp.SBQQSC__RenewedServiceContract__c = oppIdMap.get(o.Id);
                oppInsertMap.put(o.Id, opp);
            }
        }
        
        if(quoteIdSet.size() > 0)
        {
            List<SBQQ__Quote__c> quoteList = [select id, SBQQ__Account__c, SBQQ__Primary__c, Name, CLO_Quote_Name__c, 
                                              SBQQ__Status__c, CLO_Asset__c, SBQQ__Opportunity2__c,
                                              SBQQ__Type__c, SBQQ__StartDate__c, SBQQ__EndDate__c, SBQQ__BillingFrequency__c, 
                                              SBQQ__SalesRep__c, RecordTypeId,
                                              (select id, SBQQ__Product__c, SBQQ__Quote__c, SBQQ__Quantity__c, CLO_Asset__c, 
                                               SBQQ__PricingMethod__c, SBQQ__Description__c, SBQQ__ListPrice__c,
                                               SBQQ__CustomerPrice__c, SBQQ__NetPrice__c, SBQQ__SpecialPrice__c,
                                               SBQQ__RegularPrice__c, SBQQ__ProratedListPrice__c, SBQQ__SubscriptionPricing__c,
                                               SBQQ__SubscriptionBase__c, SBQQ__SubscriptionScope__c,
                                               SBQQ__DefaultSubscriptionTerm__c
                                               from SBQQ__LineItems__r)
                                              from SBQQ__Quote__c
                                              where id in : quoteIdSet];
            
            Id serviceContractId = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Service Contract').getRecordTypeId();
            
            for(SBQQ__Quote__c q : quoteList)
            {
                SBQQ__Quote__c quote = q.clone(false, false);
                quote.SBQQ__Status__c = 'Draft';
                quote.RecordTypeId = serviceContractId;
                
                if(quote.SBQQ__StartDate__c != null)
                    quote.SBQQ__StartDate__c.addYears(1).addDays(1);
                    
                if(quote.SBQQ__EndDate__c!= null)
                    quote.SBQQ__EndDate__c.addYears(1).addDays(1);
                
                quoteInsertMap.put(q.Id, quote);
                
                List<SBQQ__QuoteLine__c> qlList = new List<SBQQ__QuoteLine__c>();
                
                for(SBQQ__QuoteLine__c ql : q.SBQQ__LineItems__r)
                {
                    SBQQ__QuoteLine__c quoteLine = ql.clone(false, false);
                    
                    qlList.add(quoteLine);
                }
                
                quoteLinesInsertMap.put(q.Id, qlList);
            }
        }
        
        if(oppInsertMap.size() > 0)
        {
            insert oppInsertMap.values();
            
            List<ServiceContract> scList = new List<ServiceContract>();
            
            for(Opportunity o : oppInsertMap.values())
            {
                if(o.SBQQSC__RenewedServiceContract__c != null)
                    scList.add(new ServiceContract(Id = o.SBQQSC__RenewedServiceContract__c, SBQQSC__RenewalOpportunity__c = o.Id,
                                                   SBQQSC__RenewalOpportunityRecordTypeId__c = o.RecordTypeId,
                                                   SBQQSC__RenewalOpportunityStage__c = o.StageName,
                                                   SBQQSC__RenewalOwner__c = o.OwnerId,
                                                   SBQQSC__RenewalPricebookId__c = o.PriceBook2Id));
            }
            
            if(scList.size() > 0)
                update scList;
            
            system.debug('oppInsertMap :::::' + oppInsertMap.values()[0].Id);
            
            List<SBQQ__Quote__c> quoteInsertList = new List<SBQQ__Quote__c>();
            List<SBQQ__QuoteLine__c> quoteLineInsertList = new List<SBQQ__QuoteLine__c>();
            for(String key : quoteInsertMap.keyset())
            {
                SBQQ__Quote__c quote = quoteInsertMap.get(key);
                quote.SBQQ__Opportunity2__c = oppInsertMap.get(quote.SBQQ__Opportunity2__c).Id;
                
                quoteInsertList.add(quote);
            }
            
            if(quoteInsertList.size() > 0)
            {
                insert quoteInsertList;
                
                for(String key : quoteLinesInsertMap.keyset())
                {
                    List<SBQQ__QuoteLine__c> quoteLines = quoteLinesInsertMap.get(key);
                    if(quoteLines != null && quoteLines.size() > 0)
                    {
                        for(SBQQ__QuoteLine__c ql : quoteLines)
                        {
                            ql.SBQQ__Quote__c = quoteInsertMap.get(ql.SBQQ__Quote__c).Id;
                            
                            quoteLineInsertList.add(ql);    
                        }
                    }
                }
                
                if(quoteLineInsertList.size() > 0)
                    insert quoteLineInsertList;
            }
        }
    }
    
    public static void updServContractPartsAmt(){
        if(!serviceContractIds.isEmpty()){
            List<Opportunity> oppList = [SELECT Id, CLO_Contract_Amount__c, (SELECT Id,TotalPrice,SBQQSC__Opportunity__c FROM SBQQSC__ServiceContracts__r) 
                                         FROM Opportunity WHERE Id IN : serviceContractIds];
            if(!oppList.isEmpty()){
                List<Opportunity> updateOppList = new List<Opportunity>();
                Map<Id,Double> mapOppTotAmt = new Map<Id,Double>();
                Double totAmt = 0;
                for(Opportunity opp : oppList){
                    for(ServiceContract servContrct : opp.SBQQSC__ServiceContracts__r){
                        totAmt += servContrct.TotalPrice;
                        mapOppTotAmt.put(servContrct.SBQQSC__Opportunity__c,totAmt);                    
                    }
                    Opportunity objOpp = new Opportunity(Id=opp.Id,CLO_Contract_Amount__c = mapOppTotAmt.get(opp.Id));
                    updateOppList.add(objOpp);
                }
                
                
                if(!updateOppList.isEmpty()){
                    try{
                        CLO_DBManager.updateSObjects(updateOppList);                       
                    } Catch(DMLException de){
                        System.debug('@@@ updateOppList DML :'+de.getMessage());
                    } Catch(Exception e){
                        System.debug('@@@ updateOppList EX :'+e.getMessage());
                    }
                    
                }
            }
        }
    }
    
}
/**
  * @ClassName      : CLO_DBManagerTest 
  * @Description    : This class is used to test CLO_DBManager.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */
@isTest  
public class CLO_DBManagerTest {
    @testSetup static void setup() {
        // Create test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i));
        }
        CLO_DBManager.insertSObjects(testAccts); 
        
    }
    
    @isTest static void testGetSObjects() {
        Account acc = new Account();
        acc =  (Account) CLO_DBManager.getSObject('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct1') +' LIMIT 1');
        //System.assertEquals(1, testAccts.Size());

        // Get all account's by using a SOQL query
        List<Account> testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(2, testAccts.Size(),'success');

        testAccts = (List<Account>) CLO_DBManager.getSObjectList('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct1') +' LIMIT 1');
        System.assertEquals(1, testAccts.Size(),'success');
    }

    @isTest static void testDMLOperations() {
        // Get TestAcct0 account by using a SOQL query
        List<Account> testAccts = (List<Account>) CLO_DBManager.getSObjectList('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct0') +' LIMIT 1');
        
        // Modify first account
        testAccts[0].Phone = '555-1212';

        // This update is local to this test method only.
        CLO_DBManager.updateSObjects(testAccts); 

        // Get all account's by using a SOQL query
        testAccts = (List<Account>) CLO_DBManager.getSObjectList('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct0') +' LIMIT 1');
        System.assertEquals(1, testAccts.Size(),'success');

        // This delete is local to this test method only.
        CLO_DBManager.deleteSObjects(testAccts);
        CLO_DBManager.undeleteSObjects(testAccts);
        
        // Get all account's by using a SOQL query
        testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        testAccts[0].Phone = '555-1213';
        CLO_DBManager.upsertSObjects(testAccts);
        System.assertEquals(2, testAccts.Size(),'success');
    }

    @isTest static void testEmptyRecycleBin() {
        // Get all account's by using a SOQL query
        List<Account> testAccts = (List<Account>) CLO_DBManager.getSObjectList('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct0') +' LIMIT 1');
        
        CLO_DBManager.deleteSObjects(testAccts);
        CLO_DBManager.emptyRecycleBin(testAccts[0]);
        System.assertEquals(3, Limits.getDmlStatements(),'success');

        // Get all account's by using a SOQL query
        testAccts = (List<Account>) CLO_DBManager.getSObjectList('Id ', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct1') +' LIMIT 1');

        CLO_DBManager.deleteSObjects(testAccts);
        CLO_DBManager.emptyRecycleBin(testAccts);
        System.assertEquals(5, Limits.getDmlStatements(),'success');
    }
}
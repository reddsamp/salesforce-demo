public with sharing class CLO_SmartSheetController {
    
    @AuraEnabled
    public static String saveLeadSheet(String name){
        Lead_Sheet__c leadSheet = new Lead_Sheet__c(Name = name, Columns__c = 'FirstName,LastName,Email,Phone');
        
        try{
            insert leadSheet;
            return leadSheet.Id;
        }
        catch(Exception ex){
            return 'Error: ' + ex.getMessage();
        }
    }
    
    
    @AuraEnabled
    public static String shareLeadSheet(String lsId, String userId, String accessLevel){
        List<Lead_Sheet__Share> lsList = [select id from Lead_Sheet__Share where ParentID =: lsId and UserOrGroupId =: userId];
        
        if(lsList.size() == 0)
        {
            Lead_Sheet__Share leadSheetShare = new Lead_Sheet__Share(ParentID = lsId, AccessLevel = accessLevel, UserOrGroupId = userId);
            
            try{
                insert leadSheetShare;
                return leadSheetShare.Id;
            }
            catch(Exception ex){
                return 'Error: ' + ex.getMessage();
            }
        }
        else
            return 'Error: Access already defined for selected user.';
    }
    
    
    @AuraEnabled
    public static String deleteShareRecord(String aId){
        try{
            delete [select id from Lead_Sheet__Share where Id =: aId];
            
            return 'Access removed.';
        }
        catch(Exception ex){
            return 'Error: ' + ex.getMessage();
        }
    }
    
    
    @AuraEnabled
    public static List<Lead_Team__c> qryLeadTeams(String lsId){
        List<Lead_Team__c> leadTeamList = [select id, Name, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Sequence__c, User__c
                                           from Lead_Team__c
                                           where Lead_Sheet__c =: lsId];
                                       
        if(leadTeamList.size() > 0)
        {
        
        }
        else{
            leadTeamList = new List<Lead_Team__c>();
            leadTeamList.add(new Lead_Team__c(Lead_Sheet__c = lsId));
        }
        
        return leadTeamList;
    }
    
    
    @AuraEnabled
    public static List<Lead_Team__c> addNewRowCall(String lsId, List<Lead_Team__c> leadTeams){
        if(leadTeams != null)
        {
            leadTeams.add(new Lead_Team__c(Lead_Sheet__c = lsId));
        }
        else{
            leadTeams = new List<Lead_Team__c>();
            leadTeams.add(new Lead_Team__c(Lead_Sheet__c = lsId));
        }
        
        return leadTeams;
    }
    
    
    @AuraEnabled
    public static List<Lead_Team__c> deleteRowCall(String indx, List<Lead_Team__c> leadTeams){
        integer inx = integer.valueOf(indx);
        List<Lead_Team__c> newlst = new List<Lead_Team__c>();
        
        for(Integer i=0; i < leadTeams.size(); i++)
        {
            if(i == inx){
                if(leadTeams[i].Id != null) delete leadTeams[i];
            }
            else{
                newlst.add(leadTeams[i]);
            }
        }
        
        return newlst;
    }
    
    
    @AuraEnabled
    public static List<AccessWrapper> getShareList(String lsId){
        List<AccessWrapper> accessList = new List<AccessWrapper>();
        
        for(Lead_Sheet__Share s : [select id, AccessLevel, UserOrGroupId, UserOrGroup.Name 
                                   from Lead_Sheet__Share 
                                   where ParentId =: lsId])
        {
            accessList.add(new AccessWrapper(s.Id,
                                             s.AccessLevel,
                                             s.UserOrGroupId,
                                             s.UserOrGroup.Name,
                                             s.AccessLevel == 'All' ? null : s.AccessLevel));
        }
        
        return accessList;
    }
    
    public class AccessWrapper
    {
        @AuraEnabled public String accessId;
        @AuraEnabled public String accessLevel;
        @AuraEnabled public String showDelete;
        @AuraEnabled public String userId;
        @AuraEnabled public String userName;
        
        public AccessWrapper(String accessId,
                             String accessLevel,
                             String userId,
                             String userName,
                             String showDelete)
        {
            this.accessId = accessId;
            this.accessLevel = accessLevel;
            this.userId = userId;
            this.userName = userName;
            this.showDelete = showDelete;
        }
    }
    
    @AuraEnabled
    public static list<Lead_Sheet__c> readLeadSheets(){
        return [select id, Name, Columns__c from Lead_Sheet__c];
    }
    
    @AuraEnabled
    public static Lead_Sheet__c readLeadSheetData(String id){
        return [select id, Name, Columns__c from Lead_Sheet__c where id =: id];
    }
    
    
    @AuraEnabled
    public static List<Lead> readLeadData(String id){
        List<Lead_Sheet__Share> lsList = [select id, AccessLevel from Lead_Sheet__Share 
                                          where ParentID =: id and UserOrGroupId =: UserInfo.getUserId()];
        
        if(lsList.size() > 0 && lsList[0].AccessLevel == 'Read')
        {
            return [select id, Name, FirstName, LastName, Phone, Email, Column_Color__c 
                    from Lead where Lead_Sheet__c =: id 
                    and OwnerId =: UserInfo.getUserId()
                    and IsConverted = false order by FirstName, LastName];
        }
        
        return [select id, Name, FirstName, LastName, Phone, Email, Column_Color__c 
                from Lead where Lead_Sheet__c =: id 
                and IsConverted = false order by FirstName, LastName];
    }
    
    
    @AuraEnabled
    public static list<ColumnMappingWrapper> readLeadFields(){
        SObjectType LeadDes = Schema.getGlobalDescribe().get('lead');
        Map<String,Schema.SObjectField> leadFieldMap = LeadDes.getDescribe().fields.getMap();
        List<ColumnMappingWrapper> leadFieldString = new List<ColumnMappingWrapper>();
        
        for (String fieldName: leadFieldMap.keySet()) {
            leadFieldString.add(new ColumnMappingWrapper(leadFieldMap.get(fieldName).getDescribe().getLabel(), fieldName.toUpperCase()));
        }
        
        return leadFieldString; 
    }
    
    
    public class ColumnMappingWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public ColumnMappingWrapper(String label, String value)
        {
            this.label = label;
            this.value = value;
        }
    }
    
    @AuraEnabled
    public static list<ColumnMappingWrapper> readTaskStatus(){
        String objectName = 'Task';
        String fieldName ='Status__c';
          
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        List<ColumnMappingWrapper> leadFieldString = new List<ColumnMappingWrapper>();
        
        for( Schema.PicklistEntry pickListVal : ple){
            leadFieldString.add(new ColumnMappingWrapper(pickListVal.getLabel(), pickListVal.getValue()));
        }
        
        return leadFieldString; 
    }
    
    @AuraEnabled
    public static list<ColumnMappingWrapper> readTaskStatusReason(String status){
        List<ColumnMappingWrapper> leadFieldString = new List<ColumnMappingWrapper>();
        
        if(status == 'Call back')
        {
            leadFieldString.add(new ColumnMappingWrapper('No Answer', 'No Answer'));
            leadFieldString.add(new ColumnMappingWrapper('Got denied see notes', 'Got denied see notes'));
            leadFieldString.add(new ColumnMappingWrapper('OM not available see notes', 'OM not available see notes'));
        }
        else if(status == 'Appointment Made')
        {
            leadFieldString.add(new ColumnMappingWrapper('Level A Meeting', 'Level A Meeting'));
            leadFieldString.add(new ColumnMappingWrapper('Level B Meeting', 'Level B Meeting'));
            leadFieldString.add(new ColumnMappingWrapper('Level C Meeting', 'Level C Meeting'));
            leadFieldString.add(new ColumnMappingWrapper('Exploratory Meeting', 'Exploratory Meeting'));
            leadFieldString.add(new ColumnMappingWrapper('Demo', 'Demo'));
            leadFieldString.add(new ColumnMappingWrapper('Closing', 'Closing'));
        }
        else if(status == 'Not Ready/Interested')
        {
            leadFieldString.add(new ColumnMappingWrapper('Drop-Punt', 'Drop-Punt'));
            leadFieldString.add(new ColumnMappingWrapper('Competitive Deal', 'Competitive Deal'));
            leadFieldString.add(new ColumnMappingWrapper('Credit/Financing', 'Credit/Financing'));
            leadFieldString.add(new ColumnMappingWrapper('Product Efficacy', 'Product Efficacy'));
            leadFieldString.add(new ColumnMappingWrapper('Profitability', 'Profitability'));
            leadFieldString.add(new ColumnMappingWrapper('Timing', 'Timing'));
            leadFieldString.add(new ColumnMappingWrapper('Other', 'Other'));
        }
        
        return leadFieldString; 
    }
    
    @AuraEnabled
    public static String createTaskApex(String leadId, String status, String reason, String comment){
        try{
            Task task = new Task(WhoId = leadId, Subject = 'Call', Status__c = status, Status_Reason__c = reason, Description = comment);
            
            if(status == 'Open' || status == 'Valid')
            {
                update new Lead(Id = leadId, Column_Color__c = 'green');
            }
            else if(status == 'Call back'){
                update new Lead(Id = leadId, Column_Color__c = 'yellow');
            }
            else if(status == 'Appointment Made'){
                update new Lead(Id = leadId, Column_Color__c = 'blue');
            }
            else if(status == 'Not Ready/Interested'){
                update new Lead(Id = leadId, Column_Color__c = 'red');
            }
        
            insert task;
            return task.Id;
        }
        catch(Exception ex){
            return 'Error: ' + ex.getMessage();
        }
    }
    
    
    @AuraEnabled
    public static List<Task> getAllTasks(String leadId){
        List<Task> tasks = [select Id, WhoId, Subject, Status__c, Status_Reason__c, Description, CreatedDate from Task where WhoId =: leadId order by CreatedDate desc];
        
        for(Task t : tasks)
        {
            if(t.Status__c == 'Open' || t.Status__c == 'Valid')
            {
                t.Subject = 'greentxt';
            }
            else if(t.Status__c == 'Call back'){
                t.Subject = 'yellowtxt';
            }
            else if(t.Status__c == 'Appointment Made'){
                t.Subject = 'bluetxt';
            }
            else if(t.Status__c == 'Not Ready/Interested'){
                t.Subject = 'redtxt';
            }
        }
        
        return tasks;
    }
    
    @AuraEnabled
    public static void sendEmail(String userId)
    {
        User user = [select id, Name, Email from User where id=: userId];
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
  
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add(user.Email);
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        mail.setReplyTo('clo_useravailiabilityemailservice@ojrf3rfeaslea057ifl79as4dp5ylp9bvlcj9w4ffusbn0otf.6s-8l5neaa.cs165.apex.sandbox.salesforce.com');
        mail.setSenderDisplayName('Inside Sales');
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject('Week Availiability.');
        String body = 'Dear ' + user.Name + ', <br/><br/>';
        body += 'Please let me now your availability for next week.<br/>';
        body += 'Monday: <br/>';
        body += 'Tuesday: <br/>';
        body += 'Wednesday: <br/>';
        body += 'Thursday: <br/>';
        body += 'Friday: <br/><br/>';
        body += 'Regards,<br/>';
        body += UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        mail.setHtmlBody(body);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }
    
    @AuraEnabled
    public static String  distributeLeads(String lsId, List<Lead_Team__c> szt,
                                          String startcalldate, String startoptimaldate,
                                          String lookforwarddate, String daystodistribute)
    {
        //Variables Declared
        Decimal mon = 0;
        Decimal tue = 0;
        Decimal wed = 0;
        Decimal thur = 0;
        Decimal fri = 0;
        PageReference pg;
        boolean allocated = null;
        
        Map<String, Decimal> monMap = new Map<String, Decimal> ();
        Map<String, Decimal> tueMap = new Map<String, Decimal> ();
        Map<String, Decimal> wedMap = new Map<String, Decimal> ();
        Map<String, Decimal> thurMap = new Map<String, Decimal> ();
        Map<String, Decimal> friMap = new Map<String, Decimal> ();
        
        Lead_Sheet__c sz = [select id, Days_to_Distribute__c, Start_Call_Date__c, Start_Optimal_Date__c, Look_Forward_Date__c
                            from Lead_Sheet__c where id =: lsId];
        List<Lead> swpList = [select id from Lead where Lead_Sheet__c =: lsId];
        
        system.debug('sz :::::' + sz);
        
        Decimal dailyRecordCount;
        
        boolean batchStatusBool = true;

        //Validations
        if (szt.size() == 0) {
            return 'Please add a team member to the zone for allocation.';
        }
        if (swpList == null) {
            return 'Please add waypoint records to the zone for allocation.';
        }
        if (szt == null) {
            return 'Please update team member percent allocations.';
        }
        if (startcalldate == null) {
            return 'Please select a start date to begin calling.';
        }
        if (startoptimaldate == null) {
            return 'Please select an appropriate optimal start date to establish meetings.';
        }
        if (lookforwarddate == null) {
            return 'Please select an appropriate look forward date.';
        }
        
        sz.Start_Call_Date__c = Date.valueOf(startcalldate);
        sz.Start_Optimal_Date__c = Date.valueOf(startoptimaldate);
        sz.Look_Forward_Date__c = Date.valueOf(lookforwarddate);
        sz.Days_to_Distribute__c = 5; //Integer.valueOf(daystodistribute);
        
        if (swpList != null && sz.Days_to_Distribute__c != 0)
            dailyRecordCount = swpList.size() / Integer.valueOf(sz.Days_to_Distribute__c);
        
        if (sz.Days_to_Distribute__c == 0 || Test.isRunningTest()) {
            Integer mont = 0;
            Integer tuet = 0;
            Integer wedt = 0;
            Integer thurt = 0;
            Integer frit = 0;

            for (Lead_Team__c stz : szt) {
                if (stz.Monday__c > 0)
                mont = 1;
                if (stz.Tuesday__c > 0)
                tuet = 1;
                if (stz.Wednesday__c > 0)
                wedt = 1;
                if (stz.Thursday__c > 0)
                thurt = 1;
                if (stz.Friday__c > 0)
                frit = 1;
            }
            dailyRecordCount = swpList.size() / (mont + tuet + wedt + thurt + frit);

        }

        //Sort Swarm Zone Team in ascending order based on sequence field value
        for (Integer i = 0; i<szt.size(); i++) {
            for (integer j = i + 1; j <= szt.size() - 1; j++) {
                Lead_Team__c x;
                if (szt[i].Sequence__c> szt[j].Sequence__c) {
                    x = szt[i];
                    szt[i] = szt[j];
                    szt[j] = x;
                    system.debug('szt:' + szt);
                }
            }
        }

        for (Lead_Team__c t : szt) {
            if (t.Monday__c != null) {
                monMap.put(t.User__c + '-mon', ((t.Monday__c / 100) * dailyRecordCount).round(System.RoundingMode.UP));
                mon += t.Monday__c;
                System.debug('mon:' + mon);
            }
            if (t.Tuesday__c != null) {
                tueMap.put(t.User__c + '-tue', ((t.Tuesday__c / 100) * dailyRecordCount).round(System.RoundingMode.UP));
                tue += t.Tuesday__c;
                System.debug('tue:' + tue);
            }
            if (t.Wednesday__c != null) {
                wedMap.put(t.User__c + '-wed', ((t.Wednesday__c / 100) * dailyRecordCount).round(System.RoundingMode.UP));
                wed += t.Wednesday__c;
                System.debug('wed:' + wed);
            }
            if (t.Thursday__c != null) {
                thurMap.put(t.User__c + '-thur', ((t.Thursday__c / 100) * dailyRecordCount).round(System.RoundingMode.UP));
                thur += t.Thursday__c;
                System.debug('thur:' + thur);
            }
            if (t.Friday__c != null) {
                friMap.put(t.User__c + '-fri', ((t.Friday__c / 100) * dailyRecordCount).round(System.RoundingMode.UP));
                fri += t.Friday__c;
                System.debug('fri:' + fri);
            }
        }

        if ((mon != 100 && mon != 0) || (tue != 100 && tue != 0) || (wed != 100 && wed != 0) || (thur != 100 && thur != 0) || (fri != 100 && fri != 0)) {
            return 'Please update the percent allocation. The sum of each day should be equal to 100%.';
        }
        
        upsert szt;
        
        if (swpList != null) {
            CLO_LeadAllocationBatch wpBatch = new CLO_LeadAllocationBatch(sz, szt, monMap, tueMap, wedMap, thurMap, friMap);
            String batchId = Database.executeBatch(wpBatch,200);
            
            return 'Batch Id:' + batchId;
        }
        
        return 'Error';
    }
    
    
    @AuraEnabled
    public static String checkBatchStatus(String batchId) {
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :batchId];
        String batchStatus = job.Status;
        if(batchStatus == 'Completed') {
            return 'Completed';
        } 
        else{
            return 'Running';
        }
    }
    
    @AuraEnabled
    public static String addCalenderUser(String lsId, String userId)
    {
        insert new Lead_Sheet_Calendar__c(Lead_Sheet__c = lsId, User__c = userId);
    
        
        return 'Success';
    }
}
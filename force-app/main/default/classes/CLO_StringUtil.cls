/**
  * @ClassName      : CLO_StringUtil 
  * @Description    : This class is defined the string methods used in Cynosure.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 23th June 2020
  */
  public with sharing class CLO_StringUtil {
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : It will get empty string value.
      * @Parameters  : None
      * @Return      : String 
      */
      public static String emptyString(){
          return '';
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : It will get inquoted string value.
      * @Parameters  : String strParam
      * @Return      : String 
      */
      public static String inquoteString(String strParam){
          return '\'' + string.escapeSingleQuotes(strParam) + '\'';
      } 
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : It will returns in clause format string value in quotes.
      * @Parameters  : List<String> lstIds
      * @Return      : String 
      */
      public static String getInClauseString(List<String> lstIds) {
          String resultedString = null;
          
          if(lstIds == null) {
              return null;
          }
          resultedString = '(';
          for(Integer i = 0; i < lstIds.size()-1; i++) {
              resultedString = resultedString + inquoteString(lstIds[i]) + ',';
          }
          resultedString = resultedString + inquoteString(lstIds[lstIds.size() - 1]) + ')';
                  
          return resultedString;
      }
      
      /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : It will returns in clause format string value in quotes.
      * @Parameters  : List<id> lstIds
      * @Return      : String 
      */
      public static String getInClauseId(List<Id> lstIds) {
          String resultedString = null;
          
          if(lstIds == null) {
              return null;
          }
          resultedString = '(';
          for(Integer i = 0; i < lstIds.size()-1; i++) {
              resultedString = resultedString + inquoteString(lstIds[i]) + ',';
          }
          resultedString = resultedString + inquoteString(lstIds[lstIds.size() - 1]) + ')';
                  
          return resultedString;
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : Method to validate the given email address
      * @Parameters  : String strEmailAddress
      * @Return      : Boolean 
      */
      public static Boolean isEmailAddressValid(String strEmailAddress) 
      {
          String strEmailRegex = 
          '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
  
          if(String.isBlank(strEmailAddress))
          {
              return false;
          }
          
          return Pattern.matches(strEmailRegex, strEmailAddress);
      }
  }
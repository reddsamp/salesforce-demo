public class CLO_LeadTriggerHandler{
    
    public static void syncAccountWithSAP(List<Lead> newList)
    {
        for(Lead l : newList)
        {
            if(l.IsConverted)
                CLO_SAP_ServiceInvoker.invokeCreateAccountService(JSON.serialize(l));
        }
    }
}
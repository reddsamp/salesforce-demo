Public Class CLO_SAP_AuthenticationHelper
{   
   public static String getAccessToken(){
     HttpRequest req = new HttpRequest();
     req.setEndpoint('callout:Connection_to_SAP/oauth2/api/v1/token?grant_type=client_credentials');
     req.setMethod('POST');
     
     /*String username = 'devsfintegration';
     String password = 'Summer2020$';
      
     Blob headerValue = Blob.valueOf(username + ':' + password);
     String authorizationHeader = 'Basic ' +
     EncodingUtil.base64Encode(headerValue);
     req.setHeader('Authorization', authorizationHeader);
       */ 
     Http http = new Http();
     HTTPResponse res = http.send(req);
     
     Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
    
     return String.valueOf(responseMap.get('access_token'));
   }

}
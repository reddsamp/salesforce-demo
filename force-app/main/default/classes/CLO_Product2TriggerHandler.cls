/**
  * @ClassName      : CLO_Product2TriggerHandler 
  * @Description    : Product2 Object Trigger Handler.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th Sep 2020
  */

  public class CLO_Product2TriggerHandler implements CLO_ITriggerHandler{
     
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
    public void beforeInsert(List<sObject> newList) {
    }
     
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
        if(!UserInfo.isMultiCurrencyOrganization()){
            return;
        }
        
        List<CurrencyType> lstCurrencyType = CLO_DBManager.getSObjectList('IsoCode', 'CurrencyType', 'IsActive = TRUE' );//[SELECT IsoCode From CurrencyType WHERE IsActive = TRUE];
        Pricebook2 pb = (Pricebook2)CLO_DBManager.getSObject('ID', 'Pricebook2', 'IsStandard = TRUE AND IsActive = TRUE' );//[SELECT ID from Pricebook2 WHERE IsStandard = TRUE AND IsActive = TRUE];
    
        List<PricebookEntry> pbeList = new List<PricebookEntry>();
    
        for (Product2 p : (List<Product2>) newList) {
            for(CurrencyType currType : lstCurrencyType){
                pbeList.add(new PricebookEntry(Pricebook2Id = pb.ID, Product2Id=p.ID, UnitPrice = 0, CurrencyIsoCode = currType.IsoCode, IsActive = TRUE, UseStandardPrice = FALSE));
            }
            
        }
        If(pbeList.size() > 0){
            try{
                CLO_DBManager.insertSObjects(pbeList); 
            }   Catch(DMLException de) {
            }   Catch(Exception ex) {
            }  
        } 
    }
     
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
	@future(callout=true)
     public static void updateProductBundle()
     {
         List<Object> pdflist= new List<Object>();
          System.debug('Apex enter');
         List<Product2> lstAlpha = [SELECT Id FROM Product2 WHERE SBQQ__ConfigurationType__c = 'required' Limit 25];
         PageReference reportPage  = Page.CLO_Product_Template;
          Zippex zip = new Zippex();
        for ( Product2 mem : lstAlpha ){ 
            //System.debug(mem);
            String i = String.valueOf(mem.Id);
            //System.debug('Id value');
            //System.debug(i);
        	reportPage.getParameters().put('id', i);
        // System.debug(reportPage);
        Blob reportPdf;
          
        String pdfName;
        try {
            reportPdf = reportPage.getContent();
            Product2 record = [SELECT Id,Description,Name,ProductCode FROM Product2 WHERE Id = :i];
            pdfName = record.Name;
        }
        catch (Exception e) {
            reportPdf = Blob.valueOf(e.getMessage());
        }
            Map<String, String> atMap = new Map<String, String>();
      //atMap.put( 'Id', at.Id );
      atMap.put( 'Name', pdfName );
      atMap.put( 'Body', EncodingUtil.base64Encode( reportPdf ) );
          // system.debug(reportPdf);
            //system.debug(pdfName);
            //system.debug('pdf values');
            //if (pdfName != null && reportPdf != null ){
                zip.addFile(pdfName + '.xls', reportPdf, null);
                  //              }
      //atMap.put( 'ContentType', at.ContentType );
     
        pdflist.add(atMap);
            }
         Blob zipBlob = zip.getZipArchive();
         String Content =  EncodingUtil.base64Encode( zipBlob );
         try{
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = 'Bundle.zip'; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = 'Product Bundles zip'+String.valueof(DateTime.now().getTime()); // Display name of the files
        conVer.VersionData = EncodingUtil.base64Decode(Content); // converting your binary string to Blog
        // conVer.CurrencyIsoCode = 'USD';
        insert conVer;    //Insert ContentVersion
        System.debug(conVer);
        CLO_Product_Bundle_Setting__c myCS =CLO_Product_Bundle_Setting__c.getOrgDefaults();
              System.debug(myCS.CLO_LinkentityId__c);
        
        // First get the Content Document Id from ContentVersion Object
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        List<ContentDocumentLink> documentlink = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId =:conDoc AND LinkedEntityId =:Userinfo.getUserId()];
        if (documentlink.size() == 0){
         System.debug(conDoc);
        //create ContentDocumentLink  record 
        ContentDocumentLink conDocLink = New ContentDocumentLink();
        conDocLink.LinkedEntityId = Userinfo.getUserId(); // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
        conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
        conDocLink.shareType = 'I';
         conDocLink.Visibility = 'AllUsers';
        insert conDocLink;
            
        System.debug(conDocLink);
             }
           myCS.CLO_Product_Bundle_Id__c = conVer.Id;
        update myCS;
         }catch(Exception e){
             System.debug(e);
         }
     }
    
    
    @AuraEnabled
public static String getCompanySetting(){
   CLO_Product_Bundle_Setting__c myCS =CLO_Product_Bundle_Setting__c.getOrgDefaults();
              System.debug(myCS.CLO_Product_Bundle_Id__c);
    return myCS.CLO_Product_Bundle_Id__c;
}

}
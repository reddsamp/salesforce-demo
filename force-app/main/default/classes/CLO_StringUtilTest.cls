/**
  * @ClassName      : CLO_StringUtilTest 
  * @Description    : This class is used to test CLO_StringUtil.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */
@isTest 
public class CLO_StringUtilTest {
    @testSetup static void setup() {
        
        // Create test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i));
        }
        CLO_DBManager.insertSObjects(testAccts); 
        
    }
    
    @isTest static void testGetInClauseString() {

        List<Id> lstAccIds = new List<Id>();
        // Get all account's by using a SOQL query
        List<Account> testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(2, testAccts.Size(),'success');

        for(Account acc:testAccts){
            lstAccIds.add(acc.Id);
        }   

        CLO_StringUtil.getInClauseString(lstAccIds);
        System.assertEquals(2, lstAccIds.Size(),'success');
        
        CLO_StringUtil.getInClauseId(lstAccIds);
        System.assertEquals(2, lstAccIds.Size(),'success');
    }

    @isTest static void testIsEmailAddressValid() {
        string strValidEmail = 'test@techm.com';
        string strNotValidEmail = '';

        //This will success if the emptystring() returns true
        System.assertEquals('',CLO_StringUtil.emptyString(),'success');
                
        //This will success if the isEmailAddressValid() returns true
        System.assertEquals(true,CLO_StringUtil.isEmailAddressValid(strValidEmail),'success');

        //This will success if the isEmailAddressValid() returns false
        System.assertEquals(false,CLO_StringUtil.isEmailAddressValid(strNotValidEmail),'success');

    }
}
/**
* @ClassName      : CLO_ProductRequiredTriggerHandler 
* @Description    : Handler class for ProductRequired Trigger 
* @Author         : Markandeyulu K
* @CreatedDate    : 21st Jan 2021
*/


public class CLO_ProductRequiredTriggerHandler implements CLO_ITriggerHandler{
    
    public static List<Task> lstTask;
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
    
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle beforeInsert business logic for ProductRequired Trigger
* @Parameters  : List<sObject> newList 
* @Return      : void 
*/ 
    public void beforeInsert(List<sObject> newList) {
        Set<Id> setWo = new Set<Id>();
        Map<String,Boolean> mapWoNonBil = new Map<String,Boolean>();
        for(ProductRequired pr : (List<ProductRequired>) newList){
            setWo.add(pr.ParentRecordId);
        }
        
        if(setWo.size() > 0){
            for(WorkOrder wo : [Select Id,CLO_Non_Billable__c From WorkOrder Where Id IN : setWo]){
                mapWoNonBil.put(wo.Id,wo.CLO_Non_Billable__c);
            }
        }        
        if(mapWoNonBil.keySet().size() > 0){
            for(ProductRequired pr : (List<ProductRequired>) newList){
                if(mapWoNonBil.containsKey(pr.ParentRecordId)){
                    pr.CLO_Non_Billable__c = mapWoNonBil.get(pr.ParentRecordId);
                }
            }
        }
    
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle afterInsert business logic for ProductRequired Trigger
* @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
* @Return      : void 
*/
    
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
         
        List<Id> lstAstIds = new List<Id>();
        List<ProductRequired> lstProdReqsts = new List<ProductRequired>();
        Map<String,String> mapPrWoIds = new Map<String,String>();
        Map<String,String> mapWoAstId = new Map<String,String>();
           
        for(ProductRequired pr : (List<ProductRequired>) newList){
            mapPrWoIds.put(pr.id,pr.ParentRecordId);    
        }
        
        for(WorkOrder wo : [Select Id,AssetId From WorkOrder Where Id IN : mapPrWoIds.values() AND AssetId != Null]){
            mapWoAstId.put(wo.id,wo.AssetId);
        }
        
        If(mapWoAstId != Null && mapWoAstId.values().size() > 0){
            for(Entitlement entmnt : [Select Id,Name,AssetId,Status From Entitlement where Status =: CLO_Constants.ACTIVE AND AssetId IN : mapWoAstId.values()]){
                If(entmnt.Name.ContainsIgnoreCase(CLO_Constants.WARRANTY) || entmnt.Name.ContainsIgnoreCase(CLO_Constants.CONTRACT)){
                    If(entmnt.Name.ContainsIgnoreCase(CLO_Constants.PARTS) && !lstAstIds.contains(entmnt.AssetId)){
                        lstAstIds.Add(entmnt.AssetId);
                    }
                }
            }
        }
        
        for(ProductRequired prs : (List<ProductRequired>) newList){
        
            If(lstAstIds != Null && lstAstIds.size() > 0){
                If(mapWoAstId != Null && mapWoAstId.containsKey(prs.ParentRecordId) && lstAstIds.contains(mapWoAstId.get(prs.ParentRecordId))){
                    ProductRequired prsts = new ProductRequired(ID=prs.Id);
                    prsts.CLO_Discount__c = 100;
                    lstProdReqsts.add(prsts);
                }
            }  
        }
        
        If(lstProdReqsts.size() > 0){
             
             try{         
                    CLO_DBManager.updateSObjects(lstProdReqsts); 
                } Catch(DMLException de) {
                    System.debug('dmlExcep :: '+de.getMessage());
                } Catch(Exception ex) {
                    System.debug('Exception :' +ex.getMessage());
                }  
        }
        
       
    
    }
    
/**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle beforeUpdate business logic for ProductRequired Trigger
* @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
* @Return      : void 
*/
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
       
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle afterUpdate business logic for ProductRequired Trigger
* @Parameters  : List<sObject> newList , Map<Id, sObject> newMap,List<sObject> oldList, Map<Id, sObject> oldMap
* @Return      : void 
*/
    
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
        
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle beforeDelete business logic for ProductRequired Trigger
* @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
* @Return      : void 
*/ 
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle afterDelete business logic for ProductRequired Trigger
* @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
* @Return      : void 
*/ 
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 21st Jan 2021
* @Description : This method id used to handle afterUnDelete business logic for ProductRequired Trigger
* @Parameters  : List<sObject> newList, Map<Id, sObject> newMap
* @Return      : void 
*/ 
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
        
    }
    
}
public class CLO_AssetTriggerHandler2 {
    
    public static void CreateWorkOrder(List<Asset> Assets, Map<Id, Asset> oldmap)
    {
        List<Asset> AssetList = new List<Asset>();
        List<WorkOrderLineItem> workorderItemInsertList = new List<WorkOrderLineItem>();
        List<WorkOrder> workordersList = new List<WorkOrder>();
        
        for(Asset AI : Assets){
            if(AI.AccountId != null && AI.Product2Id != null)
            {
                AssetList.add(AI);     
            }
        }
        
        if(AssetList.size() > 0)
        {
            String installation = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CLO_Installation').getRecordTypeId();
            String training = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CLO_Training').getRecordTypeId();
            String preceptorship = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CLO_Preceptorship').getRecordTypeId();
            
            List<Workorder> workorderInsertList = new List<Workorder>();
            List<WorkOrderLineItem> woliInsertList = new List<WorkOrderLineItem>();
            
            for(Asset At : AssetList){
                if(At.CLO_Installation_Included__c){
                    workorderInsertList.add(new WorkOrder(AccountId = At.AccountId,
                                                          AssetId = At.Id,
                                                          Status = 'New',
                                                          RecordTypeId = installation,
                                                          StartDate = datetime.now()));
                    
                    woliInsertList.add(new WorkOrderLineItem(Status = 'New',
                                                             //Product1__c = At.Product2Id,
                                                             AssetId = At.Id));
                }
                
                if(At.CLO_Preceptorship__c){
                     workorderInsertList.add(new WorkOrder(AccountId = At.AccountId,
                                                           AssetId = At.Id,
                                                           Status = 'New',
                                                           RecordTypeId = preceptorship,
                                                           StartDate = datetime.now()));
                     
                     woliInsertList.add(new WorkOrderLineItem(Status = 'New',
                                                             //Product1__c = At.Product2Id,
                                                             AssetId = At.Id));
                }
                
                if(At.CLO_Clinical_Training_Included__c){
                    workorderInsertList.add(new WorkOrder(AccountId = At.AccountId,
                                                          AssetId = At.Id,
                                                          Status = 'New',
                                                          RecordTypeId = training,
                                                          StartDate = datetime.now()));
                    
                    woliInsertList.add(new WorkOrderLineItem(Status = 'New',
                                                             //Product1__c = At.Product2Id,
                                                             AssetId = At.Id));
                }
            }
            
            if(workorderInsertList.size() > 0)
            {
                insert workorderInsertList;
                
                Integer count = 0;
                for(WorkOrderLineItem woli : woliInsertList)
                {
                    woli.WorkorderId = workorderInsertList[count].Id;
                    count++;
                }
                
                insert woliInsertList;
            }
        }
    }  
}
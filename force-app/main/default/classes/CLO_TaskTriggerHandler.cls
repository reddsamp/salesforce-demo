/**
  * @ClassName      : TaskTriggerHandler 
  * @Description    : Handler class for Task trigger 
  * @Author         : Markandeyulu K
  * @CreatedDate    : 28th July 2020
  */

public class CLO_TaskTriggerHandler implements CLO_ITriggerHandler {
    public static List<Task> lstTasks;
    
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle beforeInsert business logic for Task Trigger
    * @Parameters  : List<sObject> newList 
    * @Return      : void 
    */  
    public void beforeInsert(List<sObject> newList) {
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle afterInsert business logic for Task Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
    * @Return      : void 
    */  
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle beforeUpdate business logic for Task Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap,List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */  
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
    
        

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle afterUpdate business logic for Task Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap,List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */  
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
    
        List<Task> lstTask = (List<Task>) newList;
        Map<id,task> mapOldTask = (Map<id,task>) oldMap;
        lstTasks = new List<Task>();
        for(Task tsk : lstTask){
            if(mapOldTask.get(tsk.id).status != tsk.status && tsk.status == CLO_Constants.STATUS_COMPLETED && tsk.CLO_Task_Subtype__c == CLO_Constants.NO_INTEREST && tsk.ActivityDate == tsk.CompletedDateTime.date()){
                Task objTsk = new Task(); 
                objTsk.Subject = Label.CLO_Task_Sub_Follow_Up;
                objTsk.WhatId = tsk.WhatId;
                objTsk.OwnerId = UserInfo.getUserId();
                objTsk.Status = CLO_Constants.STATUS_OPEN;
                objTsk.Type = CLO_Constants.CALL;
                objTsk.Description = Label.CLO_Task_Activity_Consumable_Sale;
                if(tsk.ActivityDate != Null){
                    objTsk.ActivityDate = tsk.ActivityDate.addDays(Integer.valueOf(Label.CLO_Consumable_Sale_Task_Days));
                }
                lstTasks.add(objTsk);
            }       
        }
        If(lstTasks.size() > 0){
            try{
                CLO_DBManager.insertSObjects(lstTasks); 
            }   Catch(DMLException de) {
                System.debug('dmlExcep :: '+de.getMessage());
            }   Catch(Exception ex) {
                System.debug('Exception :' +ex.getMessage());
            }  
        } 

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle beforeDelete business logic for Task Trigger
    * @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */  
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle afterDelete business logic for Task Trigger
    * @Parameters  : List<sObject> oldList , Map<Id, sObject> oldMap
    * @Return      : void 
    */  
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 28th July 2020
    * @Description : This method id used to handle afterUnDelete business logic for Task Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
    * @Return      : void 
    */  
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }

}
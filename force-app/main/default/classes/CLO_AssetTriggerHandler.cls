/**
* @ClassName      : AssetTriggerHandler 
* @Description    : Handler class for Asset trigger 
* @Author         : Markandeyulu K
* @CreatedDate    : 28th July 2020
*/


public class CLO_AssetTriggerHandler implements CLO_ITriggerHandler{
    
    public static List<Task> lstTask;
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
    
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle beforeInsert business logic for Asset Trigger
* @Parameters  : List<sObject> newList 
* @Return      : void 
*/ 
    public void beforeInsert(List<sObject> newList) {
        Set<String> orderIdSet = new Set<String>();
        
        List<Asset> assetList = new List<Asset>();
        List<Asset> lstAsset = (List<Asset>) newList;
        List<Entitlement> entitlementList = new List<Entitlement>();
        List<Entitlement> eSCEntitlementList = new List<Entitlement>();
        Map<String, Order> orderMap = new Map<String, Order>();
        
        String slarecordId = [SELECT id from RecordType where DeveloperName ='CLO_SLA'].Id;
        String discountrecordId = [SELECT id from RecordType where DeveloperName ='CLO_Discount'].Id;
        
        for(Asset a : lstAsset){
            if(a.AccountId != null && a.Product2Id != null && a.CLO_SAP_Order_Number__c != null)
            {
                assetList.add(a); 
                orderIdSet.add(a.CLO_SAP_Order_Number__c);
            }
        }
        
        if(assetList.size() > 0)
        {
            for(Order o : [SELECT Id, SAP_Order_Id__c, Type, 
                           (SELECT id, CLO_Embedded_Warranty__c, CLO_PC_Bypass_and_SF_Id__c, PriceBookEntryId,
                            Quantity, PriceBookEntry.Product2Id
                            from OrderItems
                            where CLO_PC_Bypass_and_SF_Id__c = 'EW') 
                           from Order
                           where SAP_Order_Id__c in: orderIdSet])
            {
                orderMap.put(o.SAP_Order_Id__c, o);
            }
            
            for(Asset a : assetList)
            {
                if (orderMap.containskey(a.CLO_SAP_Order_Number__c))
                {
                    Order order = orderMap.get(a.CLO_SAP_Order_Number__c);
                    
                    if (order.Type != 'ZRP' && order.OrderItems.size() > 0)
                        //Integer months =  order.OrderItems[0].setScale(0); 
                        a.CLO_Extended_Service_Warranty__c = order.OrderItems[0].Quantity.intValue()+'';
                }
            }
        }
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle afterInsert business logic for Asset Trigger
* @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
* @Return      : void 
*/
    
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
        Set<String> orderIdSet = new Set<String>();
        
        List<Asset> assetList = new List<Asset>();
        List<Asset> lstAsset = (List<Asset>) newList;
        List<Entitlement> entitlementList = new List<Entitlement>();
        List<Entitlement> eSCEntitlementList = new List<Entitlement>();
        List<Entitlement> newAEEntitlementList = new List<Entitlement>();
        Map<String, Order> orderMap = new Map<String, Order>();
        
        String slarecordId = [SELECT id from RecordType where DeveloperName ='CLO_SLA'].Id;
        String discountrecordId = [SELECT id from RecordType where DeveloperName ='CLO_Discount'].Id;
        
        for(Asset a : lstAsset){
            if(a.AccountId != null && a.Product2Id != null && a.CLO_SAP_Order_Number__c != null)
            {
                assetList.add(a); 
                orderIdSet.add(a.CLO_SAP_Order_Number__c);
            }
        }
        
        if(assetList.size() > 0)
        {
            for(Order o : [SELECT Id, SAP_Order_Id__c, Type, PriceBook2Id,
                           (SELECT id, CLO_Embedded_Warranty__c, CLO_PC_Bypass_and_SF_Id__c, PriceBookEntryId,
                            Quantity, PriceBookEntry.Product2Id, UnitPrice
                            from OrderItems) 
                           from Order
                           where SAP_Order_Id__c in: orderIdSet])
            {
                orderMap.put(o.SAP_Order_Id__c, o);
            }
            
            for(Asset a : assetList)
            {
                if (orderMap.containskey(a.CLO_SAP_Order_Number__c))
                {
                    Order order = orderMap.get(a.CLO_SAP_Order_Number__c);
                    
                    if (a.CLO_Embedded_Warranty__c != null && order.Type != 'ZRP' && order.OrderItems.size() > 0)
                    {
                        ServiceContract SC = new ServiceContract(AccountId = a.AccountId,
                                                                 CLO_Asset__c = a.Id,
                                                                 ApprovalStatus = 'Draft',
                                                                 PriceBook2Id = order.PriceBook2Id,
                                                                 Name = 'Warranty Contract');
                        insert SC;
                        
                        OrderItem orderItem = null;
                        for(OrderItem oi : order.OrderItems)
                        {
                            if(oi.PriceBookEntry.Product2Id == a.Product2Id)
                                orderItem = oi;
                        }
                        
                        ContractLineItem cl = new ContractLineItem(AssetId = a.Id,
                                                                   ServiceContractId = SC.Id,
                                                                   PriceBookEntryId = orderItem.PriceBookEntryId,
                                                                   UnitPrice = orderItem.UnitPrice,
                                                                   Quantity = orderItem.Quantity,
                                                                   CLO_Service_Contract_Types__c = 'Warranty');
                        insert cl;
                        
                        entitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                            AssetId = a.Id,
                                                            RecordTypeId = slarecordId,
                                                            ServiceContractId = SC.Id,
                                                            ContractLineItemId = cl.Id,
                                                            CLO_Billable_Status__c = 'Warranty',
                                                            Name = 'Standard SLA | Warranty Media12 | '+ a.SerialNumber));
                        
                        entitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                            AssetId = a.Id,
                                                            RecordTypeId = discountrecordId,
                                                            ServiceContractId = SC.Id,
                                                            ContractLineItemId = cl.Id,
                                                            CLO_Billable_Status__c = 'Warranty',
                                                            Name = 'labor | Warranty Media12 | '+ a.SerialNumber));
                       
                        entitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                            AssetId = a.Id,
                                                            RecordTypeId = discountrecordId,
                                                            ServiceContractId = SC.Id,
                                                            ContractLineItemId = cl.Id,
                                                            CLO_Billable_Status__c = 'Warranty',
                                                            Name = 'parts | Warranty Media12 | '+ a.SerialNumber));
                        
                        entitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                            AssetId = a.Id,
                                                            RecordTypeId = discountrecordId,
                                                            ServiceContractId = SC.Id,
                                                            ContractLineItemId = cl.Id,
                                                            CLO_Billable_Status__c = 'Warranty',
                                                            Name = 'travel | Warranty Media12 | '+a.SerialNumber));
                    if(entitlementList.size() > 0 && order.Type == 'Trade-In'){
                                    for (Entitlement etm : entitlementList){
                                           etm.StartDate = Date.today();
                            			   etm.EndDate = etm.StartDate.addMonths(Integer.ValueOf(a.CLO_Embedded_Warranty__c)).addDays(-1);
                            }
                        }
                    }
                    
                    if (a.CLO_Extended_Service_Warranty__c != null && order.Type != 'ZRP' && order.OrderItems.size() > 1){
                        ServiceContract SC1 = new ServiceContract(AccountId = a.AccountId,
                                                                  CLO_Asset__c = a.Id,
                                                                  ApprovalStatus = 'Draft',
                                                                  PriceBook2Id = order.PriceBook2Id,
                                                                  Name = 'Extended Service Contract');
                        insert SC1;
                        
                        OrderItem orderItem = null;
                        for(OrderItem oi : order.OrderItems)
                        {
                            if(oi.CLO_PC_Bypass_and_SF_Id__c == 'EW')
                                orderItem = oi;
                        }
                        
                        ContractLineItem CL1 = new ContractLineItem(AssetId = a.Id,
                                                                    ServiceContractId = SC1.Id,
                                                                    PriceBookEntryId = orderItem.PriceBookEntryId,
                                                                    UnitPrice = orderItem.UnitPrice,
                                                                    Quantity = orderItem.Quantity,
                                                                    CLO_Service_Contract_Types__c = 'Extended Service Contract');
                        insert CL1;
                        
                        eSCEntitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                               AssetId = a.Id,
                                                               RecordTypeId = slarecordId,
                                                               ServiceContractId = SC1.Id,
                                                               ContractLineItemId = CL1.Id,
                                                               CLO_Billable_Status__c = 'Extended Warranty',
                                                               Name = 'Standard SLA | Extended Warranty  | '+ a.SerialNumber));
                        
                        eSCEntitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                               AssetId = a.Id,
                                                               RecordTypeId = discountrecordId,
                                                               ServiceContractId = SC1.Id,
                                                               ContractLineItemId = CL1.Id,
                                                               CLO_Billable_Status__c = 'Extended Warranty',
                                                               Name = 'labor | Extended Warranty | '+ a.SerialNumber));
                        
                        eSCEntitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                               AssetId = a.Id,
                                                               RecordTypeId = discountrecordId,
                                                               ServiceContractId = SC1.Id,
                                                               ContractLineItemId = CL1.Id,
                                                               CLO_Billable_Status__c = 'Extended Warranty',
                                                               Name = 'parts | Extended Warranty | '+ a.SerialNumber));
                        
                        eSCEntitlementList.add(new Entitlement(AccountId = a.AccountId,
                                                               AssetId = a.Id,
                                                               RecordTypeId = discountrecordId,
                                                               ServiceContractId = SC1.Id,
                                                               ContractLineItemId = CL1.Id,
                                                               CLO_Billable_Status__c = 'Extended Warranty',
                                                               Name = 'travel | Extended Warranty | '+ a.SerialNumber));
                        if(eSCEntitlementList.size() > 0 && order.Type == 'Trade-In'){
                                    for (Entitlement etm : eSCEntitlementList){
                                           etm.StartDate = Date.today().addMonths(Integer.ValueOf(a.CLO_Embedded_Warranty__c));
                            			   etm.EndDate = etm.StartDate.addMonths(Integer.ValueOf(a.CLO_Extended_Service_Warranty__c)).addDays(-1);
                            }
                        }
                    }
                
                    if (order.Type == 'Trade-In' || order.Type == 'ZRP'){
                        if (a.CLO_Returned_Asset_Serial_Number__c != null){
                            String AssetID =  [SELECT id  from Asset where SerialNumber =: a.CLO_Returned_Asset_Serial_Number__c Limit 1].id;
                            List <Entitlement> oldETList = [SELECT id,Name,AccountId,ServiceContractId,ContractLineItemId,CLO_Billable_Status__c,RecordTypeId, StartDate, EndDate, AssetId
                                                            from Entitlement 
                                                            where AssetId =: AssetID
                                                            and Status ='Active'];
                            
                            if(oldETList.size() > 0){
                                for (Entitlement etm : oldETList){
                                   if (order.Type == 'Trade-In'){
                                       etm.EndDate = Date.today();
                                   }
                                    if (order.Type == 'ZRP'){
                                        Entitlement Newetm = etm.clone(false, false, false, false);
                                        Newetm.AssetId = a.Id;
                                    	Newetm.StartDate = Date.today();
                                    	newAEEntitlementList.add(Newetm);
                                        etm.EndDate = Date.today();
                                    }
                                }
                                update oldETList;
                            }
                    	}
                    }
                }
            }
            
            if(entitlementList.size() > 0)
                insert entitlementList;
            
            if(eSCEntitlementList.size() > 0)
                insert eSCEntitlementList;
            if(newAEEntitlementList.size() > 0)
                insert newAEEntitlementList;
        }
        List<Asset> WOAssetList = new List<Asset>();
        List<WorkOrderLineItem> workorderItemInsertList = new List<WorkOrderLineItem>();
        List<WorkOrder> workordersList = new List<WorkOrder>();
        
        for(Asset AI : lstAsset){
            if(AI.AccountId != null && AI.Product2Id != null)
            {
                WOAssetList.add(AI);     
            }
        }
        
        if(WOAssetList.size() > 0)
        {
            String installation = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CLO_Installation').getRecordTypeId();
            String training = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CLO_Training').getRecordTypeId();
            String preceptorship = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CLO_Preceptorship').getRecordTypeId();
            
            List<Workorder> workorderInsertList = new List<Workorder>();
            List<WorkOrderLineItem> woliInsertList = new List<WorkOrderLineItem>();
            
            for(Asset At : WOAssetList){
                if(At.CLO_Installation_Included__c){
                    workorderInsertList.add(new WorkOrder(AccountId = At.AccountId,
                                                          AssetId = At.Id,
                                                          Status = 'New',
                                                          RecordTypeId = installation,
                                                          StartDate = datetime.now()));
                    
                    woliInsertList.add(new WorkOrderLineItem(Status = 'New',
                                                             //Product1__c = At.Product2Id,
                                                             AssetId = At.Id));
                }
                
                if(At.CLO_Preceptorship__c){
                     workorderInsertList.add(new WorkOrder(AccountId = At.AccountId,
                                                           AssetId = At.Id,
                                                           Status = 'New',
                                                           RecordTypeId = preceptorship,
                                                           StartDate = datetime.now()));
                     
                     woliInsertList.add(new WorkOrderLineItem(Status = 'New',
                                                             //Product1__c = At.Product2Id,
                                                             AssetId = At.Id));
                }
                
                if(At.CLO_Clinical_Training_Included__c){
                    workorderInsertList.add(new WorkOrder(AccountId = At.AccountId,
                                                          AssetId = At.Id,
                                                          Status = 'New',
                                                          RecordTypeId = training,
                                                          StartDate = datetime.now()));
                    
                    woliInsertList.add(new WorkOrderLineItem(Status = 'New',
                                                             //Product1__c = At.Product2Id,
                                                             AssetId = At.Id));
                }
            }
            
            if(workorderInsertList.size() > 0)
            {
                insert workorderInsertList;
                
                Integer count = 0;
                for(WorkOrderLineItem woli : woliInsertList)
                {
                    woli.WorkorderId = workorderInsertList[count].Id;
                    count++;
                }
                
                insert woliInsertList;
            }
        }
    
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle beforeUpdate business logic for Asset Trigger
* @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
* @Return      : void 
*/
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
        List<Asset> lstAsset = (List<Asset>) newList;
        Map<id,Asset> mapOldAsset = (Map<id,Asset>) oldMap;
        List<Asset> AssetList = new List<Asset>();
        for(Asset ast : lstAsset){
            if(ast.Status != mapOldAsset.get(ast.id).Status && ast.Status == CLO_Constants.INSTALLED){
                ast.InstallDate = System.Today();
            }
        }
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle afterUpdate business logic for Asset Trigger
* @Parameters  : List<sObject> newList , Map<Id, sObject> newMap,List<sObject> oldList, Map<Id, sObject> oldMap
* @Return      : void 
*/
    
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
        if(CLO_CommonUtil.runOnce()){
            Map<id,Asset> mapOldAsset = (Map<id,Asset>) oldMap;
            Map<id,Asset> mapNewAsset = (Map<id,Asset>) newMap;
            List<Entitlement> lstEntitlements = new List<Entitlement>();
            List<ServiceContract> lstServices = new List<ServiceContract>();
            
            lstTask = new List<Task>();
            List<id> listTaskid = new List<id>();
            listTaskid.addAll(mapNewAsset.keySet());
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseId(listTaskid)
                + ' AND Product2ID != null AND Product2.CLO_Warranty_Not_Renewable__c = false AND Status = '+ CLO_StringUtil.inquoteString(CLO_Constants.INSTALLED);
            
            List<Asset> lstAsset = (List<Asset>)CLO_DBManager.getSObjectList(' id, Status, CLO_Extended_Service_Warranty__c, CLO_Embedded_Warranty__c, InstallDate, Product2ID, Product2.CLO_Warranty_Not_Renewable__c ',' Asset ', whereClause);
            
            for(Asset aset : lstAsset){ 
                if(aset.status != mapOldAsset.get(aset.id).status){           
                    Task tsk = new Task();    
                    tsk.Subject = Label.CLO_Task_Sub_Initial_Call;
                    tsk.WhatId = aset.id;
                    tsk.OwnerId = UserInfo.getUserId();
                    tsk.Status = CLO_Constants.STATUS_OPEN;
                    tsk.Type = CLO_Constants.CALL;
                    tsk.Description = Label.CLO_Task_Activity_Consumable_Sale;
                    if(aset.InstallDate != Null){
                        tsk.ActivityDate = aset.InstallDate.addDays(Integer.valueOf(Label.CLO_Consumable_Sale_Task_Days));
                    }
                    lstTask.add(tsk);
                }
                
                if(aset.InstallDate != null && aset.InstallDate != mapOldAsset.get(aset.id).InstallDate)
                {
                    List<Entitlement> lstEntitlementseach = [Select Id, StartDate, CLO_Billable_Status__c, EndDate from Entitlement where AssetId =: aset.Id];  
                    List<ServiceContract> lstServiceContract = [Select Id, Name, StartDate, EndDate from ServiceContract where CLO_Asset__c =: aset.Id]; 
                    
                    for (Entitlement ent : lstEntitlementseach)
                    {
                        if(ent.CLO_Billable_Status__c != null && ent.CLO_Billable_Status__c == 'Warranty')
                        { 
                            ent.StartDate = aset.InstallDate;
                            ent.EndDate = aset.InstallDate.addMonths(Integer.ValueOf(aset.CLO_Embedded_Warranty__c)).addDays(-1);
                            lstEntitlements.add(ent);
                        }
                        
                        if(ent.CLO_Billable_Status__c != null && ent.CLO_Billable_Status__c =='Extended Warranty')
                        { 
                            ent.StartDate = aset.InstallDate.addMonths(Integer.ValueOf(aset.CLO_Embedded_Warranty__c));
                            ent.EndDate = ent.StartDate.addMonths(Integer.ValueOf(aset.CLO_Extended_Service_Warranty__c)).addDays(-1);
                            lstEntitlements.add(ent);
                        }
                    }
                    
                    for (ServiceContract sc : lstServiceContract)
                    {
                        if(sc.Name == 'Warranty Contract')
                        { 
                            sc.StartDate = aset.InstallDate;
                            sc.EndDate = aset.InstallDate.addMonths(Integer.ValueOf(aset.CLO_Embedded_Warranty__c)).addDays(-1);
                        }
                        
                        if(sc.Name == 'Extended Service Contract')
                        { 
                            sc.StartDate = aset.InstallDate.addMonths(Integer.ValueOf(aset.CLO_Embedded_Warranty__c));
                            sc.EndDate =  sc.StartDate.addMonths(Integer.ValueOf(aset.CLO_Extended_Service_Warranty__c)).addDays(-1);
                        }
                        
                        lstServices.add(sc);
                    }
                }
            }
            
            if(lstTask.size() > 0){ 
                try{         
                    CLO_DBManager.insertSObjects(lstTask); 
                } Catch(DMLException de) {
                    System.debug('dmlExcep :: '+de.getMessage());
                } Catch(Exception ex) {
                    System.debug('Exception :' +ex.getMessage());
                }   
            }
            
            if(lstEntitlements.size() > 0){ 
                try{         
                    CLO_DBManager.updateSObjects(lstEntitlements); 
                } Catch(DMLException de) {
                    System.debug('dmlExcep :: '+de.getMessage());
                } Catch(Exception ex) {
                    System.debug('Exception :' +ex.getMessage());
                }   
            }
            
            if(lstServices.size() > 0){ 
                try{         
                    CLO_DBManager.updateSObjects(lstServices); 
                } Catch(DMLException de) {
                    System.debug('dmlExcep :: '+de.getMessage());
                } Catch(Exception ex) {
                    System.debug('Exception :' +ex.getMessage());
                }   
            }
        }
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle beforeDelete business logic for Asset Trigger
* @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
* @Return      : void 
*/ 
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle afterDelete business logic for Asset Trigger
* @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
* @Return      : void 
*/ 
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        
    }
    
    /**
* @Author      : Markandeyulu K
* @CreatedDate : 28th July 2020
* @Description : This method id used to handle afterUnDelete business logic for Asset Trigger
* @Parameters  : List<sObject> newList, Map<Id, sObject> newMap
* @Return      : void 
*/ 
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
        
    }
    
}

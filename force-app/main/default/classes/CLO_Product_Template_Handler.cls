public class CLO_Product_Template_Handler {

    public List<SBQQ__ProductOption__c> product_options {get;set;}
    public Decimal productUnitprice {get;set;}
    public Decimal productTotalprice {get;set;}
    public Decimal productFinalprice {get;set;}
    public Product2 record {get;set;}
   // Map<String,Decimal> pbook {get;set;}
    
    //Constructor

    public CLO_Product_Template_Handler(ApexPages.StandardController controller){
       String ProdId = ApexPages.currentPage().getParameters().get('id');
        productTotalprice = 0.00;
        productFinalprice = 0.00;
        Product2 record = [SELECT Id,Description,SBQQ__DefaultQuantity__c FROM Product2 WHERE Id = :ProdId];
        List<PricebookEntry> productpriceBook = [SELECT CurrencyIsoCode,Id,IsActive,Product2Id,UnitPrice FROM PricebookEntry WHERE Product2Id =:ProdId AND CurrencyIsoCode ='USD' LIMIT:1];
        if (productpriceBook.size() > 0){
        if (productpriceBook[0].UnitPrice != null && record.SBQQ__DefaultQuantity__c != null){
        productUnitprice = productpriceBook[0].UnitPrice;
        productTotalprice = productpriceBook[0].UnitPrice*record.SBQQ__DefaultQuantity__c;
         }
         else{
            productUnitprice = 0.00;
        	productTotalprice = 0.00;
        }
            }
        else{
            productUnitprice = 0.00;
        	productTotalprice = 0.00;
        }
       product_options = [SELECT Id,SBQQ__ProductDescription__c, SBQQ__UnitPrice__c,SBQQ__Discount__c,SBQQ__Quantity__c,SBQQ__ConfiguredSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :ProdId];
        if (product_options!=null && !product_options.isEmpty()){
          for(SBQQ__ProductOption__c opt :product_options ){
             List<PricebookEntry> priceBook = [SELECT CurrencyIsoCode,Id,IsActive,Product2Id,UnitPrice FROM PricebookEntry WHERE Product2Id =:opt.SBQQ__OptionalSKU__c AND CurrencyIsoCode ='USD' LIMIT:1];
              if (priceBook.size()>0){
              opt.put('SBQQ__UnitPrice__c',priceBook[0].UnitPrice);
              if (priceBook[0].UnitPrice != null && opt.SBQQ__Quantity__c !=null){
             	  Decimal totalPrice = priceBook[0].UnitPrice*opt.SBQQ__Quantity__c;
                  opt.put('CLO_Total_Price__c',totalPrice);
                  productFinalprice = productFinalprice+totalPrice;
                  }
              else{
             opt.put('CLO_Total_Price__c',0.00);
                  opt.put('SBQQ__Quantity__c',0.00);
        	}
                  }
               else{
             opt.put('CLO_Total_Price__c',0.00);
                  opt.put('SBQQ__Quantity__c',0.00);
                    opt.put('SBQQ__UnitPrice__c',0.00);
        	}
              }
        }
        

       System.debug(product_options);
        
       String myGeneratedFileName ='record';
       Apexpages.currentPage().getHeaders().put('content-disposition', 'inline; filename='+myGeneratedFilename); 
        
    }
   /* public Decimal getUnitPrice(String Prodid){
     PricebookEntry pbook = pbook.get(ProdId);
        return pbook.UnitPrice;
        
    }*/
}
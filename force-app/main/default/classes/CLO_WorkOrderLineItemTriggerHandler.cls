/**
  * @ClassName      : CLO_WorkOrderLineItemTriggerHandler 
  * @Description    : WorkOrder Object Trigger Handler.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */

  public class CLO_WorkOrderLineItemTriggerHandler implements CLO_ITriggerHandler{
     
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
/**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle before insert logic.
  * @Parameters  : List<sObject>
  * @Return      : Void 
  */
    public void beforeInsert(List<sObject> newList) {
        Map<String,String> mapPbEntry = new Map<String,String>();
        Map<String,String> mapWoPb = new Map<String,String>();
        List<WorkOrderLineItem> lstWoli = (List<WorkOrderLineItem>) newList;
        Set<Id> setwoids = new Set<Id>();
        for(WorkOrderLineItem woli : lstWoli){
            if(woli.WorkOrderId != Null){
                setwoids.add(woli.WorkOrderId);
            }
        }
        
        if(setwoids.size() > 0){
            for(WorkOrder wo : [Select id,Pricebook2Id From WorkOrder Where Id IN : setwoids AND Pricebook2Id != Null]){
                mapWoPb.put(wo.id,wo.Pricebook2Id);
                System.debug('$$$'+mapWoPb);
            }
        }
        
        if(mapWoPb.values().size() > 0){ 
            String serLabor = Label.CLO_Service_Labor;
            String serTravel = Label.CLO_Service_Travel;
            
            for(PricebookEntry pbe : [Select id,Name,Pricebook2Id FROM PricebookEntry 
                                      Where Pricebook2Id IN : mapWoPb.values()
                                      and (Name =: Label.CLO_Service_Labor
                                      or Name =: Label.CLO_Service_Travel)]){
                System.debug('%%%'+pbe);
                if(pbe.Name == Label.CLO_Service_Labor){
                    mapPbEntry.put(Label.CLO_Service_Labor,pbe.id);    
                }
                if(pbe.Name == Label.CLO_Service_Travel){
                    mapPbEntry.put(Label.CLO_Service_Travel,pbe.id);    
                }
                System.debug('@@@'+mapPbEntry);
            }
        }
        
        for(WorkOrderLineItem woli : lstWoli){
            if(woli.CLO_Type__C == CLO_Constants.LABOR && mapPbEntry.containskey(Label.CLO_Service_Labor)){
                woli.PricebookEntryId = mapPbEntry.get(Label.CLO_Service_Labor);
            }
              
            if(woli.CLO_Type__C == CLO_Constants.TRAVEL && mapPbEntry.containskey(Label.CLO_Service_Travel)){
                woli.PricebookEntryId = mapPbEntry.get(Label.CLO_Service_Travel);
            }
        }
             
    }
    
/**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle after insert logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
    
        Map<String,String> mapwoliWoIds = new Map<String,String>(); 
        List<Id> lstAstTravlIds = new List<Id>();
        List<Id> lstAstLabrIds = new List<Id>();
        List<WorkOrderLineItem> lstWoLi = new List<WorkOrderLineItem>();
        Map<String,String> mapWoAstId = new Map<String,String>();
           
        for(WorkOrderLineItem woli : (List<WorkOrderLineItem>) newList){
            mapwoliWoIds.put(woli.id,woli.WorkOrderId);    
        }
        
        for(WorkOrder wo : [Select Id,AssetId From WorkOrder Where Id IN : mapwoliWoIds.values() AND AssetId != Null]){
            mapWoAstId.put(wo.id,wo.AssetId);
        }
        
        If(mapWoAstId != Null && mapWoAstId.values().size() > 0){
            for(Entitlement entmnt : [Select Id,Name,AssetId,Status From Entitlement where Status =: CLO_Constants.Active AND AssetId IN : mapWoAstId.values()]){
                If(entmnt.Name.ContainsIgnoreCase(CLO_Constants.WARRANTY) || entmnt.Name.ContainsIgnoreCase(CLO_Constants.CONTRACT)){
                    If(entmnt.Name.ContainsIgnoreCase(CLO_Constants.TRAVEL) && !lstAstTravlIds.contains(entmnt.AssetId)){
                        lstAstTravlIds.Add(entmnt.AssetId);
                    }
        
                    If(entmnt.Name.ContainsIgnoreCase(CLO_Constants.LABOR) && !lstAstLabrIds.contains(entmnt.AssetId)){
                        
                        lstAstLabrIds.add(entmnt.AssetId);
                        
                    }
                }
            }
        }
        
        for(WorkOrderLineItem wolis : (List<WorkOrderLineItem>) newList){
        
            If(lstAstTravlIds != Null && lstAstTravlIds.size() > 0){
                If(wolis.CLO_Type__c == CLO_Constants.TRAVEL && mapWoAstId != Null && mapWoAstId.containsKey(wolis.WorkOrderId) && lstAstTravlIds.contains(mapWoAstId.get(wolis.WorkOrderId))){
                    WorkOrderLineItem wolit = new WorkOrderLineItem(ID=wolis.Id);
                    wolit.Discount = 100;
                    lstWoLi.add(wolit);
                }
                
                If(wolis.CLO_Type__c == CLO_Constants.LABOR && mapWoAstId != Null && mapWoAstId.containsKey(wolis.WorkOrderId) && lstAstLabrIds.contains(mapWoAstId.get(wolis.WorkOrderId))){
                    WorkOrderLineItem wolil = new WorkOrderLineItem(ID=wolis.Id);
                    wolil.Discount = 100;
                    lstWoLi.add(wolil);
                }
            }  
        }
        
        If(lstWoLi.size() > 0){
            
             try{         
                    CLO_DBManager.updateSObjects(lstWoLi); 
                } Catch(DMLException de) {
                    System.debug('dmlExcep :: '+de.getMessage());
                } Catch(Exception ex) {
                    System.debug('Exception :' +ex.getMessage());
                }  
        }
        
       
    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle before update logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */  
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
    
    }
    
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle after update logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
    public void afterUpdate(List<sObject> newList, Map<Id,sObject> newMap,  List<sObject> oldList, Map<Id,sObject> oldMap) {
             
    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle before delete logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle after un delete logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
   
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
    public void afterDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
 
}
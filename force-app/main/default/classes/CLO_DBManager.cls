/**
  * @ClassName      : CLO_DBManager 
  * @Description    : This class used to define the DML generic methods.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 24th June 2020
  */
  public with sharing class CLO_DBManager {
    /*********************************************************
      * SOQL Operations - 
      *********************************************************/ 
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : This method used to execute query to get the records.
      * @Parameters  : String strQuery
      * @Return      : SObject 
      */   
      public static SObject queryExecutor(String strQuery){
          SObject[] sObjs = null;
          String updatedQuery = updateLimitForQueryString(strQuery);
          System.debug('updatedQuery ::'+updatedQuery);
          if(String.isNotBlank(updatedQuery)) {
              sObjs = database.query(updatedQuery);	
          }
          
          if(!CLO_CommonUtil.isNullOrEmpty(sObjs)){
              return sObjs[0];
          }else{
              return null;
          }       
      }  
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : This method used to execute query to get the records.
      * @Parameters  : String fields, string objectName, String whereClause 
      * @Return      : SObject 
      */
      public static SObject getSObject(String fields, string objectName, String whereClause ){
          String strQuery = 'SELECT ' + String.escapeSingleQuotes(fields) + ' FROM ' + String.escapeSingleQuotes(objectName) + ' WHERE ' + whereClause;
          System.debug('QUERY>>>'+strQuery);
          SObject sObj = queryExecutor(strQuery);
          return sObj;
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : This method used to get list of SObject based on query
      * @Parameters  : String strQuery
      * @Return      : List<SObject> 
      */
      public static List<SObject> getSObjects(String strQuery){
          SObject[] sObjs = null;
          String updatedQuery = updateLimitForQueryString(strQuery);
          
          if(String.isNotBlank(updatedQuery)) {
              sObjs = database.query(updatedQuery);  
          }
          
          if(!CLO_CommonUtil.isNullOrEmpty(sObjs)){
              return sObjs;
          }else{
              return null;
          }       
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : This method used to execute query and get list if sobject records.
      * @Parameters  : String fields, String objectName, String whereClause 
      * @Return      : List<SObject> 
      */
      public static List<SObject> getSObjectList(String fields, String objectName, String whereClause ){
          String strQuery = 'SELECT ' + fields + ' FROM ' + objectName + ' WHERE ' + whereClause;
          System.debug('QUERY>>>'+strQuery);
          
          SObject[] sObjs = new List<SObject>();
          String updatedQuery = updateLimitForQueryString(strQuery);
          
          if(String.isNotBlank(updatedQuery)) {
              sObjs = database.query(updatedQuery);  
          }
          
          return sObjs;
      }
  
    
    /*********************************************************
      * DML Operations 
      *********************************************************/ 
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : Insert list of records to respective object.
      * @Parameters  : List<SObject> lstSObject
      * @Return      : void 
      */
      public static void insertSObjects(List<SObject> lstSObject){
          if(!CLO_CommonUtil.isNullOrEmpty(lstSObject)){
              Database.insert(lstSObject);
          }
      } 
  
      /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It is used to delete the list of records from respective object
      * @Parameters  : List<SObject> lstSObject
      * @Return      : void 
      */
      public static void deleteSObjects(List<SObject> lstSObject)
      {
          if(!CLO_CommonUtil.isNullOrEmpty(lstSObject)){
              Database.delete(lstSObject);
          }
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It will used to update records.
      * @Parameters  : List<SObject> lstSObject
      * @Return      : void 
      */
      public static void updateSObjects(List<SObject> lstSObject){
          if(!CLO_CommonUtil.isNullOrEmpty(lstSObject)){
              Database.update(lstSObject);
          }
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It will used to upsert records.
      * @Parameters  : List<SObject> lstSObject
      * @Return      : void 
      */
      public static void upsertSObjects(List<SObject> lstSObject){
          if(!CLO_CommonUtil.isNullOrEmpty(lstSObject)){
              Database.upsert(lstSObject);
          }
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It will used to undelete records.
      * @Parameters  : List<SObject> lstSObject
      * @Return      : void 
      */
      public static void undeleteSObjects(List<SObject> lstSObject){
          if(!CLO_CommonUtil.isNullOrEmpty(lstSObject)){
              Database.undelete(lstSObject);
          }
      }
      
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It will used to empty RecycleBin.
      * @Parameters  : SObject entity
      * @Return      : void 
      */
      public  static void emptyRecycleBin(SObject entity) {
        /**
        * Method to delete record from recyle bin
        */
        Object obj = entity.get('id');        
        if (!CLO_CommonUtil.isNullOrEmpty(obj)) {   
               Database.EmptyRecycleBinResult  deletedRecord= Database.emptyRecycleBin(entity);
            system.debug('delete id: '+ deletedRecord.getId());
            system.debug('delete status: '+ deletedRecord.isSuccess()); 
                 system.debug('delet status error '+ deletedRecord.getErrors());     
        }
    }
    
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It will used to empty RecycleBin.
      * @Parameters  : List<SObject> lstSObject
      * @Return      : void 
      */
      public static void emptyRecycleBin(List<SObject> lstSObject){
          /**
          * Method to delete record from recyle bin
          */
          if (!CLO_CommonUtil.isNullOrEmpty(lstSObject)) {
              Integer dmlLimit = Limits.getLimitDMLRows() - Limits.getDMLRows();
              /**
               * Avoid hiting governor limit by checking available dml limits
               * We can empty 10,000 records at a time form recycle bin 
               */
              if(dmlLimit > 10000) {
                  dmlLimit = 10000;
              }
              List<SObject> lstToDelete = new List<SObject>();        	
              if(lstSObject.size() > dmlLimit) {
                  for(integer i =0 ;i<dmlLimit;i++){
                      lstToDelete.add(lstSObject[i]);
                  }
              }else{
                  lstToDelete = lstSObject;
              }
                 List<Database.EmptyRecycleBinResult>  deletedRecord= Database.emptyRecycleBin(lstToDelete);
          }
      }
  
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 24th June 2020
      * @Description : It will used to get Limits from query string 
      * @Parameters  : String strQuery
      * @Return      : String 
      */
      public static String updateLimitForQueryString(String strQuery)
      {  
           /**
           * Logic to handle queries with all rows present in them
           */
          Integer intQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
          
          //if queryLimit is 0, then there is no point to execute the query.
          if(!(intQueryRows > 0)) {
              return null;
          }
                 
          Boolean isAllRowsPresent = false;
          String regexp = '(?i)All\\s+Rows';
          Pattern patn = Pattern.compile(regexp);
          Matcher mat = patn.matcher(strQuery);		
          String allRowsClause = '';
          
          while(mat.find()) {
              allRowsClause = mat.group();
              isAllRowsPresent = true;
              /**
               * Remove all rows clause and add it in the query at the end
               */
              strQuery = strQuery.replace(allRowsClause, '');
          }
          
          /**
           * Logic to handle queries with limits clause present in them
           */
          regexp = '(?i)Limit\\s+[\\w]*';
          patn = Pattern.compile(regexp);
          mat = patn.matcher(strQuery);
          
          String limitClause = '';
          Integer limitNo;
          while(mat.find()){
              limitClause = mat.group();
          }
          
          if(String.isNotBlank(limitClause)){
              /**
               * Remove limits clause and add it in the query at the end
               */
              strQuery = strQuery.replace(limitClause, '');
              limitClause = limitClause.trim();	
              System.debug('CLO_DBManager:: limitClause :: '+limitClause);		
              limitNo = Integer.valueof(limitClause.Substring(limitClause.lastIndexOf(' ')+1,limitClause.length()));
          }
          
          /**
           * Set the limitNo same as in query when governor limit is less than the limit else set query rows left
           */
          if(limitNo != null && limitNo < intQueryRows)
          {
              intQueryRows = limitNo;
          }
          strQuery += ' Limit ' + intQueryRows;
          if(isAllRowsPresent){
              strQuery += ' All Rows';
          }
          return strQuery;
      }
  }

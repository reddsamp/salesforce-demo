/**
  * @ClassName      : CLO_OrderTriggerHandler 
  * @Description    : Order Object Trigger Handler.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */

  public class CLO_OrderTriggerHandler implements CLO_ITriggerHandler{
     
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
    public static Set<Id> quoteIds = new Set<Id>();
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
    public void beforeInsert(List<sObject> newList) {
        List<String> customerIds = new List<String>();
        List<String> quoteIds = new List<String>();
        RecordType Recordtypequote = new RecordType();
        Map<Id, Account> customerMap = new Map<Id, Account>();
        Map<Id, SBQQ__Quote__c> quoteMap = new Map<Id, SBQQ__Quote__c>();
        Set<Id> setAstIds= new Set<Id>();
        Map<String,String> mapAstBillSts = new Map<String,String>();
                
        Map<String, String> pricebookMap = new Map<String, String>();
        
        for(Order o : (List<Order>) newList)
        {
            If(!customerIds.contains(o.AccountId) && o.AccountId != Null){
                customerIds.add(o.AccountId);
            }
            If(!quoteIds.contains(o.QuoteId) && o.QuoteId != Null){
                quoteIds.add(o.QuoteId);
            }
            If((o.Type == CLO_Constants.ZRA || o.Type == CLO_Constants.ZSB || o.Type == CLO_Constants.TRADE_IN || o.Type == CLO_Constants.ZRP) && o.CLO_Asset__c != Null){
                setAstIds.add(o.CLO_Asset__c);
            }
            
        }
        If(setAstIds != Null && setAstIds.size() > 0){
            for(Asset ast : [Select Id,CLO_Billable_Status__c From Asset Where Id IN : setAstIds]){
                mapAstBillSts.put(ast.id,ast.CLO_Billable_Status__c);
            }
        }
        
        If(customerIds.size() > 0){
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(customerIds);        
            customerMap = new Map<Id, Account>((List<Account>)CLO_DBManager.getSObjectList(' Id, CLO_Sales_Org__c, BillingCountry, BillingCountryCode, CLO_Shipping_Condition__c, CLO_Payment_Terms__c, CLO_Incoterm_1__c, CLO_Incoterm_2__c   ',' Account ', whereClause)); 
        }
        if(quoteIds.size() > 0){
            String whereClaus = ' Id IN ' + CLO_StringUtil.getInClauseString(quoteIds);  
            quoteMap = new Map<Id, SBQQ__Quote__c>((List<SBQQ__Quote__c>)CLO_DBManager.getSObjectList(' Id, Name, RecordTypeId ',' SBQQ__Quote__c ',whereClaus));
            Recordtypequote = [select id from RecordType where DeveloperName = 'CLO_Capital_Sales' Limit 1];
        }
               
        for(PriceBook2 pb : [select id, Name from PriceBook2])
        {
            pricebookMap.put(pb.Name, pb.Id);
            System.debug('@@@ : ==> '+pricebookMap);
        }
        
        for(Order o : (List<Order>) newList)
        {
            Account acc = customerMap.get(o.AccountId);
            
            String priceBookId = null; 
            If(pricebookMap != Null && pricebookMap.containskey(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode)){
                priceBookId= pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode);
            }
            
            if(priceBookId == null){
                priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountry);
            }
            
    
               
            if(o.PriceBook2Id == null)    
                o.PriceBook2Id = priceBookId;
            
            if(o.CLO_Sales_Org__c == null) o.CLO_Sales_Org__c = acc.CLO_Sales_Org__c;
            if(o.CLO_Payment_Terms__c == null) o.CLO_Payment_Terms__c = acc.CLO_Payment_Terms__c;
            if(o.CLO_Incoterm_1__c == null) o.CLO_Incoterm_1__c = acc.CLO_Incoterm_1__c;
            if(o.CLO_Incoterm_2__c == null) o.CLO_Incoterm_2__c = acc.CLO_Incoterm_2__c;
            if(o.CLO_Shipping_Condition__c == null) o.CLO_Shipping_Condition__c = acc.CLO_Shipping_Condition__c;
            //if(o.PoDate == null) o.PoDate = Date.today();
            //if(o.CLO_Requested_Delivery_Date__c == null) o.CLO_Requested_Delivery_Date__c = Date.today();
            //if(quoteMap.containskey(o.QuoteId) && o.PoNumber == null) o.PoNumber = quoteMap.get(o.QuoteId).Name;
            //if(quoteMap.containskey(o.QuoteId) && quoteMap.get(o.QuoteId).RecordTypeId == Recordtypequote.Id) o.Type = 'ZSAP';
            
            If(o.Type == CLO_Constants.ZSB && mapAstBillSts != Null && mapAstBillSts.containsKey(o.CLO_Asset__c)){
                if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP1 ){
                    o.CLO_Order_Reason__c = CLO_Constants.FS1;
                } else if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP2){
                    o.CLO_Order_Reason__c = CLO_Constants.FS2;
                } else if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP3){
                    o.CLO_Order_Reason__c = CLO_Constants.FS3;
                }
            }
            
            If(o.Type == CLO_Constants.ZRA && mapAstBillSts != Null && mapAstBillSts.containsKey(o.CLO_Asset__c)){
                if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP1 ){
                    o.CLO_Order_Reason__c = CLO_Constants.RP1;
                } else if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP2){
                    o.CLO_Order_Reason__c = CLO_Constants.RP2;
                } else if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP3){
                    o.CLO_Order_Reason__c = CLO_Constants.RP3;
                }
            }
            
            If(o.Type == CLO_Constants.ZRP && mapAstBillSts != Null && mapAstBillSts.containsKey(o.CLO_Asset__c)){
                if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP1 ){
                    o.CLO_Order_Reason__c = CLO_Constants.R05;
                } else if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP2){
                    o.CLO_Order_Reason__c = CLO_Constants.R03;
                } else if(mapAstBillSts.get(o.CLO_Asset__c) == CLO_Constants.RP3){
                    o.CLO_Order_Reason__c = CLO_Constants.R04;
                }
            }
            
            If(o.Type == CLO_Constants.TRADE_IN && mapAstBillSts != Null && mapAstBillSts.containsKey(o.CLO_Asset__c)){               
                o.CLO_Order_Reason__c = CLO_Constants.R08;
            }
            
        }
    }
     
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
        List<Order> lstOrd = (List<Order>) newList;
        //Handle the order sent created from SAP
        CreateOrderLinesForOrderFromSAP(newMap.keyset());
        Map<id,Order> mapOrdPb = new Map<id,Order>();
        Map<id,PricebookEntry> mapProdPbentry = new Map<id,PricebookEntry>();
        List<ID> lstProdIds = new List<ID>();
        List<ID> lstPbIds = new List<ID>();
        List<OrderItem> lstOrdItem = new List<OrderItem>();
        List<Asset> lstAsset = new List<Asset>();
        
        for(Order objOrd : [Select Id,CLO_Case__c,CLO_Case__r.CLO_Serial_Number__c,CLO_Case__r.Billing_Reason__c,CLO_Case__r.Description,Type,CLO_Asset__c,CLO_Asset__r.Product2ID,Pricebook2Id From Order Where Id IN : lstOrd]){
            If(objOrd.Type == CLO_Constants.ZRA && objOrd.CLO_Asset__c != Null){
                Asset objAst = new Asset(ID=objOrd.CLO_Asset__c);
                objAst.Status = 'Pending Return';
                lstAsset.add(objAst);           
            }
            mapOrdPb.put(objOrd.id,objOrd);           
            //lstProdIds.add(objOrd.CLO_Case__r.Asset.Product2ID);           
            lstProdIds.add(objOrd.CLO_Asset__r.Product2ID);           
            lstPbIds.add(objOrd.Pricebook2Id);
            
        }
        
        for(PricebookEntry objPbEntry : [Select Id,UnitPrice,Product2Id,Pricebook2Id FROM PricebookEntry Where Product2Id IN : lstProdIds AND Pricebook2Id IN : lstPbIds]){
            mapProdPbentry.put(objPbEntry.Product2Id,objPbEntry);    
        }
        
        for(Order objOrd : lstOrd){
            //Flat Rate Training Order (Clinical Dispatch)
            if(objOrd.Type == CLO_Constants.CLINICAL_DISPATCH){
                WorkOrder wo = new WorkOrder();
                wo.AccountId = objOrd.AccountId;
                wo.CLO_Order__c = objOrd.Id;
                insert wo;
                
                WorkOrderLineItem woli = new WorkOrderLineItem();
                woli.CLO_Type__c = 'Fixed Fee';
                woli.Quantity = 1;
                woli.Product__c = '';
                woli.UnitPrice = 0;
                woli.WorkOrderId = wo.Id;
                woli.OrderId = objOrd.Id;
            }
            
            else if((objOrd.Type == CLO_Constants.TRADE_IN || objOrd.Type == CLO_Constants.ZRA || objOrd.Type == CLO_Constants.INTERCOMPANY) && mapOrdPb.containskey(objOrd.Id) && mapOrdPb.get(objOrd.Id).CLO_Asset__c != Null && mapProdPbentry.containskey(mapOrdPb.get(objOrd.Id).CLO_Asset__r.Product2ID)){
                If(mapProdPbentry.get(mapOrdPb.get(objOrd.Id).CLO_Asset__r.Product2ID).Id != Null){
                    OrderItem ordItemRma = new OrderItem();
                    ordItemRma.OrderId = objOrd.Id;
                    ordItemRma.Quantity = 1;
                    ordItemRma.Product2Id = mapOrdPb.get(objOrd.Id).CLO_Asset__r.Product2ID;
                    ordItemRma.UnitPrice = 0;
                    ordItemRma.Order_Type__c = CLO_Constants.RMA;
                    ordItemRma.PricebookEntryId = mapProdPbentry.get(mapOrdPb.get(objOrd.Id).CLO_Asset__r.Product2ID).Id;  
                    ordItemRma.CLO_Asset_Serial_Number__c = mapOrdPb.get(objOrd.id).CLO_Case__r.CLO_Serial_Number__c;
                    if(mapOrdPb.get(objOrd.id).CLO_Case__r.Billing_Reason__c == CLO_Constants.RP1){
                        ordItemRma.CLO_Non_Billable__c = false;
                    }
                    lstOrdItem.add(ordItemRma); 
                }                                         
                    
             }
        }
        
        try{
            if(lstOrdItem.size() > 0){
                CLO_DBManager.insertSObjects(lstOrdItem);
            }
        } Catch(DMLException de){
            System.debug('@@@ lstOrdItem DML :'+de.getMessage());
        } Catch(Exception e){
            System.debug('@@@ lstOrdItem EX :'+e.getMessage());
        }
        
        try{
            if(lstAsset.size() > 0){
                CLO_DBManager.updateSObjects(lstAsset);
            } 
        } Catch(DMLException de){
            System.debug('@@@ lstAsset DML :'+de.getMessage());
        } Catch(Exception e){
            System.debug('@@@ lstAsset EX :'+e.getMessage());
        }
        //Added logic
        for(Order ord : lstOrd){
            if(ord.SBQQ__Quote__c != null){
                quoteIds.add(ord.SBQQ__Quote__c);
            }
        } 
        CLO_OrderTriggerHandler.UpdatePartsAmountonOpty();  
        
        
        Set<Id> orderIdSet = new Set<Id>();
        
        for(Order ord : (List<Order>) newList){
            if(ord.Type == 'ZCON'){
                orderIdSet.add(ord.Id);
            }
        }
        
        if(orderIdSet.size() > 0)
            CLO_OrderTriggerHandler.submitConsignmentOrderForApproval(orderIdSet);
    }
     
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
        //Added logic
        for(Order ord : (List<Order>) newList){
            
            if(ord.SBQQ__Quote__c != null){
                quoteIds.add(ord.SBQQ__Quote__c);
            }
        }
        
        for(Order ord : (List<Order>) oldList){
            if(ord.SBQQ__Quote__c != null){
                quoteIds.add(ord.SBQQ__Quote__c);
            }
        }
        CLO_OrderTriggerHandler.UpdatePartsAmountonOpty();
        
        Set<Id> orderIdSet = new Set<Id>();
        
        for(Order ord : (List<Order>) newList){
            if(ord.Type == 'ZCON' && ord.Status == 'Approved' && ord.Status != oldmap.get(ord.Id).get('Status')){
                orderIdSet.add(ord.Id);
            }
        }
        
        if(orderIdSet.size() > 0)
            CLO_OrderTriggerHandler.SubmitComsignmentTransferToSAP(orderIdSet);
    }
     
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {
        //Added logic
         for(Order ord : (List<Order>) oldList){
            if(ord.SBQQ__Quote__c != null){
                quoteIds.add(ord.SBQQ__Quote__c);
            }
        }
        CLO_OrderTriggerHandler.UpdatePartsAmountonOpty();
    }
     
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
        //Added logic
        for(Order ord : (List<Order>) newList){
            if(ord.SBQQ__Quote__c != null){
                quoteIds.add(ord.SBQQ__Quote__c);
            }
        }
        CLO_OrderTriggerHandler.UpdatePartsAmountonOpty();          
    }
    
    public static void UpdatePartsAmountonOpty(){
        if(!quoteIds.isEmpty()){
        List<SBQQ__Quote__c> quoteList = [SELECT Id, CLO_Total_Order_Amount__c, (SELECT Id,SBQQ__Quote__c,TotalAmount FROM SBQQ__Orders__r) 
                                          FROM SBQQ__Quote__c WHERE Id IN : quoteIds AND SBQQ__Primary__c = true];
        if(!quoteList.isEmpty()){
            List<SBQQ__Quote__c> updatequoteList = new List<SBQQ__Quote__c>();
            Map<Id,Double> mapOrdTotAmt = new Map<Id,Double>();
            Double totAmt = 0;
            for(SBQQ__Quote__c objQuot : quoteList){
                for(Order ord : objQuot.SBQQ__Orders__r){
                    totAmt += ord.TotalAmount;
                    mapOrdTotAmt.put(ord.SBQQ__Quote__c,totAmt);                    
                }
                SBQQ__Quote__c objQuote = new SBQQ__Quote__c(Id=objQuot.Id,CLO_Total_Order_Amount__c = mapOrdTotAmt.get(objQuot.Id));
                updatequoteList.add(objQuote);
            }           
            
            if(!updatequoteList.isEmpty()){
                try{
                    CLO_DBManager.updateSObjects(updatequoteList);
                } Catch(DMLException de){
                    System.debug('@@@ updatequoteList DML :'+de.getMessage());
                } Catch(Exception e){
                    System.debug('@@@ updatequoteList EX :'+e.getMessage());
                }               
            }
        }
    }
    }
    
    @future(callout=true)
    Public Static Void CreateOrderLinesForOrderFromSAP(Set<Id> ordIdSet){
        List<Order> ordList = [SELECT Id, SAP_Order_Id__c, CLO_Created_From_SAP__c, CLO_Sales_Org__c FROM Order WHERE Id in: ordIdSet AND CLO_Created_From_SAP__c = true];
        for(Order ord : ordList){
            CLO_OrderCreationFromSAP.createOrderLines(ord);
        }
    }
    
    
    @future(callout=true)
    Public Static Void SubmitComsignmentTransferToSAP(Set<Id> ordIdSet){
        List<Order> ordList = [SELECT Id, OwnerId, OrderNumber, Owner.Name, SAP_Order_Id__c, AccountId, Account.AccountNumber, 
                               CLO_Created_From_SAP__c, CLO_Sales_Org__c,
                              (Select id, Quantity, Product2.ProductCode, CLO_Asset_Serial_Number__c, Location__c, Location__r.Name,
                               CLO_Batch_Number__c, CLO_Plant__c
                               from OrderItems) 
                               FROM Order 
                               WHERE Id in: ordIdSet];
        
        Set<String> ownerNameSet = new Set<String>();
        for(Order ord : ordList){
            ownerNameSet.add(ord.Owner.Name);
        }
        
        List<Account> accList = [select id, Name, AccountNumber from Account where Name in : ownerNameSet];
        Map<String, String> accMap = new Map<String, String>();
        
        for(Account a : accList)
        {
            accMap.put(a.Name, a.AccountNumber);
        }
        
        for(Order ord : ordList){
            List<SAPMoveConsignmentStock.ZCON_ITEMS> zconItemList = new List<SAPMoveConsignmentStock.ZCON_ITEMS>();
            for(OrderItem o : ord.OrderItems)
            {
                SAPMoveConsignmentStock.ZCON_ITEMS zconItems = new SAPMoveConsignmentStock.ZCON_ITEMS();
                zconItems.MATNR = o.Product2.ProductCode;
                zconItems.LFIMG = o.Quantity+'';
                zconItems.CHARG = (o.CLO_Plant__c != null ? o.CLO_Plant__c : o.Location__r.Name);
                zconItems.BWTAR = o.CLO_Batch_Number__c;
                zconItems.SERIAL = o.CLO_Asset_Serial_Number__c;
                
                zconItemList.add(zconItems);
            }
            
            CLO_SAP_MoveConsignmentStock.invokeMoveConsignmentService(zconItemList, ord.OrderNumber, ord.Account.AccountNumber, accMap.get(ord.Owner.Name), '', null, (ord.OrderItems[0].CLO_Plant__c != null ? ord.OrderItems[0].CLO_Plant__c : ord.OrderItems[0].Location__r.Name));
        }
    }
    
    @future
    public static void submitConsignmentOrderForApproval(Set<Id> ordIdSet)
    {
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        
        while ((finishTime - startTime) < 60000) {
            //sleep for 9s
            finishTime = DateTime.now().getTime();
        }

        List<Order> ordList = [SELECT Id, Account.Name, OwnerId, Owner.Name, SAP_Order_Id__c, AccountId, Account.AccountNumber, 
                               CLO_Created_From_SAP__c, CLO_Sales_Org__c
                               FROM Order 
                               WHERE Id in: ordIdSet];
        if(ordList.size() > 0)
        {
            Set<String> ownerNameSet = new Set<String>();
            for(Order ord : ordList){
                ownerNameSet.add(ord.Account.Name);
            }
            
            List<User> userList = [select id, Name from User where Name in: ownerNameSet];
            Map<String, String> accMap = new Map<String, String>();
        
            for(User u : userList)
            {
                accMap.put(u.Name, u.Id);
            }    
            
            for(Order o : ordList)
            {
                if(accMap.containskey(o.Account.Name))
                {        
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments('Submitting request for approval.');
                    req1.setObjectId(o.Id);
                    //Submit on behalf of a specific submitter
                    req1.setSubmitterId(UserInfo.getUserId());
                    req1.setNextApproverIds(new Id[] {accMap.get(o.Account.Name)});
                    // Submit the record to specific process and skip the criteria evaluation
                    req1.setProcessDefinitionNameOrId('Consignment_Transfer_Order_Approval');
                    req1.setSkipEntryCriteria(true);
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(req1);    
                }
            }
        }
    }
}

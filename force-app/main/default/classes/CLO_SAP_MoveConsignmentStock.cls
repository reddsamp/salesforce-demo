Public class CLO_SAP_MoveConsignmentStock
{
    public Static map<string, string> invokeMoveConsignmentService(List<SAPMoveConsignmentStock.ZCON_ITEMS> zconItems, String headerText, string intialCustomer, string newCustomer, string specStockPartner, Date goodsIssueDate, string plant)
    {
        SAPMoveConsignmentStock.TABLE_OF_ZCON_ITEMS CON_ITEMS = new SAPMoveConsignmentStock.TABLE_OF_ZCON_ITEMS();

        CON_ITEMS.item = new List<SAPMoveConsignmentStock.ZCON_ITEMS>();
        
        /*SAPMoveConsignmentStock.ZCON_ITEMS zconItems = new SAPMoveConsignmentStock.ZCON_ITEMS();
        zconItems.MATNR = materialNumber;
        zconItems.LFIMG = actualQuantity;
        zconItems.CHARG = batchNumber;
        zconItems.BWTAR = valuationType;
        zconItems.SERIAL = serialNumber;*/
        
        CON_ITEMS.item.addAll(zconItems);        
        
        String I_BKTXT = headerText;
        String I_DLV_REQ = '';
        String I_N_KUNNR = newCustomer;
        String I_O_KUNNR = intialCustomer;
        String I_SB_KUNNR = specStockPartner;
        //DateTime dtGoodIssueDate = DateTime.NewInstance(goodsIssueDate, Time.newInstance(0,0,0,0));
        String I_WADAT = null;//dtGoodIssueDate.format('yyyy-MM-dd');
        String I_WERKS = plant;
        SAPMoveConsignmentStock.TABLE_OF_BAPIRET2 RETURN_x = NULL;

        /*Authentication using bearer token*/
        SAPMoveConsignmentStock.ZSF_MOVE_CONSIGNMENT_STOCK moveConsignServiceObj = new SAPMoveConsignmentStock.ZSF_MOVE_CONSIGNMENT_STOCK();
        moveConsignServiceObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSF_MOVE_CONSIGNMENT_STOCK');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        moveConsignServiceObj.inputHttpHeaders_x = new Map<String,String>();
        moveConsignServiceObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        moveConsignServiceObj.timeout_x = 120000;
        
        SAPMoveConsignmentStock.ZSF_MOVE_CONSIGNMENT_STOCKResponse_element moveConsignResponseObj = moveConsignServiceObj.ZSF_MOVE_CONSIGNMENT_STOCK(CON_ITEMS, I_BKTXT, I_DLV_REQ, I_N_KUNNR, I_O_KUNNR, I_SB_KUNNR, I_WADAT, I_WERKS, RETURN_x);
        SAPMoveConsignmentStock.TABLE_OF_BAPIRET2 objBAPIRet2 = moveConsignResponseObj.RETURN_x;
        SAPMoveConsignmentStock.TABLE_OF_ZCON_ITEMS objTableZconItems = moveConsignResponseObj.CON_ITEMS;
                
        map<string, string> mapMoveConsignResponse = new map<string, string>();
        String message_v1, message_v2;
        
        for(SAPMoveConsignmentStock.BAPIRET2 retItem : objBAPIRet2.item){
            if(retItem.MESSAGE.contains(Label.CLO_MoveStockSuccessMessageSAP))
            {
                if(message_v1 == null) message_v1 = retItem.MESSAGE_V1;
                else if(message_v2 == null) message_v2 = retItem.MESSAGE_V1;
            }
            mapMoveConsignResponse.put('TYPE_x', retItem.TYPE_x);
            mapMoveConsignResponse.put('ID', retItem.ID);
            mapMoveConsignResponse.put('NUMBER_x', retItem.NUMBER_x);
            mapMoveConsignResponse.put('MESSAGE', retItem.MESSAGE);
            mapMoveConsignResponse.put('LOG_NO', retItem.LOG_NO);
            mapMoveConsignResponse.put('LOG_MSG_NO', retItem.LOG_MSG_NO);
            mapMoveConsignResponse.put('MESSAGE_V1', retItem.MESSAGE_V1);
            mapMoveConsignResponse.put('MESSAGE_V2', retItem.MESSAGE_V2);
            mapMoveConsignResponse.put('MESSAGE_V3', retItem.MESSAGE_V3);
            mapMoveConsignResponse.put('MESSAGE_V4', retItem.MESSAGE_V4);
            mapMoveConsignResponse.put('PARAMETER', retItem.PARAMETER);
            mapMoveConsignResponse.put('ROW', String.valueOf(retItem.ROW));
            mapMoveConsignResponse.put('FIELD', retItem.FIELD);
            mapMoveConsignResponse.put('SYSTEM_x', retItem.SYSTEM_x);
        }        
        
       for(SAPMoveConsignmentStock.ZCON_ITEMS zconItem : objTableZconItems.item){
            mapMoveConsignResponse.put('MATNR', zconItem.MATNR);
            mapMoveConsignResponse.put('LFIMG', zconItem.LFIMG);
            mapMoveConsignResponse.put('CHARG', zconItem.CHARG);
            mapMoveConsignResponse.put('BWTAR', zconItem.BWTAR);                       
       } 
       
       List<Order> oList = [select id, SAP_Order_Id__c, CLO_SAP_Sync_Error__c, Status from Order where OrderNumber =: headerText];
       
       if(oList.size() > 0){
           if(message_v1 != null && message_v2 != null)
           {
               oList[0].SAP_Order_Id__c = 'Mat. Document #: ' + message_v1 + ' & ' + message_v2;
               oList[0].Status = 'FS';
               
               update oList;
           }
           else{
               oList[0].CLO_SAP_Sync_Error__c = mapMoveConsignResponse.get('MESSAGE');
               
               update oList;
           }
       }
       
       system.debug('mapMoveConsignResponse :::::' + mapMoveConsignResponse);
       system.debug('mapMoveConsignResponse :::::' + mapMoveConsignResponse.get('ID'));
       system.debug('mapMoveConsignResponse :::::' + mapMoveConsignResponse.get('MESSAGE'));
        
       return mapMoveConsignResponse;

    }
}
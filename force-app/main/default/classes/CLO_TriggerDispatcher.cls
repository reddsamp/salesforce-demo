/**
  * @ClassName      : CLO_TriggerDispatcher 
  * @Description    : Trigger Dispatcher 
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */

  public class CLO_TriggerDispatcher {
     
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : It will invoke the appropriate methods on the handler depending on the trigger context.
      * @Parameters  : ITriggerHandler handler, string triggerName
      * @Return      : void 
      */
        public static void run(CLO_ITriggerHandler handler, string triggerName) {
            
            Boolean isActive; 
            //Check if the trigger is disabled
            if (handler.IsDisabled()) {
                return;
            }

            //TO-DO : Get the values from reusable methods in DBManager class
            List<CLO_TriggerSetting__mdt> triggerSettings = (List<CLO_TriggerSetting__mdt>) CLO_DBManager.getSObjectList('Id, DeveloperName, isActive__c', 'CLO_TriggerSetting__mdt', 'DeveloperName = ' +CLO_StringUtil.inquoteString(triggerName));

            //Get the trigger active information from custom settings by trigger name
            if(!CLO_CommonUtil.isNullOrEmpty(triggerSettings)) {
                isActive = triggerSettings[0].isActive__c;
            }
             
            if(isActive){
                //Check trigger context from trigger operation type
                switch on Trigger.operationType {
                     
                    when BEFORE_INSERT {
                        //Invoke before insert trigger handler
                        handler.beforeInsert(trigger.new);
                    }
                    when AFTER_INSERT {
                        //Invoke after insert trigger handler
                        handler.afterInsert(trigger.new, trigger.newMap);
                    }
                    when BEFORE_UPDATE {
                        //Invoke before update trigger handler
                        handler.beforeUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
                    }
                    when AFTER_UPDATE {
                        //Invoke after update trigger handler
                        handler.afterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
                    }
                    when BEFORE_DELETE {
                        //Invoke before delete trigger handler
                        handler.beforeDelete(trigger.old, trigger.oldMap);
                    }
                    when AFTER_DELETE {
                        //Invoke after delete trigger handler
                        handler.afterDelete(trigger.old, trigger.oldMap);
                    }
                    when AFTER_UNDELETE {
                        //Invoke after undelete trigger handler
                        handler.afterUnDelete(trigger.new, trigger.newMap);
                    }
                }
            }
        }
    }
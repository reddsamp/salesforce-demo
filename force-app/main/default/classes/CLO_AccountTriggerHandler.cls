/**
  * @ClassName      : CLO_AccountTriggerHandler 
  * @Description    : Account Object Trigger Handler.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */

  public class CLO_AccountTriggerHandler implements CLO_ITriggerHandler{
     
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
    public void beforeInsert(List<sObject> newList) {
       CLO_AccountTriggerHandler.updateParent((List<Account>) newList);
       CLO_AccountTriggerHelper.calAccountScore(newList);
    }
     
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {

    }
     
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
    public static void updateParent(List<Account> newList)
    {
        Set<String> parentAccNumbers = new Set<String>();
        Map<String, String> parentAccMap = new Map<String, String>();
        
        for(Account a : newList)
        {
            if(a.CLO_Parent_Account_Number__c != null)
                parentAccNumbers.add(a.CLO_Parent_Account_Number__c);
        }
        
        for(Account a : [select id, AccountNumber from Account where AccountNumber in : parentAccNumbers])
        {
            parentAccMap.put(a.AccountNumber, a.Id);
        }
        
        for(Account a : newList)
        {
            if(parentAccMap.containskey(a.CLO_Parent_Account_Number__c))
                a.ParentId = parentAccMap.get(a.CLO_Parent_Account_Number__c);
        }
    }
}
public class CLO_AMPS_Activities_Controller {

    @AuraEnabled(cacheable=true)
    public static List<Task> getAMPSTasks(String accountID) {
        System.debug(accountID);
        String Type = 'AMPS Task';
        String whereClause = 'WhatId = '+CLO_StringUtil.inquoteString(accountID)+'AND CLO_Type__c = '+CLO_StringUtil.inquoteString(CLO_Constants.AMPS_TASK)+' ORDER BY ActivityDate DESC';
        List<Task> accTasks = (List<Task>)CLO_DBManager.getSObjectList(' id, subject, Status, Owner.Name, Description,ActivityDate ', 'Task', whereClause);
        System.debug(accTasks);
        return accTasks;
    }
    @AuraEnabled(cacheable=true)
    public static Task saveAMPSTasks(String description,String TaskID) {
       
        Task u = new Task();
        u.Id =TaskID;
        u.Description = description;
        update u;
        return u;
    }
}
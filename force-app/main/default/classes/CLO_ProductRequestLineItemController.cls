public class CLO_ProductRequestLineItemController {
    
    public ProductRequestLineItem prli {get;set;}
    public String productCode {get;set;}
    public String prliId {get;set;}
    public String woId {get;set;}
    
    public CLO_ProductRequestLineItemController(ApexPages.StandardController controller) {
        prli = new ProductRequestLineItem();
        prli.ParentId = ApexPages.currentPage().getParameters().get('Parent_lkid');
        
        List<ProductRequest> prList = [select id, WorkorderId from ProductRequest where Id =: prli.ParentId];
        
        if(prList.size() > 0)
        {
            woId = prList[0].WorkorderId;
        }
    }
    
    public void savePRLI()
    {
        try
        {
            List<Product2> productList = [select id from Product2 where ProductCode =: productCode];
            
            if(productList.size() > 0)
            {
                prli.Product2Id = productList[0].Id;
                prli.QuantityUnitOfMeasure = 'Each';
                prli.CLO_Unit_Price__c = 1;
                
                insert prli;
                
                prliId = prli.Id;
            }
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
}
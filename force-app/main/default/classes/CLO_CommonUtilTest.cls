/**
  * @ClassName      : CLO_CommonUtilTest 
  * @Description    : This class is used to test CLO_CommonUtil.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */
@isTest 
public class CLO_CommonUtilTest {
    @testSetup static void setup() {
        // Create test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i));
        }
        CLO_DBManager.insertSObjects(testAccts); 
        
    }
    
    @isTest static void testIsNullOrEmpty() {
        Account acc;
        List<Account> lstAccounts;
        System.assertEquals(true, CLO_CommonUtil.isNullOrEmpty(acc),'success');
        System.assertEquals(true, CLO_CommonUtil.isNullOrEmpty(lstAccounts),'success');

        acc =  (Account) CLO_DBManager.getSObject('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct1') +' LIMIT 1');
        System.assertEquals(false, CLO_CommonUtil.isNullOrEmpty(acc),'success');

        // Get all account's by using a SOQL query
        lstAccounts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(false, CLO_CommonUtil.isNullOrEmpty(lstAccounts),'success');
    }
}
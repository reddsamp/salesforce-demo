Public Class CLO_SAP_ServiceContract
{
    public Static void invokeCreateServiceContract(ServiceContract serviceCon, boolean isSimulate)
    {
        /*Authentication using bearer token*/
        SAPOrderCreationServiceNew.ZSD_SALESDOCUMENT_CREATE createOrderServiceObj = new SAPOrderCreationServiceNew.ZSD_SALESDOCUMENT_CREATE();
        createOrderServiceObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSD_SALESDOCUMENT_CREATE');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        createOrderServiceObj.inputHttpHeaders_x = new Map<String,String>();
        createOrderServiceObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        createOrderServiceObj.timeout_x = 120000;
        /*
        if(!isSimulate && serviceCon.CLO_Payment_Terms__c == 'CRCD' && serviceCon.CLO_Payment_Card__c == null) {
            String errorMessage = 'Please select a Payment Card';
            update new ServiceContract(Id = serviceCon.Id, CLO_SAP_Sync_Error__c = '', CLO_Paymetric_Error__c = errorMessage);
            return;
        }
        */
        List<CLO_Paymetric_Payment__c> lstPayments = new List<CLO_Paymetric_Payment__c>();
        /*
        if(ServiceContract.SBQQSC__Quote__c != null)
        {
            Set<Id> quoteIds =new Set<Id>();
            quoteIds.add(serviceCon.SBQQSC__Quote__r.Id);
            lstPayments = [SELECT id, CLO_Authorization_Code__c, CLO_Authorization_Reference_Code__c, CLO_Credit_Card_Authorized_Date__c, CLO_Quote__c, CLO_TransactionID__c, CLO_StatusCode__c, CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c, CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c FROM CLO_Paymetric_Payment__c WHERE CLO_Quote__c = :quoteIds];
        }   
        System.debug('lstPayments :::: '+lstPayments);
        System.debug('lstPayments :::: CC Type ::  '+lstPayments[0].CLO_Payment_Card__r.CLO_CC_Type__c);
        */
        /*1st param in order service SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPICCARD CCARD_IN*/
        SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPICCARD CCARD_IN = new SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPICCARD();
        if(!isSimulate && serviceCon.CLO_Payment_Terms__c == 'CRCD')
        {
            if(serviceCon.SBQQSC__Quote__c == null)
            {
                String errorMessage = 'Please select Quote for Service Contract';
                update new ServiceContract(Id = serviceCon.Id, CLO_SAP_Sync_Error__c = errorMessage, CLO_Paymetric_Error__c = '');
                return;
            }
            /*
            Set<Id> quoteIds =new Set<Id>();
            quoteIds.add(serviceCon.SBQQSC__Quote__r.Id);
            */
            lstPayments = [SELECT id, CLO_Authorization_Code__c, CLO_Authorization_Reference_Code__c, CLO_Credit_Card_Authorized_Date__c, CreatedDate, CLO_Quote__c, CLO_TransactionID__c, CLO_StatusCode__c, CLO_Payment_Card__r.CLO_CC_Type__c, CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c, CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, CLO_Payment_Card__r.CLO_CC_Expiry_Month__c, CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c, CLO_Message__c,CLO_Credit_Card_Authorized_Time__c FROM CLO_Paymetric_Payment__c WHERE CLO_TransactionID__c = :serviceCon.CLO_Payment_Transction_ID__c AND CLO_StatusCode__c = '100' ORDER BY CreatedDate DESC LIMIT 1 ];
            System.debug('lstPayments :::: '+lstPayments);
            System.debug('lstPayments :::: CC Type ::  '+lstPayments[0].CLO_Payment_Card__r.CLO_CC_Type__c);
            /*
            Map<String, String>  mapResponse = CXiIntercept3.callRFC(serviceCon.CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c,
                                                                        serviceCon.CLO_Payment_Card__r.CLO_CC_Expiry_Month__c+'/'+serviceCon.CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, 
                                                                        serviceCon.CLO_Authorization_Amount__c, 
                                                                        serviceCon.CLO_Payment_Card__r.CLO_CC_Type__c, 
                                                                        serviceCon.Account.AccountNumber);
            System.debug('mapResponse ::: '+mapResponse);   
            String status;
            if (mapResponse.get('StatusCode') != null && mapResponse.get('StatusCode') != '' && Integer.ValueOf(mapResponse.get('StatusCode')) >= 0) {
                status = 'SUCCESS';
            } else {
                status = 'FAILED';
            }
            if(status == 'FAILED')
            {
                String errorMessage = ' Status Code : ' + mapResponse.get('StatusCode') + ' and ' + 'Status Message : ' + mapResponse.get('StatusTXN');
                update new ServiceContract(Id = serviceCon.Id, CLO_SAP_Sync_Error__c = '', CLO_Paymetric_Error__c = errorMessage);
                return;
            }
            */

            if(lstPayments == null || lstPayments.size() == 0) {
                String errorMessage = 'Payment Authorization is pending, Please Authorize using Make Payment button';
                update new ServiceContract(Id = serviceCon.Id, CLO_SAP_Sync_Error__c = '', CLO_Paymetric_Error__c = errorMessage);
                return ;
            }
            Map<String, String> ccTypeMap = new Map<String, String>{'AX' => 'AMEX',
                                                                'MC' => 'MasterCard',
                                                                'VI' => 'Visa',
                                                                'DI' => 'Discover'};
            
            if(lstPayments.size() > 0){
                SAPOrderCreationServiceNew.ZSF_BAPICCARD[] item = new SAPOrderCreationServiceNew.ZSF_BAPICCARD[]{};
                SAPOrderCreationServiceNew.ZSF_BAPICCARD bapIccCardItem = new SAPOrderCreationServiceNew.ZSF_BAPICCARD();
                
                CCARD_IN.item = new List<SAPOrderCreationServiceNew.ZSF_BAPICCARD>(); 
                bapIccCardItem.CC_TYPE = ccTypeMap.get(lstPayments[0].CLO_Payment_Card__r.CLO_CC_Type__c).toUpperCase();
                bapIccCardItem.CC_NUMBER = lstPayments[0].CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c;
                bapIccCardItem.CC_VALID_T = '20'+lstPayments[0].CLO_Payment_Card__r.CLO_CC_Expiry_Year__c+'-'+lstPayments[0].CLO_Payment_Card__r.CLO_CC_Expiry_Month__c+'-01';
                bapIccCardItem.CC_NAME = lstPayments[0].CLO_Payment_Card__r.CLO_Credit_Card_Name_on_Card__c;
                bapIccCardItem.BILLAMOUNT = '';
                bapIccCardItem.AUTH_FLAG = 'X';//serviceCon.CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c;
                bapIccCardItem.AUTHAMOUNT = serviceCon.CLO_Authorization_Amount__c != NULL ? String.valueOf(serviceCon.CLO_Authorization_Amount__c) : String.valueOf(serviceCon.CLO_NetAmount__c);//Is Auth Amount and bill Amount are same?
                bapIccCardItem.CURRENCY_x = serviceCon.CurrencyIsoCode;
                bapIccCardItem.CURR_ISO = serviceCon.CurrencyIsoCode;
                DateTime dtAuthDate = DateTime.NewInstance(lstPayments[0].CLO_Credit_Card_Authorized_Date__c, Time.newInstance(0,0,0,0));
                bapIccCardItem.AUTH_DATE = dtAuthDate.format('yyyy-MM-dd');
                DateTime dtAuthTime = DateTime.NewInstance(Date.newInstance(0,0,0), lstPayments[0].CLO_Credit_Card_Authorized_Time__c);
                bapIccCardItem.AUTH_TIME = dtAuthTime.format('HH:MM:SS');
                bapIccCardItem.AUTH_CC_NO = lstPayments[0].CLO_Authorization_Code__c;//mapResponse.get('AuthorizationCode');
                bapIccCardItem.AUTH_REFNO = lstPayments[0].CLO_TransactionID__c;//mapResponse.get('TransactionID');
                bapIccCardItem.CC_REACT = 'A';
                bapIccCardItem.CC_RE_AMOUNT = serviceCon.CLO_Authorization_Amount__c != NULL ? String.valueOf(serviceCon.CLO_Authorization_Amount__c) : String.valueOf(serviceCon.CLO_NetAmount__c);//Is Auth Amount and bill Amount are same? //String.valueOf(serviceCon.CLO_NetAmount__c);
                bapIccCardItem.CC_STAT_EX = 'C';
                bapIccCardItem.CC_REACT_T = lstPayments[0].CLO_Message__c;
                bapIccCardItem.DATAORIGIN = 'E';
                //bapIccCardItem.CC_VERIF_VALUE = serviceCon.CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c;

                CCARD_IN.item.add(bapIccCardItem);
            }       
        }
        
        /*2nd param in order service SAPOrderCreationServiceNew.TABLE_OF_BAPICCARD_EX CCARD_OUT*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPICCARD_EX CCARD_OUT = new SAPOrderCreationServiceNew.TABLE_OF_BAPICCARD_EX();
        
        /*3rd param in order service SAPOrderCreationServiceNew.TABLE_OF_BAPICOND CONDITIONS_OUT*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPICOND CONDITIONS_OUT = new SAPOrderCreationServiceNew.TABLE_OF_BAPICOND();
        
        /*4th param in order service SAPOrderCreationServiceNew.ZSF_BAPISDHD1 HEADER_IN*/
        SAPOrderCreationServiceNew.ZSF_BAPISDHD1 HEADER_IN = new SAPOrderCreationServiceNew.ZSF_BAPISDHD1();

        HEADER_IN.STATUS = serviceCon.Status;
        HEADER_IN.DOC_TYPE = 'ZWV2';
        HEADER_IN.DOC_DATE = serviceCon.CreatedDate.format('yyyy-MM-dd');
        HEADER_IN.SALES_ORG = serviceCon.CLO_Sales_Org__c;
        HEADER_IN.DISTR_CHAN = '10';//Hardcoded by design
        HEADER_IN.DIVISION = '10';//Hardcoded by design
        system.debug('SOLD_TO::'+ serviceCon.account.AccountNumber);
        system.debug('SHIP_TO::'+ serviceCon.account.CLO_Customer_Number__c);
        HEADER_IN.SOLD_TO = serviceCon.account.AccountNumber;
        HEADER_IN.SHIP_TO = serviceCon.CLO_Ship_To__c != NULL ? serviceCon.CLO_Ship_To__r.AccountNumber : serviceCon.account.AccountNumber;
        HEADER_IN.SALES_REP_EMAIL = serviceCon.Owner.Email;
        HEADER_IN.SB_REP_EMAIL = '';//Not needed
        HEADER_IN.NAME = serviceCon.ContractNumber;
        HEADER_IN.PO_METHOD = '';//Not Needed
        HEADER_IN.PURCH_NO_C = serviceCon.CLO_PONumber__c;
        HEADER_IN.PURCH_NO_S = serviceCon.CLO_PONumber__c;
        //DateTime dtPODate = DateTime.NewInstance(serviceCon.SBQQSC__CustomerSignedDate__c, Time.newInstance(0,0,0,0));
        HEADER_IN.PURCH_DATE = serviceCon.CreatedDate.format('yyyy-MM-dd');
        HEADER_IN.REQ_DATE_H = '';
        HEADER_IN.PMNTTRMS = serviceCon.CLO_Payment_Terms__c;
        HEADER_IN.INCOTERMS1 = serviceCon.CLO_Incoterm_1__c;
        HEADER_IN.INCOTERMS2 = serviceCon.CLO_Incoterm_2__c;
        HEADER_IN.ORD_REASON = serviceCon.CLO_Order_Reason__c;
        HEADER_IN.PRICE_DATE = serviceCon.CreatedDate.format('yyyy-MM-dd');
        DateTime dtOrderEffectiveDate = DateTime.NewInstance(serviceCon.StartDate, Time.newInstance(0,0,0,0));
        HEADER_IN.CT_VALID_F = dtOrderEffectiveDate.format('yyyy-MM-dd');
        DateTime dtOrderEndDate = serviceCon.EndDate != NULL ? DateTime.NewInstance(serviceCon.EndDate, Time.newInstance(0,0,0,0)) : NULL;
        HEADER_IN.CT_VALID_T = dtOrderEndDate != NULL ? dtOrderEndDate.format('yyyy-MM-dd') : '';
        HEADER_IN.SHIP_COND = serviceCon.CLO_Shipping_Condition__c;
        HEADER_IN.PP_SEARCH = '';//TBD
        HEADER_IN.CURRENCY_x = '';//TBD
        HEADER_IN.CURR_ISO = serviceCon.CurrencyIsoCode;
        HEADER_IN.VERSION = serviceCon.CLO_Opportunity_Type__c;
        HEADER_IN.ORDER_NOTES = serviceCon.Description;
        
        //Asset & auth amount
        HEADER_IN.ASSET_NUMBER = serviceCon.CLO_Asset_Number__c;
        HEADER_IN.ASSET_MATERIAL = serviceCon.CLO_Asset_Material_Number__c;     
        /*5th param in order service SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPISDITM ITEMS_IN*/
        SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPISDITM ITEMS_IN = new SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPISDITM();                        
        ITEMS_IN.item = new List<SAPOrderCreationServiceNew.ZSF_BAPISDITM>(); 
         
        for(ContractLineItem contractItem : serviceCon.ContractLineItems){
            SAPOrderCreationServiceNew.ZSF_BAPISDITM bapisdItm = new SAPOrderCreationServiceNew.ZSF_BAPISDITM();
            bapisdItm.ITM_NUMBER = contractItem.LineItemNumber;
            bapisdItm.HG_LV_ITEM = '';//contractItem.LineItemNumber;
            bapisdItm.PO_ITM_NO = String.valueOf(contractItem.LineItemNumber).RIGHT(6);
            bapisdItm.MATERIAL = contractItem.SBQQSC__Product__r.ProductCode;            
            bapisdItm.BATCH = ''; //TBD
            bapisdItm.REASON_REJ = '';//TBD
            bapisdItm.PLANT = serviceCon.CLO_Sales_Org__c;
            bapisdItm.STORE_LOC = '';
            bapisdItm.TARGET_QTY = String.valueOf(contractItem.SBQQSC__Quantity__c);
            bapisdItm.TARGET_QU = contractItem.SBQQSC__Product__r.Primary_Unit_of_Measure__c;
            bapisdItm.ITEM_PRICE = String.valueOf(contractItem.UnitPrice);
            bapisdItm.REQ_DATE = '';
            bapisdItm.ITEM_CATEG = ''; //TBD
            bapisdItm.CURR_ISO = serviceCon.CurrencyIsoCode;
            bapisdItm.SHORT_TEXT = '';//TBD 
            bapisdItm.PURCH_NO_S = contractItem.Id;
            bapisdItm.CUST_MAT35 = '';//TBD
            bapisdItm.REF_1 = '';//TBD
            bapisdItm.MATERIAL_LONG = '';//TBD
            bapisdItm.SERIAL_NUMBER = contractItem.CLO_Asset_Serial_Number__c;
            //Start date and end date needs to be add here
            //custom fields == null ? standard :custom   
            bapisdItm.CON_ST_DAT = contractItem.CLO_Start_Date__c != NULL ? String.valueOf(contractItem.CLO_Start_Date__c) : String.valueOf(contractItem.StartDate);         
            bapisdItm.CON_EN_DAT = contractItem.CLO_End_Date__c != NULL ? String.valueOf(contractItem.CLO_End_Date__c) : String.valueOf(contractItem.EndDate);         
            ITEMS_IN.item.add(bapisdItm);
        }
        
        /*6th param in order service SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPIITEMEX ITEMS_OUT*/
        SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPIITEMEX ITEMS_OUT = new SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPIITEMEX();
        
        /*7th param in order service SAPOrderCreationServiceNew.TABLE_OF_BAPIRET2 RETURN_x*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPIRET2 RETURN_x = new SAPOrderCreationServiceNew.TABLE_OF_BAPIRET2();
        
        /*8th param in order service SAPOrderCreationServiceNew.TABLE_OF_TDS_REPAIRI_POSNR_SERIAL SERIALS_IN*/
        //SAPOrderCreationServiceNew.TABLE_OF_TDS_REPAIRI_POSNR_SERIAL SERIALS_IN = new SAPOrderCreationServiceNew.TABLE_OF_TDS_REPAIRI_POSNR_SERIAL();
        
        /*9th param in order service TABLE_OF_BAPI1175_SOLURL SO_URLS*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPI1175_SOLURL SO_URLS = new SAPOrderCreationServiceNew.TABLE_OF_BAPI1175_SOLURL();
        SO_URLS.item = new List<SAPOrderCreationServiceNew.BAPI1175_SOLURL>();

        SAPOrderCreationServiceNew.BAPI1175_SOLURL itemSOLURL = new SAPOrderCreationServiceNew.BAPI1175_SOLURL();
        String filesListView = 'https://{0}/lightning/r/{1}/related/AttachedContentDocuments/view';
        String urlInstance = String.valueof(System.URL.getSalesforceBaseURL().gethost());
        List<String> params =  new List<String>{urlInstance, String.valueOf(serviceCon.Id)};
        itemSOLURL.URL = String.Format(filesListView, params);
        SO_URLS.item.add(itemSOLURL);

        /*10th and last param in order service String TESTRUN*/
        String TESTRUN = isSimulate ? 'X' : '';
        
        SAPOrderCreationServiceNew.ZSD_SALESDOCUMENT_CREATEResponse_element responseObj = createOrderServiceObj.ZSD_SALESDOCUMENT_CREATE(CCARD_IN,CCARD_OUT,CONDITIONS_OUT,HEADER_IN,ITEMS_IN,ITEMS_OUT,RETURN_x,SO_URLS,TESTRUN);
        
        //system.debug('Response::'+String.valueOf(JSON.serializePretty(responseObj)));
        //system.debug('Response::'+responseObj.RETURN_x.item[0].MESSAGE);
        
        //if(TESTRUN == 'X')
        //{
            List<ContractLineItem> lstItems = [Select Id, SBQQSC__PackageProductDescription__c, LineItemNumber, SBQQSC__AdditionalDiscountAmount__c, ListPrice, TotalPrice, CLO_Tax__c FROM ContractLineItem Where ServiceContractId =: serviceCon.Id];
            List<ContractLineItem> LstItemsToUpdate = new List<ContractLineItem>();
            system.debug('responseObj----->>>'  + responseObj);
            system.debug('responseObj::ITEMS_OUT----->>>'  + responseObj.ITEMS_OUT);
            system.debug('responseObj::ITEMS_OUT::item----->>>'  + responseObj.ITEMS_OUT.item);
            if(responseObj.ITEMS_OUT.item != NULL && responseObj.ITEMS_OUT.item.size() > 0){
                for(SAPOrderCreationServiceNew.ZSF_BAPIITEMEX item : responseObj.ITEMS_OUT.item){
                    for(ContractLineItem itemToUpdate : lstItems){
                        string itmNo = String.valueOf(itemToUpdate.LineItemNumber).RIGHT(6);
                        /*if(String.isNotBlank(itemToUpdate.CLO_Primary_Line_Number__c)){
                            itmNo = removeLeadingZeros(itemToUpdate.CLO_Primary_Line_Number__c);
                        }
                        else{
                            itmNo = itemToUpdate.LineItemNumber.subString(4);
                        }*/
                        system.debug('item.PO_ITM_NO::'+item.PO_ITM_NO);                
                        system.debug('itmNo::'+itmNo);                
                        if(item.PO_ITM_NO == itmNo){
                            itemToUpdate.CLO_Tax__c = Decimal.valueOf(item.TX_DOC_CUR);
                            /*itemToUpdate.ListPrice = Decimal.valueOf(item.SUBTOTAL1);
                            //itemToUpdate.TotalPrice = Decimal.valueOf(item.SUBTOTAL2);
                            if(Decimal.valueOf(item.SUBTOTAL5) != 0)
                            itemToUpdate.CLO_Total_Discount_Amount__c = Decimal.valueOf(item.SUBTOTAL5);
                            itemToUpdate.SBQQSC__PackageProductDescription__c = item.SHORT_TEXT;
                            /*itemToUpdate.CLO_Cost_in_document_currency__c = Decimal.valueOf(item.COST_DOC_C);
                            itemToUpdate.CLO_Material_group_1__c = item.MAT_GROUP1;
                            itemToUpdate.CLO_PC_Bypass_and_SF_Id__c = item.MAT_GROUP2;
                            itemToUpdate.CLO_Embedded_Warranty__c = item.MAT_GROUP3;
                            itemToUpdate.Capital_Item__c = item.MAT_GROUP4;
                            itemToUpdate.CLO_Weight__c = item.MAT_GROUP5;
                            itemToUpdate.CLO_Installation_Included__c = Boolean.valueOf(item.PROD_ATTR1);
                            itemToUpdate.CLO_Clinical_Training_Included__c = Boolean.valueOf(item.PROD_ATTR2);
                            itemToUpdate.CLO_Preceptorship__c = Boolean.valueOf(item.PROD_ATTR3);
                            */                           
                            LstItemsToUpdate.add(itemToUpdate);
                        }
                    }
                    
                }
                
                if(LstItemsToUpdate.size() > 0){
                    update LstItemsToUpdate;
                }
                
                if(responseObj.AUTH_AMOUNT != NULL){
                    update new ServiceContract(Id = serviceCon.Id, CLO_Authorization_Amount__c = Decimal.ValueOf(responseObj.AUTH_AMOUNT));
                }                

            }
            
           
        //}
        system.debug('responseObj.SALESDOCUMENT_EX::'+responseObj.SALESDOCUMENT_EX);
        if(responseObj.SALESDOCUMENT_EX != null && responseObj.SALESDOCUMENT_EX.trim().length() > 0)
        {
            if(TESTRUN != 'X'){
                update new ServiceContract(Id = serviceCon.Id, SAP_Order_Id__c = responseObj.SALESDOCUMENT_EX, Synced_To_SAP__c = true);
            }
            
        }
        else 
        {
            String errorMessage = '';
            if(responseObj.RETURN_x != NULL && responseObj.RETURN_x.item != NULL){
                for(SAPOrderCreationServiceNew.BAPIRET2 bapiReturn : responseObj.RETURN_x.item){
                    if(bapiReturn.TYPE_x == 'E'){
                        errorMessage += bapiReturn.Message + '\n';
                    }
                } 
                update new ServiceContract(Id = serviceCon.Id, CLO_Paymetric_Error__c = '', CLO_SAP_Sync_Error__c = errorMessage);
            }
        }
    }
}
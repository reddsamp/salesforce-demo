/**
  * @Interfacename  : CLO_ITriggerHandler
  * @Description    : Trigger Handler Interface
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */

  public interface CLO_ITriggerHandler{
     
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework before insert of the records
      * @Parameters  : List<sObject> newList 
      * @Return      : void 
      */
        void beforeInsert(List<sObject> newList);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework after insert of the records
      * @Parameters  : List<sObject> newList, Map<Id, sObject> newMap
      * @Return      : void 
      */
        void afterInsert(List<sObject> newList, Map<Id, sObject> newMap);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework before update of the records
      * @Parameters  : List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap
      * @Return      : void 
      */
        void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework after update of the records
      * @Parameters  : List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap
      * @Return      : void 
      */
        void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework before delete of the records
      * @Parameters  : List<sObject> oldList , Map<Id, sObject> oldMap
      * @Return      : void 
      */           
        void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework after delete of the records
      * @Parameters  : List<sObject> oldList , Map<Id, sObject> oldMap
      * @Return      : void 
      */
        void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework after undelete of the records
      * @Parameters  : List<sObject> newList, Map<Id, sObject> newMap
      * @Return      : void 
      */
        void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap);
         
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 22th June 2020
      * @Description : Called by the trigger framework to check the trigger for the object is enabled or disabled
      * @Parameters  : None
      * @Return      : Boolean 
      */
        Boolean isDisabled();
    }
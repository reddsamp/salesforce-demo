global class CLO_SAP_MaterialAvailability
{
    global Static List<MaterialAvailabilityWrapper> invokeMaterialAvailabilityService(string salesOrg, string plant, string material, string repEmail, string accountNumber, String consignmentStock)
    {
        /*Authentication using bearer token*/
        SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITY materialServiceObj = new SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITY();
        materialServiceObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSF_MATERIAL_AVAILABILITY');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        materialServiceObj.inputHttpHeaders_x = new Map<String,String>();
        materialServiceObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        materialServiceObj.timeout_x = 120000;
        
        String I_CONS_STOCK = consignmentStock;
        
        String I_CUSTOMER = accountNumber;
        String I_MATERIAL = material;
        String I_PLANT = plant;
        String I_SALES_ORG = salesOrg;
        String I_SALES_REP_EMAIL = repEmail;
        
        SAPMaterialAvailabilityService.TABLE_OF_ZMAT_STOCK T_MAT_STOCK = new SAPMaterialAvailabilityService.TABLE_OF_ZMAT_STOCK();
        
        SAPMaterialAvailabilityService.ZSF_MATERIAL_AVAILABILITYResponse_element responseObj = materialServiceObj.ZSF_MATERIAL_AVAILABILITY(I_CONS_STOCK, I_CUSTOMER, I_MATERIAL, I_PLANT, I_SALES_ORG, I_SALES_REP_EMAIL, T_MAT_STOCK); 
        
        system.debug('responseObj---->>>>'+responseObj);
        system.debug('responseObj---->>>>'+responseObj.T_MAT_STOCK);
        
        map<string, string> responseMap = new map<string, string>();

        responseMap.put('Type', responseObj.E_RETURN.TYPE_x);            
        responseMap.put('Code', responseObj.E_RETURN.Code);
        responseMap.put('Message', responseObj.E_RETURN.Message);
        responseMap.put('Log_No', responseObj.E_RETURN.Log_No);
        responseMap.put('LOG_MSG_NO', responseObj.E_RETURN.LOG_MSG_NO);
        responseMap.put('Message_V1', responseObj.E_RETURN.Message_V1);
        responseMap.put('Message_V2', responseObj.E_RETURN.Message_V2);
        responseMap.put('Message_V3', responseObj.E_RETURN.Message_V3);
        responseMap.put('Message_V4', responseObj.E_RETURN.Message_V4);
        
        List<MaterialAvailabilityWrapper> responseWapper = new List<MaterialAvailabilityWrapper>();
        
        if(responseObj.T_MAT_STOCK != null && responseObj.T_MAT_STOCK.item != null)
        {
            Integer count = 1;
            Set<String> productCodeSet = new Set<String>();
            Set<String> accNumSet = new Set<String>();
            
            for(SAPMaterialAvailabilityService.ZMAT_STOCK stock : responseObj.T_MAT_STOCK.item){            
                responseMap.put('MATNR', stock.MATNR);
                responseMap.put('WERKS', stock.WERKS);
                responseMap.put('LABST', stock.LABST);
                responseMap.put('CHARG', stock.CHARG);
                responseMap.put('SERNR', stock.SERNR);
                
                productCodeSet.add(stock.MATNR);
                accNumSet.add(stock.KUNNR);
                
                responseWapper.add(new MaterialAvailabilityWrapper(stock.MATNR, stock.WERKS, stock.KUNNR, stock.LABST, stock.CHARG, stock.SERNR, count));
                count++;
            }
            
            Map<String, Product2> pcDescMap = new Map<String, Product2>();
            for(Product2 p : [select id, ProductCode, Description, Primary_Unit_of_Measure__c from Product2 where ProductCode in : productCodeSet])
            {
                pcDescMap.put(p.ProductCode, p);
            }
            
            Map<String, String> accMap = new Map<String, String>();
            for(Account a : [select id, Name, AccountNumber from Account where AccountNumber in : accNumSet])
            {
                accMap.put(a.AccountNumber, a.Name);
            }
            
            for(MaterialAvailabilityWrapper r : responseWapper)
            {
                if(pcDescMap.containskey(r.MATNR))
                {
                    r.description = pcDescMap.get(r.MATNR).Description;
                    r.UoM = pcDescMap.get(r.MATNR).Primary_Unit_of_Measure__c;
                }
                r.customerName = accMap.get(r.KUNNR);
            }
        }

        return responseWapper;
    }
    
    
    global class MaterialAvailabilityWrapper {
        @AuraEnabled global String MATNR{get;set;}
        @AuraEnabled global String WERKS{get;set;}
        @AuraEnabled global String KUNNR{get;set;}
        @AuraEnabled global String LABST{get;set;}
        @AuraEnabled global String CHARG{get;set;}
        @AuraEnabled global String SERNR{get;set;}
        @AuraEnabled global Integer SRNO{get;set;}
        @AuraEnabled global Integer quantity {get;set;}
        @AuraEnabled global String description{get;set;}
        @AuraEnabled global String customerName{get;set;}
        @AuraEnabled global boolean isSelected{get;set;}
        @AuraEnabled global String UoM{get;set;}
        @AuraEnabled global boolean showQty{get;set;}
        
        global MaterialAvailabilityWrapper(String MATNR,
                                           String WERKS,
                                           String KUNNR,
                                           String LABST,
                                           String CHARG,
                                           String SERNR,
                                           Integer SRNO)
        {
            this.MATNR = MATNR;
            this.WERKS = WERKS;
            this.KUNNR = KUNNR;
            this.LABST = LABST;
            this.CHARG = CHARG;
            this.SERNR = SERNR;
            this.SRNO = SRNO;
            this.isSelected = false;
            this.quantity = 1;
            
            try
            {
                if(this.LABST != null && Integer.valueOf(this.LABST) == 1)
                    this.quantity = 1;
                else
                    this.showQty = true;
            }
            catch(Exception ex){
                this.quantity = 0;
            }
        }
    }
}
global class CLO_MaterialAviabilityController{
    
    global List<List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>> dataList {get;set;}
    
    global CLO_MaterialAviabilityController(ApexPages.StandardController controller) {
        dataList = new List<List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>>();
        
        String woId = ApexPages.currentPage().getParameters().get('id');
        String productCode = ApexPages.currentPage().getParameters().get('productcode');
        
        List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> tempList = fetchFromWorkOrder(woId, productcode);
        List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> rowList =  new List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>();
        
        Integer count = 0;
        for(CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper s : tempList)
        {
            count++;
            rowList.add(s);
            
            if(count == 999)
            {
                count = 0;
                dataList.add(rowList);
                rowList = new List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>();
                break;
            }
        }
        
        if(rowList.size() > 0)
            dataList.add(rowList);
    }

    
    @AuraEnabled
    global static List<String> getSalesOrg(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.Industry.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
    
    
    @AuraEnabled
    global static List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> fetchFromOrder(String orderId)
    {
        List<Order> orderList = [select id, AccountId, Account.AccountNumber, Account.CLO_Sales_Org__c, Account.CLO_Delivering_Plant__c
                                 from Order
                                 where Id =: orderId];
                                 
        return CLO_MaterialAviabilityController.fetchMaterialAvaliablity(orderList[0].Account.CLO_Sales_Org__c, 
                                                                         '',//orderList[0].Account.CLO_Delivering_Plant__c, 
                                                                         '', 
                                                                         '', 
                                                                         orderList[0].Account.AccountNumber, 
                                                                         '');
    }
    
    
    @AuraEnabled
    global static List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> fetchFromWorkOrder(String woId, String productcode)
    {
        List<WorkOrder> orderList = [select id, AccountId, Account.AccountNumber, Account.CLO_Sales_Org__c, Account.CLO_Delivering_Plant__c
                                     from WorkOrder
                                     where Id =: woId];
                                 
        return CLO_MaterialAviabilityController.fetchMaterialAvaliablity(orderList[0].Account.CLO_Sales_Org__c, 
                                                                         '',//orderList[0].Account.CLO_Delivering_Plant__c, 
                                                                         productcode, 
                                                                         '', 
                                                                         '', 
                                                                         '');
    }
    
    
    @AuraEnabled
    @RemoteAction
    global static String saveWOProduct(String woId, String productCode)
    {
        if(productCode == 'remove')
        {
            update new Workorder(Id = woId,
                                 CLO_Product__c = null);
        }
        else
        {
            List<Product2> productList = [select id
                                         from Product2
                                         where ProductCode =: productCode];
                                     
            if(productList.size() > 0)
            {
                update new Workorder(Id = woId,
                                     CLO_Product__c = productList[0].Id);
            }
        }
        
        return 'Success';
    }
    
    
    @AuraEnabled
    global static String saveProducts(String orderId, List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> dList)
    {
        system.debug('items :::::' + dList);
        List<Order> orderList = [select id, AccountId, Account.AccountNumber, Account.CLO_Delivering_Plant__c, Pricebook2Id,
                                 CLO_Ship_To__c, CLO_Ship_To__r.AccountNumber
                                 from Order
                                 where Id =: orderId];
        
        Set<String> productCodes = new Set<String>();
        if(dList != null && dList.size() > 0)
        {
            List<SAPMoveConsignmentStock.ZCON_ITEMS> zconItems = new List<SAPMoveConsignmentStock.ZCON_ITEMS>();
                
            for(CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper c : dList){
                if(c.isSelected)
                {
                    productCodes.add(c.MATNR);
                    
                    SAPMoveConsignmentStock.ZCON_ITEMS zconItem = new SAPMoveConsignmentStock.ZCON_ITEMS();
                    zconItem.MATNR = c.MATNR;
                    zconItem.LFIMG = c.quantity+'';
                    zconItem.CHARG = c.CHARG;
                    //zconItem.BWTAR = valuationType;
                    zconItem.SERIAL = c.SERNR;
                    
                    zconItems.add(zconItem);
                }
            }
            
            if(productCodes.size() > 0)
            {
                List<PriceBookEntry> pbeList = [select id, UnitPrice from PriceBookEntry 
                                                where PriceBook2Id =: orderList[0].Pricebook2Id
                                                and Product2.ProductCode in : productCodes];
                
                if(pbeList.size() > 0)
                {
                    List<OrderItem> orderItemList = new List<OrderItem>();
                    
                    for(PriceBookEntry pbe : pbeList)
                    {
                        orderItemList.add(new OrderItem(OrderId = orderId,
                                                        PriceBookEntryId = pbe.Id,
                                                        Quantity = 1,
                                                        UnitPrice = pbe.UnitPrice));
                    }
                    
                    if(orderItemList.size() > 0)
                    {
                        insert orderItemList;
                    }
                }
                
                /*CLO_SAP_MoveConsignmentStock.invokeMoveConsignmentService(zconItems, 
                                                                          '', 
                                                                          orderList[0].Account.AccountNumber, 
                                                                          orderList[0].CLO_Ship_To__r.AccountNumber, 
                                                                          '', 
                                                                          null, 
                                                                          orderList[0].Account.CLO_Delivering_Plant__c);*/
            }
        }
                                 
        return 'Success';
    }
    
    
    @AuraEnabled
    global static List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> fetchMaterialAvaliablity(String selectedSalesOrg, 
                                                String selectedPlant,
                                                String selectedMaterial,
                                                String selectedEmail,
                                                String selectedCustomer,
                                                String consignmentStock)
    {
    
        List<Account> accList = [select id, AccountNumber from Account where id =: selectedCustomer];
        
        String accountNumber;
        
        if(accList.size() > 0)
            accountNumber = accList[0].AccountNumber;
        
        //SAPMaterialAvailabilityService.
        List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> responseList = CLO_SAP_MaterialAvailability.invokeMaterialAvailabilityService(selectedSalesOrg, 
                                                                                                              selectedPlant, 
                                                                                                              selectedMaterial, 
                                                                                                              selectedEmail, 
                                                                                                              accountNumber,
                                                                                                              consignmentStock);
        system.debug('responseList :::::' + responseList);
        
        return responseList;
    }
    
    global class MaterialAvailabilityWrapper {
        @AuraEnabled global String MATNR{get;set;}
        @AuraEnabled global String WERKS{get;set;}
        @AuraEnabled global String KUNNR{get;set;}
        @AuraEnabled global String LABST{get;set;}
        @AuraEnabled global String CHARG{get;set;}
        @AuraEnabled global String SERNR{get;set;}
        @AuraEnabled global Integer SRNO{get;set;}
        @AuraEnabled global Integer quantity {get;set;}
        @AuraEnabled global String description{get;set;}
        @AuraEnabled global String customerName{get;set;}
        @AuraEnabled global boolean isSelected{get;set;}
        @AuraEnabled global String UoM{get;set;}
        @AuraEnabled global boolean showQty{get;set;}
        
        global MaterialAvailabilityWrapper(){}
        
        global MaterialAvailabilityWrapper(String MATNR,
                                           String WERKS,
                                           String KUNNR,
                                           String LABST,
                                           String CHARG,
                                           String SERNR,
                                           Integer SRNO)
        {
            this.MATNR = MATNR;
            this.WERKS = WERKS;
            this.KUNNR = KUNNR;
            this.LABST = LABST;
            this.CHARG = CHARG;
            this.SERNR = SERNR;
            this.SRNO = SRNO;
            this.isSelected = false;
            this.quantity = 1;
            if(this.LABST != null && Integer.valueOf(this.LABST) == 1)
                this.quantity = 1;
            else
                this.showQty = true;
        }
    }
    
    @AuraEnabled
    public static String queryAccountId(String cName)
    {
        List<Account> accountList = [select id from Account where Name =: cName];
        
        if(accountList.size() > 0)
            return accountList[0].Id;
        
        return 'Error';
    }
    
    @AuraEnabled
    public static String submitForApproval(String orderId, String productCode, List<MaterialAvailabilityWrapper> items){
        //List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper> itemList = (List<CLO_SAP_MaterialAvailability.MaterialAvailabilityWrapper>) items;
        List<Order> orderList = [select id, AccountId, Account.Name, Pricebook2Id from Order where id =: orderId];
        
        system.debug('items :::::' + items);
        
        if(orderList.size() > 0)
        {
            Set<String> prductCodes = new Set<String>();
            
            for(MaterialAvailabilityWrapper w : items){
                prductCodes.add(w.MATNR);
            }
            
            Map<String, PricebookEntry> pbeMap = new Map<String, PricebookEntry>();
            
            List<PricebookEntry> pbeList = [select id, UnitPrice, Product2.ProductCode from PriceBookEntry 
                                            where Pricebook2Id =: orderList[0].Pricebook2Id
                                            and Product2.ProductCode in: prductCodes];
            
            for(PricebookEntry pbe : pbeList)
            {
                pbeMap.put(pbe.Product2.ProductCode, pbe);
            }
            
            if(pbeMap.size() > 0)
            {        
                List<OrderItem> oiList = new List<OrderItem>();
                                        
                for(MaterialAvailabilityWrapper w : items){
                    PricebookEntry pbe = pbeMap.get(w.MATNR);
                    
                    OrderItem oi = new OrderItem(OrderId = orderId,
                                                 Quantity = w.quantity,
                                                 PriceBookEntryId = pbe.Id,
                                                 UnitPrice = pbe.UnitPrice,
                                                 CLO_Plant__c = w.WERKS,
                                                 CLO_Batch_Number__c = w.CHARG,
                                                 CLO_Asset_Serial_Number__c = w.SERNR);
                    
                    oiList.add(oi);
                }
                
                if(oiList.size() > 0)
                    insert oiList;
            }
            else
            {
                delete orderList;
                return 'Pricebook entry not found with selected Material.';
            }
        }
        
        return 'Success';
    }     
}
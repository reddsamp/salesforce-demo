public class CLO_SAPAccountSyncController
{
    
    public CLO_SAPAccountSyncController()
    {
    
    }
    
    @AuraEnabled
    public static Account getAccount(String accountId)
    {
        system.debug('SDFSDF :::::');
        return [select Id, AccountNumber, CLO_SAP_Sync_Error__c, RecordType.Name from Account where id =: accountId];
    }
    
    @AuraEnabled
    public static Account syncWithSAP(String accountId)
    {
        List<Account> accountList = [select Id, AccountNumber, BillingStreet, BillingCity, BillingState, BillingCountry, Name, RecordType.Name,
                                     CLO_SAP_Sync_Error__c, CurrencyISOCode, Phone, BillingCountryCode, BillingStateCode, BillingPostalCode, CLO_Price_Group__c,
                                     ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingCountryCode, ShippingStateCode, ShippingPostalCode,
                                     CLO_Sales_Org__c, Parent.CLO_Sales_Org__c, CLO_Shipping_Condition__c, RecordTypeId,
                                     CLO_Incoterm_1__c, CLO_Incoterm_2__c, Fax, CLO_Billing_Email__c, CLO_Customer_Group__c, ParentId, CLO_BP_Group__c 
                                     from Account where id =: accountId];
        
        CLO_SAP_ServiceInvoker.invokeCreateAccountServiceFromAccount(JSON.serialize(accountList [0]));
        
        return [select Id, AccountNumber, CLO_SAP_Sync_Error__c, RecordType.Name from Account where id =: accountId];
    }
}
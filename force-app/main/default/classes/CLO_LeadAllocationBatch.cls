/*----------------------------------------------------------------------------------------------------------------------------------------------
Author:     Mani Sundaresan
Description:  Batch job to distribute waypoints. Invoked from WaypointAssignment apex class.
Test Class: waypointAssignment_Test
        
        
 History:
<Date>      <Authors Name>    <Brief Description of Change>
12/25/2019  Mani Sundaresan    Initial Creation (US-8360)
-------------------------------------------------------------------------------------------------------------------------------------------------*/

global class CLO_LeadAllocationBatch implements Database.Batchable<SObject>, Database.Stateful {

    public Lead_Sheet__c sz { get; set; }
    public List<Lead_Team__c> szt { get; set; }
    public List<Lead> swpList { get; set; }
    public List<Lead> swpworkingList = new List<Lead> ();
    public List<Id> deleteswp = new List<Id> ();
    public Map<String, Decimal> monMap = new Map<String, Decimal> ();
    public Map<String, Decimal> tueMap = new Map<String, Decimal> ();
    public Map<String, Decimal> wedMap = new Map<String, Decimal> ();
    public Map<String, Decimal> thurMap = new Map<String, Decimal> ();
    public Map<String, Decimal> friMap = new Map<String, Decimal> ();
    public Boolean allocated { get; set; }
    public Integer ordCount = 0;
    global final String query;
    public List<String> wpStatus = new List<String> { 'Open', 'Valid', 'Call Back' };
    public Boolean monCompleted = false;
    public Boolean tueCompleted = false;
    public Boolean wedCompleted = false;
    public Boolean thurCompleted = false;
    public Boolean friCompleted = false;
    public Integer monCount = 0;
    public Integer tueCount = 0;
    public Integer wedCount = 0;
    public Integer thurCount = 0;
    public Integer friCount = 0;
    
    //Constructor to pass in values from waypointAllocation apex class
    global CLO_LeadAllocationBatch(Lead_Sheet__c sz, List<Lead_Team__c> szt, Map<String, Decimal> monMap,
                                   Map<String, Decimal> tueMap, Map<String, Decimal> wedMap, Map<String, Decimal> thurMap, Map<String, Decimal> friMap) {
        this.sz = sz;
        this.szt = szt;
        this.monMap = monMap;
        this.tueMap = tueMap;
        this.wedMap = wedMap;
        this.thurMap = thurMap;
        this.friMap = friMap;
        
        query = 'SELECT Id, Name, OwnerId, Owner.Name, Target_Call_Date__c, Optimal_Date__c, Order__c, Status, Look_Forward_Date__c from Lead where Lead_Sheet__c = \'' + sz.id + '\' AND Status IN :wpStatus ORDER BY Order__c ASC';
    }

    /**
     * @description gets invoked when the batch job starts
     * @param context contains the job ID
     * @returns the record set as a QueryLocator object that will be batched for execution
     */
    global Database.QueryLocator start(Database.BatchableContext context) {
        System.debug('sz:' + sz);
        System.debug('szt:' + szt);
        System.debug('swpList:' + swpList);
        System.debug('monMap:' + monMap);
        System.debug('tueMap:' + tueMap);
        System.debug('wedMap:' + wedMap);
        System.debug('thurMap:' + thurMap);
        System.debug('friMap:' + friMap);
        
        //Trigger_Switch__c pmpSwitch = Trigger_Switch__c.getValues('UpdateAlignment');
        //pmpSwitch.Active__c = false;
        
        //update pmpSwitch;

        
        return Database.getQueryLocator(query);
    }

    /**
     * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
     * @param context contains the job ID
     * @param scope contains the batch of records to process.
     */
    global void execute(Database.BatchableContext context, List<Lead> scope) {

        System.debug('scope size:'+scope.size());
        swpList = new List<Lead>();

        for (Lead swp : scope) {
            ordCount++;
            swp.Order__c = ordCount;
            swpworkingList.add(swp);
            System.debug('workinglist:' + swp);
            System.debug('swpWorkingList:' + swpworkingList);
        }
        
        List<Lead> swpList1 = swpworkingList;
        List<Lead> swpList2 = swpList1;
        List<Integer> countList = new List<Integer> ();

        System.debug('swpList1 size:' + swpList1.size());
        System.debug('swpList2 size:' + swpList2.size());
        System.debug('swpList size:' + swpList.size());

        if (monMap.size() > 0 && !monCompleted) {
            Integer count = 0;
            Integer count1 = 0;
            Integer mapCount = 0;
            for (String s : monMap.keySet()) {
                mapCount += Integer.valueOf(monMap.get(s));
                for (Integer i = 0; i<swpList2.size(); i++) {
                    if (count<monMap.get(s) && count1<swpList2.size() && monCount < mapCount) {
                        System.debug('count1:' + count1);
                        System.debug('Order#:' + swpList2[count1].Order__c);
                        swpList2[count1].OwnerId = s.split('-') [0];
                        if (sz.Start_Call_Date__c != null)
                        swpList2[count1].Target_Call_Date__c = sz.Start_Call_Date__c;
                        if (sz.Start_Optimal_Date__c != null)
                        swpList2[count1].Optimal_Date__c = sz.Start_Optimal_Date__c;
                        if (sz.Look_Forward_Date__c != null)
                        swpList2[count1].Look_Forward_Date__c = sz.Look_Forward_Date__c;
                        swpList.add(swpList2[count1]);
                        countList.add(count1);
                        count++;
                        count1++;
                        monCount++;
                        System.debug('count:' + count);
                        System.debug('count1:' + count1);
                    }
                }
                count = 0;
            }
            System.debug('count1:'+count1);
            System.debug('mapCount:'+mapCount);
            if(count1 == mapCount)
                monCompleted = true;
            System.debug('swpList3 size:' + countList.size());
            System.debug('swpList4 size:' + swpList2.size());
            System.debug('swpList1 size:' + swpList1.size());
            System.debug('swpList size:' + swpList.size());

            if (countList.size()> 0)
            for (Integer i = countList.size() - 1; i >= 0; i--) {
                System.debug('list val:' + swpList1.get(i));
                swpList1.remove(i);
            }

            System.debug('swpList99 size:' + swpList1.size());
            System.debug('swpList100' + swpList1);
        }

        if (tueMap.size()> 0 && !tueCompleted) {
            System.debug('swpList1 size:' + swpList1);
            swpList2 = null;
            swpList2 = swpList1;
            Integer count = 0;
            Integer count1 = 0;
            Integer mapCount = 0;
            countList.clear();
            for (String s : tueMap.keySet()) {
                mapCount += Integer.valueOf(tueMap.get(s));
                for (Integer i = 0; i<swpList2.size(); i++) {
                    if (count<tueMap.get(s) && count1<swpList2.size() && tueCount < mapCount) {
                        System.debug('count1:' + count1);
                        System.debug('Order#:' + swpList2[count1].Order__c);
                        swpList2[count1].OwnerId = s.split('-') [0];
                        if (sz.Start_Call_Date__c != null)
                        swpList2[count1].Target_Call_Date__c = sz.Start_Call_Date__c.addDays(1);
                        if (sz.Start_Optimal_Date__c != null)
                        swpList2[count1].Optimal_Date__c = sz.Start_Optimal_Date__c.addDays(1);
                        if (sz.Look_Forward_Date__c != null)
                        swpList2[count1].Look_Forward_Date__c = sz.Look_Forward_Date__c.addDays(1);
                        swpList.add(swpList2[count1]);
                        countList.add(count1);
                        count++;
                        count1++;
                        tueCount++;
                        System.debug('count:' + count);
                        System.debug('count1:' + count1);
                    }
                }
                count = 0;
            }
            System.debug('count1:'+count1);
            System.debug('mapCount:'+mapCount);
            if(count1 == mapCount)
                tueCompleted = true;
            System.debug('swpList3 size:' + countList.size());
            System.debug('swpList4 size:' + swpList2.size());
            System.debug('swpList1 size:' + swpList1.size());
            System.debug('swpList size:' + swpList.size());

            if (countList.size()> 0)
            for (Integer i = countList.size() - 1; i >= 0; i--) {
                System.debug('list val:' + swpList1.get(i));
                swpList1.remove(i);
            }

            System.debug('swpList99 size:' + swpList1.size());
            System.debug('swpList100' + swpList1);
        }


        if (wedMap.size()> 0 && !wedCompleted) {
            System.debug('swpList1 size:' + swpList1);
            swpList2 = null;
            swpList2 = swpList1;
            Integer count = 0;
            Integer count1 = 0;
            Integer mapCount = 0;
            countList.clear();
            for (String s : wedMap.keySet()) {
                mapCount += Integer.valueOf(wedMap.get(s));
                for (Integer i = 0; i<swpList2.size(); i++) {
                    if (count<wedMap.get(s) && count1<swpList2.size() && wedCount < mapCount) {
                        System.debug('count1:' + count1);
                        System.debug('Order#:' + swpList2[count1].Order__c);
                        swpList2[count1].OwnerId = s.split('-') [0];
                        if (sz.Start_Call_Date__c != null)
                        swpList2[count1].Target_Call_Date__c = sz.Start_Call_Date__c.addDays(2);
                        if (sz.Start_Optimal_Date__c != null)
                        swpList2[count1].Optimal_Date__c = sz.Start_Optimal_Date__c.addDays(2);
                        if (sz.Look_Forward_Date__c != null)
                        swpList2[count1].Look_Forward_Date__c = sz.Look_Forward_Date__c.addDays(2);
                        swpList.add(swpList2[count1]);
                        countList.add(count1);
                        count++;
                        count1++;
                        wedCount++;
                        System.debug('count:' + count);
                        System.debug('count1:' + count1);
                    }
                }
                count = 0;
            }
            System.debug('count1:'+count1);
            System.debug('mapCount:'+mapCount);
            if(count1 == mapCount)
                wedCompleted = true;
            System.debug('swpList3 size:' + countList.size());
            System.debug('swpList4 size:' + swpList2.size());
            System.debug('swpList1 size:' + swpList1.size());
            System.debug('swpList size:' + swpList.size());

            if (countList.size()> 0)
            for (Integer i = countList.size() - 1; i >= 0; i--) {
                System.debug('list val:' + swpList1.get(i));
                swpList1.remove(i);
            }

            System.debug('swpList99 size:' + swpList1.size());
            System.debug('swpList100' + swpList1);
        }

        if (thurMap.size()> 0 && !thurCompleted) {
            System.debug('swpList1 size:' + swpList1);
            swpList2 = null;
            swpList2 = swpList1;
            Integer count = 0;
            Integer count1 = 0;
            Integer mapCount = 0;
            countList.clear();
            for (String s : thurMap.keySet()) {
                mapCount += Integer.valueOf(thurMap.get(s));
                for (Integer i = 0; i<swpList2.size(); i++) {
                    if (count<thurMap.get(s) && count1<swpList2.size() && thurCount < mapCount) {
                        System.debug('count1:' + count1);
                        System.debug('Order#:' + swpList2[count1].Order__c);
                        swpList2[count1].OwnerId = s.split('-') [0];
                        if (sz.Start_Call_Date__c != null)
                        swpList2[count1].Target_Call_Date__c = sz.Start_Call_Date__c.addDays(3);
                        if (sz.Start_Optimal_Date__c != null)
                        swpList2[count1].Optimal_Date__c = sz.Start_Optimal_Date__c.addDays(3);
                        if (sz.Look_Forward_Date__c != null)
                        swpList2[count1].Look_Forward_Date__c = sz.Look_Forward_Date__c.addDays(3);
                        swpList.add(swpList2[count1]);
                        countList.add(count1);
                        count++;
                        count1++;
                        thurCount++;
                        System.debug('count:' + count);
                        System.debug('count1:' + count1);
                    }
                }
                count = 0;
            }
            System.debug('count1:'+count1);
            System.debug('mapCount:'+mapCount);
            if(count1 == mapCount)
                thurCompleted = true;
            System.debug('swpList3 size:' + countList.size());
            System.debug('swpList4 size:' + swpList2.size());
            System.debug('swpList1 size:' + swpList1.size());
            System.debug('swpList size:' + swpList.size());

            if (countList.size()> 0)
            for (Integer i = countList.size() - 1; i >= 0; i--) {
                System.debug('list val:' + swpList1.get(i));
                swpList1.remove(i);
            }

            System.debug('swpList99 size:' + swpList1.size());
            System.debug('swpList100' + swpList1);
        }

        if (friMap.size()> 0 && !friCompleted) {
            System.debug('swpList1 size:' + swpList1);
            swpList2 = null;
            swpList2 = swpList1;
            Integer count = 0;
            Integer count1 = 0;
            Integer mapCount = 0;
            countList.clear();
            for (String s : friMap.keySet()) {
                mapCount += Integer.valueOf(friMap.get(s));
                for (Integer i = 0; i<swpList2.size(); i++) {
                    if (count<friMap.get(s) && count1<swpList2.size() && friCount < mapCount) {
                        System.debug('count1:' + count1);
                        System.debug('Order#:' + swpList2[count1].Order__c);
                        swpList2[count1].OwnerId = s.split('-') [0];
                        if (sz.Start_Call_Date__c != null)
                        swpList2[count1].Target_Call_Date__c = sz.Start_Call_Date__c.addDays(4);
                        if (sz.Start_Optimal_Date__c != null)
                        swpList2[count1].Optimal_Date__c = sz.Start_Optimal_Date__c.addDays(4);
                        if (sz.Look_Forward_Date__c != null)
                        swpList2[count1].Look_Forward_Date__c = sz.Look_Forward_Date__c.addDays(4);
                        swpList.add(swpList2[count1]);
                        countList.add(count1);
                        count++;
                        count1++;
                        friCount++;
                        System.debug('count:' + count);
                        System.debug('count1:' + count1);
                    }
                }
                count = 0;
            }
            System.debug('count1:'+count1);
            System.debug('mapCount:'+mapCount);
            if(count1 == mapCount)
                friCompleted = true;
            System.debug('swpList3 size:' + countList.size());
            System.debug('swpList4 size:' + swpList2.size());
            System.debug('swpList1 size:' + swpList1.size());
            System.debug('swpList size:' + swpList.size());

            if (countList.size()> 0)
            for (Integer i = countList.size() - 1; i >= 0; i--) {
                System.debug('list val:' + swpList1.get(i));
                swpList1.remove(i);
            }

            System.debug('swpList99 size:' + swpList1.size());
            System.debug('swpList100' + swpList1);
        }
        
        System.debug('swpList100' + swpList1);
        if(swpList1.size() > 0){
            System.debug('swpList1 size:'+swpList1.size());
            List<String> mapKey = new List<String>();
            if(monMap.size() > 0)
                mapKey.addAll(monMap.keySet());
            else if(tueMap.size() > 0)
                mapKey.addAll(tueMap.keySet());
            else if(wedMap.size() > 0)
                mapKey.addAll(wedMap.keySet());
            else if(thurMap.size() > 0)
                mapKey.addAll(thurMap.keySet());
            else
                mapKey.addAll(friMap.keySet());
                
            System.debug('mapKey:'+mapKey);
            
            Integer count = 0;  
            System.debug('swpList**:'+swpList);  
            
            for (Integer i = 0; i<swpList1.size(); i++) {
                System.debug('swpList1[i]:'+swpList1[count]);
                swpList1[i].OwnerId = mapKey[0].split('-')[0];
                if (sz.Start_Call_Date__c != null)
                swpList1[i].Target_Call_Date__c = sz.Start_Call_Date__c;
                if (sz.Start_Optimal_Date__c != null)
                swpList1[i].Optimal_Date__c = sz.Start_Optimal_Date__c;
                if (sz.Look_Forward_Date__c != null)
                swpList1[i].Look_Forward_Date__c = sz.Look_Forward_Date__c;
                swpList.add(swpList1[i]);
            }
            
            System.debug('swpList:'+swpList);
            
        }

        if (sz != null) {
            Database.SaveResult updateZone = Database.update(sz, false);

        }

        //upsert szt;
        if (szt.size()> 0) {
            Database.UpsertResult[] upsertResult = Database.upsert(szt, false);


        }

        if (swpList.size()> 0) {
            //update swpList;
            Database.SaveResult[] updateResult = Database.update(swpList, false);

        }

        if (deleteswp.size()> 0) {
            Database.DeleteResult[] deleteswpResult = Database.delete(deleteswp, false);
        }


    }

    /**
     * @description gets invoked when the batch job finishes. Place any clean up code in this method.
     * @param context contains the job ID
     */
    global void finish(Database.BatchableContext context) {
    
        /*Trigger_Switch__c pmpSwitch = Trigger_Switch__c.getValues('UpdateAlignment');
        pmpSwitch.Active__c = true;
        
        update pmpSwitch;*/
    
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :context.getJobId()];
        
        if(job.Status == 'Completed'){
    
        //Send chatter message on Swarm Zone to Team members once the allocation is completed (US-7330)
                    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                    //List<ConnectApi.MentionSegmentInput> mentionSegmentInput = new List<ConnectApi.MentionSegmentInput>();
                    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                    ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                    messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                    textSegmentInput.text = 'Lead allocation completed successfully!';
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    if(!Test.isRunningTest()){
                        for(Lead_Team__c szt : szt){
                            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                            mentionSegmentInput.id = szt.User__c;
                            messageBodyInput.messageSegments.add(mentionSegmentInput);
                            System.debug('swarm zone team user:'+szt.User__c);
                        }
                        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                        mentionSegmentInput.id = UserInfo.getUserId();
                        messageBodyInput.messageSegments.add(mentionSegmentInput);
                        
                    }
                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = sz.Id ;
                    System.debug('feed item body:'+feedItemInput.body);
                    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }
        else{
            //Send chatter message on Swarm Zone to Team members once the allocation is completed (US-7330)
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            //List<ConnectApi.MentionSegmentInput> mentionSegmentInput = new List<ConnectApi.MentionSegmentInput>();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            textSegmentInput.text = 'Lead allocation failed. Contact your administrator!';
            messageBodyInput.messageSegments.add(textSegmentInput);
            if(!Test.isRunningTest()){
                for(Lead_Team__c szt : szt){
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput.id = szt.User__c;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                    System.debug('swarm zone team user:'+szt.User__c);
                }
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput.id = UserInfo.getUserId();
                messageBodyInput.messageSegments.add(mentionSegmentInput);
            } 
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = sz.Id ;
            System.debug('feed item body:'+feedItemInput.body);
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }

    }
}
public with sharing class CLO_WorkOrderController {
    public CLO_WorkOrderController() {

    }

    public Static void simulateWOEstimateSAPService(WorkOrder wo)
    {
        /*Authentication using bearer token*/
        SAPOrderCreationServiceNew.ZSD_SALESDOCUMENT_CREATE simulateWorkOrderObj = new SAPOrderCreationServiceNew.ZSD_SALESDOCUMENT_CREATE();
        simulateWorkOrderObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSD_SALESDOCUMENT_CREATE');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        simulateWorkOrderObj.inputHttpHeaders_x = new Map<String,String>();
        simulateWorkOrderObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        simulateWorkOrderObj.timeout_x = 120000;
        
        /*1st param in order service SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPICCARD CCARD_IN*/
        SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPICCARD CCARD_IN = new SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPICCARD();
        
        /*2nd param in order service SAPOrderCreationServiceNew.TABLE_OF_BAPICCARD_EX CCARD_OUT*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPICCARD_EX CCARD_OUT = new SAPOrderCreationServiceNew.TABLE_OF_BAPICCARD_EX();
        
        /*3rd param in order service SAPOrderCreationServiceNew.TABLE_OF_BAPICOND CONDITIONS_OUT*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPICOND CONDITIONS_OUT = new SAPOrderCreationServiceNew.TABLE_OF_BAPICOND();
        
        /*4th param in order service SAPOrderCreationServiceNew.ZSF_BAPISDHD1 HEADER_IN*/
        SAPOrderCreationServiceNew.ZSF_BAPISDHD1 HEADER_IN = new SAPOrderCreationServiceNew.ZSF_BAPISDHD1();

        HEADER_IN.STATUS = wo.status;
        HEADER_IN.DOC_TYPE = 'ZSB';
        HEADER_IN.DOC_DATE = DateTime.Now().format('yyyy-MM-dd');
        HEADER_IN.SALES_ORG = wo.Account.CLO_Sales_Org__c;
        HEADER_IN.DISTR_CHAN = '10';//Hardcoded by design
        HEADER_IN.DIVISION = '10';//Hardcoded by design
        system.debug('SOLD_TO::'+ wo.Account.AccountNumber);
        HEADER_IN.SOLD_TO = wo.Account.AccountNumber;
        HEADER_IN.SHIP_TO = wo.Account.AccountNumber;
        HEADER_IN.SALES_REP_EMAIL = wo.CreatedBy.Email;
        HEADER_IN.SB_REP_EMAIL = '';//Not needed
        HEADER_IN.NAME = 'WO'+wo.WorkOrderNumber;
        HEADER_IN.PO_METHOD = '';//Not Needed
        HEADER_IN.PURCH_NO_C = '';
        HEADER_IN.PURCH_NO_S = '';
        HEADER_IN.PURCH_DATE = DateTime.Now().format('yyyy-MM-dd');
        HEADER_IN.REQ_DATE_H = DateTime.Now().format('yyyy-MM-dd');
        HEADER_IN.PMNTTRMS = '';
        HEADER_IN.INCOTERMS1 = wo.Account.CLO_Incoterm_1__c;
        HEADER_IN.INCOTERMS2 = wo.Account.CLO_Incoterm_2__c;
        HEADER_IN.ORD_REASON = wo.CLO_Billing_Reason__c;
        HEADER_IN.PRICE_DATE = DateTime.Now().format('yyyy-MM-dd');
        HEADER_IN.CT_VALID_F = DateTime.Now().format('yyyy-MM-dd');
        HEADER_IN.CT_VALID_T = DateTime.Now().format('yyyy-MM-dd');
        HEADER_IN.SHIP_COND = '';
        HEADER_IN.PP_SEARCH = '';//TBD
        HEADER_IN.CURRENCY_x = '';//TBD
        HEADER_IN.CURR_ISO = wo.CurrencyIsoCode;
        HEADER_IN.VERSION = '';//TBD
        HEADER_IN.ORDER_NOTES = '';
        HEADER_IN.DELIVERY_NOTES = '';
        HEADER_IN.ASSET_MATERIAL = wo.Asset.ProductCode;
        HEADER_IN.ASSET_NUMBER = wo.Asset.SerialNumber;

        /*5th param in order service SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPISDITM ITEMS_IN*/
        SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPISDITM ITEMS_IN = new SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPISDITM();                        
        ITEMS_IN.item = new List<SAPOrderCreationServiceNew.ZSF_BAPISDITM>(); 
         
        for(WorkOrderLineItem woli : wo.WorkOrderLineItems){
            SAPOrderCreationServiceNew.ZSF_BAPISDITM bapisdItm = new SAPOrderCreationServiceNew.ZSF_BAPISDITM();
            system.debug('woli  ::::: '+woli);
            bapisdItm.ITM_NUMBER = String.valueOf(woli.LineItemNumber).RIGHT(6);
            bapisdItm.HG_LV_ITEM = '';
            bapisdItm.PO_ITM_NO = String.valueOf(woli.LineItemNumber).RIGHT(6);
            bapisdItm.MATERIAL = woli.Product__r.ProductCode;            
            bapisdItm.BATCH = ''; //TBD
            bapisdItm.REASON_REJ = '';//TBD
            bapisdItm.PLANT = wo.Account.CLO_Delivering_Plant__c;//'1000'; //From the Account : delivering plant field on Account
            bapisdItm.STORE_LOC = '';
            bapisdItm.TARGET_QTY = String.valueOf(woli.Quantity);
            bapisdItm.TARGET_QU = woli.Product__r.Primary_Unit_of_Measure__c;
            bapisdItm.ITEM_PRICE = String.valueOf(woli.UnitPrice);
            bapisdItm.REQ_DATE = DateTime.Now().format('yyyy-MM-dd');
            bapisdItm.ITEM_CATEG = ''; //TBD
            bapisdItm.CURR_ISO = woli.CurrencyIsoCode;
            bapisdItm.SHORT_TEXT = '';//TBD 
            bapisdItm.PURCH_NO_S = woli.Id;
            bapisdItm.CUST_MAT35 = '';//TBD
            bapisdItm.REF_1 = '';//TBD
            bapisdItm.MATERIAL_LONG = '';//TBD
            if(woli.CLO_Non_Billable__c){
                bapisdItm.BILLABLE = 'X';
            } else {
                bapisdItm.BILLABLE = '';
            }
                        
            ITEMS_IN.item.add(bapisdItm);
        }
        //Quantity, MATERIAL, TARGET_QU
        if(wo.Status != 'Completed')
        {
            for(ProductRequired pr : wo.ProductsRequired){
                SAPOrderCreationServiceNew.ZSF_BAPISDITM bapisdItm = new SAPOrderCreationServiceNew.ZSF_BAPISDITM();
                system.debug('pr  ::::: '+pr);
                String strItemNo = String.valueOf(pr.ProductRequiredNumber);
                String[] strItemNumTemp = strItemNo.split('-');
                System.debug('strItemNumTemp :: '+strItemNumTemp);
                bapisdItm.ITM_NUMBER =  strItemNo.split('-')[1];
                System.debug('PR :: bapisdItm.ITM_NUMBER :: '+bapisdItm.ITM_NUMBER);
                bapisdItm.HG_LV_ITEM = '';
                bapisdItm.PO_ITM_NO = String.valueOf(pr.ProductRequiredNumber).RIGHT(6);
                bapisdItm.MATERIAL = pr.Product2.ProductCode;            
                bapisdItm.BATCH = ''; //TBD
                bapisdItm.REASON_REJ = '';//TBD
                bapisdItm.PLANT = wo.Account.CLO_Delivering_Plant__c; //From the Account : delivering plant field on Account
                bapisdItm.STORE_LOC = '';
                bapisdItm.TARGET_QTY = String.valueOf(pr.QuantityRequired);
                bapisdItm.TARGET_QU = pr.Product2.Primary_Unit_of_Measure__c;
                bapisdItm.ITEM_PRICE = '';//String.valueOf(woli.UnitPrice);
                bapisdItm.REQ_DATE = DateTime.Now().format('yyyy-MM-dd');
                bapisdItm.ITEM_CATEG = ''; //TBD
                bapisdItm.CURR_ISO = pr.CurrencyIsoCode;
                bapisdItm.SHORT_TEXT = '';//TBD 
                bapisdItm.PURCH_NO_S = pr.Id;
                bapisdItm.CUST_MAT35 = '';//TBD
                bapisdItm.REF_1 = '';//TBD
                bapisdItm.MATERIAL_LONG = '';//TBD
                bapisdItm.BILLABLE = pr.CLO_Non_Billable__c? 'X' : '';
                            
                ITEMS_IN.item.add(bapisdItm);
            }
        }
        if(wo.Status == 'Completed')
        {
            for(ProductConsumed pc : wo.ProductsConsumed){
                SAPOrderCreationServiceNew.ZSF_BAPISDITM bapisdItm = new SAPOrderCreationServiceNew.ZSF_BAPISDITM();
                system.debug('pc  ::::: '+pc);
                String strItemNo = String.valueOf(pc.ProductConsumedNumber);
                String[] strItemNumTemp = strItemNo.split('-');
                System.debug('strItemNumTemp :: '+strItemNumTemp);
                bapisdItm.ITM_NUMBER =  strItemNo.split('-')[1];
                System.debug('PC :: bapisdItm.ITM_NUMBER :: '+bapisdItm.ITM_NUMBER);
                bapisdItm.HG_LV_ITEM = '';
                bapisdItm.PO_ITM_NO = String.valueOf(pc.ProductConsumedNumber).RIGHT(6);
                bapisdItm.MATERIAL = pc.Product2.ProductCode;            
                bapisdItm.BATCH = ''; //TBD
                bapisdItm.REASON_REJ = '';//TBD
                bapisdItm.PLANT = wo.Account.CLO_Delivering_Plant__c; //From the Account : delivering plant field on Account
                bapisdItm.STORE_LOC = '';
                bapisdItm.TARGET_QTY = String.valueOf(pc.QuantityConsumed);
                bapisdItm.TARGET_QU = pc.Product2.Primary_Unit_of_Measure__c;
                bapisdItm.ITEM_PRICE = '';//String.valueOf(woli.UnitPrice);
                bapisdItm.REQ_DATE = DateTime.Now().format('yyyy-MM-dd');
                bapisdItm.ITEM_CATEG = ''; //TBD
                bapisdItm.CURR_ISO = pc.CurrencyIsoCode;
                bapisdItm.SHORT_TEXT = '';//TBD 
                bapisdItm.PURCH_NO_S = pc.Id;
                bapisdItm.CUST_MAT35 = '';//TBD
                bapisdItm.REF_1 = '';//TBD
                bapisdItm.MATERIAL_LONG = '';//TBD
                bapisdItm.BILLABLE = pc.CLO_Non_Billable__c? 'X' : '';
                            
                ITEMS_IN.item.add(bapisdItm);
            }
        }
        /*6th param in order service SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPIITEMEX ITEMS_OUT*/
        SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPIITEMEX ITEMS_OUT = new SAPOrderCreationServiceNew.TABLE_OF_ZSF_BAPIITEMEX();
        
        /*7th param in order service SAPOrderCreationServiceNew.TABLE_OF_BAPIRET2 RETURN_x*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPIRET2 RETURN_x = new SAPOrderCreationServiceNew.TABLE_OF_BAPIRET2();
        
        /*8th param in order service SAPOrderCreationServiceNew.TABLE_OF_TDS_REPAIRI_POSNR_SERIAL SERIALS_IN*/
        //SAPOrderCreationServiceNew.TABLE_OF_TDS_REPAIRI_POSNR_SERIAL SERIALS_IN = new SAPOrderCreationServiceNew.TABLE_OF_TDS_REPAIRI_POSNR_SERIAL();
        
        /*9th param in order service TABLE_OF_BAPI1175_SOLURL SO_URLS*/
        SAPOrderCreationServiceNew.TABLE_OF_BAPI1175_SOLURL SO_URLS = new SAPOrderCreationServiceNew.TABLE_OF_BAPI1175_SOLURL();
        SO_URLS.item = new List<SAPOrderCreationServiceNew.BAPI1175_SOLURL>();

        SAPOrderCreationServiceNew.BAPI1175_SOLURL itemSOLURL = new SAPOrderCreationServiceNew.BAPI1175_SOLURL();
        String filesListView = 'https://{0}/lightning/r/{1}/related/AttachedContentDocuments/view';
        String urlInstance = String.valueof(System.URL.getSalesforceBaseURL().gethost());
        List<String> params =  new List<String>{urlInstance, String.valueOf(wo.Id)};
        itemSOLURL.URL = String.Format(filesListView, params);
        SO_URLS.item.add(itemSOLURL);

        /*10th and last param in order service String TESTRUN*/
        String TESTRUN = 'X';
        
        SAPOrderCreationServiceNew.ZSD_SALESDOCUMENT_CREATEResponse_element responseObj = simulateWorkOrderObj.ZSD_SALESDOCUMENT_CREATE(CCARD_IN,CCARD_OUT,CONDITIONS_OUT,HEADER_IN,ITEMS_IN,ITEMS_OUT,RETURN_x,SO_URLS,TESTRUN);
        
        system.debug('responseObj.ITEMS_OUT.item::'+String.valueOf(JSON.serializePretty(responseObj.ITEMS_OUT.item)));
        //system.debug('Response::'+responseObj.RETURN_x.item[0].MESSAGE);
        //system.debug('responseObj::ITEMS_OUT::item----->>>'  + responseObj.ITEMS_OUT.item);
        Double authAmount = 0.0;
        if(TESTRUN == 'X')
        {
            List<WorkOrderLineItem> lstItems = [Select Id, LineItemNumber, Description, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM WorkOrderLineItem Where WorkOrder.Id  =: wo.Id];
            List<WorkOrderLineItem> LstItemsToUpdate = new List<WorkOrderLineItem>();
            List<ProductRequired> lstPrItems = new List<ProductRequired>();
            List<ProductConsumed> lstPcItems = new List<ProductConsumed>();
            List<ProductRequired> LstPrToUpdate = new List<ProductRequired>();
            List<ProductConsumed> LstPcToUpdate = new List<ProductConsumed>();

            if(wo.Status == 'Completed')
            {
                lstPcItems = [Select Id, ProductConsumedNumber, CLO_Description__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM ProductConsumed Where WorkOrderId =: wo.Id];
            }
            else {
                lstPrItems = [Select Id, ProductRequiredNumber, CLO_Description__c, CLO_Discount__c, CLO_Net_Value__c, CLO_Gross_Value__c, CLO_Tax__c FROM ProductRequired Where ParentRecordId =: wo.Id];
            }
            if(responseObj.ITEMS_OUT.item != NULL && responseObj.ITEMS_OUT.item.size() > 0){
                for(SAPOrderCreationServiceNew.ZSF_BAPIITEMEX item : responseObj.ITEMS_OUT.item){
                    for(WorkOrderLineItem itemToUpdate : lstItems){
                        string itmNo = String.valueOf(itemToUpdate.LineItemNumber).RIGHT(6);                        
                        system.debug('item.PO_ITM_NO::'+item.PO_ITM_NO);                
                        system.debug('itmNo::'+itmNo);                
                        if(item.PO_ITM_NO == itmNo){
                            
                            itemToUpdate.CLO_Tax__c = Decimal.valueOf(item.TX_DOC_CUR);
                            authAmount += Decimal.valueOf(item.TX_DOC_CUR);
                            itemToUpdate.CLO_Gross_Value__c = Decimal.valueOf(item.SUBTOTAL1);
                            itemToUpdate.CLO_Net_Value__c = Decimal.valueOf(item.SUBTOTAL2);
                            authAmount += Decimal.valueOf(item.SUBTOTAL2);
                            itemToUpdate.CLO_Discount__c = Decimal.valueOf(item.SUBTOTAL5);
                            itemToUpdate.Description = item.SHORT_TEXT;
                                                        
                            LstItemsToUpdate.add(itemToUpdate);
                        }
                    }

                    for(ProductRequired prItemToUpdate : lstPrItems){
                        string itmNo = String.valueOf(prItemToUpdate.ProductRequiredNumber).RIGHT(6);                        
                        system.debug(' PR :: item.PO_ITM_NO::'+item.PO_ITM_NO);                
                        system.debug('PR :: itmNo::'+itmNo);                
                        if(item.PO_ITM_NO == itmNo){
                            
                            prItemToUpdate.CLO_Tax__c = Decimal.valueOf(item.TX_DOC_CUR);
                            authAmount += Decimal.valueOf(item.TX_DOC_CUR);
                            prItemToUpdate.CLO_Gross_Value__c = Decimal.valueOf(item.SUBTOTAL1);
                            prItemToUpdate.CLO_Net_Value__c = Decimal.valueOf(item.SUBTOTAL2);
                            authAmount += Decimal.valueOf(item.SUBTOTAL2);
                            prItemToUpdate.CLO_Discount__c = Decimal.valueOf(item.SUBTOTAL5);
                            prItemToUpdate.CLO_Description__c = item.SHORT_TEXT;
                                                        
                            LstPrToUpdate.add(prItemToUpdate);
                        }
                    }

                    for(ProductConsumed pcItemToUpdate : lstPcItems){
                        string itmNo = String.valueOf(pcItemToUpdate.ProductConsumedNumber).RIGHT(6);                        
                        system.debug(' PR :: item.PO_ITM_NO::'+item.PO_ITM_NO);                
                        system.debug('PR :: itmNo::'+itmNo);                
                        if(item.PO_ITM_NO == itmNo){
                            
                            pcItemToUpdate.CLO_Tax__c = Decimal.valueOf(item.TX_DOC_CUR);
                            authAmount += Decimal.valueOf(item.TX_DOC_CUR);
                            pcItemToUpdate.CLO_Gross_Value__c = Decimal.valueOf(item.SUBTOTAL1);
                            pcItemToUpdate.CLO_Net_Value__c = Decimal.valueOf(item.SUBTOTAL2);
                            authAmount += Decimal.valueOf(item.SUBTOTAL2);
                            pcItemToUpdate.CLO_Discount__c = Decimal.valueOf(item.SUBTOTAL5);
                            pcItemToUpdate.CLO_Description__c = item.SHORT_TEXT;
                                                        
                            LstPcToUpdate.add(pcItemToUpdate);
                        }
                    }
                    
                }
                
                if(LstItemsToUpdate.size() > 0){
                    system.debug('WOLI :: LstItemsToUpdate::'+LstItemsToUpdate);  
                    update LstItemsToUpdate;
                }  

                if(LstPcToUpdate.size() > 0){
                    system.debug('PR :: LstPcToUpdate::'+LstPcToUpdate);  
                    update LstPcToUpdate;
                }
                if(LstPrToUpdate.size() > 0){
                    system.debug('PR :: LstPrToUpdate::'+LstPrToUpdate);  
                    update LstPrToUpdate;
                }
                
                system.debug('WO :: Double authAmount ::'+authAmount);      
                authAmount = CLO_CommonUtil.getUpliftedAmount('ZSB', authAmount);
                system.debug('authAmount after uplift ::'+authAmount);
                update new WorkOrder(Id = wo.Id, CLO_Authorization_Amount__c = authAmount);
            }
       
        if(responseObj.RETURN_x.item != null && responseObj.RETURN_x.item.size() > 0)
        {
            String errorMessage = '';
            for(SAPOrderCreationServiceNew.BAPIRET2 bapiReturn : responseObj.RETURN_x.item){
                if(bapiReturn.TYPE_x == 'E'){
                    errorMessage += bapiReturn.Message + '\n';
                }
            } 

            System.debug('errorMessage :: '+errorMessage);
            update new WorkOrder(Id = wo.Id, CLO_SAP_Sync_Error__c = errorMessage);
            
        }
    }
}
}
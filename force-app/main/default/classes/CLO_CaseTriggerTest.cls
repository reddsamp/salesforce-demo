/**
  * @ClassName      : CLO_CaseTriggerTest 
  * @Description    : This class is used to test Case trigger.
  * @Author         : Markandeyulu K
  * @CreatedDate    : 25th Aug 2020
  */
@isTest
private class CLO_CaseTriggerTest{

    @testSetup static void setup() {
        String triggerName = 'CLO_CaseTrigger';
        List<CLO_TriggerSetting__mdt> triggerSettings= (List<CLO_TriggerSetting__mdt>) CLO_DBManager.getSObjectList('Id, DeveloperName, isActive__c', 'CLO_TriggerSetting__mdt', 'DeveloperName = ' +CLO_StringUtil.inquoteString('CLO_CaseTrigger'));
        
        // Create test Cases
        List<Case> testcass = new List<Case>();
        for(Integer i=0;i<5;i++) {
            testcass.add(new Case(Status = 'Open'));
        }
        CLO_DBManager.insertSObjects(testcass); 
        
        // Create test contact
        List<Contact> testcass1 = new List<Contact>();
        for(Integer i=0;i<2;i++) {
            testcass1.add(new Contact(LastName = 'Test'));
        }
        CLO_DBManager.insertSObjects(testcass1); 
        
    }
    
    @isTest static void testCaseCRUD() {
        // Get all Case's by using a SOQL query
        List<Case> testcass = (List<Case>) CLO_DBManager.getSObjects('SELECT Id FROM Case');
        System.assertEquals(5, testcass.Size(),'success');
        
        List<Contact> testcass1 = (List<Contact>) CLO_DBManager.getSObjects('SELECT Id FROM Contact');
        System.assertEquals(2, testcass1.Size(),'success');

        // Get the first test Case by using a SOQL query
        Case cas = (Case) CLO_DBManager.getSObject('Id', 'Case', 'Status= '+ CLO_StringUtil.inquoteString('Open') +' LIMIT 1');
        //[SELECT Id FROM Case WHERE Name='Test0' LIMIT 1];
        // Modify first Case
        cas.Status = 'In Progress';
        cas.Type = 'Complaint';
        cas.CLO_Clinical_Findings__c = 'Other'; 
        cas.CLO_Sub_type__c = 'Warranty Failure';
        testcass[1].Status = 'In Progress';
        testcass[1].CLO_Clinical_Findings__c = 'Other'; 
        testcass[1].CLO_Sub_type__c = 'Warranty Failure';
        testcass[1].CLO_Case_Resolution__c = 'Other';
        testcass[2].Status = 'In Progress';
        testcass[2].CLO_Clinical_Findings__c = 'Other'; 
        testcass[2].CLO_Sub_type__c = 'Warranty Failure';
        testcass[2].CLO_Case_Resolution__c = 'Other';
        testcass[2].ContactID = testcass1[0].id;
        testcass[3].Status = 'In Progress';
        testcass[3].CLO_Clinical_Findings__c = 'Other'; 
        testcass[3].CLO_Sub_type__c = 'Warranty Failure';
        testcass[3].CLO_Case_Resolution__c = 'Other';
        testcass[3].ContactID = testcass1[0].id;
        testcass[3].CLO_Patient_User_Impact__c = 'Yes';
        testcass[3].CLO_Patient_User_Impact_details__c = '';
        testcass[4].Status = 'In Progress';
        testcass[4].CLO_Clinical_Findings__c = 'Other'; 
        testcass[4].CLO_Sub_type__c = 'Warranty Failure';
        testcass[4].CLO_Case_Resolution__c = 'Other';
        testcass[4].ContactID = testcass1[0].id;
        testcass[4].CLO_Patient_User_Impact__c = 'Yes';
        testcass[4].CLO_Patient_User_Impact_details__c = 'test';
        
        try{
            update testcass;
        } catch(Exception e){}
        
        // This update is local to this test method only.
        try{
        update cas;
        } catch(Exception e){}
        // Get all Case's by using a SOQL query
        testcass = (List<Case>) CLO_DBManager.getSObjects('SELECT Id FROM Case');
        System.assertEquals(5, testcass.Size(),'success');

        // Delete second Case
        Case cas2 = (Case) CLO_DBManager.getSObject('Id', 'Case', 'Status= '+ CLO_StringUtil.inquoteString('Open') +' LIMIT 1');
        // This deletion is local to this test method only.
        delete cas2;
        
        // Get all Case's by using a SOQL query
        testcass = (List<Case>) CLO_DBManager.getSObjects('SELECT Id FROM Case');
        System.assertEquals(4, testcass.Size(),'success');
        
        undelete cas2;
        
        // Get all Case's by using a SOQL query
        testcass = (List<Case>) CLO_DBManager.getSObjects('SELECT Id FROM Case');
        System.assertEquals(5, testcass.Size(),'success');
    }
    
    @isTest static void testTriggerDisabled() {
        CLO_CaseTriggerHandler.triggerDisabled = true;
 
        // Get all Case's by using a SOQL query
        List<Case> testcass = (List<Case>) CLO_DBManager.getSObjects('SELECT Id FROM Case');
        System.assertEquals(5, testcass.Size(),'success');
    }
}
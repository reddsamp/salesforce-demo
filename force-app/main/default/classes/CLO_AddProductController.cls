public class CLO_AddProductController{
    
    
    @AuraEnabled
    public static Order queyOrder(String orderId)
    {
        return [select id, Name, Type, Pricebook2Id, Pricebook2.Name, CLO_Requested_Delivery_Date__c from Order where id =: orderId order by Name];
    }
    
    @AuraEnabled
    public static List<OrderItem> gtOrderIteams(Order order, List<Product2> products, List<OrderItem> orderItms)
    {
        List<OrderItem> orderItems = new List<OrderItem>();
        
        Map<String, OrderItem> orderItemMap = new Map<String, OrderItem>();
        
        if(orderItms != null && orderItms.size() > 0)
        {
            for(OrderItem oi : orderItms)
            {
                orderItemMap.put(oi.PriceBookEntryId, oi);
            }
        }
        
        for(Product2 p : products){
            OrderItem orderitem = new OrderItem(CLO_Requested_Date__c = order.CLO_Requested_Delivery_Date__c,
                                                 Description = p.Description,
                                                 CLO_COMMENTS__c = p.Name,
                                                 OrderId = order.Id,
                                                 //Product2Id = p.Id,
                                                 PriceBookEntryId = p.ExternalId,
                                                 UnitPrice = p.SBQQ__SortOrder__c,
                                                 SBQQ__SubscriptionType__c = p.Primary_Unit_of_Measure__c);
            
            if(orderItemMap.containskey(p.ExternalId))
            {
                OrderItem oi = orderItemMap.get(p.ExternalId);
                
                orderitem.Quantity = oi.Quantity;
                orderitem.CLO_Asset_Serial_Number__c = oi.CLO_Asset_Serial_Number__c;
                orderitem.CLO_Batch_Number__c = oi.CLO_Batch_Number__c;
                orderitem.UnitPrice = oi.UnitPrice;
                orderitem.CLO_Price_Override__c = oi.CLO_Price_Override__c;
                orderitem.CLO_Requested_Date__c = oi.CLO_Requested_Date__c;
                orderitem.Description = oi.Description;
                //orderitem.CLO_Billable__c = oi.CLO_Billable__c;
                orderitem.CLO_Fee_Amount__c = oi.CLO_Fee_Amount__c;
                orderitem.CLO_Referral_and_Finder_Fee_Notes__c = oi.CLO_Referral_and_Finder_Fee_Notes__c;
            }
            
            orderItems.add(orderitem);
        }
        
        return orderItems;
    }
    
    @AuraEnabled
    public static List<OrderItem> gtEditOrderIteams(String orderId)
    {
        List<OrderItem> orderItems = [Select id, Product2.Name, CLO_Price_Override__c, Product2.Primary_Unit_of_Measure__c, Order_Type__c, CLO_COMMENTS__c, CLO_Asset_Serial_Number__c, UnitPrice, Quantity, ListPrice, CLO_Gross_Value__c, CLO_Discount__c,
                                        CLO_Net_Value__c, CLO_Tax__c, CLO_Batch_Number__c, CLO_Charges__c, CLO_Requested_Date__c, Description, CLO_Non_Billable__c, CLO_Fee_Amount__c, CLO_Referral_and_Finder_Fee_Notes__c from OrderItem
                                        where OrderId =: orderId];
        
        for(OrderItem o : orderItems){
            o.CLO_COMMENTS__c = o.Product2.Name;
        }
        
        return orderItems;
    }
    
    @AuraEnabled
    public static List<Product2> queyProducts(String orderId, String priceBookId, String searchStr)
    {
        List<Order> orderList = [select id, Name, Type, Pricebook2Id, Pricebook2.Name, 
                                 Account.CLO_Sales_Org__c, Account.BillingCountryCode,
                                 CLO_Requested_Delivery_Date__c, Account.BillingCountry
                                 from Order 
                                 where id =: orderId order by Name];
        
        if(orderList.size() > 0 && orderList[0].Pricebook2Id == null)
        {
            
            List<PriceBook2> pbList =  [select id, Name from PriceBook2
                                        where Name =: (orderList[0].Account.CLO_Sales_Org__c + ' ' +  orderList[0].Account.BillingCountryCode)
                                        or  Name =: (orderList[0].Account.CLO_Sales_Org__c + ' ' +  orderList[0].Account.BillingCountry)];
            
            if(pbList.size() > 0)
            {
                priceBookId = pbList[0].Id;
                
                orderList[0].Pricebook2Id = priceBookId;
                
                update orderList;
            }    
        }
                                 
        List<PriceBookEntry> pbeList = [select id, Product2.CurrencyIsoCode, Product2.Primary_Unit_of_Measure__c, Product2.Name, UnitPrice, 
                                        Product2.ProductCode, Product2.Family, Product2.Description 
                                        from PriceBookEntry 
                                        where PriceBook2Id =: priceBookId 
                                        and IsActive = true 
                                        order by Name
                                        limit 500];
        
        if(searchStr != null && searchStr.trim().length() > 0 && searchStr != 'undefined')
        {
            searchStr = searchStr.trim();
            searchStr = '%'+searchStr+'%';
            pbeList = [select id, Product2.CurrencyIsoCode, Product2.Primary_Unit_of_Measure__c, Product2.Name, UnitPrice, 
                       Product2.ProductCode, Product2.Family, Product2.Description from PriceBookEntry 
                       where PriceBook2Id =: priceBookId 
                       and IsActive = true 
                       and (Product2.Name like: searchStr
                       or Product2.ProductCode like: searchStr
                       or Product2.Family like: searchStr
                       or Product2.Description like: searchStr)
                       order by Name
                       limit 500];
        
        }
        
        List<Product2> productList = new List<Product2>();
        for(PriceBookEntry pbe : pbeList){
            productList.add(new Product2(Name = pbe.Product2.Name,
                                         ProductCode = pbe.Product2.ProductCode,
                                         Family = pbe.Product2.Family,
                                         Description = pbe.Product2.Description,
                                         ExternalId = pbe.Id,
                                         CurrencyISOCode = pbe.Product2.CurrencyIsoCode,
                                         SBQQ__SortOrder__c = pbe.UnitPrice,
                                         Primary_Unit_of_Measure__c = pbe.Product2.Primary_Unit_of_Measure__c,
                                         StockKeepingUnit = pbe.Product2.CurrencyIsoCode +' ' + pbe.UnitPrice));
        }
        return productList;
    }
    
    @AuraEnabled
    public static String saveProducts(List<OrderItem> orderItems)
    {
        try
        {
            for(OrderItem oi : orderItems){
                oi.SBQQ__SubscriptionType__c = null;
            }
            
            upsert orderItems;
        }
        catch(Exception ex){
            return ex.getMessage();
        }
        return 'Success';
    }
}
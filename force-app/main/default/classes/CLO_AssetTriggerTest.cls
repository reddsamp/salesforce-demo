/**
  * @ClassName      : CLO_AssetTriggerTest 
  * @Description    : This class is used to test Asset trigger.
  * @Author         : Markandeyulu K
  * @CreatedDate    : 22th June 2020
  */
@isTest
private class CLO_AssetTriggerTest{

    @testSetup static void setup() {
        String triggerName = 'CLO_AssetTrigger';
        List<CLO_TriggerSetting__mdt> triggerSettings= (List<CLO_TriggerSetting__mdt>) CLO_DBManager.getSObjectList('Id, DeveloperName, isActive__c', 'CLO_TriggerSetting__mdt', 'DeveloperName = ' +CLO_StringUtil.inquoteString('CLO_AssetTrigger'));
        
        // Create test accounts
        List<Product2> testProds = new List<Product2>();
        for(Integer i=0;i<2;i++) {
            Product2 prod = new Product2();
            prod.name = 'TestProd'+i;
            prod.isActive = true;
            prod.CLO_Warranty_Not_Renewable__c = false;
            testProds.add(prod);
        }
        CLO_DBManager.insertSObjects(testProds);
        Product2 prd = (Product2) CLO_DBManager.getSObject('Id', 'Product2', 'Name= '+ CLO_StringUtil.inquoteString('TestProd0') +' LIMIT 1');
        
        // Create test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i));
        }
        CLO_DBManager.insertSObjects(testAccts); 
        Account acc = (Account) CLO_DBManager.getSObject('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct0') +' LIMIT 1');
        
        // Create test Assets
        List<Asset> testAssts = new List<Asset>();
        for(Integer i=0;i<2;i++) {
            Asset ast=new Asset();
            ast.Name='Test'+i;
            ast.AccountId = acc.id;
            ast.Product2Id = prd.id;
            ast.InstallDate = System.today();
            testAssts.add(ast);
        }
        CLO_DBManager.insertSObjects(testAssts); 
        
    }
    
    @isTest static void testAssetCRUD() {
        // Get all Asset's by using a SOQL query
        List<Asset> testAssts = (List<Asset>) CLO_DBManager.getSObjects('SELECT Id FROM Asset');
        System.assertEquals(2, testAssts.Size(),'success');

        // Get the first test Asset by using a SOQL query
        Asset Asst = (Asset) CLO_DBManager.getSObject('Id', 'Asset', 'Name= '+ CLO_StringUtil.inquoteString('Test0') +' LIMIT 1');
        //[SELECT Id FROM Asset WHERE Name='Test0' LIMIT 1];
        // Modify first Asset
        Asst.Status = 'Installed';
        // This update is local to this test method only.
        update Asst;
        // Get all Asset's by using a SOQL query
        testAssts = (List<Asset>) CLO_DBManager.getSObjects('SELECT Id FROM Asset');
        System.assertEquals(2, testAssts.Size(),'success');

        // Delete second Asset
        Asset Asst2 = (Asset) CLO_DBManager.getSObject('Id', 'Asset', 'Name= '+ CLO_StringUtil.inquoteString('Test1') +' LIMIT 1');
        // This deletion is local to this test method only.
        delete Asst2;
        
        // Get all Asset's by using a SOQL query
        testAssts = (List<Asset>) CLO_DBManager.getSObjects('SELECT Id FROM Asset');
        System.assertEquals(1, testAssts.Size(),'success');
        
        undelete Asst2;
        
        // Get all Asset's by using a SOQL query
        testAssts = (List<Asset>) CLO_DBManager.getSObjects('SELECT Id FROM Asset');
        System.assertEquals(2, testAssts.Size(),'success');
    }
    
    @isTest static void testTriggerDisabled() {
        CLO_AssetTriggerHandler.triggerDisabled = true;
 
        // Get all Asset's by using a SOQL query
        List<Asset> testAssts = (List<Asset>) CLO_DBManager.getSObjects('SELECT Id FROM Asset');
        System.assertEquals(2, testAssts.Size(),'success');
    }
}
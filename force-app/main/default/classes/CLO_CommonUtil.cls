/**
  * @ClassName      : CLO_CommonUtil 
  * @Description    : This class is defined the common util methods used in Cynosure.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 23th June 2020
  */
  public with sharing class CLO_CommonUtil {
      //Boolean variable it will set the default flag for trigger recurssive.
      private static boolean run = true;
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : It will Checks whether the given List is NULL or EMPTY.
      * @Parameters  : List<Object> lstObj
      * @Return      : Boolean 
      */
      public static Boolean isNullOrEmpty(List<Object> lstObj) {
      
        if(lstObj != null && lstObj.size() > 0) {
          return false;
        } else  {
          return true;
        } 
      }
    /**
      * @Author      : Ramana Thippireddy
      * @CreatedDate : 23th June 2020
      * @Description : It will Checks whether the given objects is NULL or NOT.
      * @Parameters  : Object objRec
      * @Return      : Boolean 
      */
      public static Boolean isNullOrEmpty(Object objRec) {
      
        if(objRec != null) {
          return false;
        } else  {
          return true;
        }
      }
      
      /**
      * @Author      : Markandeyulu K
      * @CreatedDate : 11th Aug 2020
      * @Description : It will prevent the trigger recursive..
      * @Parameters  : NA
      * @Return      : Boolean 
      */
      
      public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
      }
      
      /**
      * @Author      : Ambadas Joshi
      * @CreatedDate : 23rd Dec 2020
      * @Description : To get the endpoint for SAP webservice from custom setting
      * @Parameters  : String: custom setting Name 
      * @Return      : String: Endpoint 
      */
      
      Public Static String getEndPointURL(String settingName){
          Map<String, CLO_SAP_Integration_Settings__c> mapSAPIntegrationSettings = CLO_SAP_Integration_Settings__c.getAll();
          CLO_SAP_Integration_Settings__c sapIntegrationSetting = mapSAPIntegrationSettings.get(settingName);
            
          if(sapIntegrationSetting != NULL){
              return sapIntegrationSetting.CLO_SAP_Endpoint_Prefix__c + sapIntegrationSetting.CLO_SAP_Service_Name__c;
          }
          return null;
      }
      
       /**
      * @Author      : Ambadas Joshi
      * @CreatedDate : 22nd Jan 2021
      * @Description : To add uplift (if applicable) to the total order amount 
      *                or estimated amount in case of work order
      * @Parameters  : String: Order type (API Name) 
      * @Return      : Decimal: Uplifted amount 
      */
      
      Public Static Decimal getUpliftedAmount(String orderType, Decimal ordAmount){
          if(ordAmount == NULL || ordAmount == 0){
              return ordAmount;
          }
          
          Decimal upliftedAmount = 0;
          
          if(ordAmount > 400){
              orderType = orderType + '_H';
          }
          system.debug('CommonUtil::orderType::'+orderType);
          system.debug('CommonUtil::ordAmount::'+ordAmount);
          List<sObject> lstUpliftSetting = CLO_DBManager.getSObjectList('Id, DeveloperName, CLO_Order_Or_Work_Order_Type__c, CLO_Uplift_Multiplication_Factor__c', 'CLO_Order_Amount_Uplift_Setting__mdt', 'CLO_Order_Or_Work_Order_Type__c = ' +CLO_StringUtil.inquoteString(orderType));
            
          if(lstUpliftSetting != NULL && lstUpliftSetting.size() > 0){
              CLO_Order_Amount_Uplift_Setting__mdt upliftSetting = (CLO_Order_Amount_Uplift_Setting__mdt) lstUpliftSetting[0];
              Decimal upliftValue = upliftSetting.CLO_Uplift_Multiplication_Factor__c != NULL ? upliftSetting.CLO_Uplift_Multiplication_Factor__c : 0;
              upliftedAmount = ordAmount*upliftValue;
          }
          return (ordAmount + upliftedAmount);
      }
  }

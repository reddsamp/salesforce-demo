/**
  * @ClassName      : CLO_Constants 
  * @Description    : CLO_Constants used in Cynosure.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 23th June 2020
  */
  public with sharing class CLO_Constants {

    /**
     * Object API names used in Cynosure
     */
    public static final String ACCOUNT_OBJ_NAME = 'Account';
    public static final String CONTACT_OBJ_NAME = 'Contact';
    
    /**
     * Picklist values and constants used in Cynosure
     */
    
    public static final String STATUS_COMPLETED = 'Completed';
    public static final String NO_INTEREST = 'No Interest';
    public static final String STATUS_OPEN = 'Open';
    public static final String CALL = 'Call';
    public static final String INSTALLED = 'Installed';
    public static final String INITIAL_CALL = 'Initial Call';
    public static final String AMPS_TASK = 'AMPS Task';
    public static final String YES = 'Yes';
    public static final String STATUS_CLOSED = 'Closed';
    public static final String SERVICE_OPERATIONS = 'Service Operations';
    public static final String RE_OPENED = 'Re Opened';
    public static final String COMPLAINT = 'Complaint';
    public static final String ACCEPTED = 'Accepted';
    public static final String REJECTED = 'Rejected';
    public static final String ZSB = 'ZSB';
    public static final String ZRP = 'ZRP';
    public static final String ZRA = 'ZRA';
    public static final String RMA = 'RMA';
    public static final String DRAFT = 'Draft';
    public static final String TRADE_IN = 'Trade-In';
    public static final String INTERCOMPANY = 'Intercompany';
    public static final String RP1 = 'RP1';
    public static final String RP2 = 'RP2';
    public static final String RP3 = 'RP3';
    public static final String FS1 = 'FS1';
    public static final String FS2 = 'FS2';
    public static final String FS3 = 'FS3';
    public static final String R03 = 'R03';
    public static final String R04 = 'R04';
    public static final String R05 = 'R05';
    public static final String R08 = 'R08';
    public static final String OR610 = '610';
    public static final String OR422 = '422';
    public static final String APPROVED = 'Approved';
    public static final String CLINICAL_DISPATCH = 'Clincal Dispatch';
    public static final String TRAVEL = 'Travel';
    public static final String LABOR = 'Labor';
    public static final String WARRANTY = 'Warranty';
    public static final String CONTRACT = 'Contract';
    public static final String ACTIVE = 'Active';  
    public static final String PARTS = 'Parts';    
}
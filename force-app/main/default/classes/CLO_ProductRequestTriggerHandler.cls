/**
  * @ClassName      : ProductRequestTriggerHandler 
  * @Description    : Handler class for ProductRequest trigger 
  * @Author         : Markandeyulu K
  * @CreatedDate    : 3rd Nov 2020
  */


public class CLO_ProductRequestTriggerHandler implements CLO_ITriggerHandler{
    
    public static List<Task> lstTask;
     //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle beforeInsert business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> newList 
    * @Return      : void 
    */ 
    public void beforeInsert(List<sObject> newList) {
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle afterInsert business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
    * @Return      : void 
    */

    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle beforeUpdate business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap
    * @Return      : void 
    */
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
          

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle afterUpdate business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> newList , Map<Id, sObject> newMap,List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */

    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {
        List<Order> orderInsertList = new List<Order>();
        List<OrderItem> orderItemInsertList = new List<OrderItem>();
        List<ProductRequest> ProductRequestsList = new List<ProductRequest>();
        String priceBookId ='';
        Map<Id,ProductRequest> mapOld = (Map<Id,ProductRequest>) oldMap;
        List<Id> lstProdIds = new List<Id>();
        Map<Id,Pricebookentry> mapProdPbEntry = new Map<Id,Pricebookentry>();
        
        for(ProductRequest pr : (List<ProductRequest>) newList){
            if(pr.Status == 'Approved' && pr.Status != mapOld.get(pr.Id).Status)
            {
                ProductRequestsList.add(pr);
            }
        }
        
        Map<Id, Account> customerMap = new Map<Id, Account>();
        Map<String, String> pricebookMap = new Map<String, String>();
        Set<String> customerIds = new Set<String>();
        
        for(ProductRequest o : (List<ProductRequest>) newList)
        {
            customerIds.add(o.AccountId);
            
        }
        
        customerMap = new Map<Id, Account>([select Id, CLO_Sales_Org__c, BillingCountry, BillingCountryCode from Account where Id in : customerIds]);      
    
        for(PriceBook2 pb : [select id, Name from PriceBook2])
        {
            pricebookMap.put(pb.Name, pb.Id);
        }
        
        for(ProductRequest pr : (List<ProductRequest>) newList)
        {
            Account acc = customerMap.get(pr.AccountId);       
            priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode);
            
            if(priceBookId == null){
                priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountry);
            }    
            
        }
        
        
        
        List<ProductRequest> ProductRequestsTempList = [select Id, Status, NeedByDate, AccountId,WorkOrder.CLO_Product__c, (select id,Product2Id,QuantityRequested,CLO_Unit_Price__c from ProductRequestLineItems) from ProductRequest where id in : ProductRequestsList];
        
        if(ProductRequestsTempList.size() > 0)
        {
            for(ProductRequest pr : ProductRequestsTempList){
                if(pr.NeedByDate != null)
                    orderInsertList.add(new Order(AccountId = pr.AccountId,
                                                  EffectiveDate = Date.valueOf(pr.NeedByDate),
                                                  Pricebook2Id = priceBookId,
                                                  CLO_Product_Request__c = pr.id,
                                                  Status = 'Draft'));
            }
        }
        
        if(orderInsertList.size() > 0)
        {
            insert orderInsertList;
            
            integer count = 0;
            
            for(ProductRequest pr : ProductRequestsTempList){
            
                for(ProductRequestLineItem prli : pr.ProductRequestLineItems){
                    orderItemInsertList.add(new OrderItem(OrderId = orderInsertList[count].Id,
                                                          Product2id = prli.Product2id,
                                                          Quantity = prli.QuantityRequested,
                                                          //PricebookEntryId = prli.PricebookEntryId,
                                                          UnitPrice = prli.CLO_Unit_Price__c));
                    lstProdIds.add(prli.Product2id);                                      
                }
                count++;
            }
            System.debug('@@lstProdIds: '+ lstProdIds+' '+priceBookId);  
            If(priceBookId != '' && lstProdIds.size() > 0){
                for(Pricebookentry  pbentry : [SELECT id,Name,Product2id,Pricebook2Id FROM PricebookEntry WHERE Pricebook2Id =: priceBookId AND Product2id IN : lstProdIds]){               
                    mapProdPbEntry.put(pbentry.Product2id,pbentry);  
                    System.debug('@@@$$ : '+ pbentry.Product2id + ' ' +pbentry.id);              
                }
            }
            
            for(OrderItem ordItm : orderItemInsertList ){
            System.debug('@@@==>> : '+mapProdPbEntry.containsKey(ordItm.Product2id));
                If(mapProdPbEntry != Null && mapProdPbEntry.containsKey(ordItm.Product2id)){
                    ordItm.PricebookEntryId = mapProdPbEntry.get(ordItm.Product2id).id;
                }
            }
            
            if(orderItemInsertList.size() > 0){
                insert orderItemInsertList;
            }    
        }   
    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle beforeDelete business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */ 
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle afterDelete business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> oldList, Map<Id, sObject> oldMap
    * @Return      : void 
    */ 
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
    /**
    * @Author      : Markandeyulu K
    * @CreatedDate : 3rd Nov 2020
    * @Description : This method id used to handle afterUnDelete business logic for ProductRequest Trigger
    * @Parameters  : List<sObject> newList, Map<Id, sObject> newMap
    * @Return      : void 
    */ 
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
}
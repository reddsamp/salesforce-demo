/**
  * @ClassName      : CLO_OrderTriggerHandler 
  * @Description    : Order Object Trigger Handler.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */

  public class CLO_OrderItemTriggerHandler implements CLO_ITriggerHandler{
     
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
    public void beforeInsert(List<sObject> newList) {
        Map<String, List<OrderItem>> orderItemMap = new Map<String, List<OrderItem>>();
        Set<String> productIdSet = new Set<String>();
        
        Map<String, String> quoteLineIdMap = new Map<String, String>();
        Set<String> orderIdSet = new Set<String>();
        
        for(OrderItem rec : (List<OrderItem>) trigger.new)
        {
            productIdSet.add(rec.Product2Id);
            
            if(rec.SBQQ__QuoteLine__c != null)
                quoteLineIdMap.put(rec.Id, rec.SBQQ__QuoteLine__c);
            
            orderIdSet.add(rec.OrderId);
        }
        
        Map<Id, SBQQ__QuoteLine__c> qlMap = new Map<Id, SBQQ__QuoteLine__c>();
        Map<Id, Order> orderMap = new Map<Id, Order>();
        
        if(quoteLineIdMap.size() > 0)
        {
            qlMap = new Map<Id, SBQQ__QuoteLine__c>([Select id, SBQQ__Quote__c, SBQQ__Quote__r.CLO_Asset__r.SerialNumber,SBQQ__Quote__r.SBQQ__Type__c, CLO_Asset__r.SerialNumber 
                                                     from SBQQ__QuoteLine__c 
                                                     where id in : quoteLineIdMap.values()]);
        }
        
        if(orderIdSet.size() > 0)
        {
            orderMap = new Map<Id, Order>([Select id, CLO_Asset__c, CLO_Asset__r.SerialNumber,Type
                                           from Order 
                                           where id in : orderIdSet]);
        }
        
        for(OrderItem rec : (List<OrderItem>) trigger.new)
        {
            if(rec.CLO_Asset_Serial_Number__c == null)
            {
                if(quoteLineIdMap.containskey(rec.Id))
                {
                    String qlId = quoteLineIdMap.get(rec.Id);
                    
                    if(qlMap.containskey(qlId))
                    {
                        SBQQ__QuoteLine__c ql = qlMap.get(qlId);
                        
                        if(ql.SBQQ__Quote__r.CLO_Asset__r.SerialNumber != null)
                            rec.CLO_Asset_Serial_Number__c = ql.SBQQ__Quote__r.CLO_Asset__r.SerialNumber;
                    	else 
                            rec.CLO_Asset_Serial_Number__c = ql.CLO_Asset__r.SerialNumber;
                    }
                }
                
                if(rec.CLO_Asset_Serial_Number__c == null)
                {
                    rec.CLO_Asset_Serial_Number__c = orderMap.get(rec.OrderId).CLO_Asset__r.SerialNumber;
                }
            }
        }
        
        if(productIdSet.size() > 0)
        {
            List<SBQQ__ProductOption__c> poList = [select id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c
                                                   from SBQQ__ProductOption__c
                                                   where SBQQ__ConfiguredSKU__c in : productIdSet];
            
            Map<String, integer> parentMap = new Map<String, integer>();
            Map<String, integer> childMap = new Map<String, integer>();
            
            integer d = 1;
            
            for(SBQQ__ProductOption__c po : poList){
                if(parentMap.containsKey(po.SBQQ__ConfiguredSKU__c))
                {
                    integer dd = parentMap.get(po.SBQQ__ConfiguredSKU__c);
                    childMap.put(po.SBQQ__OptionalSKU__c, dd);
                }
                else
                {
                    parentMap.put(po.SBQQ__ConfiguredSKU__c, d);
                    childMap.put(po.SBQQ__OptionalSKU__c, d);
                    d++;
                }
            }
            
            
            for(OrderItem rec : (List<OrderItem>) trigger.new)
            {
                if(quoteLineIdMap.containskey(rec.Id))
                {
                    String qlId = quoteLineIdMap.get(rec.Id);
                    
                    if(qlMap.containskey(qlId))
                    {
                        SBQQ__QuoteLine__c ql = qlMap.get(qlId);
                        
                        if(ql.SBQQ__Quote__r.SBQQ__Type__c != 'Service Agreement'){
                            
                            if(parentMap.containskey(rec.Product2Id))
                                rec.CLO_Primary_Line_Number__c = parentMap.get(rec.Product2Id)+'';
                            if(childMap.containskey(rec.Product2Id))
                                rec.CLO_Parent_Bundle_Number__c = childMap.get(rec.Product2Id)+'';
                        }
                    }
                }
            }
        }
    }
     
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
        Set<String> orderIds = new Set<String>();
        for(OrderItem oi : (List<OrderItem>) newList)
        {
            if(!oi.Order.CLO_Created_From_SAP__c){
                orderIds.add(oi.OrderId);
            }
        }
        
        if(orderIds.size() > 0)
            deleteOrderItems(orderIds);
    }
     
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void afterUpdate(List<sObject> newList, Map<Id, sObject> newMap,  List<sObject> oldList, Map<Id, sObject> oldMap) {

    }
     
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
     
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
    //@future - Joshi: commented this as I am getting an error from another flow
    public static void deleteOrderItems(Set<String> orderIds)
    {
        List<OrderItem> orderItemList = [select id from OrderItem 
                                         where OrderId in : orderIds
                                         and Order.SBQQ__Quote__c != null
                                         and Product2.Family = 'Contracts'
                                         and Product2.CLO_PC_Bypass_and_SF_Id__c = 'SC'
                                         and Order.Type = 'OR'];
    
        if(orderItemList.size() > 0)
            delete orderItemList;
    }
}
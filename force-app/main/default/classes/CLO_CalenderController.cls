public class CLO_CalenderController {
  @AuraEnabled
    public static list<Event> getAllEvents(String userid){
        system.debug('userid :::::' + userid);
        if(userid == null) userid = UserInfo.getUserId();
        list<Event> eventlist =[Select id, EndDateTime,StartDateTime,Subject from Event where OwnerId =:userid] ;
        return eventlist;
    }
    
    
    @AuraEnabled
    public static void updateEvent(string eventid,string eventdate){
        string eventdatelist = eventdate.replace('T',' ');
       
        Event e= new event(id = eventid);
        e.StartDateTime = DateTime.valueof(eventdatelist);
    
        update e;
    }
    
    @AuraEnabled
    public static List<User> getUserList(String lsId)
    {
        List<User> userList = new List<User>();
        Set<String> userIdSet = new Set<String>();
        userIdSet.add(UserInfo.getUserId());
        
        for(Lead_Team__c team : [select id, Name, User__c from Lead_Team__c where Lead_Sheet__c =: lsId])
        {
            userIdSet.add(team.User__c);
        }
        
        for(Lead_Sheet_Calendar__c team : [select id, Name, User__c from Lead_Sheet_Calendar__c where Lead_Sheet__c =: lsId])
        {
            userIdSet.add(team.User__c);
        }
        
        userList = [select id, Name from User where id in : userIdSet];
    
        
        return userList;
    }
    
    @AuraEnabled
    public static String addCalenderUser(String lsId, String userId)
    {
        insert new Lead_Sheet_Calendar__c(Lead_Sheet__c = lsId, User__c = userId);
    
        
        return 'Success';
    }
}
public class CLO_ProductRequiredController {

    public ProductRequired pr {get;set;}
    public String productCode {get;set;}
    public String prId {get;set;}
    
    public CLO_ProductRequiredController(ApexPages.StandardController controller) {
        pr = new ProductRequired();
        pr.ParentRecordId = ApexPages.currentPage().getParameters().get('ParentRecord_lkid');
    }
    
    public void savePRLI()
    {
        try
        {
            List<Product2> productList = [select id from Product2 where ProductCode =: productCode];
            
            if(productList.size() > 0)
            {
                pr.Product2Id = productList[0].Id;
                insert pr;
                
                prId = pr.Id;
            }
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }

}
public class CLO_ImportLeadController {
    
    @AuraEnabled
    public static list<Lead> readCSVFile(String objVersion){
        list<Lead> lstAccsToInsert = new list<Lead>();
        string fieldLabel;
        if(objVersion != null) {
            list<String> lstCSVLines = objVersion.split('\n');
            // get the column header
            list<String> csvHeaders = lstCSVLines[0].split(',');
         
            lstAccsToInsert.clear();
             
            SObjectType LeadDes = Schema.getGlobalDescribe().get('lead');
            Map<String,Schema.SObjectField> leadFieldMap = LeadDes.getDescribe().fields.getMap();
            Map<String, String> leadFieldLabelMap = new Map<String, String>();
            
            for (String fieldName: leadFieldMap.keySet()) {
                leadFieldLabelMap.put(leadFieldMap.get(fieldName).getDescribe().getLabel(), fieldName);
            }
                
            for(Integer i = 1; i < lstCSVLines.size(); i++){
                Lead objAcc = new Lead();
                list<String> csvRowData = lstCSVLines[i].split(',');
                
                for (Integer j = 0; j < csvRowData.size(); j++) {
                    fieldLabel = csvHeaders[j].replace('\r', '');
                    
                    objAcc.put(fieldLabel, csvRowData[j].replace('~~~~~', ','));
                }
                
                lstAccsToInsert.add(objAcc);
            }
            
            Savepoint sp = Database.setSavepoint();
            
            Database.SaveResult[] srList = Database.insert(lstAccsToInsert, false);
            
            Integer count = -1; 
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                count++;
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted lead. Lead ID: ' + sr.getId());
                }
                else {
                    List<String> e = new List<String>();
                    
                    for(Database.Error err : sr.getErrors()) {
                        e.add(err.getMessage());
                    }
                    
                    lstAccsToInsert[count].put('Exception__c', String.join(e, ','));
                }
            }
            
            Database.rollback(sp);
        }
        
        return lstAccsToInsert;    
    }
    
    
    @AuraEnabled
    public static list<Lead> revalidateData(list<Lead> lstAccsToInsert){
        Savepoint sp = Database.setSavepoint();
        list<Lead> newLeads = new list<Lead>();
        
        for(Lead l : lstAccsToInsert){
            newLeads.add(l.clone(false, false));
        }
            
        Database.SaveResult[] srList = Database.insert(newLeads, false);
        
        Integer count = -1; 
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            count++;
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                newLeads[count].put('Exception__c', null);
                System.debug('Successfully inserted lead. Lead ID: ' + sr.getId());
            }
            else {
                List<String> e = new List<String>();
                
                for(Database.Error err : sr.getErrors()) {
                    e.add(err.getMessage());
                }
                
                newLeads[count].put('Exception__c', String.join(e, ','));
            }
        }
        
        Database.rollback(sp);
        
        return newLeads;    
    }
    
    
    @AuraEnabled
    public static list<Lead> uploadLeadData(list<Lead> lstAccsToInsert, String leadSheetId){
        list<Lead> newLeads = new list<Lead>();
        
        for(Lead l : lstAccsToInsert){
            l.Lead_Sheet__c = leadSheetId;
            
            if(l.Status == null)
                l.Status = 'Open';
            
            newLeads.add(l.clone(false, false));
        }
            
        Database.SaveResult[] srList = Database.insert(newLeads, false);
        
        Integer count = -1; 
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            count++;
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                newLeads[count].put('Exception__c', null);
                System.debug('Successfully inserted lead. Lead ID: ' + sr.getId());
            }
            else {
                List<String> e = new List<String>();
                
                for(Database.Error err : sr.getErrors()) {
                    e.add(err.getMessage());
                }
                
                newLeads[count].put('Exception__c', String.join(e, ','));
            }
        }
        
        return newLeads;    
    }
    
    
    @AuraEnabled
    public static list<LabelDescriptionWrapper> readCSVHeaders(String objVersion){
        string fieldLabel;
        list<String> csvHeaders;
        list<LabelDescriptionWrapper> headers = new list<LabelDescriptionWrapper>();
        if(objVersion != null) {
            list<String> lstCSVLines = objVersion.split('\n');
            // get the column header
            csvHeaders = lstCSVLines[0].split(',');
            
            headers.add(new LabelDescriptionWrapper('Error', 'Exception__c', null, false, false, null));
            
            for(String s: csvHeaders)
            {
                headers.add(new LabelDescriptionWrapper(s.replace('\r', ''), s.replace('\r', ''), null, true, true, null));
            }
        }           
        return headers; 
    }
    
    
    @AuraEnabled
    public static list<ColumnMappingWrapper> readLeadFields(){
        SObjectType LeadDes = Schema.getGlobalDescribe().get('lead');
        Map<String,Schema.SObjectField> leadFieldMap = LeadDes.getDescribe().fields.getMap();
        List<ColumnMappingWrapper> leadFieldString = new List<ColumnMappingWrapper>();
        
        for (String fieldName: leadFieldMap.keySet()) {
            leadFieldString.add(new ColumnMappingWrapper(leadFieldMap.get(fieldName).getDescribe().getLabel(), fieldName.toUpperCase()));
        }
        
        return leadFieldString; 
    }
    
    
    public class ColumnMappingWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public ColumnMappingWrapper(String label, String value)
        {
            this.label = label;
            this.value = value;
        }
    }
    
    
    //Wrapper class to store Field details
    public class LabelDescriptionWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String type;
        @AuraEnabled
        public boolean sortable;
        @AuraEnabled
        public boolean editable;
        @AuraEnabled
        public TypeAttributes typeAttributes;
        
        public LabelDescriptionWrapper(String labelTemp, String fieldNameTemp, String typeTemp, boolean sortableTemp, boolean editableTemp,TypeAttributes typeAttributesTemp) {
            label     = labelTemp;
            fieldName = fieldNameTemp;
            type      = typeTemp;
            sortable  = false;//sortableTemp;
            editable = editableTemp;
            typeAttributes = typeAttributesTemp;
        }
    }
    
    //Wrapper class to bind dropdown action with data row
    public class TypeAttributes  {
        @AuraEnabled
        public List<Actions> rowActions;
        
        public typeAttributes(List<Actions> rowActionsTemp) {
            rowActions    = rowActionsTemp;
        }
    }

    //Wrapper class  to store dropdown action
    public class Actions {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String name;
        
        public Actions(String labelTemp, String nameTemp) {
            label       = labelTemp;
            name        = nameTemp;
        }
    } 
}
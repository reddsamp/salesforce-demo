Public Class CLO_SAP_AssetInstallDateUpdate
{
    Public Static void UpdateAssetInstallDate(String equipmentNumber, Date extSCEndDate, Date extSCStardtDate, Date installDate)
    {
        /*Authentication using bearer token*/
        SAPAssetInstallDateUpdateService.ZSF_UPDATE_ASSET_INSTALL_DATE assetInstallDtUpdateObj = new SAPAssetInstallDateUpdateService.ZSF_UPDATE_ASSET_INSTALL_DATE();
        assetInstallDtUpdateObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSF_UPDATE_ASSET_INSTALL_DATE');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        assetInstallDtUpdateObj.inputHttpHeaders_x = new Map<String,String>();
        assetInstallDtUpdateObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        assetInstallDtUpdateObj.timeout_x = 120000;
        
        String I_EQUNR = equipmentNumber;
        DateTime dtEndDate = DateTime.NewInstance(extSCEndDate, Time.newInstance(0,0,0,0));
        String I_ESC_END_DATE = dtEndDate.format('yyyy-MM-dd');
        DateTime dtStartDate = DateTime.NewInstance(extSCStardtDate, Time.newInstance(0,0,0,0));
        String I_ESC_STA_DATE = dtStartDate.format('yyyy-MM-dd');
        DateTime dtInstallDate = DateTime.NewInstance(installDate, Time.newInstance(0,0,0,0));
        String I_INST_DATE = dtInstallDate.format('yyyy-MM-dd');
        
        SAPAssetInstallDateUpdateService.BAPIRET2 responseObj = assetInstallDtUpdateObj.ZSF_UPDATE_ASSET_INSTALL_DATE(I_EQUNR, I_ESC_END_DATE, I_ESC_STA_DATE, I_INST_DATE);
    }
}
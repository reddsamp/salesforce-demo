public class CLO_SimulateQuoteWithSAPController {
 	@AuraEnabled
    public static SBQQ__Quote__c getQuote(String quoteId)
    {
        return [select Id, Name, SBQQ__Account__r.AccountNumber, CLO_SAP_Sync_Error__c, (Select Id, SBQQ__Description__c, Name, CLO_Primary_Line_Number__c, SBQQ__TotalDiscountAmount__c, SBQQ__NetTotal__c, SBQQ__ListTotal__c, CLO_Tax__c, SBQQ__Quantity__c,SBQQ__NetPrice__c FROM SBQQ__LineItems__r) FROM SBQQ__Quote__c where id =: quoteId];
    }
    
    @AuraEnabled
    public static List<SBQQ__QuoteLine__c> getquoteItems(String quoteId)
    {
        return [Select Id, SBQQ__Description__c, Name,CLO_Fee_Amount__c, CLO_Primary_Line_Number__c, SBQQ__TotalDiscountAmount__c, SBQQ__NetTotal__c, SBQQ__ListTotal__c, CLO_Tax__c, SBQQ__Product__r.Primary_Unit_of_Measure__c, SBQQ__Quantity__c, SBQQ__NetPrice__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =: quoteId];
    }
    
    @AuraEnabled
    public static SBQQ__Quote__c syncWithSAP(String quoteId)
    {
        List<String> productNames = new List<String>();
        List<SBQQ__QuoteLine__c> quoteItems = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__Quote__c quote:[select Id, CLO_SAP_Sync_Error__c, SBQQ__Status__c, SBQQ__Type__c, SBQQ__Account__r.AccountNumber,
                                  		 SBQQ__Account__r.CLO_Sales_Org__c, SBQQ__Account__r.CLO_Customer_Number__c, CreatedBy.Email,
                                  		 Name, SBQQ__Account__r.CLO_Incoterm_1__c, SBQQ__Account__r.CLO_Incoterm_2__c, SBQQ__Account__r.CLO_Shipping_Condition__c,
                                  		 CurrencyIsoCode, CreatedDate,
                                  		 (Select Id, SBQQ__Description__c, Name, CLO_Primary_Line_Number__c, SBQQ__TotalDiscountAmount__c, CLO_Net_Value__c, SBQQ__ListTotal__c, 
                                         		 CLO_Tax__c, SBQQ__Product__r.ProductCode, SBQQ__Product__r.Primary_Unit_of_Measure__c, SBQQ__Quantity__c, CurrencyIsoCode, SBQQ__ListPrice__c, SBQQ__NetTotal__c, SBQQ__NetPrice__c FROM SBQQ__LineItems__r) FROM SBQQ__Quote__c where id =: quoteId]){
                                       
        	CLO_SAP_ServiceInvoker.simulateQuoteWithSAPService(quote);    
                                       
               
        }
                
        return [select Id, CLO_SAP_Sync_Error__c, 
                (Select Id, SBQQ__Description__c, CLO_Primary_Line_Number__c, SBQQ__TotalDiscountAmount__c, CLO_Net_Value__c, SBQQ__ListTotal__c, CLO_Tax__c, SBQQ__Quantity__c, SBQQ__NetTotal__c, SBQQ__NetPrice__c FROM SBQQ__LineItems__r) from SBQQ__Quote__c where id =: quoteId];
    }
}
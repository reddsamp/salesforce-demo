/**
  * @ClassName      : CLO_TaskTriggerTest 
  * @Description    : This class is used to test Task trigger.
  * @Author         : Markandeyulu K
  * @CreatedDate    : 22th June 2020
  */
@isTest
private class CLO_TaskTriggerTest{

    @testSetup static void setup() {
        String triggerName = 'CLO_TaskTrigger';
        List<CLO_TriggerSetting__mdt> triggerSettings= (List<CLO_TriggerSetting__mdt>) CLO_DBManager.getSObjectList('Id, DeveloperName, isActive__c', 'CLO_TriggerSetting__mdt', 'DeveloperName = ' +CLO_StringUtil.inquoteString('CLO_TaskTrigger'));
        
        // Create test Tasks
        List<Task> testtsks = new List<Task>();
        for(Integer i=0;i<2;i++) {
            testtsks.add(new Task(Subject = 'Test'+i));
        }
        CLO_DBManager.insertSObjects(testtsks); 
        
    }
    
    @isTest static void testTaskCRUD() {
        // Get all Task's by using a SOQL query
        List<Task> testtsks = (List<Task>) CLO_DBManager.getSObjects('SELECT Id FROM Task');
        System.assertEquals(2, testtsks.Size(),'success');

        // Get the first test Task by using a SOQL query
        Task tsk = (Task) CLO_DBManager.getSObject('Id', 'Task', 'Subject= '+ CLO_StringUtil.inquoteString('Test0') +' LIMIT 1');
        //[SELECT Id FROM Task WHERE Name='Test0' LIMIT 1];
        // Modify first Task
        tsk.Status = 'Completed';
        tsk.CLO_Task_Subtype__c = 'No Interest';
        tsk.ActivityDate = System.Today();
        // This update is local to this test method only.
        update tsk;
        // Get all Task's by using a SOQL query
        testtsks = (List<Task>) CLO_DBManager.getSObjects('SELECT Id FROM Task');
        System.assertEquals(3, testtsks.Size(),'success');

        // Delete second Task
        Task tsk2 = (Task) CLO_DBManager.getSObject('Id', 'Task', 'Subject= '+ CLO_StringUtil.inquoteString('Test1') +' LIMIT 1');
        // This deletion is local to this test method only.
        delete tsk2;
        
        // Get all Task's by using a SOQL query
        testtsks = (List<Task>) CLO_DBManager.getSObjects('SELECT Id FROM Task');
        System.assertEquals(2, testtsks.Size(),'success');
        
        undelete tsk2;
        
        // Get all Task's by using a SOQL query
        testtsks = (List<Task>) CLO_DBManager.getSObjects('SELECT Id FROM Task');
        System.assertEquals(3, testtsks.Size(),'success');
    }
    
    @isTest static void testTriggerDisabled() {
        CLO_TaskTriggerHandler.triggerDisabled = true;
 
        // Get all Task's by using a SOQL query
        List<Task> testtsks = (List<Task>) CLO_DBManager.getSObjects('SELECT Id FROM Task');
        System.assertEquals(2, testtsks.Size(),'success');
    }
}
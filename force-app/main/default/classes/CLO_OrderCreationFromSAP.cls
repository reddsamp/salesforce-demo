Public Class CLO_OrderCreationFromSAP
{
    Public Static void createOrderLines(Order ord){
        /*Authentication using bearer token*/
        SAPGetSalesOrderDetails.BAPISDORDER_GETDETAILEDLIST createOrderLinesServiceObj = new SAPGetSalesOrderDetails.BAPISDORDER_GETDETAILEDLIST();
        
        createOrderLinesServiceObj.endpoint_x = CLO_CommonUtil.getEndPointURL('BAPISDORDER_GETDETAILEDLIST');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        createOrderLinesServiceObj.inputHttpHeaders_x = new Map<String,String>();
        createOrderLinesServiceObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        createOrderLinesServiceObj.timeout_x = 120000;
                
        SAPGetSalesOrderDetails.TABLE_OF_BAPIPAREX EXTENSIONOUT = NULL;
        SAPGetSalesOrderDetails.ORDER_VIEW I_BAPI_VIEW = new SAPGetSalesOrderDetails.ORDER_VIEW();
        I_BAPI_VIEW.HEADER = 'X';
        I_BAPI_VIEW.ITEM  = 'X';
        I_BAPI_VIEW.SDSCHEDULE = 'X';
        I_BAPI_VIEW.BUSINESS = 'X';
        I_BAPI_VIEW.PARTNER = 'X';
        I_BAPI_VIEW.ADDRESS = 'X';
        I_BAPI_VIEW.STATUS_H = 'X';
        I_BAPI_VIEW.STATUS_I = 'X';
        I_BAPI_VIEW.SDCOND = 'X';
        I_BAPI_VIEW.SDCOND_ADD = 'X';
        I_BAPI_VIEW.CONTRACT = 'X';
        I_BAPI_VIEW.TEXT = 'X';
        I_BAPI_VIEW.FLOW = 'X';
        I_BAPI_VIEW.BILLPLAN = 'X';
        I_BAPI_VIEW.CONFIGURE = 'X';
        I_BAPI_VIEW.CREDCARD = 'X';
        I_BAPI_VIEW.INCOMP_LOG = 'X';
        
        String I_MEMORY_READ = NULL;
        String I_WITH_HEADER_CONDITIONS = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDCOAD ORDER_ADDRESS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDBPLD ORDER_BILLINGDATES_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDBPL ORDER_BILLINGPLANS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDBUSI ORDER_BUSINESS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUBLBM ORDER_CFGS_CUBLBS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUCFGM ORDER_CFGS_CUCFGS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUINSM ORDER_CFGS_CUINS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUPRTM ORDER_CFGS_CUPRTS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUREFM ORDER_CFGS_CUREFS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUVALM ORDER_CFGS_CUVALS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICUVKM ORDER_CFGS_CUVKS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDCOND ORDER_CONDITIONS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICONDHD ORDER_COND_HEAD = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICONDIT ORDER_COND_ITEM = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICONDQS ORDER_COND_QTY_SCALE = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICONDVS ORDER_COND_VAL_SCALE = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDCNTR ORDER_CONTRACTS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPICCARDM ORDER_CREDITCARDS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDFLOW ORDER_FLOWS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDHD ORDER_HEADERS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDIT ORDER_ITEMS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDPART ORDER_PARTNERS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDHEDU ORDER_SCHEDULES_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDHDST ORDER_STATUSHEADERS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDITST ORDER_STATUSITEMS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPISDTEHD ORDER_TEXTHEADERS_OUT = NULL;
        SAPGetSalesOrderDetails.TABLE_OF_BAPITEXTLI ORDER_TEXTLINES_OUT = NULL;
        
        SAPGetSalesOrderDetails.TABLE_OF_SALES_KEY SALES_DOCUMENTS = new SAPGetSalesOrderDetails.TABLE_OF_SALES_KEY();
        SALES_DOCUMENTS.item = new List<SAPGetSalesOrderDetails.SALES_KEY>();        
        SAPGetSalesOrderDetails.SALES_KEY salesKeyObj = new SAPGetSalesOrderDetails.SALES_KEY();
        salesKeyObj.VBELN = ord.SAP_Order_Id__c;
        SALES_DOCUMENTS.item.add(salesKeyObj);
        
        SAPGetSalesOrderDetails.BAPISDORDER_GETDETAILEDLISTResponse_element responseObj = createOrderLinesServiceObj.BAPISDORDER_GETDETAILEDLIST(EXTENSIONOUT,I_BAPI_VIEW,I_MEMORY_READ,I_WITH_HEADER_CONDITIONS,ORDER_ADDRESS_OUT,ORDER_BILLINGDATES_OUT,ORDER_BILLINGPLANS_OUT,ORDER_BUSINESS_OUT,ORDER_CFGS_CUBLBS_OUT,ORDER_CFGS_CUCFGS_OUT,ORDER_CFGS_CUINS_OUT,ORDER_CFGS_CUPRTS_OUT,ORDER_CFGS_CUREFS_OUT,ORDER_CFGS_CUVALS_OUT,ORDER_CFGS_CUVKS_OUT,ORDER_CONDITIONS_OUT,ORDER_COND_HEAD,ORDER_COND_ITEM,ORDER_COND_QTY_SCALE,ORDER_COND_VAL_SCALE,ORDER_CONTRACTS_OUT,ORDER_CREDITCARDS_OUT,ORDER_FLOWS_OUT,ORDER_HEADERS_OUT,ORDER_ITEMS_OUT,ORDER_PARTNERS_OUT,ORDER_SCHEDULES_OUT,ORDER_STATUSHEADERS_OUT,ORDER_STATUSITEMS_OUT,ORDER_TEXTHEADERS_OUT,ORDER_TEXTLINES_OUT,SALES_DOCUMENTS);
        
        if(responseObj != NULL && responseObj.ORDER_ITEMS_OUT != NULL && responseObj.ORDER_ITEMS_OUT.item != NULL && responseObj.ORDER_ITEMS_OUT.item.size() > 0){
            List<SAPGetSalesOrderDetails.BAPISDIT> lstSAPOrderItems = responseObj.ORDER_ITEMS_OUT.item;
            
            List<OrderItem> lstOrdItem = new List<OrderItem>();
            
            for(SAPGetSalesOrderDetails.BAPISDIT itemSAPOrder : lstSAPOrderItems){
                OrderItem ordItem = new OrderItem(OrderId = ord.Id);
                ordItem.CLO_Primary_Line_Number__c = itemSAPOrder.ITM_NUMBER;
                ordItem.CLO_Parent_Bundle_Number__c = itemSAPOrder.HG_LV_ITEM;
                //Need to change this later - wrong to have it in the loop but doing it for temp testing
                Product2 Prod = [select id from product2 where ProductCode =: itemSAPOrder.MATERIAL];
                String pricingKey = itemSAPOrder.MATERIAL + '%' + itemSAPOrder.CURREN_ISO + '%' + ord.CLO_Sales_Org__c + '%';
                PriceBooKEntry pbe = [SELECT Id from PriceBooKEntry WHERE ProductCode =: itemSAPOrder.MATERIAL AND SAP_Pricing_Key__c LIKE: pricingKey];
                ordItem.Product2Id = Prod.Id;            
                ordItem.PricebookEntryId = pbe.Id;
                //ord.CLO_Sales_Org__c = itemSAPOrder.PLANT;
                ordItem.quantity = Decimal.valueOf(itemSAPOrder.REQ_QTY);
                //ordItem.Product2.QuantityUnitOfMeasure = itemSAPOrder.TARGET_QU;
                ordItem.UnitPrice = Decimal.valueOf(itemSAPOrder.NET_PRICE);
                //ordItem.CLO_Requested_Date__c = itemSAPOrder.REQ_DATE;
                //ordItem.CurrencyIsoCode = itemSAPOrder.CURREN_ISO;
                //itemSAPOrder.PURCH_NO_S = ordItem.Id;
                ordItem.CLO_Tax__c = Decimal.valueOf(itemSAPOrder.TAX_AMOUNT);
                ordItem.CLO_Gross_Value__c = Decimal.valueOf(itemSAPOrder.SUBTOT_PP1);
                ordItem.CLO_Net_Value__c = Decimal.valueOf(itemSAPOrder.SUBTOT_PP2);
                ordItem.CLO_Discount__c = Decimal.valueOf(itemSAPOrder.SUBTOT_PP5);
                ordItem.Description = itemSAPOrder.SHORT_TEXT;
                ordItem.CLO_Cost_in_document_currency__c = Decimal.valueOf(itemSAPOrder.COST_DOC_C);
                ordItem.CLO_Material_group_1__c = itemSAPOrder.PRC_GROUP1;
                ordItem.CLO_PC_Bypass_and_SF_Id__c = itemSAPOrder.PRC_GROUP2;
                ordItem.CLO_Embedded_Warranty__c = itemSAPOrder.PRC_GROUP3;
                ordItem.Capital_Item__c = itemSAPOrder.PRC_GROUP4;
                ordItem.CLO_Weight__c = itemSAPOrder.PRC_GROUP5;
                ordItem.CLO_Price_Override__c = false;
                ordItem.CLO_Charges__c = Decimal.valueOf(itemSAPOrder.SUBTOT_PP4);
                
                /* Not using these for now
                //ordItem.CLO_Installation_Included__c = Boolean.valueOf(itemSAPOrder.PROD_ATTR1);
                //ordItem.CLO_Clinical_Training_Included__c = Boolean.valueOf(itemSAPOrder.PROD_ATTR2);
                //ordItem.CLO_Preceptorship__c = Boolean.valueOf(itemSAPOrder.PROD_ATTR3);
                */
                system.debug('Gross Value===>>>>' + ordItem.CLO_Gross_Value__c);
                system.debug('Net Value===>>>>' + ordItem.CLO_Net_Value__c);
                system.debug('Discount Value===>>>>' + ordItem.CLO_Discount__c);
                
                lstOrdItem.add(ordItem);
            }
            
            if(lstOrdItem.size() > 0){
                insert lstOrdItem;
            }
        }        
    }
}
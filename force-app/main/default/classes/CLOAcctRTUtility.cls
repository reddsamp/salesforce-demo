public class CLOAcctRTUtility {
    public static Map<String, Id> recordTypeIds(){

        Map<String, Id> retVal = new Map<String, Id>();
        
        Schema.DescribeSObjectResult accountObjDetails = Account.SObjectType.getDescribe();
        //retval.put('Bill_To', accountObjDetails.getRecordTypeInfosByName().get('Sold To').getRecordTypeId());
        retval.put('Ship_To', accountObjDetails.getRecordTypeInfosByName().get('Ship To').getRecordTypeId());
        retval.put('Internal_Ship_To',accountObjDetails.getRecordTypeInfosByName().get('Internal Ship-To').getRecordTypeId());
        retval.put('Internal_Sold_To',accountObjDetails.getRecordTypeInfosByName().get('Internal Sold-To').getRecordTypeId());

        return retVal;
    }
}
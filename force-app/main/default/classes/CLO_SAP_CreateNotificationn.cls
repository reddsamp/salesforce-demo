Public Class CLO_SAP_CreateNotificationn
{
    Public Static String CreateCaseNotification(Boolean isTestRun, String caseNumber, String soldTo, String materialNumber, String serialNumber, String contactName, String contactEmail, Boolean patientImpact, Boolean potentialComplaint, String plant, String personResponsible, String caseDescription,String awarenessDate)
    {
        /*Authentication using bearer token*/
        SAPCreateNotificationn.ZSF_CREATE_NOTIFICATION notificationServiceObj = new SAPCreateNotificationn.ZSF_CREATE_NOTIFICATION();
        notificationServiceObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSF_CREATE_NOTIFICATION');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();
        notificationServiceObj.inputHttpHeaders_x = new Map<String,String>();
        notificationServiceObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        notificationServiceObj.timeout_x = 120000;
        
        SAPCreateNotificationn.ZSF_NOTIFICATION IS_CASE_DATA = new SAPCreateNotificationn.ZSF_NOTIFICATION();
        IS_CASE_DATA.NOTIF_TYPE = 'ZP';
        IS_CASE_DATA.CASE_NUMBER = caseNumber;
        IS_CASE_DATA.SOLD_TO = soldTo;
        IS_CASE_DATA.MATERIAL_NUMBER = materialNumber;
        IS_CASE_DATA.SERIAL_NUMBER = serialNumber;
        IS_CASE_DATA.CONTACT_NAME = contactName;
        IS_CASE_DATA.CONTACT_EMAIL = contactEmail;
        IS_CASE_DATA.PATIENT_IMPACT = patientImpact ? 'Y' : 'N';
        IS_CASE_DATA.POTENTIAL_COMPLAINT = potentialComplaint ? 'Y' : 'N';
        IS_CASE_DATA.RETURN_PLANT = plant;
        IS_CASE_DATA.PERSON_RESPONSIBLE = personResponsible;
        IS_CASE_DATA.DESCRIPTION = caseDescription;
        IS_CASE_DATA.AWARENESS_DATE = awarenessDate;
        SAPCreateNotificationn.TABLE_OF_BAPIRET2 RETURN_x = NULL;
        String TESTRUN = isTestRun ? 'X' : '';
            
        SAPCreateNotificationn.ZSF_CREATE_NOTIFICATIONResponse_element responseObj = notificationServiceObj.ZSF_CREATE_NOTIFICATION(IS_CASE_DATA, RETURN_x, TESTRUN); 
        
        system.debug('CreateNotificationResponse-->>'+ JSON.SerializePretty(responseObj));
        
        return responseObj.E_NOTIF_NO;
        
    }
}
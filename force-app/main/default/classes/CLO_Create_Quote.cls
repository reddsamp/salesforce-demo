public class CLO_Create_Quote {
 public CLO_Create_Quote (apexPages.standardController cont){}
    public pageReference NewQuote()
    {
        SBQQ__Quote__c f = new SBQQ__Quote__c();
        // defaults will load into the record on insert
        insert f;

        pageReference page = new pageReference('/' + f.Id);
        page.setRedirect(true);
        return page;
    }
}
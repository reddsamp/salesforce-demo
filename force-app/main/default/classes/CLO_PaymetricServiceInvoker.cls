public with sharing class CLO_PaymetricServiceInvoker {
    public CLO_PaymetricServiceInvoker() {

    }

    @future (callout = true)
    public static void invokeCreditCardPaymentService(String accountNumber, String quoteNumber, String orderNumber, String amount, 
                                                      String ccType, String expDate, String tokenizeCardNum,
                                                      String CurrencyIsoCode, String SalesOrg)
    {
        system.debug('accountNumber :: '+accountNumber);
        system.debug('quoteNumber :: '+quoteNumber);
        system.debug('orderNumber :: '+orderNumber);
        system.debug('amount :: '+amount);
        system.debug('ccType :: '+ccType);
        system.debug('expDate :: '+expDate);
        system.debug('tokenizeCardNum :: '+tokenizeCardNum);
        system.debug('CurrencyIsoCode :: '+CurrencyIsoCode);
        system.debug('SalesOrg :: '+SalesOrg);


        CLOSAPPaymetricDirectAR.ZSF_PMPAY_WEB_DIRECT_AR payMetricObj = new CLOSAPPaymetricDirectAR.ZSF_PMPAY_WEB_DIRECT_AR();
        payMetricObj.endpoint_x = CLO_CommonUtil.getEndPointURL('ZSF_PMPAY_WEB_DIRECT_AR');
        String AuthHeader = 'Bearer ' + CLO_SAP_AuthenticationHelper.getAccessToken();        
        payMetricObj.inputHttpHeaders_x = new Map<String,String>();
        payMetricObj.inputHttpHeaders_x.put('Authorization', AuthHeader);
        payMetricObj.timeout_x = 120000;

        /*1st param*/
        CLOSAPPaymetricDirectAR.TABLE_OF_BDCMSGCOLL BDCMSG = NULL;

        /*2nd param*/
        CLOSAPPaymetricDirectAR.TABLE_OF_CCADDRESS CCARD_ADDRESS = new CLOSAPPaymetricDirectAR.TABLE_OF_CCADDRESS();
        CCARD_ADDRESS.item = new List<CLOSAPPaymetricDirectAR.CCADDRESS>();
        
        /*3rd param*/
        CLOSAPPaymetricDirectAR.TABLE_OF_CCDATA CCARD_DATA = new CLOSAPPaymetricDirectAR.TABLE_OF_CCDATA();

        CCARD_DATA.item = new List<CLOSAPPaymetricDirectAR.CCDATA>();
        CLOSAPPaymetricDirectAR.CCDATA itemCCDATA = new CLOSAPPaymetricDirectAR.CCDATA();

        itemCCDATA.CCINS = ccType.toUpperCase();//ccDetails.CLO_CC_Type__c.toUpperCase();
        itemCCDATA.CCNUM = tokenizeCardNum;//'-E803-1111-ZC0PZWPJY6NR18';//String.valueOf(ccDetails.CLO_Tokenize_Card_Number__c);
        itemCCDATA.CCFOL = '';
        itemCCDATA.DATAB = '';
        itemCCDATA.DATBI = expDate;//dtExpirydDate.format('yyyy-MM-dd');
        itemCCDATA.CCNAME = '';
        itemCCDATA.CSOUR = '';
        itemCCDATA.AUTWR = amount;
        itemCCDATA.CCWAE = '';
        itemCCDATA.SETTL = '';
        itemCCDATA.AUNUM = '';
        itemCCDATA.AUTRA = '';
        itemCCDATA.AUDAT = '';
        itemCCDATA.AUTIM = '';
        itemCCDATA.MERCH = '';
        itemCCDATA.LOCID = '';
        itemCCDATA.TRMID = '';
        itemCCDATA.CCBTC = '';
        itemCCDATA.CCTYP = '';
        itemCCDATA.CCARD_GUID = '';
        itemCCDATA.DP_TOKEN = '';
        itemCCDATA.DP_PSP = '';
        itemCCDATA.DP_PAYID = '';
        itemCCDATA.DP_PSP_TRANSID = '';

        CCARD_DATA.item.add(itemCCDATA);

        /*4th param*/
        CLOSAPPaymetricDirectAR.TABLE_OF_CCAUT_R CCAUT_R = new CLOSAPPaymetricDirectAR.TABLE_OF_CCAUT_R();
        CCAUT_R.item = new List<CLOSAPPaymetricDirectAR.CCAUT_R>();

        CLOSAPPaymetricDirectAR.CCAUT_R itemCCAUTR = new CLOSAPPaymetricDirectAR.CCAUT_R();

        itemCCAUTR.VBELN = ''; 
        itemCCAUTR.FPLTR = '';
        itemCCAUTR.REACT = '';
        itemCCAUTR.AUTWR = amount;
        itemCCAUTR.AUNUM = '';
        itemCCAUTR.AUTRA = '';
        itemCCAUTR.RCAVR = '';
        itemCCAUTR.RCAVA = '';
        itemCCAUTR.RCAVZ = '';
        itemCCAUTR.RCRSP = '';
        itemCCAUTR.RTEXT = '';
        itemCCAUTR.RCCVV = '';

        CCAUT_R.item.add(itemCCAUTR);

        /*5th param*/
        CLOSAPPaymetricDirectAR.TABLE_OF_x_PMPAY_xECP ECP = new CLOSAPPaymetricDirectAR.TABLE_OF_x_PMPAY_xECP();
        
        /*few String params*/
        String I_AUGLV = '';
        String I_AUGTX = 'Customer DAR posting';
        String I_AUTH = 'X';
        string strPaymentRef = (quoteNumber != null ? quoteNumber : orderNumber);
        String I_BKTXT = strPaymentRef;//ccDetails.CLO_Payment_Reference__c;
        String I_BLART = 'DZ';
        String I_BLDAT = DateTime.now().format('yyyy-MM-dd');
        String I_BUDAT = DateTime.now().format('yyyy-MM-dd');
        String I_BUKRS = SalesOrg; //ccDetails.account__r.CLO_Sales_Org__c;
        String I_EXTERNAL = '';
        String I_KALSM = 'ZPAYAR';
        String I_KUNNR = accountNumber;//ccDetails.account__r.AccountNumber;
        String I_MODE = 'N';
        String I_PRCTR = '';
        String I_RESIDUAL = '';

        /*20th param*/
        CLOSAPPaymetricDirectAR.TABLE_OF_BAPIRET2 I_RETURN = new CLOSAPPaymetricDirectAR.TABLE_OF_BAPIRET2();

        /*few more string params*/
        String I_SGTXT = 'Payment card DAR posting';
        String I_USER = '';
        String I_WAERS = CurrencyIsoCode;//ccDetails.account__r.CurrencyISOCode;
        String I_XBLNR = 'POST DAR PAYMENT';//hardcoded

        CLOSAPPaymetricDirectAR.ZSF_PMPAY_WEB_DIRECT_ARResponse_element responseObj = payMetricObj.ZSF_PMPAY_WEB_DIRECT_AR(BDCMSG, CCARD_ADDRESS, CCARD_DATA, CCAUT_R, ECP, I_AUGLV, I_AUGTX, I_AUTH, I_BKTXT, I_BLART, I_BLDAT, I_BUDAT, I_BUKRS, I_EXTERNAL, I_KALSM, I_KUNNR, I_MODE, I_PRCTR, I_RESIDUAL, I_RETURN, I_SGTXT, I_USER, I_WAERS, I_XBLNR);
        system.debug('>>>>>>>>MSGTYP :: '+responseObj.BDCMSG);
        /*system.debug('>>>>>>>>MSGTYP :: '+responseObj.BDCMSG.item);
        for(CLOSAPPaymetricDirectAR.BDCMSGCOLL obj : responseObj.BDCMSG.item){
            system.debug('>>>>>>>>MSGTYP :: '+obj.MSGTYP);
            system.debug('>>>>>>>>MSGSPRA :: '+obj.MSGSPRA);
        }*/
        //Update new CLO_Credit_Card_Details__c(Id = ccDetails.Id, CLO_SAP_Response__c = Json.serializePretty(responseObj));
        String errorMessage = '';
        if(responseObj.I_RETURN.item != null) {
            
            for(CLOSAPPaymetricDirectAR.BAPIRET2 bapiReturn : responseObj.I_RETURN.item){
                if(bapiReturn.TYPE_x == 'E'){
                    errorMessage += bapiReturn.Message + '\n';
                }
            }
            system.debug('>>>>>>>>response  Obj.E_BELNR :: '+responseObj.E_BELNR);
        }
        if(quoteNumber != null)
        {
            List<SBQQ__Quote__c>  lstQuotes = [SELECT Id, Name FROM SBQQ__Quote__c WHERE Name=: quoteNumber];
            system.debug('lstQuotes :: '+lstQuotes);
            Update new SBQQ__Quote__c(Id = lstQuotes[0].Id, CLO_SAP_Sync_Error__c = errorMessage, CLO_Deposit_Amount__c = Decimal.ValueOf(amount), CLO_GL_Document_Number__c = responseObj.E_BELNR);   
        }
        else if(orderNumber != null)
        {
            List<Order>  lstOrders = [SELECT Id, OrderNumber FROM Order WHERE OrderNumber=: orderNumber];
            system.debug('lstOrders :: '+lstOrders);
            
            //Update new Order(Id = lstOrders[0].Id, CLO_SAP_Sync_Error__c = errorMessage, CLO_Deposit_Amount__c = Decimal.ValueOf(amount), CLO_GL_Document_Number__c = responseObj.E_BELNR);   
        }
        system.debug('response---cc-->>'+ responseObj);
    }
}
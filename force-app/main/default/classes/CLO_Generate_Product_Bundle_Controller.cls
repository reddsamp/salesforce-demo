public class CLO_Generate_Product_Bundle_Controller {

    public List<Product2> productList {get;set;}
    public List<String> memIds {get;set;}
    public list <Account> acc {get;set;}
    public String searchKey {get;set;}
    public Id selectedAccount { get; set; }
    public List<String> optProdIds  {get; set;}
    public List<String> priceBookIds  {get; set;}
    public List<PricebookEntry> priceBook {get; set;}
    public Map<String, List<SBQQ__ProductOption__c>> options {get;set;}
    public Map<String, PricebookEntry> pBooks {get;set;}
    PageReference reportPage {get;set;}// Account selected on Visualforce page
    public List<SelectOption> recentAccounts {
        get {
            if(null == recentAccounts){
                recentAccounts = new List<SelectOption>();
                for(Account acct : [SELECT Id,Name,LastModifiedDate 
                                    FROM Account 
                                    ORDER BY LastModifiedDate DESC 
                                    LIMIT 10]) {
                    recentAccounts.add(new SelectOption(acct.Id, acct.Name));
                }
            }
            return recentAccounts;
        }
        set;
    }
     public CLO_Generate_Product_Bundle_Controller(ApexPages.StandardSetController controller)
    {
       productList = new List<Product2>();
      	
        memIds = new List<String>();
        for (Product2 mem : (List<Product2>)controller.getSelected()){
            System.debug('entered');
            memIds.add(mem.Id);
            System.debug(memIds);
        }
        
        
        productList = [select id, Name,Family, SBQQ__DefaultQuantity__c,ProductCode from Product2 where id in : memIds];
    }
        
    public void search(){
        string searchquery='select Name,id from account where name like  '+searchKey+'%';
        acc= Database.query(searchquery);
    }
    public void generateBundleList(List<String> memIds){
        for (String mem : memIds){ 
            List<SBQQ__ProductOption__c> product_options = [SELECT Id,SBQQ__ProductDescription__c, SBQQ__UnitPrice__c,SBQQ__Discount__c,SBQQ__Quantity__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :mem];
        }
        
        string searchquery='select Name,id from account where name like  '+searchKey+'%';
        acc= Database.query(searchquery);
    }
}
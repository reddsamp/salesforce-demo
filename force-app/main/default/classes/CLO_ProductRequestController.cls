public class CLO_ProductRequestController
{
    public String woId {get;set;}
    public String prliId {get;set;}
    public ProductRequest prli {get;set;}
    
    public CLO_ProductRequestController(ApexPages.StandardController controller) {
        String countryCode = [select id, CountryCode from User where Id =: UserInfo.getUserId()].CountryCode;
        system.debug('countryCode :::::'+ countryCode);
        
        woId = ApexPages.currentPage().getParameters().get('WorkOrder_lkid');
        
        if(woId == null)
            woId = ApexPages.currentPage().getParameters().get('id');
            
        prli = new ProductRequest();
        prli.WorkorderId = woId;
        
        
        
        List<Workorder> woList = [select id, Owner.Name from Workorder where id =: woId];
        
        if(woList.size() > 0)
        {
            List<Account> accList = [select id, Name from Account where Name =: woList[0].Owner.Name];
            
            if(accList.size() > 0)
                prli.AccountId = accList[0].Id;
        }
            
        
        Product_Request_Source_Location__c prsl = Product_Request_Source_Location__c.getAll().get(countryCode);
        
        if(prsl != null)
            prli.CLO_Source_Location__c = prsl.Source_Location__c;
            
        queryProductRequest(ApexPages.currentPage().getParameters().get('id'));
    }
    
    public void queryProductRequest(String prId)
    {
        if(prId != null && woId != prId)
        {
            prli = [select id, WorkorderId, Status, AccountId, ShipmentType, Parts_o__c, CLO_Source_Location__c, Description,
                    NeedByDate, Ship_to_Account__c, ShipToCountryCode, ShipToStreet, ShipToCity, ShipToStateCode, ShipToPostalCode
                    from ProductRequest
                    where id =: prId];
        }
    }
    
    public void fillShipToAddess()
    {
        List<Account> accList = [select id, ShippingStreet, ShippingCity, ShippingCountryCode, ShippingStateCode, ShippingPostalCode
                                 from Account
                                 where id =: prli.Ship_to_Account__c];
    
        if(accList.size() > 0)
        {
            prli.ShipToStreet = accList[0].ShippingStreet;
            prli.ShipToCity = accList[0].ShippingCity;
            prli.ShipToCountryCode = accList[0].ShippingCountryCode;
            prli.ShipToStateCode = accList[0].ShippingStateCode;
            prli.ShipToPostalCode = accList[0].ShippingPostalCode;
        }
    }

    public void savePRLI()
    {
        try
        {
            upsert prli;
            prliId = prli.Id;
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
}
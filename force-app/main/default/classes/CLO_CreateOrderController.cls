public class CLO_CreateOrderController {
    
    public Order order {get;set;}
    String accountId;
    public List<SelectOption> shipToOptions {get;set;}
    
    public CLO_CreateOrderController(ApexPages.StandardController controller) {
        accountId = ApexPages.currentPage().getParameters().get('id');
        shipToOptions = new List<SelectOption>();
        
        Account acc = [select id, CLO_Sales_Org__c, CLO_Shipping_Condition__c, CurrencyIsoCode, BillingCountryCode, BillingCountry,
                       CLO_Payment_Terms__c, CLO_Incoterm_1__c, CLO_Incoterm_2__c from Account where id =: accountId];
        
        order = new Order();
        
        Set<String> nameList = new Set<String>();
        nameList.add(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode);
        nameList.add(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountry);
        
        system.debug('nameList :::::' + nameList);
        for(PriceBook2 pb : [select id, Name from PriceBook2 
                             where Name in : nameList])
        {
            order.Pricebook2Id = pb.Id;
        }
        
        order.AccountId = accountId ;
        order.CLO_Ship_To__c = accountId ;
        order.Status = 'Draft';
        order.CLO_Sales_Org__c = acc.CLO_Sales_Org__c;
        order.PODate = Date.today();
        order.CLO_Requested_Delivery_Date__c = Date.today();
        
        order.CLO_Shipping_Condition__c = acc.CLO_Shipping_Condition__c;
        order.CurrencyIsoCode = acc.CurrencyIsoCode;
        order.CLO_Payment_Terms__c = acc.CLO_Payment_Terms__c;
        order.CLO_Incoterm_1__c = acc.CLO_Incoterm_1__c;
        order.CLO_Incoterm_2__c = acc.CLO_Incoterm_2__c;
        order.EffectiveDate = Date.today();
        
        for(Account a : [select id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry 
                         from Account 
                         where id =: accountId
                         or ParentId =: accountId])
        {
            String address = '';
            if(a.ShippingStreet != null) address += a.ShippingStreet+', ';
            if(a.ShippingCity != null) address += a.ShippingCity+', ';
            if(a.ShippingState != null) address += a.ShippingState+', ';
            if(a.ShippingPostalCode != null) address += a.ShippingPostalCode+', ';
            if(a.ShippingCountry  != null) address += a.ShippingCountry;
            
            shipToOptions.add(new SelectOption(a.Id, a.Name + ' (Address: ' + address + ')'));
        }
    }
    
    public String oredrId {get;set;}
    public String oredrName {get;set;}
    
    public PageReference createOrder()
    {
        try{
            insert order;
            
            oredrId = order.Id;
            oredrName = [select id, OrderNumber from Order where id =: order.Id].OrderNumber;
            
            return null;//new PageReference('/lightning/r/Account/'+accountId +'/view').setRedirect(true);
        }
        catch(Exception ex)
        {
            String err = ex.getMessage();
            if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                err = err.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,')[1];
                
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, err));
            return null;
        }
    }
    
    public PageReference refreshView()
    {
        return new PageReference('/lightning/r/Account/'+accountId +'/view').setRedirect(true);
    }
    
    public void refreshOrder(){}
}
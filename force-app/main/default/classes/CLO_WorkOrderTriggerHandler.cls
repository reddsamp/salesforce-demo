/**
  * @ClassName      : CLO_WorkOrderTriggerHandler 
  * @Description    : WorkOrder Object Trigger Handler.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */

  public class CLO_WorkOrderTriggerHandler implements CLO_ITriggerHandler{
     
    //Use this variable to disable this trigger from transaction
    public static Boolean triggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return triggerDisabled;
    }
     
/**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle before insert logic.
  * @Parameters  : List<sObject>
  * @Return      : Void 
  */
    public void beforeInsert(List<sObject> newList) {
        Map<Id, Account> customerMap = new Map<Id, Account>();
        Map<String, String> pricebookMap = new Map<String, String>();
        List<String> customerIds = new List<String>();
        List<WorkOrder> lstWorkOrders = new List<WorkOrder>();

        for(WorkOrder o : (List<WorkOrder>) newList)
        {
            If(!customerIds.contains(o.AccountId) && o.AccountId != Null){
                customerIds.add(o.AccountId);
            }
            /*
            if(o.CLO_Payment_Terms__c == 'CRCD' && o.CLO_Payment_Card__c != null) {
                //String errorMessage = 'Please select a Payment Card';
                //o.addError(errorMessage);
                
                Map<String, String>  mapResponse = CXiIntercept3.callRFC(o.CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c,
                o.CLO_Payment_Card__r.CLO_CC_Expiry_Month__c+'/'+o.CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, 
                                                                        11,
                                                                        o.CLO_Payment_Card__r.CLO_CC_Type__c, 
                                                                        o.Account.AccountNumber);
                                                                            
                System.debug('mapResponse ::: '+mapResponse);                                                                        
                o.CLO_Pre_authorized__c = mapResponse.get('status');
                o.CLO_Pre_authorized_Reason__c = mapResponse.get('StatusCode') + ' and ' + mapResponse.get('StatusTXN');
                
                lstWorkOrders.add(o);
                
            }*/
        }
        
        If(customerIds.size() > 0){
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(customerIds);        
            customerMap = new Map<Id, Account>((List<Account>)CLO_DBManager.getSObjectList(' Id, CLO_Sales_Org__c, BillingCountry, BillingCountryCode ',' Account ', whereClause));      
        }
        for(PriceBook2 pb : (List<PriceBook2>)CLO_DBManager.getSObjects(' Select id, Name From PriceBook2 '))
        {
            pricebookMap.put(pb.Name, pb.Id);
        }
        
        for(WorkOrder o : (List<WorkOrder>) newList)
        {
            Account acc = customerMap.get(o.AccountId);       
            String priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode);
            
            if(priceBookId == null){
                priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountry);
            }    
            
            o.PriceBook2Id = priceBookId;
            
        }
        if(lstWorkOrders.size() > 0){
            try{
                CLO_DBManager.updateSObjects(lstWorkOrders);
            } Catch(DMLException de){
                System.debug('lstWorkOrders DML @@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstWorkOrders EX @@@ : '+e.getMessage());
            }   
        }
    }
    
/**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle after insert logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
    public void afterInsert(List<sObject> newList , Map<Id, sObject> newMap) {
       
    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle before update logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */  
    public void beforeUpdate(List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {
    
        Map<id,WorkOrder> mapOldWo = (Map<id,WorkOrder>) oldMap;
        Map<id,id> mapWoIdUsrId = new Map<id,id>();
        List<Id> lstWoAcptd = new List<Id>();
        List<Id> lstWoRejectd = new List<Id>();
        List<WorkOrder> lstWoUpdate = new List<WorkOrder>();
        List<AssignedResource> lstAssResource = new List<AssignedResource>();
        List<AssignedResource> lstAssResUpd = new List<AssignedResource>();
        List<ServiceAppointment> lstSerAppointment = new List<ServiceAppointment>();
        Map<Id,Id> mapWIdUid = new Map<Id,Id>();
        List<Id> lstSr = new List<Id>();
        List<WorkOrder> lstWorkOrders = new List<WorkOrder>();

        for(WorkOrder objWo : (List<WorkOrder>) newList){
            If(objWo.Status == 'Assigned' && mapOldWo.get(objWo.id).Status != objWo.Status){
               lstWoAcptd.add(objWo.id); 
            }
            /*
            if(objWo.CLO_Payment_Terms__c == 'CRCD' && objWo.CLO_Payment_Card__c != null) {
                //String errorMessage = 'Please select a Payment Card';
                //objWo.addError(errorMessage);

                Map<String, String>  mapResponse = CXiIntercept3.callRFC(objWo.CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c,
                objWo.CLO_Payment_Card__r.CLO_CC_Expiry_Month__c+'/'+objWo.CLO_Payment_Card__r.CLO_CC_Expiry_Year__c, 
                                                                        11,
                                                                        objWo.CLO_Payment_Card__r.CLO_CC_Type__c, 
                                                                        objWo.Account.AccountNumber);
                                                                            
                System.debug('mapResponse ::: '+mapResponse);                                                                        
                objWo.CLO_Pre_authorized__c = mapResponse.get('status');
                objWo.CLO_Pre_authorized_Reason__c = mapResponse.get('StatusCode') + ' and ' + mapResponse.get('StatusTXN');
                
                lstWorkOrders.add(objWo);
            }
           */
        }
        if(lstWorkOrders.size() > 0){
            try{
                CLO_DBManager.updateSObjects(lstWorkOrders);
            } Catch(DMLException de){
                System.debug('lstWorkOrders DML @@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstWorkOrders EX @@@ : '+e.getMessage());
            }   
        }

        If(lstWoAcptd.size() > 0){
            String whereClause = ' ServiceAppointment.ParentRecordId IN ' + CLO_StringUtil.getInClauseId(lstWoAcptd);        
            for(AssignedResource objAr : (List<AssignedResource>)CLO_DBManager.getSObjectList(' Id,ServiceAppointment.ParentRecordId,ServiceResourceId,ServiceResource.RelatedRecordId ',' AssignedResource ', whereClause)){
                mapWoIdUsrId.put(objAr.ServiceAppointment.ParentRecordId,objAr.ServiceResource.RelatedRecordId);
            }
        }
        
        for(WorkOrder objWos : (List<WorkOrder>) newList){
            if(mapWoIdUsrId != Null && mapWoIdUsrId.get(objWos.id) != Null){
                objWos.OwnerId = mapWoIdUsrId.get(objWos.id);  
                lstWoUpdate.add(objWos);  
            }
        }
        
        If(lstWoUpdate.size() > 0){
            try{
                CLO_DBManager.updateSObjects(lstWoUpdate);
            } Catch(DMLException de){
                System.debug('lstWoUpdate DML @@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstWoUpdate EX @@@ : '+e.getMessage());
            }   
        }
        
    }
    
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle after update logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
    public void afterUpdate(List<sObject> newList, Map<Id,sObject> newMap,  List<sObject> oldList, Map<Id,sObject> oldMap) {
        
        Map<id,WorkOrder> mapOldWo = (Map<id,WorkOrder>) oldMap;
        List<Id> lstWoRejectd = new List<Id>();
        List<WorkOrder> lstWoUpdate = new List<WorkOrder>();
        List<AssignedResource> lstAssResource = new List<AssignedResource>();
        List<AssignedResource> lstAssResUpd = new List<AssignedResource>();
        List<ServiceAppointment> lstSerAppointment = new List<ServiceAppointment>();
        Map<Id,Id> mapWIdUid = new Map<Id,Id>();
        List<Id> lstSr = new List<Id>();
        List<Id> lstCasIds = new List<Id>();
        Map<Id,String> mapDisNote = new Map<Id,String>();
        List<Case> lstCasDn = new List<Case>();
        String strDispatchNote ='';
        List<Id> lstCCIds = new List<Id>();
        Map<Id, CLO_Credit_Card_Details__c> mapCCDetails;
        for(WorkOrder objWo : (List<WorkOrder>) newList){
            if(objWo.CLO_Payment_Card__c != null) {
                lstCCIds.add(objWo.CLO_Payment_Card__c);
            }

        }

        if(lstCCIds.size() > 0) {
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(lstCCIds);
            mapCCDetails = new Map<Id, CLO_Credit_Card_Details__c>((List<CLO_Credit_Card_Details__c>)CLO_DBManager.getSObjectList(' id, CLO_Tokenize_Card_Number__c, CLO_CC_Expiry_Month__c, CLO_CC_Expiry_Year__c, CLO_CC_Type__c, Account__r.AccountNumber ', ' CLO_Credit_Card_Details__c ', whereClause));
        }
        
        for(WorkOrder objWo : (List<WorkOrder>) newList){
            
            If(objWo.CLO_Dispatch_Note__c != mapOldWo.get(objWo.id).CLO_Dispatch_Note__c){
                lstCasIds.add(objWo.caseId);
            }
            If(objWo.Status == CLO_Constants.REJECTED && mapOldWo.get(objWo.id).Status != objWo.Status){
               lstWoRejectd.add(objWo.id); 
            }
            If(mapOldWo.get(objWo.id).OwnerId != objWo.OwnerId){
                 mapWIdUid.put(objWo.id,objWo.OwnerId);   
            }
            if(!System.isFuture() && objWo.CLO_Payment_Terms__c == 'CRCD' && mapCCDetails != null) {
                CXiIntercept3.callRFCAsync(mapCCDetails.get(objWo.CLO_Payment_Card__c).CLO_Tokenize_Card_Number__c,
                mapCCDetails.get(objWo.CLO_Payment_Card__c).CLO_CC_Expiry_Month__c+'/'+mapCCDetails.get(objWo.CLO_Payment_Card__c).CLO_CC_Expiry_Year__c, 
                                                                        1.0,
                                                                        mapCCDetails.get(objWo.CLO_Payment_Card__c).CLO_CC_Type__c, 
                                                                        mapCCDetails.get(objWo.CLO_Payment_Card__c).Account__r.AccountNumber, objWo.Id, null, null);
                                                                            
            }
            
        }
        
        If(lstCasIds.size() > 0){
            for(Case objCase : [Select Id,CLO_Dispatch_Note__c,(Select Id,CaseId,CLO_Dispatch_Note__c From WorkOrders) From Case where Id IN : lstCasIds]){
                for(WorkOrder objWorkOrd : objCase.WorkOrders){
                    If(mapDisNote != Null && mapDisNote.containsKey(objWorkOrd.CaseId)){
                        strDispatchNote = mapDisNote.get(objWorkOrd.CaseId) +' : '+objWorkOrd.CLO_Dispatch_Note__c;
                        mapDisNote.put(objWorkOrd.CaseId,strDispatchNote.remove('null'));
                    } else {
                        If(objWorkOrd.CLO_Dispatch_Note__c != Null){
                            mapDisNote.put(objWorkOrd.CaseId,objWorkOrd.CLO_Dispatch_Note__c);
                        } else {
                            mapDisNote.put(objWorkOrd.CaseId,'');
                        }
                    }
                    
                }
            }
        
        }
        
        System.debug('%%%'+mapDisNote);
        
        If(mapDisNote != Null && mapDisNote.keySet().size() > 0){
            for(Id casId : mapDisNote.keySet()){
                Case objCas = new Case(ID = casId);
                objCas.CLO_Dispatch_Note__c = mapDisNote.get(casId);
                lstCasDn.add(objCas);
            }
        }
        
        if(mapWIdUid != null && mapWIdUid.values().size() > 0){
            String whereClause = ' RelatedRecordId IN ' + CLO_StringUtil.getInClauseId(mapWIdUid.values());
            for(ServiceResource objSr : (List<ServiceResource>)CLO_DBManager.getSObjectList(' Id,RelatedRecordId ',' ServiceResource ', whereClause)){
                lstSr.add(objSr.Id);    
            }
        }
        
        if(mapWIdUid != null && mapWIdUid.keyset().size() > 0){
            List<Id> lstUids = new List<Id>();
            lstUids.addAll(mapWIdUid.keyset());
            String whereClause = ' ServiceAppointment.ParentRecordId IN ' + CLO_StringUtil.getInClauseId(lstUids);
            for(AssignedResource objAr : (List<AssignedResource>)CLO_DBManager.getSObjectList(' Id,ServiceAppointment.ParentRecordId,ServiceResourceId ', ' AssignedResource ', whereClause)){
                objAr.ServiceResourceId = lstSr[0];
                lstAssResUpd.add(objAr);   
            }
        }
        
        If(lstWoRejectd.size() > 0){
            String whereClause = ' ServiceAppointment.ParentRecordId IN ' + CLO_StringUtil.getInClauseId(lstWoRejectd);
            for(AssignedResource objArs : (List<AssignedResource>)CLO_DBManager.getSObjectList(' Id,ServiceAppointment.ParentRecordId,ServiceResourceId,ServiceResource.RelatedRecordId ', ' AssignedResource ', whereClause)){
                ServiceAppointment objSa = new ServiceAppointment(Id = objArs.ServiceAppointmentId);
                objSa.Status = '';
                lstSerAppointment.add(objSa);
                lstAssResource.add(objArs);
            }
        }
        
        If(lstCasDn.size() > 0 ){
            try{
                              
                CLO_DBManager.updateSObjects(lstCasDn);
                
            } Catch(DMLException de){
                System.debug('lstCasDn DML @@@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstCasDn EX @@@ : '+e.getMessage());
            }   
        }
        
        If(lstSerAppointment.size() > 0 ){
            try{
                              
                CLO_DBManager.updateSObjects(lstSerAppointment); 
                
            } Catch(DMLException de){
                System.debug('lstSerAppointment DML @@@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstSerAppointment EX @@@ : '+e.getMessage());
            }   
        }
        
        If(lstAssResource.size() > 0){
            try{
                
                CLO_DBManager.deleteSObjects(lstAssResource);
                
            } Catch(DMLException de){
                System.debug('lstAssResource DML @@@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstAssResource EX @@@ : '+e.getMessage());
            }    
        }
        
        If(lstAssResUpd.size() > 0 ){
            try{
                
                CLO_DBManager.updateSObjects(lstAssResUpd); 
            } Catch(DMLException de){
                System.debug('lstAssResUpd DML @@@ : '+de.getMessage());
            } Catch(Exception e){
                System.debug('lstAssResUpd EX @@@ : '+e.getMessage());
            }    
        }
      
    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle before delete logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
    public void beforeDelete(List<sObject> oldList , Map<Id, sObject> oldMap) {

    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle after un delete logic.
  * @Parameters  : List<sObject>, Map<Id, sObject>
  * @Return      : Void 
  */ 
   
    public void afterUnDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
    public void afterDelete(List<sObject> newList, Map<Id, sObject> newMap) {
      
    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle create order logic.
  * @Parameters  : List<Workorder>, Map<Id, Workorder>
  * @Return      : Void 
  */
    public static void CreateOrder(List<Workorder> workorders, Map<Id, Workorder> oldmap)
    {
        List<Order> orderInsertList = new List<Order>();
        List<OrderItem> orderItemInsertList = new List<OrderItem>();
        List<OrderItem> orderItemInsertListPc = new List<OrderItem>();
        List<Workorder> workordersList = new List<Workorder>();
        
        for(Workorder wo : workorders){
            if(wo.Status == 'Closed' && wo.Status != oldmap.get(wo.Id).Status)
            {
                workordersList.add(wo);
                if(wo.AssetId != null){
                    Asset Assetitem = [select Id  from Asset where id=:wo.AssetId Limit 1];
                    System.debug(Assetitem);
                    Assetitem.Status = 'Installed';
                    Assetitem.InstallDate= Date.today();
                    Update Assetitem;
                }
                
            }
        }
        
        List<Workorder> workordersTempList = [select Id, Status,StartDate,owner.email,AccountId,Pricebook2Id,CLO_Payment_Terms__c,CLO_Billable_Status__c,OwnerId,CLO_Authorization_Amount__c, CLO_Payment_Transction_ID__c,CLO_Pre_authorized__c, CLO_Product__c,CLO_Billing_Reason__c,CLO_PO_Number__c,CaseId,Case.CLO_Serial_Number__c,Case.Asset.ProductCode,CLO_Payment_Card__c,Case.AssetId, (select id,Product__c,Quantity,UnitPrice,PricebookEntryId,CLO_Non_Billable__c,CLO_Discount__c,Discount from WorkorderLineItems),(Select Id,Product2Id,QuantityConsumed,UnitPrice,PricebookEntryId,ProductItemId,ProductItem.SerialNumber,CLO_Non_Billable__c From ProductsConsumed) from Workorder where id in : workordersList];
        
        if(workordersTempList.size() > 0)
        {
            for(Workorder wo : workordersTempList){
                if(wo.StartDate != null)
                    orderInsertList.add(new Order(AccountId = wo.AccountId,
                                                  CLO_Requested_Delivery_Date__c = wo.StartDate.date(),
                                                  EffectiveDate = wo.StartDate.date(),
                                                  CLO_Product__c = wo.CLO_Product__c,
                                                  Pricebook2Id = wo.Pricebook2Id,
                                                  CLO_Work_Order__c = wo.id,
                                                  Type = CLO_Constants.ZSB,
                                                  PoDate = System.today(),
                                                  CLO_Payment_Terms__c = wo.CLO_Payment_Terms__c,
                                                  PoNumber = wo.CLO_PO_Number__c,
                                                  CLO_Asset__c = wo.Case.AssetId,
                                                  OwnerId=wo.ownerid,
                                                  CLO_Asset_Material_Number__c = wo.Case.Asset.ProductCode,
                                                  CLO_Asset_Number__c = wo.Case.CLO_Serial_Number__c,
                                                  CLO_Payment_Card__c = wo.CLO_Payment_Card__c,
                                                  CLO_Billing_Reason__c = wo.CLO_Billable_Status__c,
                                                  CLO_Authorization_Amount__c = wo.CLO_Authorization_Amount__c,
                                                  CLO_Payment_Transction_ID__c = wo.CLO_Payment_Transction_ID__c,
                                                  CLO_Pre_authorized__c = wo.CLO_Pre_authorized__c,
                                                  Status = CLO_Constants.DRAFT));
            }
        }
        
        if(orderInsertList.size() > 0)
        {
            insert orderInsertList;
            
            integer count = 0;
            
            if(workordersTempList.size() > 0){
                for(Workorder wo : workordersTempList){
                    System.debug('@@@ : '+wo.WorkorderLineItems);
                    for(WorkorderLineItem woli : wo.WorkorderLineItems){
                        orderItemInsertList.add(new OrderItem(OrderId = orderInsertList[count].Id,
                                                              Product2id = woli.Product__c,
                                                              Quantity = woli.Quantity,
                                                              CLO_Discount__c = woli.Discount,
                                                              PricebookEntryId = woli.PricebookEntryId,
                                                              CLO_Non_Billable__c = woli.CLO_Non_Billable__c,
                                                              CLO_Asset_Serial_Number__c = wo.Case.CLO_Serial_Number__c,
                                                              UnitPrice = woli.UnitPrice));
                    }
                    
                    System.debug('@@@ : '+wo.ProductsConsumed);
                    for(ProductConsumed pc : wo.ProductsConsumed){
                        orderItemInsertList.add(new OrderItem(OrderId = orderInsertList[count].Id,
                                                              Product2id = pc.Product2Id,
                                                              Quantity = pc.QuantityConsumed,
                                                              PricebookEntryId = pc.PricebookEntryId,
                                                              CLO_Asset_Serial_Number__c = pc.ProductItem.SerialNumber,
                                                              CLO_Non_Billable__c = pc.CLO_Non_Billable__c,
                                                              UnitPrice = pc.UnitPrice));
                    }
                    count++;
                   
                }
                
                if(orderItemInsertList.size() > 0){
                    
                    insert orderItemInsertList;
                    
                } 
            }   
        }
    }
    
    public static void UpdateAsset(List<Workorder> workorders, Map<Id, Workorder> oldmap)
    {
    /*for(Workorder wo : workorders){
        if (wo.AssetId == null){
         System.debug('entered update asset');
        System.debug(wo.CLO_Order__c);
           OrderItem OrderItemTemp = [select Product2Id  from OrderItem where OrderId =:wo.CLO_Order__c AND CLO_Primary_Line_Number__c ='1' Limit 1];
         System.debug(OrderItemTemp);
        Asset Assetitem = [select Id  from Asset where Product2Id =:OrderItemTemp.Product2Id AND AccountId =:wo.AccountId Limit 1];
        System.debug(Assetitem);
        WorkOrder Woitem = [select AssetId  from WorkOrder where id=:wo.id Limit 1];
        Woitem.AssetId = Assetitem.Id;
        update Woitem;
         }
        
        }  */      
    }
    
 /**
  * @Author      : Markandeyulu K
  * @CreatedDate : 17th Oct 2020
  * @Description : It will handle update Workorder Template logic.
  * @Parameters  : List<Workorder>
  * @Return      : Void 
  */
    public static void updateWorkorderTemplate(List<Workorder> workorders)
    {
        Set<String> woTypeSet = new Set<String>();
        for(Workorder wo : workorders)
        {
            if(wo.WorkTypeId != null)
                woTypeSet.add(wo.WorkTypeId);    
        }
        
        if(woTypeSet.size() > 0)
        {
            List<String> lstWoTyp = new List<String>();
            lstWoTyp.addAll(woTypeSet);
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(lstWoTyp);
            Map<Id, WorkType> woTypeMap = new Map<Id, WorkType>((List<WorkType>)CLO_DBManager.getSObjectList(' id, Name, ServiceReportTemplateId ', ' WorkType ', whereClause));
            
            for(Workorder wo : workorders)
            {
                if(woTypeMap.containskey(wo.WorkTypeId))
                    wo.ServiceReportTemplateId = woTypeMap.get(wo.WorkTypeId).ServiceReportTemplateId;    
            }
        }
    }
    
    public static void updatePriceBook(List<Workorder> newList) {
        List<String> customerIds = new List<String>();
        Map<Id, Account> customerMap = new Map<Id, Account>();
        
        Map<String, String> pricebookMap = new Map<String, String>();
        
        for(Workorder o : newList)
        {
            if(!customerIds.contains(o.AccountId) && o.AccountId != Null){
                customerIds.add(o.AccountId);
            }
        }
        If(customerIds.size() > 0){
            String whereClause = ' Id IN ' + CLO_StringUtil.getInClauseString(customerIds);        
            customerMap = new Map<Id, Account>((List<Account>)CLO_DBManager.getSObjectList(' Id, CLO_Sales_Org__c, BillingCountry, BillingCountryCode ',' Account ', whereClause)); 
        }
               
        for(PriceBook2 pb : [select id, Name from PriceBook2])
        {
            pricebookMap.put(pb.Name, pb.Id);
        }
        
        for(Workorder o : newList)
        {
            Account acc = customerMap.get(o.AccountId);
            
            String priceBookId = null; 
            If(pricebookMap != Null && pricebookMap.containskey(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode)){
                priceBookId= pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode);
            }
            
            if(priceBookId == null){
                priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountry);
            }
    
               
            if(o.PriceBook2Id == null)    
                o.PriceBook2Id = priceBookId;
        }
    }
}
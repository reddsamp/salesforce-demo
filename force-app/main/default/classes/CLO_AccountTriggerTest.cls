/**
  * @ClassName      : CLO_AccountTriggerTest 
  * @Description    : This class is used to test account trigger.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */
@isTest
private class CLO_AccountTriggerTest{

    @testSetup static void setup() {
        String triggerName = 'CLO_AccountTrigger';
        List<CLO_TriggerSetting__mdt> triggerSettings= (List<CLO_TriggerSetting__mdt>) CLO_DBManager.getSObjectList('Id, DeveloperName, isActive__c', 'CLO_TriggerSetting__mdt', 'DeveloperName = ' +CLO_StringUtil.inquoteString('CLO_AccountTrigger'));
        
        // Create test accounts
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i));
        }
        CLO_DBManager.insertSObjects(testAccts); 
        
    }
    
    @isTest static void testAccountCRUD() {
        // Get all account's by using a SOQL query
        List<Account> testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(2, testAccts.Size(),'success');

        // Get the first test account by using a SOQL query
        Account acct = (Account) CLO_DBManager.getSObject('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct0') +' LIMIT 1');
        //[SELECT Id FROM Account WHERE Name='TestAcct0' LIMIT 1];
        // Modify first account
        acct.Phone = '555-1212';
        // This update is local to this test method only.
        update acct;
        // Get all account's by using a SOQL query
        testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(2, testAccts.Size(),'success');

        // Delete second account
        Account acct2 = (Account) CLO_DBManager.getSObject('Id', 'Account', 'Name= '+ CLO_StringUtil.inquoteString('TestAcct1') +' LIMIT 1');
        // This deletion is local to this test method only.
        delete acct2;
        
        // Get all account's by using a SOQL query
        testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(1, testAccts.Size(),'success');
        
        undelete acct2;
        
        // Get all account's by using a SOQL query
        testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(2, testAccts.Size(),'success');
    }
    
    @isTest static void testTriggerDisabled() {
        CLO_AccountTriggerHandler.triggerDisabled = true;
 
        // Get all account's by using a SOQL query
        List<Account> testAccts = (List<Account>) CLO_DBManager.getSObjects('SELECT Id FROM Account');
        System.assertEquals(2, testAccts.Size(),'success');
    }
}
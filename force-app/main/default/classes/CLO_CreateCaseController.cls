public class CLO_CreateCaseController
{
    
    @AuraEnabled
    public static boolean fetchCreditHold(String assetId)
    {
        List<Asset> assetList = [select id, AccountId, Account.CLO_Credit_Hold__c from Asset where id =: assetId];
        
        if(assetList.size() > 0)
        {
            return assetList[0].Account.CLO_Credit_Hold__c;
        }
        
        return false;
    }
}
<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CLO_Send_Approval_Email_Notification_to_Order_Owner</fullName>
        <description>Send Approval Email Notification to Order Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CLO_Send_Email_Notification_On_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>CLO_Send_Rejection_Notification_to_Order_Owner</fullName>
        <description>Send Rejection Notification to Order Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CLO_Send_Email_Notification_On_Approval_Status</template>
    </alerts>
    <fieldUpdates>
        <fullName>CLO_Approve_Order_Status</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Approve Order Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Reject_Order_Status</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Reject Order Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	 <fieldUpdates>
        <fullName>CLO_update_onbehalf</fullName>
        <field>CLO_On_behalf_of__c</field>
        <formula>SBQQ__Quote__r.SBQQ__SalesRep__r.Email</formula>
        <name>Update Onbehalf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_Order_Lock_Field</fullName>
        <field>CLO_Record_Locked__c</field>
        <literalValue>1</literalValue>
        <name>Update Order Lock Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_Order_Status_Field</fullName>
        <field>Status</field>
        <literalValue>Ordered</literalValue>
        <name>Update Order Status Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_order_start_date</fullName>
        <field>EffectiveDate</field>
        <formula>SBQQ__Quote__r.SBQQ__StartDate__c</formula>
        <name>Update order start date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_Asset_number</fullName>
        <field>CLO_Asset_Number__c</field>
        <formula>SBQQ__Quote__r.CLO_Asset__r.SerialNumber</formula>
        <name>Update Asset Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_order_end_date</fullName>
        <field>EndDate</field>
        <formula>SBQQ__Quote__r.SBQQ__EndDate__c</formula>
        <name>update order end date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_order_reason</fullName>
        <field>CLO_Order_Reason__c</field>
        <literalValue>ZWQ</literalValue>
        <name>update order reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>CLO_Update_Ord_Record</fullName>
        <field>Description</field>
        <formula>Description</formula>
        <name>Update Ord Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_order_type</fullName>
        <field>Type</field>
        <literalValue>ZSAP</literalValue>
        <name>update order type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_po_date</fullName>
        <field>PoDate</field>
        <formula>IF(ISNULL( PoDate ), Today(), PoDate)</formula>
        <name>Update PO Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_po_number</fullName>
        <field>PoNumber</field>
        <formula>IF(ISBLANK( PoNumber ), SBQQ__Quote__r.Name, PoNumber)</formula>
        <name>Update PO Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>CLO_update_transaction_id_payment</fullName>
        <field>CLO_Payment_Transction_ID__c</field>
        <formula>SBQQ__Quote__r.CLO_Payment_Transction_ID__c</formula>
        <name>Update Transaction Id Payment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_request_delivery</fullName>
        <field>CLO_Requested_Delivery_Date__c</field>
        <formula>IF(ISNULL( CLO_Requested_Delivery_Date__c ), Today(), CLO_Requested_Delivery_Date__c)</formula>
        <name>Update Request Delivery Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_service_ordertype</fullName>
        <field>Type</field>
        <literalValue>OR</literalValue>
        <name>update service ordertype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Order Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Order Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Paymetric_Error</fullName>
        <field>CLO_Paymetric_Error__c</field>
        <name>Update Paymetric Error</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_asset_matrial_number</fullName>
        <field>CLO_Asset_Material_Number__c</field>
        <formula>SBQQ__Quote__r.CLO_Asset__r.Product2.ProductCode</formula>
        <name>Update Asset Material Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Billing_City</fullName>
        <field>BillingCity</field>
        <formula>Account.BillingCity</formula>
        <name>Update Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Country</fullName>
        <field>BillingCountry</field>
        <formula>Account.BillingCountry</formula>
        <name>Update Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Postal_Code</fullName>
        <field>BillingPostalCode</field>
        <formula>Account.BillingPostalCode</formula>
        <name>Update Billing Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_State</fullName>
        <field>BillingState</field>
        <formula>Account.BillingState</formula>
        <name>Update Billing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Street</fullName>
        <field>BillingStreet</field>
        <formula>Account.BillingStreet</formula>
        <name>Update Billing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>IF( ISBLANK( CLO_Ship_To__c ) , Account.ShippingCity, CLO_Ship_To__r.ShippingCity)</formula>
        <name>Update Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Country</fullName>
        <field>ShippingCountry</field>
        <formula>IF( ISBLANK( CLO_Ship_To__c ) , Account.ShippingCountry, CLO_Ship_To__r.ShippingCountry)</formula>
        <name>Update Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Postal_Code</fullName>
        <field>ShippingPostalCode</field>
        <formula>IF( ISBLANK( CLO_Ship_To__c ) , Account.ShippingPostalCode, CLO_Ship_To__r.ShippingPostalCode)</formula>
        <name>Update Shipping Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_State</fullName>
        <field>ShippingState</field>
        <formula>IF( ISBLANK( CLO_Ship_To__c ) , Account.ShippingState, CLO_Ship_To__r.ShippingState)</formula>
        <name>Update Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Street</fullName>
        <field>ShippingStreet</field>
        <formula>IF( ISBLANK( CLO_Ship_To__c ) , Account.ShippingStreet, CLO_Ship_To__r.ShippingStreet)</formula>
        <name>Update Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order Billing Address Update</fullName>
        <actions>
            <name>Update_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(AccountId)), ISBLANK(BillingStreet), ISBLANK(BillingCity),  ISBLANK(BillingState),  ISBLANK(BillingPostalCode))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order Shipping Address Update</fullName>
        <actions>
            <name>Update_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(CLO_Ship_To__c)), ISBLANK(ShippingStreet), ISBLANK(ShippingCity),  ISBLANK(ShippingState),  ISBLANK(ShippingPostalCode))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Workflow Rule To Update Order Lock Field</fullName>
        <actions>
            <name>CLO_Update_Order_Lock_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_Update_Order_Status_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!ISBLANK(SAP_Order_Id__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Remove Paymetric Error</fullName>
        <actions>
            <name>Update_Paymetric_Error</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(CLO_Payment_Card__r.CLO_Tokenize_Card_Number__c  )), TEXT(CLO_Payment_Terms__c) == &apos;CRCD&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update capital contract values</fullName>
        <actions>
            <name>CLO_update_Asset_number</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>CLO_update_onbehalf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_po_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_po_number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_request_delivery</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_asset_matrial_number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Type</field>
            <operation>equals</operation>
            <value>ZSAP,OR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.QuoteId</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update Capital order type</fullName>
        <actions>
            <name>CLO_update_order_type</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>CLO_update_transaction_id_payment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__Quote__r.RecordType.DeveloperName == &apos;CLO_Capital_Sales&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Total Ord Amount</fullName>
        <actions>
            <name>CLO_Update_Ord_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( TotalAmount )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update Service Order type</fullName>
        <actions>
            <name>CLO_update_service_ordertype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__Quote__r.RecordType.DeveloperName == &apos;CLO_Service_Contract&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

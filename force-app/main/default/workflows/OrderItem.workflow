<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CLO_update_discount</fullName>
        <field>CLO_Discount__c</field>
        <formula>SBQQ__QuoteLine__r.SBQQ__TotalDiscountAmount__c</formula>
        <name>update discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>CLO_Update_fee_amount</fullName>
        <field>CLO_Fee_Amount__c</field>
        <formula>SBQQ__QuoteLine__r.CLO_Fee_Amount__c</formula>
        <name>Update fee amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	 <fieldUpdates>
        <fullName>CLO_update_fee_details</fullName>
        <field>CLO_Referral_and_Finder_Fee_Notes__c</field>
        <formula>SBQQ__QuoteLine__r.CLO_Fee_Payable_To__c</formula>
        <name>update fee details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_gross_value</fullName>
        <field>CLO_Gross_Value__c</field>
        <formula>SBQQ__QuoteLine__r.SBQQ__ListTotal__c</formula>
        <name>update gross value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>CLO_update_price_override</fullName>
        <field>CLO_Price_Override__c</field>
        <literalValue>1</literalValue>
        <name>update price override</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_net_value</fullName>
        <field>CLO_Net_Value__c</field>
        <formula>SBQQ__QuoteLine__r.SBQQ__NetTotal__c</formula>
        <name>update net value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>update_Serial_number</fullName>
        <field>CLO_Asset_Serial_Number__c</field>
        <formula>SBQQ__QuoteLine__r.CLO_Asset__r.SerialNumber</formula>
        <name>update Serial number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>update line items capital</fullName>
        <actions>
            <name>CLO_update_discount</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>CLO_Update_fee_amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_fee_details</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_gross_value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_price_override</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_Serial_number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_net_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK( SBQQ__QuoteLine__r.Id )), NOT(Order.CLO_Created_From_SAP__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

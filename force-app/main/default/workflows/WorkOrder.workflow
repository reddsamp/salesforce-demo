<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approve_Work_Order_Email_Alert</fullName>
        <description>Approve Work Order Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>kr00627727@cynosure.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/B_SH_Work_Order_Ready_for_Approval</template>
    </alerts>
    <alerts>
        <fullName>Work_Order_Service_Admin_Review_for_Approval_Process</fullName>
        <description>Work Order Service Admin Review for Approval Process</description>
        <protected>false</protected>
        <recipients>
            <recipient>kr00627727@cynosure.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/B_SH_Work_Order_Ready_for_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>CLO_Update_WO_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update WO Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_WO_Clinical</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Clinical</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CLO WO Clinical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_WO_In_House_Repair</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_In_House_Repair</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CLO WO In-House Repair</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_WO_Install</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Install_Upgrade</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CLO WO Install</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_WO_Service</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Service</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CLO WO Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_For_Service_Admin_Review</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Reject</literalValue>
        <name>Reject For Service Admin Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_For_Work_Order</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Reject</literalValue>
        <name>Reject For Work Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approval</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_WO_status_Approved</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update WO status Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CLO WO RecType Update Clinical</fullName>
        <actions>
            <name>CLO_WO_Clinical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(WorkType.CLO_Type__c, &apos;Clinical&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CLO WO RecType Update In-House Repair</fullName>
        <actions>
            <name>CLO_WO_In_House_Repair</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(WorkType.CLO_Type__c, &apos;In-House Repair&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CLO WO RecType Update Install Upgrade</fullName>
        <actions>
            <name>CLO_WO_Install</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(WorkType.CLO_Type__c, &apos;Install&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CLO WO RecType Update Service</fullName>
        <actions>
            <name>CLO_WO_Service</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(WorkType.CLO_Type__c, &apos;Service&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

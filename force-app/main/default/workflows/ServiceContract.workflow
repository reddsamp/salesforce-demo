<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CLO_po_number_update</fullName>
        <field>CLO_PONumber__c</field>
        <formula>SBQQSC__Quote__r.Name</formula>
        <name>po number update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	 <fieldUpdates>
        <fullName>CLO_update_payment_transaction_id</fullName>
        <field>CLO_Payment_Transction_ID__c</field>
        <formula>SBQQSC__Quote__r.CLO_Payment_Transction_ID__c</formula>
        <name>update payment transaction id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_Record</fullName>
        <field>Description</field>
        <formula>Description</formula>
        <name>Update Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Service contract fields</fullName>
        <actions>
            <name>CLO_po_number_update</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>CLO_update_payment_transaction_id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQSC__Opportunity__r.RecordType.DeveloperName ==&apos;CLO_Service_Contract_Opportunity&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Service Contract</fullName>
        <actions>
            <name>CLO_Update_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( CLO_NetAmount__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

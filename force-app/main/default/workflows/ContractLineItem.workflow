<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CLO_Update_Discount_on_contract</fullName>
        <field>CLO_Total_Discount_Amount__c</field>
        <formula>SBQQSC__QuoteLine__r.SBQQ__TotalDiscountAmount__c</formula>
        <name>Update Discount on contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_list_total</fullName>
        <field>CLO_List_Total__c</field>
        <formula>SBQQSC__QuoteLine__r.SBQQ__ListTotal__c</formula>
        <name>update list total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_net_value</fullName>
        <field>SBQQSC__NetPrice__c</field>
        <formula>SBQQSC__QuoteLine__r.SBQQ__NetTotal__c</formula>
        <name>update net value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_total_discount</fullName>
        <field>Discount</field>
        <formula>SBQQSC__QuoteLine__r.SBQQ__TotalDiscountAmount__c</formula>
        <name>update total discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>update line items service</fullName>
        <actions>
            <name>CLO_Update_Discount_on_contract</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CLO_update_list_total</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ContractLineItem.LineItemNumber</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

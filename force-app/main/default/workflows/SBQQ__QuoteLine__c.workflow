<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CLO_update_discount_on_line</fullName>
        <field>SBQQ__AdditionalDiscountAmount__c</field>
        <formula>(SBQQ__ListTotal__c -  CLO_Required_Total_Amount__c )/ SBQQ__Quantity__c</formula>
        <name>update discount on line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>CLO_update_line_end_date</fullName>
        <field>CLO_End_Date__c</field>
        <formula>SBQQ__Quote__r.SBQQ__EndDate__c</formula>
        <name>update line end date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_update_line_start_date</fullName>
        <field>CLO_Start_Date__c</field>
        <formula>SBQQ__Quote__r.SBQQ__StartDate__c</formula>
        <name>update line start date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Quote line end dates</fullName>
        <actions>
            <name>CLO_update_line_end_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISBLANK( CLO_End_Date__c ) ,  SBQQ__Quote__r.RecordType.DeveloperName ==&apos;CLO_Service_Contract&apos;, TEXT(SBQQ__Product__r.CLO_PC_Bypass_and_SF_Id__c) == &apos;SC&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote line start dates</fullName>
        <actions>
            <name>CLO_update_line_start_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISBLANK(  CLO_Start_Date__c  ) ,  SBQQ__Quote__r.RecordType.DeveloperName ==&apos;CLO_Service_Contract&apos;, TEXT(SBQQ__Product__r.CLO_PC_Bypass_and_SF_Id__c) == &apos;SC&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CLO_Update_Discount</fullName>
        <actions>
            <name>CLO_update_discount_on_line</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__QuoteLine__c.CLO_Required_Total_Amount__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Contract</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

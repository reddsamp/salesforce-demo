<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>email_for_Product_request</fullName>
        <description>email for Product request</description>
        <protected>false</protected>
        <recipients>
            <recipient>CLO_Field_Service_Engineer_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Product_Request_Received_Surg</template>
    </alerts>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CLO_Campaign_Prepend_Campaign_CodetoName</fullName>
        <description>Prepends Campaign Code to the Campaign Title</description>
        <field>Name</field>
        <formula>CLO_Campaign_Code__c +&apos; &apos;+ Name</formula>
        <name>Campaign: Prepend Campaign Code to Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Campaign%3A Update Campaign Name</fullName>
        <actions>
            <name>CLO_Campaign_Prepend_Campaign_CodetoName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.CLO_Campaign_Code__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Updates Campaign Name by prepending Campaign Code Number</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CLO_1_Day_Before_from_Due_Date_Reminder_Email_Alert_for_TSA</fullName>
        <ccEmails>mark.eee2011@gmail.com</ccEmails>
        <description>CLO 1 Day Before from Due Date Reminder Email Alert for TSA</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CLO_Task_Due_Reminder_for_TSA</template>
    </alerts>
</Workflow>

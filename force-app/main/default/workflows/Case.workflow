<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CLO_Case_Notifications_Cls</fullName>
        <description>Case Notifications Cls</description>
        <protected>false</protected>
        <recipients>
            <recipient>ABBOT</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CLO_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>CLO_Case_Notifications_RO</fullName>
        <description>Case Notifications RO</description>
        <protected>false</protected>
        <recipients>
            <recipient>ABBOT</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CLO_Case_ReOpened</template>
    </alerts>
    <fieldUpdates>
        <fullName>CLO_RecType_Update_to_Close_Case</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Close_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecType Update to Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_Inquiry</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Inquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Inquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_Service</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Service</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CLO_Update_compliant</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CLO_Complaints</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update compliant</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CLO RecType Update Close Case</fullName>
        <actions>
            <name>CLO_RecType_Update_to_Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISNEW()), ISCHANGED( Status ), ISPICKVAL(Status, &apos;Closed&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CLO RecType Update Complaint</fullName>
        <actions>
            <name>CLO_Update_compliant</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CLO RecType Update Inquiry</fullName>
        <actions>
            <name>CLO_Update_Inquiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CLO RecType Update Service</fullName>
        <actions>
            <name>CLO_Update_Service</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

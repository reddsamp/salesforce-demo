<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>email_for_absence_request</fullName>
        <description>email for absence request</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CLO_Send_Email_Notification_On_Approval_Status</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approve_task</fullName>
        <field>FSL__Approved__c</field>
        <literalValue>1</literalValue>
        <name>Approve task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>

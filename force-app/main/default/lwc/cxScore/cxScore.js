import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import SCORE_FIELD from '@salesforce/schema/Account.Net_Promoter_Score__c';

const CX_CARD_TITTLE = 'CX Score'
const fields = [SCORE_FIELD];
export default class CxScore extends LightningElement {
    cxCardTitle = CX_CARD_TITTLE;
    @api recordId;

    @wire(getRecord, { recordId: '$recordId', fields })
    accountScore;

    get cxScore() {
        return (getFieldValue(this.accountScore.data, SCORE_FIELD) != null && getFieldValue(this.accountScore.data, SCORE_FIELD) != '') ? getFieldValue(this.accountScore.data, SCORE_FIELD) : 0;
    }

    get cxColourCode() {
        if (this.cxScore != undefined) {
            if (this.cxScore >= 0 && this.cxScore <= 29) {
                return 'red';
            } else if (this.cxScore >= 30 && this.cxScore <= 69) {
                return 'orange';
            } else if (this.cxScore >= 70 && this.cxScore <= 100) {
                return 'green';
            }
        }
    }

}
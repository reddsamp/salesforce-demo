import { LightningElement, track, api } from 'lwc';
import fetchFromOrder from '@salesforce/apex/CLO_MaterialAviabilityController.fetchFromOrder';
import saveProducts from '@salesforce/apex/CLO_MaterialAviabilityController.saveProducts';

export default class CLO_AddProductConsignmentTransfer extends LightningElement {
    @track showLoader;

    @track dataList;
    @track selectedList = [];
    @track selectedDataList;
    @track dataListLength;
    @track showModal;
    @api recordId;
    @track showSelectTable = true;
    @track showSelectedTable;
    @track showNextBtn;

    handleClick(){
        this.showModal = true;
        this.showSelectTable = true;
        this.showSelectedTable = null;
        this.dataList = [];
        this.dataListLength = null;
        this.selectedList = [];
        this.showNextBtn = null;
        this.showLoader = true;

        fetchFromOrder({orderId : this.recordId})
                .then(result => {
                    console.log(result);
                    this.showLoader = null;
                    this.dataList = result;

                    this.dataListLength = result.length;

                    if(this.dataListLength == 0)
                        this.dataListLength = null;
                });
    }

    getSelectedRow(event){
        let selectedRows = this.template.querySelectorAll('lightning-input');
        this.selectedList = [];
        this.showNextBtn = null;

        for(let i = 0; i < selectedRows.length; i++) {
            if(selectedRows[i].checked) {
                this.dataList[i].isSelected = true;
                this.selectedList.push(this.dataList[i]);
                this.showNextBtn = true;
            }
        }
    }

    validateQuantity(event){

    }

    handleEdit(){

    }

    closeModal(){
        this.showModal = null;
    }

    goNext(){
        this.showSelectTable = null;
        this.showSelectedTable = true;
        this.showNextBtn = null;
    }

    goBack(){
        this.showSelectTable = true;
        this.showSelectedTable = null;
        this.showNextBtn = true;
    }

    callSave(){
        this.showLoader = true;
        alert(this.selectedList.length);
        for(let i = 0; i < this.selectedList.length; i++) {
            alert(selectedRows[i].quantity);
        }

        saveProducts({orderId : this.recordId,
            dataList : this.selectedList})
                .then(result => {
                    this.showLoader = null;
                    console.log(result);
                    this.showLoader = null;
                });
    }
}
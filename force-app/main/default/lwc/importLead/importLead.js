import { LightningElement, api, track } from 'lwc';
import readCSVFile from '@salesforce/apex/CLO_ImportLeadController.readCSVFile';
import readCSVHeaders from '@salesforce/apex/CLO_ImportLeadController.readCSVHeaders';
import readLeadFields from '@salesforce/apex/CLO_ImportLeadController.readLeadFields';
import revalidateData from '@salesforce/apex/CLO_ImportLeadController.revalidateData';
import uploadLeadData from '@salesforce/apex/CLO_ImportLeadController.uploadLeadData';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

const columns = [
   { label: 'FirstName', fieldName: 'FirstName', type: 'lookup', editable: true },
   { label: 'LastName', fieldName: 'LastName', type: 'lookup', editable: true },
   { label: 'Name', fieldName: 'Name', type: 'lookup', editable: true },
   { label: 'Email', fieldName: 'Email', editable: true },
    { label: 'Phone', fieldName: 'Phone', editable: true }, 
   { label: 'Title', fieldName: 'Title', editable: true }, 
   { label: 'Company', fieldName: 'Company', editable: true},
   { label: 'Specialties', fieldName: 'Specialties__c ', editable: true},
   { label: 'Rating', fieldName: 'Rating', editable: true},
   { label: 'Lead Source', fieldName: 'LeadSource', type:'Picklist' , editable: true},
   { label: 'Address', fieldName: 'Address', type: 'lookup', editable: true},
   { label: 'Street', fieldName: 'Street', editable: true},
   { label: 'Website', fieldName: 'Website', editable: true},
   { label: 'Salutation', fieldName: 'Salutation', editable: true},
   { label: 'Suffix', fieldName: 'Suffix', editable: true},
   { label: 'Status', fieldName: 'Status', editable: true}

];
export default class CustomTableForLead extends LightningElement {
    @api recordId;
    @api leadSheetId;
    @track error;
    @track columns = columns;
    @track data;
    @track columnMappingData;
    @track leadFields;
    @track draftValues;
    @track csvcolumns;
    @track csvcolumnsDatatable = [];
    @track csvdata = [];
    @track uploadedFiles =[];
    @track recordCount = 0;
    @track invalidCount = 0;
    @track duplicateCount = 0;
    @track newCount = 0;
    @track showLoader;
    @track showMapFields;
    @track currentStep = "1";
    @track step1 = true;
    @track step2;
    @track step3;
    @track step4;
    @track csvFileData;

    @track column = [{'label':'Id', 'fieldName':'Id'},
                        {'label':'Name', 'fieldName':'Name', 'editable': true},
                        {'label':'Phone', 'fieldName':'Phone'},
                        {'label':'Action', 'fieldName': 'Edit'}];

    @track accounts;

    get acceptedFormats() {
        return ['.csv'];
    }
    
    handleUpload() {
        var d;
        this.showLoader = true;

        [...this.template
            .querySelector('lightning-input')
            .files].forEach(async file => {
                var result = await this.load(file);
                d = result;
                this.csvFileData = d;
                readCSVHeaders({objVersion : result})
            .then(result => {
                    this.csvcolumnsDatatable = result;
                    this.step1 = null;
                    this.step2 = true;
                    this.getHeaders(d);
                    this.showMapFields = true;
                    this.currentStep = "2";
                })
            });

        readLeadFields()
            .then(result => {
                    this.leadFields = result;
                    this.showLoader = null;
                });
    }
    
    async load(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = function() {
                resolve(reader.result);
            };
            reader.onerror = function() {
                reject(reader.error);
            };
            reader.readAsText(file);
        });
    }
    
    saveValidateData(){
        this.showLoader = true;
        readCSVFile({objVersion : this.csvFileData})
        .then(result => {
            this.data = result;
            this.recordCount = result.length;
            this.step2 = null;
            this.step3 = true;
            this.currentStep = "3";
            this.calculateCount(this.data);
            this.showLoader = null;
        });
    }

    calculateCount(res){
        this.recordCount = res.length;
        this.invalidCount = 0;
        this.duplicateCount = 0;
        this.newCount = 0;

        for(var i=0; i < res.length; i++){
            if(res[i].Exception__c == 'Use one of these records?' || res[i].Exception__c == 'Duplicate Record'){
                res[i].Exception__c = 'Duplicate Record';
                this.duplicateCount++;
            }    
            else if(res[i].Exception__c != undefined && res[i].Exception__c != '')
            {
                this.invalidCount++;
            }
            else{
                this.newCount++;
            }
            
            this.showLoader = null;
        }
    }

    getHeaders(res){
        var temp=res.split('\n');
        var header=temp[0].split(',');
        this.csvcolumns = header;
        
        for(var i=0; i < this.csvcolumns.length; i++){
            this.csvcolumns[i] = this.csvcolumns[i].replace('\r', '').toUpperCase();
        }
    }

    resetData(){
        this.csvcolumns = null;
        this.csvdata = [];
        this.data = null;
        this.recordCount = 0;
        this.invalidCount = 0;
        this.duplicateCount = 0;
        this.newCount = 0;
        this.currentStep = "1";
        this.step1 = true;
        this.step4 = null;
    }

    editRow(event){
        console.log(event.target.id.split('-')[0]);
    }

    revalData(){
        console.log(this.data);
        this.showLoader = true;
        revalidateData({lstAccsToInsert : this.data})
        .then(result => {
            this.data = result;
            this.showLoader = null;
            this.calculateCount(this.data);
        });
    }

    uploadData(){
        this.showLoader = true;
        uploadLeadData({lstAccsToInsert : this.data, leadSheetId : this.leadSheetId})
        .then(result => {
            
            if(this.leadSheetId != undefined && this.leadSheetId.trim().length > 0){
                const selectedEvent = new CustomEvent("leaddatachange", {
                    detail: 'done'
                });
              
                // Dispatches the event.
                this.dispatchEvent(selectedEvent);
            }
            else{
                this.data = result;
                this.showLoader = null;
                this.step4 = true;
                this.step3 = null;
                this.currentStep = "4";
            }
        });
    }

    handleSave(event) {
        var arr = [];
        
        for(var i=0; i < event.detail.draftValues.length; i++){
            var indx = parseInt(event.detail.draftValues[i].id.replace('row-',''));
            var d = event.detail.draftValues[i];
            var dta = this.data;

            Object.keys(d).forEach(function(key){
                var value = d[key];
                console.log(key + ':' + value);
                if(d[key] != undefined)
                    dta[indx][key] = value;
                
                dta[indx].id = null;

                delete dta[indx].id;
            });

            this.data = dta;
            
            this.step3 = null;
            alert('df');
            //this.step3 = true;
            /*for(var j=0; j < this.csvcolumns.length; j++){
                var cname = this.csvcolumns[j].charAt(0).toUpperCase() + this.csvcolumns[j].slice(1).toLowerCase();
                if(d[cname] != undefined)
                    this.data[indx][cname] = d[cname];
            }*/
        }

        const e = new ShowToastEvent({
            title: '',
            message: 'Changes saved locally. Please click on Revalidate button to validate the data.',
            mode:'sticky'
        });
        this.dispatchEvent(e);
    }

    showMapping(){
        this.showMapFields = true;
    }

    hideMapping(){
        this.showMapFields = null;
    }

    downloadSFile(){
        this.showLoader = true;

        var sData = '';//this.csvcolumns.join(",") + "\r\n";
        
        var firstCol = this.data[0];
        if(firstCol != undefined){
            var count = 0;
            for(var i=0; i < this.data.length; i++){
                var d = this.data[i];
                if(d.Exception__c == undefined || d.Exception__c == '')
                {   
                    if(count == 0){
                        firstCol = this.data[i];
                        var hArr = [];
                        var hc = 0;
                        Object.keys(firstCol).forEach(function(key){
                            console.log(key);
                            hArr[hc] = key;
                            hc++;
                        });
                        sData += hArr.join(",");
                        
                        sData += "\r\n";

                        count++;
                    }

                    var vlArr = [];
                    var c = 0;
                    Object.keys(d).forEach(function(key){
                        var value = d[key];
                        if(key.indexOf('Title') != -1)
                            vlArr[c] = '';
                        else if(key.indexOf('Exception__c') != -1)
                            vlArr[c] = '';
                        else    
                            vlArr[c] = value.split(",").join("~~~~~").replace("\r\n", "");
                        c++;
                    });
                    sData += vlArr.join(",");
                    
                    sData += "\r\n";
                }
            }
        }

        let downloadElement = document.createElement('a');
        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(sData);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'Success.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click(); 

        this.showLoader = null;
    }

    downloadErrFile(){
        this.showLoader = true;
        var sData = '';//this.csvcolumns.join(",") + "\r\n";
        
        var firstCol = this.data[0];
        if(firstCol != undefined){
            var count = 0;
            for(var i=0; i < this.data.length; i++){
                var d = this.data[i];
                if(this.data[i].Exception__c != undefined && this.data[i].Exception__c != '')
                {   
                    if(count == 0){
                        firstCol = this.data[i];
                        var hArr = [];
                        var hc = 0;
                        Object.keys(firstCol).forEach(function(key){
                            console.log(key);
                            hArr[hc] = key;
                            hc++;
                        });
                        sData += hArr.join(",");
                        
                        sData += "\r\n";

                        count++;
                    }

                    var vlArr = [];
                    var c = 0;
                    Object.keys(d).forEach(function(key){
                        var value = d[key];
                        if(key.indexOf('Title') != -1)
                            vlArr[c] = '';
                        else    
                            vlArr[c] = value.split(",").join("~~~~~").replace("\r\n", "");
                        c++;
                    });
                    sData += vlArr.join(",");
                    
                    sData += "\r\n";
                }
            }
        }

        let downloadElement = document.createElement('a');
        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(sData);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'Error.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click(); 

        this.showLoader = null;
    }
}
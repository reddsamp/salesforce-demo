import { LightningElement, api, track } from 'lwc';
import fetchOrderStatus from '@salesforce/apex/CLO_OrderStatusViewController.fetchOrderStatus';
import fetchOrderList from '@salesforce/apex/CLO_OrderStatusViewController.fetchOrderList';

export default class CLO_OrderStatusView extends LightningElement {

    @track statusList = [];
    @track orderList = [];
    @track orderCount = 0;

    connectedCallback() {
        fetchOrderStatus()
        .then(result => {
            this.statusList = result;
        });

        fetchOrderList()
        .then(result2 => {
            this.orderList = result2;
            this.orderCount = result2.length;
        });
    }
}
import { LightningElement, track, api } from 'lwc';
import fetchFromOrder from '@salesforce/apex/CLO_MaterialAviabilityController.fetchFromOrder';
import saveProducts from '@salesforce/apex/CLO_MaterialAviabilityController.saveProducts';

const columns = [
    {label: 'Material Number', fieldName: 'MATNR'},
    {label: 'Customer Number', fieldName: 'KUNNR'},
    {label: 'Customer Name', fieldName: 'customerName'},
    {label: 'Description', fieldName: 'description'},
    {label: 'Plant', fieldName: 'WERKS'},
    {label: 'Available Stock', fieldName: 'LABST'},
    {label: 'Batch Number', fieldName: 'CHARG'},
    {label: 'Serial Number', fieldName: 'SERNR'}
];

const data = [{
    id: 'a',
    opportunityName: 'Cloudhub',
    confidence: 0.2,
    amount: 25000,
    contact: 'jrogers@cloudhub.com',
    phone: '2352235235',
    trendIcon: 'utility:down'
},
{
    id: 'b',
    opportunityName: 'Quip',
    confidence: 0.78,
    amount: 740000,
    contact: 'quipy@quip.com',
    phone: '2352235235',
    trendIcon: 'utility:up'
}];

export default class CLO_AddProductConsignmentTransfer extends LightningElement {
    @track showLoader;

    @track dataList;
    @track dList;
    @track selectedList = [];
    @track selectedDataList;
    @track dataListLength;
    @track showModal;
    @api recordId;
    @track showSelectTable = true;
    @track showSelectedTable;
    @track showNextBtn;

    handleClick(){
        this.columns = columns;
        this.showModal = true;
        this.showSelectTable = true;
        this.showSelectedTable = null;
        this.dataList = [];
        this.dataListLength = null;
        this.selectedList = [];
        this.showNextBtn = null;
        this.showLoader = true;

        fetchFromOrder({orderId : this.recordId})
                .then(result => {
                    console.log(result);
                    this.showLoader = null;
                    this.dataList = result;
                    this.dList = result;
                    this.dataListLength = result.length;

                    if(this.dataListLength == 0)
                        this.dataListLength = null;
                });
    }

    getSelectedName(event) {
        /*const selectedRows = event.detail.selectedRows;
        // Display that fieldName of the selected rows
        for (let i = 0; i < selectedRows.length; i++){
            alert("You selected: " + selectedRows[i].opportunityName);
        }*/
    }

    getSelectedRow(event){
        let selectedRows = this.template.querySelectorAll('lightning-input');
        this.selectedList = [];
        this.showNextBtn = null;
        var count = 0;

        for(let i = 0; i < selectedRows.length; i++) {
            if(selectedRows[i].checked) {
                this.dataList[i].isSelected = true;
                this.selectedList[count] = this.dataList[i];
                count++;
                this.showNextBtn = true;
            }
        }
        console.log(this.dataList);
    }

    validateQuantity(event){

    }

    handleEdit(){

    }

    closeModal(){
        this.showModal = null;
    }

    goNext(){
        this.showSelectTable = null;
        this.showSelectedTable = true;
        this.showNextBtn = null;
    }

    goBack(){
        this.showSelectTable = true;
        this.showSelectedTable = null;
        this.showNextBtn = true;
    }

    callSave(){
        this.showLoader = true;
        console.log(this.dataList);
        saveProducts({orderId : this.recordId, dList: this.dataList})
                .then(result => {
                    this.showLoader = null;
                    this.showModal = null;
                });
    }
}
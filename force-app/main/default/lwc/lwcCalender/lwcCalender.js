/* eslint-disable no-restricted-globals */
/* eslint-disable no-unused-vars */
/* eslint-disable no-alert */
/**
 * @description       : 
 * Modifications Log 
**/
import { LightningElement, track, api } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import FullCalendarJS from '@salesforce/resourceUrl/FullCalender';
import { NavigationMixin } from 'lightning/navigation';
import fetchAllEvents from '@salesforce/apex/CLO_CalenderController.getAllEvents';
import getUserList from '@salesforce/apex/CLO_CalenderController.getUserList';
import addCalenderUser from '@salesforce/apex/CLO_CalenderController.addCalenderUser';

/**
 * FullCalendarJs
 * @description Full Calendar JS - Lightning Web Components
 */
export default class FullCalendarJs extends NavigationMixin(LightningElement) {

    fullCalendarJsInitialised = false;
    @track allEvents = [];
    @track allUsers = [];
    @track selectedEvent = undefined;
    @api lsId;
    @track showLogCall;
    @track selectedUserId;

    /**
    * @description Standard lifecyle method 'renderedCallback'
    *              Ensures that the page loads and renders the 
    *              container before doing anything else
    */
    renderedCallback() {
        // Performs this operation only on first render
        if (this.fullCalendarJsInitialised) {
            return;
        }
        this.fullCalendarJsInitialised = true;

        // Executes all loadScript and loadStyle promises
        // and only resolves them once all promises are done
        Promise.all([
            loadScript(this, FullCalendarJS + '/FullCalendarJS/jquery.min.js'),
            loadScript(this, FullCalendarJS + '/FullCalendarJS/moment.min.js'),
            loadScript(this, FullCalendarJS + '/FullCalendarJS/theme.js'),
            loadScript(this, FullCalendarJS + '/FullCalendarJS/fullcalendar.min.js'),
            loadStyle(this, FullCalendarJS + '/FullCalendarJS/fullcalendar.min.css'),
        ])
        .then(() => {
            // Initialise the calendar configuration
            this.getAllEvents();
        })
        .catch(error => {
            // eslint-disable-next-line no-console
            console.error({
                message: 'Error occured on FullCalendarJS',
                error
            });
        })
    }

    /**
    * @description Initialise the calendar configuration
    *              This is where we configure the available options for the calendar.
    *              This is also where we load the Events data.
    */
    initialiseFullCalendarJs() {
        const ele = this.template.querySelector('div.fullcalendarjs');
        // eslint-disable-next-line no-undef
        $(ele).fullCalendar('destroy');
        $(ele).fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay,listWeek'
            },
            themeSystem : 'standard',
            defaultDate: new Date(), 
            defaultView: 'listWeek',
            navLinks: true,
            editable: true,
            eventLimit: true,
            events: this.allEvents,
            dragScroll : true,
            height: 500,
            droppable: true,
            weekNumbers : false,
            eventDrop: function(event, delta, revertFunc) {
                if (!confirm("Are you sure about this change? ")) {
                    revertFunc();
                }
            },
            eventClick: function(event, jsEvent, view) {
                this.selectedEvent =  event;
            },
            dayClick :function(date, jsEvent, view) {
                jsEvent.preventDefault();
            },
            eventMouseover : function(event, jsEvent, view) {
                
            }
        });
    }

    loadUserEvents(event){
        this.selectedUserId = event.target.value;
        this.getAllEvents();
    }

    @api getAllEvents(){
        this.allEvents = [];
        fetchAllEvents({userid : this.selectedUserId})
            .then(result => {
                this.allEvents = result.map(item => {
                    return {
                        id : item.Id,
                        editable : true,
                        title : item.Subject,
                        start : item.StartDateTime,
                        end : item.EndDateTime,
                        description : item.Description,
                        allDay : false,
                        extendedProps : {
                        whoId : item.WhoId,
                        whatId : item.WhatId
                        }
                    };
                });
            // Initialise the calendar configuration
            this.initialiseFullCalendarJs();
        }).catch(error => {
            window.console.log(' Error Occured ', error)
        }).finally(()=>{
            //this.initialiseFullCalendarJs();
        })

        getUserList({lsId : this.lsId})
            .then(result => {
                this.allUsers = result;
                if(result.length > 0)
                    this.selectedUserId = result[0].Id;
        }).catch(error => {
            window.console.log(' Error Occured ', error)
        }).finally(()=>{
            //this.initialiseFullCalendarJs();
        })
    }

    closeModal(){
        this.selectedEvent = undefined;
    }

    addUser(){
        const selectedEvent = new CustomEvent('valueselectedcall', {
        });
        //dispatching the custom event
        this.dispatchEvent(selectedEvent);
    }

    closeModal(){
        this.showLogCall = null;
    }

    handleValueSelcted(event) {
        var lst = event.detail.split('::::::');
        this.selectedUserId = lst[0];
    }

    createTaskCall(){
        addCalenderUser({lsId : this.lsId, userId : this.selectedUserId})
        .then(result => {
            const e = new ShowToastEvent({
                title: '',
                message: 'User calendar added successfully.',
                variant: 'success'
            });
            this.dispatchEvent(e);
            this.getAllEvents();
        }).catch(error => {
            window.console.log(' Error Occured ', error)
        }).finally(()=>{
            //this.initialiseFullCalendarJs();
        })
    }
}
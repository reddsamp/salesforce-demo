import { LightningElement , api, track, } from 'lwc';
import saveLeadSheet from '@salesforce/apex/CLO_SmartSheetController.saveLeadSheet';
import readLeadSheets from '@salesforce/apex/CLO_SmartSheetController.readLeadSheets';
import readLeadSheetData from '@salesforce/apex/CLO_SmartSheetController.readLeadSheetData';
import readLeadFields from '@salesforce/apex/CLO_SmartSheetController.readLeadFields';
import readLeadData from '@salesforce/apex/CLO_SmartSheetController.readLeadData';
import shareLeadSheet from '@salesforce/apex/CLO_SmartSheetController.shareLeadSheet';
import getShareList from '@salesforce/apex/CLO_SmartSheetController.getShareList';
import readTaskStatus from '@salesforce/apex/CLO_SmartSheetController.readTaskStatus';
import readTaskStatusReason from '@salesforce/apex/CLO_SmartSheetController.readTaskStatusReason';
import createTaskApex from '@salesforce/apex/CLO_SmartSheetController.createTaskApex';
import getAllTasks from '@salesforce/apex/CLO_SmartSheetController.getAllTasks';
import qryLeadTeams from '@salesforce/apex/CLO_SmartSheetController.qryLeadTeams';
import sendEmail from '@salesforce/apex/CLO_SmartSheetController.sendEmail';
import addNewRowCall from '@salesforce/apex/CLO_SmartSheetController.addNewRowCall';
import distributeLeads from '@salesforce/apex/CLO_SmartSheetController.distributeLeads';
import deleteShareRecord from '@salesforce/apex/CLO_SmartSheetController.deleteShareRecord';
import deleteRowCall from '@salesforce/apex/CLO_SmartSheetController.deleteRowCall';
import addCalenderUser from '@salesforce/apex/CLO_SmartSheetController.addCalenderUser';

import {ShowToastEvent} from 'lightning/platformShowToastEvent';


export default class CLO_SmartSheetView extends LightningElement {
    @track showNewBtn = true;
    @track showImportLead;
    @track showAddColumn = true;
    @track showNew;
    @api sheetName;
    @track showLoader;
    @track showColumnModal;
    @track selectLeadSheetId;
    @track leadSheetData;
    @track columnsData;
    @track shareData;
    @track leadData;
    @track showShare;
    @track showTable;
    @track showEmail;
    @track showCalender;
    @track showLeadDistribution;
    @track showLeadDistributionBtn;
    @track showAvailabilityEmail;
    @track showLogCall;
    @track showTasks;
    @track midSectionCls = 'slds-col slds-size_12-of-12';
    @track calSectionCls = 'slds-col slds-size_5-of-12';
    @track statusPicklist;
    @track statusReasonPicklist;
    @track tasksData;

    @track leadId;
    @track status;
    @track reason;
    @track comment;

    @track startcalldate;
    @track startoptimaldate;
    @track lookforwarddate;
    @track daystodistribute;
    
    @track selectedUserId;
    @track leadTeamData;
    @track accLevValue = 'Read';

    @track showCalendarCall;

    get accessOptions() {
        return [
            { label: 'Edit', value: 'Edit' },
            { label: 'Read', value: 'Read' },
            { label: 'All', value: 'All' },
        ];
    }

    openLeadSheet(event){
        var id = event.target.id.substr(0, 15);
        window.open('/'+id);
    }

    viewTasks(event){
        this.leadId = event.target.id.substr(0, 15);
        this.showTasks = true;
        this.showLoader = true;
        getAllTasks({leadId:this.leadId}).then(result => {
            this.leadId = null;
            this.tasksData = result;
            this.showLoader = null;
        });
    }

    createTaskCall(){
        this.showLoader = true;
        this.showLogCall = null;
        createTaskApex({leadId:this.leadId, status:this.status, reason:this.reason, comment:this.comment}).then(result => {
            this.leadId = null;
            this.status = null;
            this.reason = null;
            this.comment = null;

            if(result.indexOf('Error') != -1){
                this.showLoader = null;
                const e = new ShowToastEvent({
                    title: '',
                    message: result,
                    variant: 'error'
                });
                this.dispatchEvent(e);
            }
            else{
                const e = new ShowToastEvent({
                    title: '',
                    message: 'Activity saved successfully.',
                    variant: 'success'
                });
                this.dispatchEvent(e);

                readLeadData({id : this.selectLeadSheetId})
                .then(result => {
                    this.leadData = result;
                    this.lCount = result.length;
                    result.length > 0 ? this.showTable = true : this.showTable = null;
                    this.showLoader = null;
                });
            }
        });
    }

    showLeadDistributionPopup(){
        this.showLeadDistribution = true;
        qryLeadTeams({lsId : this.selectLeadSheetId}).then(result => {
            this.leadTeamData = result;
        });
    }

    addTask(event){
        this.leadId = event.target.id.substr(0, 15);
        this.showLogCall = true;
        readTaskStatus().then(result => {
            this.statusPicklist = result;
        });
    }

    fillStatusReason(event){
        this.reason = event.target.value;
    }

    fillComment(event){
        this.comment = event.target.value;
    }

    getStatusReason(event){
        this.status = event.target.value;
        readTaskStatusReason({status: this.status}).then(result => {
            this.statusReasonPicklist = result;
        });
    }

    addNewRow(){
        this.showLoader = true;
        addNewRowCall({lsId : this.selectLeadSheetId, leadTeams : this.leadTeamData}).then(result => {
            this.showLoader = null;
            this.leadTeamData = result;
        });
    }

    toggleCalendar(){
        if(this.midSectionCls == 'slds-col slds-size_12-of-12'){
            this.midSectionCls = 'slds-col slds-size_7-of-12';
            this.showCalender = true;
        }
        else{
            this.midSectionCls = 'slds-col slds-size_12-of-12';
            this.showCalender = null;
        }
    }

    showNewPopup(){
        this.showNew = true;
    }

    connectedCallback() {
        readLeadSheets()
        .then(result => {
            this.leadSheetData = result; 
            if(this.leadSheetData.length > 0)
            {
                var lsId = this.leadSheetData[0].Id.substr(0, 15);
                this.showLoader = true;
                readLeadSheetData({id : lsId})
                .then(result => {
                    console.log(result);
                    this.selectLeadSheetId = result.Id;
                    this.sheetName = result.Name;
                });

                readLeadData({id : lsId})
                .then(result => {
                    this.leadData = result;
                    this.lCount = result.length;
                    result.length > 0 ? this.showTable = true : this.showTable = null;
                    result.length > 0 ? this.showLeadDistributionBtn = true : this.showLeadDistributionBtn = null;
                    this.showLoader = null;
                });
            }
        });

        readLeadFields().then(result => {
            this.columnsData = result;
        });
    }

    showImportLeadPopup(){
        this.showImportLead = true;
    }

    showAddColumnPopup(){
        this.showColumnModal = true;
    }

    closeModal(){
        this.showNew = null;
        this.showColumnModal = null;
        this.showShare = null;
        this.showEmail = null;
        this.showLogCall = null;
        this.showTasks = null;
        this.showLeadDistribution = null;
        this.showAvailabilityEmail = null;
        this.showCalendarCall = null;
    }

    handleValueSelcted(event) {
        var lst = event.detail.split('::::::');
        this.selectedUserId = lst[0];
        
        if(lst[1] != 'undefined')
        {
            var indx = parseInt(lst[1]);
            var count = 0;
            this.leadTeamData.forEach(element => {
                if (count == indx) {
                    element.User__c = lst[0];
                }
                count++;
            });
        }
    }

    handlestartcalldate(event){
        this.startcalldate = event.target.value;
    }

    handlestartoptimaldate(event){
        this.startoptimaldate = event.target.value;
    }

    handlelookforwarddate(event){
        this.lookforwarddate = event.target.value;
    }

    handledaystodistribute(event){
        this.daystodistribute = event.target.value;
    }

    addUser(){
        this.showCalendarCall = true;
    }

    createCalendarCall(){
        addCalenderUser({lsId : this.selectLeadSheetId, userId : this.selectedUserId})
        .then(result => {
            const e = new ShowToastEvent({
                title: '',
                message: 'User calendar added successfully.',
                variant: 'success'
            });
            this.dispatchEvent(e);
            this.showCalendarCall = null;
            this.template.querySelector("c-lwc-calender").getAllEvents();
        }).catch(error => {
        }).finally(()=>{
            //this.initialiseFullCalendarJs();
        })
    }

    rowSelChangeEvent(event){
        var count = 0;
        this.leadTeamData.forEach(element => {
            if (count == event.currentTarget.dataset.id) {
                element.Sequence__c = event.target.value;
            }
            count++;
        });
    }

    rowMonChangeEvent(event){
        var count = 0;
        this.leadTeamData.forEach(element => {
            if (count == event.currentTarget.dataset.id) {
                element.Monday__c = event.target.value;
            }
            count++;
        });
    }

    rowTueChangeEvent(event){
        var count = 0;
        this.leadTeamData.forEach(element => {
            if (count == event.currentTarget.dataset.id) {
                element.Tuesday__c = event.target.value;
            }
            count++;
        });
    }

    rowWedChangeEvent(event){
        var count = 0;
        this.leadTeamData.forEach(element => {
            if (count == event.currentTarget.dataset.id) {
                element.Wednesday__c = event.target.value;
            }
            count++;
        });
    }

    rowThuChangeEvent(event){
        var count = 0;
        this.leadTeamData.forEach(element => {
            if (count == event.currentTarget.dataset.id) {
                element.Thursday__c = event.target.value;
            }
            count++;
        });
    }

    rowFriChangeEvent(event){
        var count = 0;
        this.leadTeamData.forEach(element => {
            if (count == event.currentTarget.dataset.id) {
                element.Friday__c = event.target.value;
            }
            count++;
        });
    }

    deleteTeam(event){
        var inx = event.currentTarget.dataset.id;
        deleteRowCall({indx : inx, leadTeams : this.leadTeamData})
        .then(result => {
            this.leadTeamData = result;
        });
    }

    distributeLeads(){
        var id = this.selectLeadSheetId+'';

        distributeLeads({lsId : id, szt : this.leadTeamData, startcalldate: this.startcalldate, startoptimaldate : this.startoptimaldate, lookforwarddate : this.lookforwarddate, daystodistribute: this.daystodistribute})
        .then(result => {
            if(result.indexOf('Batch Id:') != -1){
                this.showLeadDistribution = null;
                const e = new ShowToastEvent({
                    title: '',
                    message: 'Lead Distribution Successfull. ' + result,
                    variant: 'success'
                });
                this.dispatchEvent(e);
            }
            else{
                const e = new ShowToastEvent({
                    title: '',
                    message: result,
                    variant: 'error'
                });
                this.dispatchEvent(e);
            }
        });
    }

    handleTextChange(event){
        this.sheetName = event.target.value;
    }

    getSheetData(event){
        var lsId = event.target.id.substr(0, 15);
        this.selectLeadSheetId = lsId;
        this.showLoader = true;
        readLeadSheetData({id : lsId})
        .then(result => {
            console.log(result);
            this.selectLeadSheetId = result.Id;
            this.sheetName = result.Name;
        });

        readLeadData({id : lsId})
        .then(result => {
            this.leadData = result;
            this.lCount = result.length;
            result.length > 0 ? this.showTable = true : this.showTable = null;
            result.length > 0 ? this.showLeadDistributionBtn = true : this.showLeadDistributionBtn = null;
            this.showLoader = null;
        });
    }

    submitDetails(){
        this.showNew = null;
        this.showLoader = true;
        
        saveLeadSheet({name : this.sheetName})
        .then(result => {
            this.selectLeadSheetId = result;
            this.showLoader = null;
            
            if(result.indexOf('Error') != -1){
                const e = new ShowToastEvent({
                    title: '',
                    message: result,
                    variant: 'error'
                });
                this.dispatchEvent(e);
            }
            else{
                const e = new ShowToastEvent({
                    title: '',
                    message: 'Lead Sheet saved successfully. Please add leads, columns etc.',
                    variant: 'success'
                });
                this.dispatchEvent(e);
                this.connectedCallback();
                this.refreshData();
            }
        });
    }

    refreshData(){
        this.showImportLead = null;
        this.showLoader = true;
        
        readLeadSheetData({id : this.selectLeadSheetId})
        .then(result => {
            console.log(result);
            this.selectLeadSheetId = result.Id;
            this.sheetName = result.Name;
        });

        readLeadData({id : this.selectLeadSheetId})
        .then(result => {
            this.leadData = result;
            this.lCount  = result.length;
            result.length > 0 ? this.showTable = true : this.showTable = null;
            result.length > 0 ? this.showLeadDistributionBtn = true : this.showLeadDistributionBtn = null;
            this.showLoader = null;
        });
    }

    emailSheet(){
        this.showEmail = true;
    }

    availabilityEmail(){
        this.showAvailabilityEmail = true;
    }

    emailAvailabilityCall(){
        this.showLoader = true;
        var uId = this.selectedUserId+'';
        sendEmail({userId : uId}).then(result => {
            this.showLoader = null;
            this.showAvailabilityEmail = null;

            const e = new ShowToastEvent({
                title: '',
                message: 'Email sent successfully.',
                variant: 'success'
            });
            this.dispatchEvent(e);
        });
    }

    exportSheet(){
        this.showLoader = true;

        var sData = '';
        
        sData += 'First Name, Last Name, Email, Phone \r\n';

        for(var i=0; i < this.leadData.length; i++){
            var d = this.leadData[i];
            sData += d.FirstName+','+d.LastName+','+d.Email+','+d.Phone;
            sData += "\r\n";
        }

        let downloadElement = document.createElement('a');
        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(sData);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'LeadSheet.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click(); 

        this.showLoader = null;
    }

    clickToDial(){

    }

    shareSheet(){
        this.showShare = true;
        this.showLoader = true;
        
        getShareList({lsId : this.selectLeadSheetId})
        .then(result => {
            this.shareData = result;
            this.showLoader = null;
        });
    }

    handleAccessChange(event){
        this.accLevValue = event.detail.value;
    }

    deleteAccess(event){
        var acessId = event.target.id.substr(0, 15);

        deleteShareRecord({aId : acessId}).then(result => {
            this.showLoader = null;
            if(result.indexOf('Error') != -1){
                const e = new ShowToastEvent({
                    title: '',
                    message: result,
                    variant: 'error'
                });
                this.dispatchEvent(e);
            }
            else{
                this.accLevValue = 'Read';
                this.shareSheet();
                const e = new ShowToastEvent({
                    title: '',
                    message: result,
                    variant: 'success'
                });
                this.dispatchEvent(e);
            }
        });
    }

    shareSheetCall(){
        if(this.selectedUserId != undefined)
        {
            var usrId = this.selectedUserId+'';
            var acLvl = this.accLevValue+'';

            shareLeadSheet({lsId : this.selectLeadSheetId, userId : usrId, accessLevel : acLvl}).then(result => {
                this.showLoader = null;
                if(result.indexOf('Error') != -1){
                    const e = new ShowToastEvent({
                        title: '',
                        message: result,
                        variant: 'error'
                    });
                    this.dispatchEvent(e);
                }
                else{
                    this.showShare = null;
                    this.accLevValue = 'Read';
                    const e = new ShowToastEvent({
                        title: '',
                        message: 'User access saved successfully.',
                        variant: 'success'
                    });
                    this.dispatchEvent(e);
                }
            });
        }
        else{
            alert('Please select the user.');
        }
    }

    emailLeadSheetCall(){
        const e = new ShowToastEvent({
            title: '',
            message: 'Email sent successfully.',
            variant: 'success'
        });
        this.dispatchEvent(e);
        this.closeModal();
    }
}
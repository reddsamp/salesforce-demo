import { LightningElement, api, track } from 'lwc';

export default class FormField extends LightningElement {
    @api fieldName;
    @api objectName;

    handleChange(event) {
        var vl = event.target.value;

        if(this.fieldName == 'AccountId'){
            const custEvent = new CustomEvent(
                'callpasstoparentai', {
                    detail: vl
                });
            this.dispatchEvent(custEvent);
        }
        else if(this.fieldName == 'CLO_Sales_Org__c'){
            const custEvent = new CustomEvent(
                'callpasstoparentso', {
                    detail: vl
                });
            this.dispatchEvent(custEvent);
        }
    }
}
import { LightningElement, api, track } from 'lwc';
import fetchMaterialAvaliablity from '@salesforce/apex/CLO_MaterialAviabilityController.fetchMaterialAvaliablity';
import submitForApproval from '@salesforce/apex/CLO_MaterialAviabilityController.submitForApproval';
import queryAccountId from '@salesforce/apex/CLO_MaterialAviabilityController.queryAccountId';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import Id from '@salesforce/user/Id';

export default class CLO_MaterialAvailability extends LightningElement {
    @track selectedSalesOrg = '';
    @track selectedPlant = '';
    @track selectedMaterial = '';
    @track selectedEmail = '';
    @track selectedCustomer = '';
    @track consignmentStock = false;
    
    @track showLoader;
    @track showModal;

    @track dataList;
    @track selectedList;
    @track dataListLength;
    @track accId;
    @track mNum;

    @track sDate = new Date();
    @track status = 'Draft';
    @track oType = 'ZCON';
    @track butnDisabled = true;
    @track userId;

    handlePlantChange(event) {
        this.selectedPlant = event.target.value;
    }

    openPopup(){
        this.showModal = true;
    }

    openOrderPopup(event){
        this.showLoader = true;
            
        var acName = event.currentTarget.dataset.id;
        this.mNum = event.currentTarget.dataset.id2;
        
        queryAccountId({cName : acName})
        .then(result => {
            this.accId = result;
            this.showLoader = null;
            this.showModal = true;
        });
    }

    updateUserId(event){
        this.userId = event.target.value;
    }

    checkSelectedProducts(event){
        this.userId = Id;
        var ck = event.target.checked;
        var srno = event.currentTarget.dataset.id3;
        var KUNNR = event.currentTarget.dataset.id;
        var plant = event.currentTarget.dataset.id2;
        var acName = event.currentTarget.dataset.id4;

        queryAccountId({cName : acName})
        .then(result => {
            this.accId = result;
        });

        this.dataList.forEach(function(d){
            if(srno == d.SRNO){
                d.isSelected = ck;
            }
            
            if(d.KUNNR == KUNNR && d.WERKS == plant){
                d.showQty = false;
            }
            else{
                d.showQty = true;
            }
        });

        var isOneSelected = false;
        this.selectedList = [];
        var dta = []
        var i = 0;
        this.dataList.forEach(function(d){
            if(d.isSelected){
                dta[i] = d;
                i++;
                isOneSelected = true;
            }
        });

        this.selectedList = dta;

        if(isOneSelected == false){
            this.butnDisabled = true;
            this.dataList.forEach(function(d){
                d.showQty = false;
            });
        }
        else
            this.butnDisabled = false;
    }

    quantityChange(event){
        var qun = event.currentTarget.dataset.id;
        var vl = event.target.value;

        var i = 0;
        this.selectedList.forEach(function(d){
            if(i == qun){
                if(vl.trim() == ''){
                    d.quantity = 1;
                }
                else{
                    d.quantity = parseInt(vl);
                    var aq = parseInt(d.LABST);

                    if(d.quantity > aq)
                        d.quantity = 1;
                }
            }
            i++;
        });
    }

    closeModal(){
        this.showModal = null;
    }

    onSubmitHandler(event) {
        event.preventDefault();
        // Get data from submitted form
        const fields = event.detail.fields;
        this.template
            .querySelector('lightning-record-edit-form').submit(fields);
    }

    handleSuccess(event) {
        var oId = event.detail.id;
        this.showLoader = true;
            
        submitForApproval({orderId : oId, productCode : this.mNum, items : this.selectedList})
        .then(result => {
            this.showLoader = null;
            if(result == 'Success'){
                this.showModal = null;
                //window.open('/'+oId, '_blank');
                const e = new ShowToastEvent({
                    "title": "Success!",
                    variant: 'success',
                    "message": "{0} submitted for approval. Click {1} to view.",
                    "messageData": [
                        'Consignment Transfer request',
                        {
                            url: '/'+oId,
                            label: 'here'
                        }
                    ]
                });
                this.dispatchEvent(e);
            }
            else{
                const e = new ShowToastEvent({
                    title: '',
                    message: result,
                    variant: 'error'
                });
                this.dispatchEvent(e);
            }
        });
    }

    handleMaterialChange(event) {
        this.selectedMaterial = event.target.value;
    }

    handleEmailChange(event) {
        this.selectedEmail = event.target.value;
    }

    handleConsignmentStockChange(event) {
        this.consignmentStock = event.target.checked;
    }

    passToParentSalesOrg(event){
        this.selectedSalesOrg = event.detail;
    }

    passToParentAccountId(event){
        this.selectedCustomer = event.detail;
    }

    callSearch(){
        this.showLoader = true;

        if(this.consignmentStock) this.consignmentStock = 'X';
        else this.consignmentStock = '';

        fetchMaterialAvaliablity({selectedSalesOrg : this.selectedSalesOrg,
                            selectedPlant : this.selectedPlant,
                            selectedMaterial : this.selectedMaterial,
                            selectedEmail : this.selectedEmail,
                            selectedCustomer : this.selectedCustomer,
                            consignmentStock: this.consignmentStock})
                .then(result => {
                    console.log(result);
                    this.showLoader = null;
                    this.dataList = result;

                    this.dataList.forEach(function(d){
                        d.showQty = false;
                    });

                    this.dataListLength = result.length;

                    if(this.dataListLength == 0)
                        this.dataListLength = null;
                });
    }
}
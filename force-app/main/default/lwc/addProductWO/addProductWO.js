import { LightningElement, track, api } from 'lwc';
import fetchFromWorkOrder from '@salesforce/apex/CLO_MaterialAviabilityController.fetchFromWorkOrder';
import { updateRecord } from 'lightning/uiRecordApi';
import saveWOProduct from '@salesforce/apex/CLO_MaterialAviabilityController.saveWOProduct';

export default class CLO_AddProductConsignmentTransfer extends LightningElement {
    @track showLoader;

    @track dataList;
    @track selectedList = [];
    @track selectedDataList;
    @track dataListLength;
    @track showModal;
    @api recordId;
    @track showSelectTable = true;
    @track showSelectedTable;
    @track showNextBtn;
    @track selectedProdcut;

    handleClick(){
        this.showModal = true;
        this.showSelectTable = true;
        this.showSelectedTable = null;
        this.dataList = [];
        this.dataListLength = null;
        this.selectedList = [];
        this.showNextBtn = null;
        this.showLoader = true;

        fetchFromWorkOrder({woId : this.recordId})
                .then(result => {
                    console.log(result);
                    this.showLoader = null;
                    this.dataList = result;

                    this.dataListLength = result.length;

                    if(this.dataListLength == 0)
                        this.dataListLength = null;
                });
    }

    getSelectedRow(event){
        this.selectedProdcut = event.target.id.substr(0, event.target.id.length-3);
        this.showLoader = true;
        
        saveWOProduct({woId : this.recordId, productCode : this.selectedProdcut})
                .then(result => {
                    this.showLoader = null;
                    window.location.reload(true);
                    this.showModal = null;
                });
    }

    removeProduct(event){
        if(confirm('Are you sure removing the product from workorder?')){
            saveWOProduct({woId : this.recordId, productCode : 'remove'})
            .then(result => {
                this.showLoader = null;
                window.location.reload(true);
                this.showModal = null;
            });
        }
    }

    closeModal(){
        this.showModal = null;
    }

    callSave(){
        this.showLoader = true;
        
        saveWOProduct({woId : this.recordId, productCode : this.selectedProdcut})
                .then(result => {
                    this.showLoader = null;
                });
    }
}
import { LightningElement, track, wire,api } from 'lwc';
import getAMPSTasks from '@salesforce/apex/CLO_AMPS_Activities_Controller.getAMPSTasks';
import saveAMPSTasks from '@salesforce/apex/CLO_AMPS_Activities_Controller.saveAMPSTasks';

import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import SUBJECT_FIELD from '@salesforce/schema/Task.Subject';
import COMMENT_FIELD from '@salesforce/schema/Task.Description';
import ID_FIELD from '@salesforce/schema/Task.Id';
// importing to refresh the apex if any record changes the datas
import {refreshApex} from '@salesforce/apex';
const actions = [
    { label: 'View Details', name: 'record_details'}
];
const columns = [
    { label:'Subject', fieldName: 'oppLink', type: 'url', sortable: true, typeAttributes: {label: {fieldName: 'Subject'}, tooltip:'Go to detail page', target: '_blank'}},
    { label: 'Status', fieldName: 'Status', type: 'text' ,sortable: true, cellAttributes: { alignment: 'left' }},
    { label: 'Comments', fieldName: 'Description', type: 'text',sortable: true, cellAttributes: { alignment: 'left' } },
    //{ label: 'Comments', fieldName: 'Amount', type: 'currency', cellAttributes: { alignment: 'left' } },
    { label: 'Due Date', fieldName: 'ActivityDate', type: 'date',sortable: true, typeAttributes:{timeZone:'UTC', year:'numeric', month:'numeric', day:'numeric'}},
    { label: 'Assigned To', fieldName: 'ownerURL', type: 'url', sortable: true, typeAttributes: {label: {fieldName: 'ownername'}, tooltip:'Go to detail page', target: '_blank'}},
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions,
            menuAlignment: 'right'
        }
    }
];
export default class AmpsTasks extends LightningElement {
    @track dataavailable = false;
    @track error;
    @track columns = columns;
    @track opps; //All opportunities available for data table    
    @track showTable = false; //Used to render table after we get the data from apex controller    
    @track recordsToDisplay = []; //Records to be displayed on the page
    @track rowNumberOffset = 0; //Row number
    @api recordId;
    @track ranger;
	@track left;
    @track top;
    @track hideLink = false;
    @track isModalOpen = false;
    @track sortBy='ActivityDate';
    @track sortDirection='asc';
    @track task;
    @track record = [];
    @track bShowModal = false;
    @track currentRecordId;
    @track isEditForm = false;
    @track showLoadingSpinner = false;

    selectedRecords = [];
    refreshTable;
    error;

    /*openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }*/
    @wire(getAMPSTasks,{accountID:'$recordId'})
    wopps({error,data}){
        
        if(data){
            this.refreshTable = data;
            this.dataavailable = true;
            console.log(data);
            let recs = [];
            for(let i=0; i<data.length; i++){
                let opp = {};
                opp.rowNumber = ''+(i+1);
                opp.oppLink = '/'+data[i].Id;
                opp.ownername = data[i].Owner.Name;
                opp.ownerURL = '/'+data[i].Owner.Id
                opp = Object.assign(opp, data[i]);
                recs.push(opp);
            }
            this.opps = recs;
            this.showTable = true;
        }else{
            this.error = error;
        }       
    }
    handleSave(event) {

        const fields = {};
        fields[ID_FIELD.fieldApiName] = event.detail.draftValues[0].Id;
        fields[COMMENT_FIELD.fieldApiName] = event.detail.draftValues[0].Description;

        const recordInput = {fields};

        updateRecord(recordInput)
        .then(() => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Task updated',
                    variant: 'success'
                })
            );
            // Clear all draft values
            this.draftValues = [];

            // Display fresh data in the datatable
            return refreshApex(this.getAMPSTasks);
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error creating record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }
    handleRowActions(event) {
        let actionName = event.detail.action.name;

        window.console.log('actionName ====> ' + actionName);

        let row = event.detail.row;

        window.console.log('row ====> ' + row);
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
            case 'delete':
                this.deleteCons(row);
                break;
        }
    }
    viewCurrentRecord(currentRow) {
        this.bShowModal = true;
        this.isEditForm = false;
        this.record = currentRow;
    }
    closeModal() {
        this.bShowModal = false;
    }
    editCurrentRecord(currentRow) {
        // open modal box
        this.bShowModal = true;
        this.isEditForm = true;

        // assign record id to the record edit form
        this.currentRecordId = currentRow.Id;
    }
    // handleing record edit form submit
    handleFirstSubmit(event) {
        // prevending default type sumbit of record edit form
        event.preventDefault();
        console.log("entered1");
        saveAMPSTasks( this.template.querySelector('lightning-input[data-formfield="Description"]').value,this.currentRecordId)
            .then(result => {
                console.log("entered");
             this.bShowModal = false;

        // showing success message
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success!!',
            message:  this.template.querySelector('lightning-input[data-formfield="Description"]').value+ ' '+this.currentRecordId  +' Contact updated Successfully!!.',
            variant: 'success'
        }),);})
            .catch(error => {this.bShowModal = false;
                console.log("error");

                // showing success message
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Success!!',
                    message: error,
                    variant: 'error'
                }),);
            });
        


       

        // querying the record edit form and submiting fields to form
        //this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);

        
    }

    // refreshing the datatable after record edit form success
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }
    updateColumnSorting(event){
          // field name
          this.sortBy = event.detail.fieldName;

          // sort direction
          this.sortDirection = event.detail.sortDirection;
  
          // calling sortdata function to sort the data based on direction and selected field
          this.sortData(event.detail.fieldName, event.detail.sortDirection);
      }
      
      //This sorting logic here is very simple. This will be useful only for text or number field.
      // You will need to implement custom logic for handling different types of field.
      sortData(fieldname, direction) {
        let parseData = JSON.parse(JSON.stringify(this.recordsToDisplay));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.recordsToDisplay = parseData;
      }

    //Capture the event fired from the paginator00 component
    handlePaginatorChange(event){
        this.recordsToDisplay = event.detail;
        try{
        this.rowNumberOffset = this.recordsToDisplay[0].rowNumber-1;
    }
    catch(Exception){
        this.rowNumberOffset = 0;
    }
    }
    showData(event){
        this.ranger = event.currentTarget.dataset.rangerid;
        this.left = event.clientX;
        this.top=event.clientY;
    }
    handleClick(event){
        if (this.hideLink==false){
        this.hideLink=true;}
        else{
            this.hideLink=false;
        }
    }
    hideData(event){
        this.ranger = "";
    }
    get assignClass() { 
		return this.active ? '' : 'slds-hint-parent';
  }
}
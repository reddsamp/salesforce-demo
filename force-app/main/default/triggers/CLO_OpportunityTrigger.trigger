trigger CLO_OpportunityTrigger on Opportunity (before insert) {

    
     Set<String> customerIds = new Set<String>();
    
    Map<Id, Account> customerMap = new Map<Id, Account>();
    
    Map<String, String> pricebookMap = new Map<String, String>();
    
    for(Opportunity o : trigger.new)
    {
        customerIds.add(o.AccountId);
        
    }
     customerMap = new Map<Id, Account>([select id, CurrencyIsoCode,CLO_Sales_Org__c, BillingCountry, BillingCountryCode from Account where id in : customerIds]);
   for(PriceBook2 pb : [select id, Name from PriceBook2])
    {
        pricebookMap.put(pb.Name, pb.Id);
    }
     for(Opportunity o : trigger.new)
    {
        Account acc = customerMap.get(o.AccountId);
        
        String priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountryCode);
        
        if(priceBookId == null)
            priceBookId = pricebookMap.get(acc.CLO_Sales_Org__c + ' ' + acc.BillingCountry);
            
        o.PriceBook2Id = priceBookId;
        o.CurrencyIsoCode = acc.CurrencyIsoCode;
    }
}
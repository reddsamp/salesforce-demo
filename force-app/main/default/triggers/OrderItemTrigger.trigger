trigger OrderItemTrigger on OrderItem (before insert) {

    Map<String, List<OrderItem>> orderItemMap = new Map<String, List<OrderItem>>();
    Set<String> productIdSet = new Set<String>();
    
    for(OrderItem rec : trigger.new)
    {
        productIdSet.add(rec.Product2Id);
    }
    
    if(productIdSet.size() > 0)
    {
        List<SBQQ__ProductOption__c> poList = [select id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c
                                               from SBQQ__ProductOption__c
                                               where SBQQ__ConfiguredSKU__c in : productIdSet];
        
        Map<String, integer> parentMap = new Map<String, integer>();
        Map<String, integer> childMap = new Map<String, integer>();
        
        integer d = 1;
        
        for(SBQQ__ProductOption__c po : poList){
            if(parentMap.containsKey(po.SBQQ__ConfiguredSKU__c))
            {
                integer dd = parentMap.get(po.SBQQ__ConfiguredSKU__c);
                childMap.put(po.SBQQ__OptionalSKU__c, dd);
            }
            else
            {
                parentMap.put(po.SBQQ__ConfiguredSKU__c, d);
                childMap.put(po.SBQQ__OptionalSKU__c, d);
                d++;
            }
        }
        
        
        for(OrderItem rec : trigger.new)
        {
            if(parentMap.containskey(rec.Product2Id))
                rec.CLO_Primary_Line_Number__c = parentMap.get(rec.Product2Id)+'';
            if(childMap.containskey(rec.Product2Id))
                rec.CLO_Parent_Bundle_Number__c = childMap.get(rec.Product2Id)+'';
        }
    }
}
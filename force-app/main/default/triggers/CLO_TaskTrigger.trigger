/**
  * @ClassName      : AssetTrigger 
  * @Description    : Trigger which handles all DML's on Task Object
  * @Author         : Markandeyulu K
  * @CreatedDate    : 28th July 2020
  */

trigger CLO_TaskTrigger on Task (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    //framework class which handles all events on this trigger.
    CLO_TriggerDispatcher.run(new CLO_TaskTriggerHandler(), 'CLO_TaskTrigger');
        
}
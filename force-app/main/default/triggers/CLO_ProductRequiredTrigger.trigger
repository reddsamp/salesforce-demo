/**
* @TriggerName    : CLO_ProductRequiredTrigger 
* @Description    : Trigger for ProductRequired Trigger 
* @Author         : Markandeyulu K
* @CreatedDate    : 21st Jan 2021
*/
trigger CLO_ProductRequiredTrigger on ProductRequired (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    CLO_TriggerDispatcher.run(new CLO_ProductRequiredTriggerHandler(), 'CLO_ProductRequiredTrigger');
}
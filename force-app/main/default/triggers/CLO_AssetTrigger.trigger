/**
  * @ClassName      : AssetTrigger 
  * @Description    : Trigger which handles all DML's on Asset Object
  * @Author         : Markandeyulu K
  * @CreatedDate    : 28th July 2020
  */

trigger CLO_AssetTrigger on Asset (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    //framework class which handles all events on this trigger.
     /*if (trigger.isAfter)
    {
        if (trigger.isInsert) CLO_AssetTriggerHandler2.CreateWorkOrder(trigger.new, trigger.oldmap);
        //if (trigger.isUpdate) CLO_WorkorderTriggerHandler.CreateOrder(trigger.new, trigger.oldmap);
        
    }*/
    CLO_TriggerDispatcher.run(new CLO_AssetTriggerHandler(), 'CLO_AssetTrigger');
}
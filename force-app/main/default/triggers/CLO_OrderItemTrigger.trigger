trigger CLO_OrderItemTrigger on OrderItem (before insert, after insert) {

    CLO_TriggerDispatcher.run(new CLO_OrderItemTriggerHandler(), 'CLO_OrderItemTrigger');
}
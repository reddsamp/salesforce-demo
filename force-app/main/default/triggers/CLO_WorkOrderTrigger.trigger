/**
  * @TriggerName    : CLO_WorkOrderTrigger 
  * @Description    : WorkOrder Object Trigger.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */
  trigger CLO_WorkOrderTrigger on WorkOrder(before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    CLO_TriggerDispatcher.run(new CLO_WorkOrderTriggerHandler(), 'CLO_WorkOrderTrigger');
    
    if (trigger.isAfter)
    {
        if (trigger.isInsert) CLO_WorkorderTriggerHandler.UpdateAsset(trigger.new, trigger.oldmap);
        if (trigger.isUpdate) CLO_WorkorderTriggerHandler.CreateOrder(trigger.new, trigger.oldmap);
        
    }
    
    if (trigger.isBefore)
    {
        CLO_WorkorderTriggerHandler.updateWorkorderTemplate(trigger.new);
    }
    
    if(trigger.isInsert && trigger.isBefore)
        CLO_WorkorderTriggerHandler.updatePriceBook(trigger.new);
}
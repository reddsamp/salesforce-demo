Trigger CLO_InsertMultiCurrencyPricebookEntries on Product2 (after insert) {
    
    if(!UserInfo.isMultiCurrencyOrganization()){
        return;
    }
    
    List<CurrencyType> lstCurrencyType = [SELECT IsoCode From CurrencyType WHERE IsActive = TRUE];
    Pricebook2 pb = [SELECT ID from Pricebook2 WHERE IsStandard = TRUE AND IsActive = TRUE];

    List<PricebookEntry> pbeList = new List<PricebookEntry>();

    if(trigger.isinsert && trigger.isAfter) {
        for (Product2 p : Trigger.new) {
            for(CurrencyType currType : lstCurrencyType){
                pbeList.add(new PricebookEntry(Pricebook2Id = pb.ID, Product2Id=p.ID, UnitPrice = 0, CurrencyIsoCode = currType.IsoCode, IsActive = TRUE, UseStandardPrice = FALSE));
            }
            
        }
        
        insert pbeList;
    }    
}
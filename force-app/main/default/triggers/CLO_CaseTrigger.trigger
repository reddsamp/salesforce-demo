/**
  * @ClassName      : CaseTrigger 
  * @Description    : Trigger which handles all DML's on Case Object
  * @Author         : Markandeyulu K
  * @CreatedDate    : 25th aug 2020
  */

trigger CLO_CaseTrigger on Case (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    //framework class which handles all events on this trigger. 
    CLO_TriggerDispatcher.run(new CLO_CaseTriggerHandler(), 'CLO_CaseTrigger');
}
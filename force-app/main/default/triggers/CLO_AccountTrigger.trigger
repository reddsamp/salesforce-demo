/**
  * @TriggerName    : CLO_AccountTrigger 
  * @Description    : Account Object Trigger.
  * @Author         : Ramana Thippireddy
  * @CreatedDate    : 22th June 2020
  */
  trigger CLO_AccountTrigger on Account(before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    CLO_TriggerDispatcher.run(new CLO_AccountTriggerHandler(), 'CLO_AccountTrigger');
}
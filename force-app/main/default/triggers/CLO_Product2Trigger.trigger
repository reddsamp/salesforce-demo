/**
  * @ClassName      : CLO_Product2Trigger 
  * @Description    : Trigger which handles all DML's on Product2 Object
  * @Author         : Ramana Reddy T
  * @CreatedDate    : 22nd Sep 2020
  */
trigger CLO_Product2Trigger on Product2 (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    CLO_TriggerDispatcher.run(new CLO_Product2TriggerHandler(), 'CLO_Product2Trigger');
    CLO_Product2TriggerHandler.updateProductBundle();
}
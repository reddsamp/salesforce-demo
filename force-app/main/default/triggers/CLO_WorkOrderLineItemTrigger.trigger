/**
  * @TriggerName    : CLO_WorkOrderLineItemTrigger 
  * @Description    : WorkOrder Object Trigger.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */
  trigger CLO_WorkOrderLineItemTrigger on WorkOrderLineItem(before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    CLO_TriggerDispatcher.run(new CLO_WorkOrderLineItemTriggerHandler(), 'CLO_WorkOrderLineItemTrigger');   
  }
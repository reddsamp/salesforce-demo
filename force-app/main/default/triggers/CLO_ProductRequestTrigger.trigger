/**
  * @ClassName      : ProductRequestTrigger 
  * @Description    : Trigger which handles all DML's on ProductRequest Object
  * @Author         : Markandeyulu K
  * @CreatedDate    : 3rd Nov 2020
  */

trigger CLO_ProductRequestTrigger on ProductRequest (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    //framework class which handles all events on this trigger.
    CLO_TriggerDispatcher.run(new CLO_ProductRequestTriggerHandler(), 'CLO_ProductRequestTrigger');
}
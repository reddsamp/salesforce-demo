/**
  * @ClassName      : CLO_AssessmentResTrigger 
  * @Description    : Trigger which handles all DML's on FCXM__Assessment_Response__c Object
  * @Author         : Charan K
  * @CreatedDate    : 9th Nov 2020
  */

trigger CLO_AssessmentResTrigger on FCXM__Assessment_Response__c (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    
    CLO_TriggerDispatcher.run(new CLO_AssessmentResTriggerHandler(), 'CLO_AssessmentResTrigger');
    
}
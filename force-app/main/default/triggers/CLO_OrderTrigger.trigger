/**
  * @TriggerName    : CLO_OrderTrigger 
  * @Description    : Order Object Trigger.
  * @Author         : Sreedhar
  * @CreatedDate    : 17th Oct 2020
  */
  trigger CLO_OrderTrigger on Order(before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
      CLO_TriggerDispatcher.run(new CLO_OrderTriggerHandler(), 'CLO_OrderTrigger');     
  }
trigger CLO_ServiceContractTrigger on ServiceContract (before insert, after insert, before update, after update, before delete, after delete, after unDelete) {
    
    CLO_TriggerDispatcher.run(new CLO_ServiceContractTriggerHandler(), 'CLO_ServiceContractTrigger'); 
   
}
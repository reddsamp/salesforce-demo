trigger CLO_ContractLineItemTrigger on ContractLineItem (before insert, after insert) {
if (trigger.isAfter && trigger.isInsert)
    {
        
        List<ContractLineItem> ContractLineItemList = new List<ContractLineItem>();
        List<ContractLineItem> lstContractLineItem = (List<ContractLineItem>) trigger.new;
        List<Entitlement> EntitlementList = new List<Entitlement>();
        for(ContractLineItem CI : trigger.new){
            if(CI.CLO_Asset_Id__c != null)
            {
                ContractLineItemList.add(CI); 
            }
        }
        
        if(ContractLineItemList.size() > 0)
        {	
            Boolean isStartDate = True;
            Boolean isEndDate = True;
            string slarecordId = [SELECT id from RecordType where DeveloperName ='CLO_SLA'].Id;
            string discountrecordId = [SELECT id from RecordType where DeveloperName ='CLO_Discount'].Id;
            for(ContractLineItem At : ContractLineItemList){
                if(At.CLO_End_Date__c != null && At.CLO_Start_Date__c != null )
                    isStartDate = False; isEndDate = False; 
                ServiceContract ServiceContractItem = [SELECT id,AccountId from ServiceContract where Id = :At.ServiceContractId Limit 1];
                Entitlement ET = new Entitlement(AccountId = ServiceContractItem.AccountId,
                                                     AssetId = At.CLO_Asset_Id__c,
                                                     RecordTypeId = slarecordId,
                                                     StartDate = isStartDate ? At.StartDate : At.CLO_Start_Date__c ,
                                                     EndDate = isEndDate ? At.EndDate : At.CLO_End_Date__c ,
                                                     ServiceContractId = ServiceContractItem.Id,
                                                      CLO_Billable_Status__c = 'Service Contract',
                                                     ContractLineItemId = At.Id,
                                                     Name = 'Standard SLA | Service Contract | '+ At.CLO_Asset_Serial_Number__c);
                    EntitlementList.add(ET);
                    
                    Entitlement ET2 = new Entitlement(AccountId = ServiceContractItem.AccountId,
                                                      AssetId = At.CLO_Asset_Id__c,
                                                      RecordTypeId = discountrecordId,
                                                      StartDate = isStartDate ? At.StartDate : At.CLO_Start_Date__c ,
                                                     EndDate = isEndDate ? At.EndDate : At.CLO_End_Date__c ,
                                                      ServiceContractId = ServiceContractItem.Id,
                                                       CLO_Billable_Status__c = 'Service Contract',
                                                      ContractLineItemId = At.Id,
                                                      Name = 'labor | Service Contract | '+ At.CLO_Asset_Serial_Number__c);
                    EntitlementList.add(ET2);
                    Entitlement ET3 = new Entitlement(AccountId = ServiceContractItem.AccountId,
                                                      AssetId = At.CLO_Asset_Id__c,
                                                      RecordTypeId = discountrecordId,
                                                      StartDate = isStartDate ? At.StartDate : At.CLO_Start_Date__c ,
                                                     EndDate = isEndDate ? At.EndDate : At.CLO_End_Date__c ,
                                                      ServiceContractId = ServiceContractItem.Id,
                                                       CLO_Billable_Status__c = 'Service Contract',
                                                     ContractLineItemId = At.Id,
                                                      Name = 'parts | Service Contract | '+ At.CLO_Asset_Serial_Number__c);
                    EntitlementList.add(ET3);
                    Entitlement ET4 = new Entitlement(AccountId = ServiceContractItem.AccountId,
                                                      AssetId = At.CLO_Asset_Id__c,
                                                      RecordTypeId = discountrecordId,
                                                      StartDate = isStartDate ? At.StartDate : At.CLO_Start_Date__c ,
                                                     EndDate = isEndDate ? At.EndDate : At.CLO_End_Date__c ,
                                                      ServiceContractId = ServiceContractItem.Id,
                                                      CLO_Billable_Status__c = 'Service Contract',
                                                      ContractLineItemId = At.Id,
                                                      Name = 'travel | Service Contract | '+ At.CLO_Asset_Serial_Number__c);
                    EntitlementList.add(ET4);
            }
             if(EntitlementList.size() > 0)
                insert EntitlementList;
            
        }
        
    }
}
trigger CLO_LeadTrigger on Lead (after update) {

    CLO_LeadTriggerHandler.syncAccountWithSAP(trigger.new);
}
({
	doInit : function(component, event, helper) {
         var getSettingsAction = component.get("c.getCompanySetting");

    getSettingsAction.setCallback(this, function(response) {
        if (component.isValid() && response !== null && response.getState() == 'SUCCESS') {
            component.set("v.companySetting", response.getReturnValue());
            console.log("Company Setting loaded.");
        } else {
            console.log("Failed to load Company Setting.");
        }
    });
    $A.enqueueAction(getSettingsAction);
		
	}
})
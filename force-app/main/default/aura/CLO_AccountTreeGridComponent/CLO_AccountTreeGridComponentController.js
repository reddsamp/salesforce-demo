({
    doInit: function(component) {
        var action = component.get("c.fetchHierarchyData");
        action.setParams({
            "recId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(actionResult) {
            component.set("v.data", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    showDivs: function(component, event) {
        var selectedItem = event.currentTarget;// this will give current element
        var recIndex = selectedItem.dataset.row;// this will give the count row index
        
        var x = document.getElementById(recIndex);
        if (x.style.display === "none") {
            x.style.display = "contents";
            document.getElementById(recIndex+'down').style.display = "block";
            document.getElementById(recIndex+'right').style.display = "none";
        } else {
            x.style.display = "none";
            document.getElementById(recIndex+'down').style.display = "none";
            document.getElementById(recIndex+'right').style.display = "block";
        }
    },
    gotoURL : function (component, event, helper) {
        var selectedItem = event.currentTarget;// this will give current element
        var recIndex = selectedItem.dataset.row;// this will give the count row index
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recIndex,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})
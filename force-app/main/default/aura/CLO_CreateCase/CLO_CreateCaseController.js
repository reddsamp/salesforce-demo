({
    doinit: function (cmp, event, helper) {
        var action = cmp.get("c.fetchCreditHold");
        action.setParams({
            'assetId' : cmp.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                if(result){
                    cmp.set('v.showModal', true);
                }
                else{
                    helper.fireGlobalHelper(cmp, event, helper);
                }
            }
        });
        $A.enqueueAction(action);
    },
    fireGlobal: function (cmp, event, helper) {
        cmp.set('v.showSpinner', false);
        cmp.set('v.showModal', false);
        helper.fireGlobalHelper(cmp, event, helper);
    },
    closeModal : function(component, event, helper) {
        component.set('v.showModal', false);
    }
})
({
    doInit : function(component, event, helper) {
        helper.getAccounts(component);
    },
    
    showPanel : function(component, event, helper){
        helper.onLoadPage(component);
    },
    
    navigateToAccountHierarchy: function(component, event, helper) {
        var acctId = component.get('v.recordId');
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "sfa:hierarchyFullView",
            componentAttributes: {
                recordId: acctId,
                sObjectName: "Account"
            }
        });
        evt.fire();
    }
})
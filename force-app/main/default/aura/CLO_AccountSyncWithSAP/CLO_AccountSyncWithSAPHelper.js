({
	helperInit : function(component, event, helper) {
		var action = component.get("c.getAccount");
        action.setParams({
            'accountId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                component.set('v.account', result);
                component.set('v.showComponent', true);
                if(result.RecordType == undefined || result.RecordType.Name == '')
                    component.set('v.recordTypeName', 'Prospect');
                else
                    component.set('v.recordTypeName', result.RecordType.Name);
            }
        });
        $A.enqueueAction(action);
	}
})
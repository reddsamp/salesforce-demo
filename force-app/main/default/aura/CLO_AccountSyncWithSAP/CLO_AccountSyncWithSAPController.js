({
	doInit : function(component, event, helper) {
        helper.helperInit(component, event, helper);
    },
    syncWithSAPJS : function(component, event, helper) {
        component.set('v.showSpinner', true);
        var action = component.get("c.syncWithSAP");
        action.setParams({
            'accountId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.helperInit(component, event, helper);
                component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                
                if(result.AccountNumber != undefined && result.AccountNumber != '')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Account successfully synced with SAP."
                    });
                    toastEvent.fire();
                	
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "SAP Sync Error!",
                        "type": "error",
                        "message": result.CLO_SAP_Sync_Error__c
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})
({
    showHidePanel : function(component, event, helper) {
        helper.getChildAccountsData(component);
        
        var id = component.get("v.acc.Id");
        var e = document.getElementById(id);
        if (e.style.display == 'block' || e.style.display==""){
            e.style.display = 'none';
            component.set("v.ext","plus");
        }else{
            e.style.display = 'block';
            component.set("v.ext","minus");
        }
    },
    showrecord : function(component, event, helper)
    {
        var selectedItem = event.currentTarget;
        var accountId= selectedItem.dataset.accId;
        alert(accountId);
    }
})
({
    getChildAccountsData: function(component) {
        var action = component.get("c.getChildAccounts");
        //Set up the callback
        var self = this;
        action.setParams({
            "ParentAccountid": component.get("v.acc.Id")
        });
        action.setCallback(this, function(actionResult) {
            component.set("v.lstChildAccounts", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})
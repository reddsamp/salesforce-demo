({
    doInit : function(component, event, helper) {
        helper.helperInit(component, event, helper);
        helper.helperInitOrderItems(component, event, helper);
    },
    closeModal : function(component, event, helper) {
        component.set('v.showModal', false);
    },
    syncWithSAPJS : function(component, event, helper) {
        component.set('v.showSpinner', true);
        var a = event.getSource();
        var id = a.getLocalId();
        var action = component.get("c.syncWithSAP");
        action.setParams({
            'orderId' : component.get("v.recordId"),
            'isSimulateOrder': (id=='btnSimulate'? true : false)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.helperInit(component, event, helper);
                component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                if(result.SAP_Order_Id__c != undefined && result.SAP_Order_Id__c != '')
                {
                    helper.helperInit(component, event, helper);
                    if(id=='btnSimulate'){
                        $A.get('e.force:refreshView').fire();
                        component.set('v.showModal', true);
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type": "success",
                            "message": "Order successfully synced with SAP."
                        });
                        toastEvent.fire();      
                        component.set('v.showModal', false);
                        $A.get('e.force:refreshView').fire();
                    }
                }
                else if(result.CLO_SAP_Sync_Error__c != undefined) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "SAP Sync Error!",
                        "type": "error",
                        "message": result.CLO_SAP_Sync_Error__c
                    });
                    toastEvent.fire();
                }
                else if(result.CLO_Paymetric_Error__c != undefined) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Paymetric Error!",
                        "type": "error",
                        "message": result.CLO_Paymetric_Error__c
                    });
                    toastEvent.fire();
                }
                else{
                    helper.helperInit(component, event, helper);
                    helper.helperInitOrderItems(component, event, helper);
                    if(id=='btnSimulate'){
                        $A.get('e.force:refreshView').fire();
                        component.set('v.showModal', true);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    }
})
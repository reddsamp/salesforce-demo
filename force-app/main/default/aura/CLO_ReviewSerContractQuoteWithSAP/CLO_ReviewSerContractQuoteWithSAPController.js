({
    doInit : function(component, event, helper) {
        helper.helperInit(component, event, helper);
        helper.helperInitOrderItems(component, event, helper);
    },
    closeModal : function(component, event, helper) {
        component.set('v.showModal', false);
    },
    syncWithSAPJS : function(component, event, helper) {
        component.set('v.showSpinner', true);
        var a = event.getSource();
        var id = a.getLocalId();
        var action = component.get("c.syncWithSAP");
        action.setParams({
            'quoteId' : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.helperInit(component, event, helper);
                component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                if(result.Name != undefined && result.Name != '')
                {
                    helper.helperInit(component, event, helper);
                    if(id=='btnSimulate'){
                        component.set('v.showModal', true);
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type": "success",
                            "message": "Quote successfully synced with SAP."
                        });
                        toastEvent.fire();      
                    }
                }
                else if(result.CLO_SAP_Sync_Error__c != undefined) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "SAP Sync Error!",
                        "type": "error",
                        "message": result.CLO_SAP_Sync_Error__c
                    });
                    toastEvent.fire();
                }
                    else{
                        helper.helperInit(component, event, helper);
                        helper.helperInitOrderItems(component, event, helper);
                        if(id=='btnSimulate'){
                            component.set('v.showModal', true);
                        }
                    }
            }
        });
        $A.enqueueAction(action);
    }
})
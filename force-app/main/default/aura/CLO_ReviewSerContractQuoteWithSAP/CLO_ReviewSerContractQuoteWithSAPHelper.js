({
	helperInit : function(component, event, helper) {
		var action = component.get("c.getQuote");
        action.setParams({
            'quoteId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.quote', result);
                component.set('v.showComponent', true);
                console.log('result :: '+result);
            }
            
        });
        $A.enqueueAction(action);
	},
    helperInitOrderItems : function(component, event, helper) {
		var action = component.get("c.getquoteItems");
        action.setParams({
            'quoteId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                component.set('v.quoteItems', result);
                
                var gross = 0;
                var discount = 0;
                var netvalue = 0;
                var tax = 0;
                	
                if(result.length > 0){
                    for(var i=0; i < result.length; i++){
                        if(result[i].SBQQ__ListTotal__c != undefined)
                            gross += parseFloat(result[i].SBQQ__ListTotal__c);
                        if(result[i].SBQQ__TotalDiscountAmount__c != undefined)
                            discount += parseFloat(result[i].SBQQ__TotalDiscountAmount__c);
                        if(result[i].SBQQ__NetTotal__c != undefined)
                            netvalue += parseFloat(result[i].SBQQ__NetTotal__c);
                        if(result[i].CLO_Tax__c != undefined)
                            tax += parseFloat(result[i].CLO_Tax__c);
                    }
                    
                    component.set('v.gross', gross);
                    component.set('v.discount', discount);
                    component.set('v.netvalue', netvalue);
                    component.set('v.tax', tax);
                }
                
                component.set('v.showComponent', true);
            }
        });
        $A.enqueueAction(action);
	}
})
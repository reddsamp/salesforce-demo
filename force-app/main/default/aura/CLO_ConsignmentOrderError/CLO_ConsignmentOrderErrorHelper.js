({
	helperInit : function(component, event, helper) {
		var action = component.get("c.getOrder");
        action.setParams({
            'orderId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.order', result);
                component.set('v.showComponent', true);
            }
        });
        $A.enqueueAction(action);
	},
    helperInitOrderItems : function(component, event, helper) {
		var action = component.get("c.getOrderItems");
        action.setParams({
            'orderId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                component.set('v.orderItems', result);
                
                var gross = 0;
                var discount = 0;
                var netvalue = 0;
                var tax = 0;
                	
                if(result.length > 0){
                    for(var i=0; i < result.length; i++){
                        if(result[i].CLO_Gross_Value__c != undefined)
                            gross += parseFloat(result[i].CLO_Gross_Value__c);
                        if(result[i].CLO_Discount__c != undefined)
                            discount += parseFloat(result[i].CLO_Discount__c);
                        if(result[i].CLO_Net_Value__c != undefined)
                            netvalue += parseFloat(result[i].CLO_Net_Value__c);
                        if(result[i].CLO_Tax__c != undefined)
                            tax += parseFloat(result[i].CLO_Tax__c);
                    }
                    
                    component.set('v.gross', gross);
                    component.set('v.discount', discount);
                    component.set('v.netvalue', netvalue);
                    component.set('v.tax', tax);
                }
                
                component.set('v.showComponent', true);
            }
        });
        $A.enqueueAction(action);
	}
})
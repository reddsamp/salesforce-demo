({
    doInit : function(component, event, helper) {
        helper.helperInit(component, event, helper);
        helper.helperInitOrderItems(component, event, helper);
    },
    closeModal : function(component, event, helper) {
        component.set('v.showModal', false);
    },
    syncWithSAPJS : function(component, event, helper) {
        
    }
})
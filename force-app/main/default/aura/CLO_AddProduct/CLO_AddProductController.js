({
	init: function (component, event, helper) {
        component.set('v.columns', [{label: 'Product Name', fieldName: 'Name', type: 'text'},
            {label: 'Product Code', fieldName: 'ProductCode', type: 'text'},
            {label: 'List Price', fieldName: 'StockKeepingUnit', type: 'text'},
            {label: 'Product Description', fieldName: 'Description', type: 'text'},
            {label: 'Product Family', fieldName: 'Family', type: 'text'},
        ]);
    },
    handleClick : function(component, event, helper) {
		component.set('v.editModal', false);
        component.set('v.stepnumber', 1);
        component.set('v.header', 'Add Products');
        component.set('v.selectedRowsCount', 0);
        component.set('v.selectedData', []);
        component.set('v.selectedDataPage', []);
        component.set('v.orderItems', []);
        component.set("v.searchText", "");
        component.set('v.showModal', true);
                                    
        helper.queryOrder(component, event, helper);
    },
    closeModal : function(component, event, helper) {
        component.set('v.showModal', false);
    },
    updateSelectedText: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        /*var previewRows = component.get('v.selectedData');
        
        if(previewRows == null)
        	previewRows = [];
        
        var newArray = [];
        alert(selectedRows.length);
        alert(previewRows.length);
        if(selectedRows.length > 1){
            var ar = [];
            for(var i=0; i < (previewRows.length-1); i++){
                ar[i] = previewRows[i];
            }
            alert(ar.length);
            newArray = ar.concat(selectedRows);
        }
        else{
            newArray = previewRows.concat(selectedRows);
        }*/
        
        component.set('v.selectedDataPage', selectedRows);
        component.set('v.selectedRowsCount', selectedRows.length);
    },
    goNext: function (component, event, helper) {
        var previewRows = component.get('v.selectedData');
        var selectedRows = component.get('v.selectedDataPage');
        if(previewRows == null)
        	previewRows = [];
        
        var newArray = previewRows.concat(selectedRows);
        component.set('v.selectedData', newArray);
        
        helper.fillOrderItems(component, event, helper);
        component.set('v.stepnumber', 2);
        component.set('v.header', 'Edit selected Order Products');
    },
    goBack: function (component, event) {
        component.set('v.stepnumber', 1);
        component.set('v.header', 'Add Products');
    },
    handleEdit: function (component, event) {
        component.set('v.showModal', true);
        component.set('v.editModal', true);
       	component.set('v.stepnumber', 2);
        component.set('v.header', 'Edit selected Order Products');
        component.set('v.orderItems', []);
                                    
        var action = component.get("c.gtEditOrderIteams");
        action.setParams({
            'orderId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                component.set("v.orderItems", result);
            }
        });
        $A.enqueueAction(action);
    },
    callSave: function (component, event) {
        var orderItms = component.get("v.orderItems");
        
        var isQAdded = true;
        for(var i=0; i < orderItms.length; i++){
            if(orderItms[i].Quantity == undefined || orderItms[i].Quantity == '')
                isQAdded = false;
        }
        
        if(isQAdded){
            component.set('v.showSpinner', true);
            var action = component.get("c.saveProducts");
            action.setParams({
                'orderItems' : component.get("v.orderItems")
            });
            action.setCallback(this, function(response) {
                var result = response.getReturnValue();
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set('v.showSpinner', false);
                    component.set('v.showModal', false);
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Order Items saved successfully."
                    });
                    toastEvent.fire();    
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": result
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Please enter quantity."
            });
            toastEvent.fire();
        }
    },
    searchProducts: function (component, event) {
        component.set('v.showSpinner', true);
        var searchStr = component.get("v.searchText");
        /*var alldata = component.get("v.alldata");
        component.set("v.data", []);
        var dta = [];
        searchStr = searchStr.trim();
        
        if(searchStr != ''){
            console.log(alldata.length);
        	for(var i=0; i < alldata.length; i++){
                console.log('alldata');
                console.log(alldata[i]);
                console.log(alldata[i].ProductCode.indexOf(searchStr));
                
                if(alldata[i].Name != undefined && alldata[i].Name.indexOf(searchStr) != -1)
                    dta.push(alldata[i]);
                else if(alldata[i].ProductCode != undefined && alldata[i].ProductCode.indexOf(searchStr) != -1)
                    dta.push(alldata[i]);
                else if(alldata[i].Family != undefined && alldata[i].Family.indexOf(searchStr) != -1)
                    dta.push(alldata[i]);
                else if(alldata[i].Description != undefined && alldata[i].Description.indexOf(searchStr) != -1)
                    dta.push(alldata[i]);
            }    
            component.set("v.data", dta);
        }
        else{
            component.set("v.data", alldata);
        }*/
        
        var action2 = component.get("c.queyProducts");
        action2.setParams({
            'orderId' : component.get("v.recordId"),
            'priceBookId' : component.get("v.order").Pricebook2Id,
            'searchStr' : component.get("v.searchText")
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                component.set("v.data", result);
            }
        });
        $A.enqueueAction(action2);
    },
    callAddMore: function (component, event, helper) {
        component.set('v.selectedDataPage', []);
        component.set('v.stepnumber', 1);
        component.set('v.header', 'Add Products');
        component.set('v.selectedRowsCount', 0);
        component.set("v.searchText", "");
                                    
        helper.queryOrder(component, event, helper);
    }
})
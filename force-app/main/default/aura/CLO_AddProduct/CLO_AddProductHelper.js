({
	queryOrder: function (component, event, helper) {
        var action = component.get("c.queyOrder");
        action.setParams({
            'orderId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                component.set("v.order", result);
                
                var action2 = component.get("c.queyProducts");
                action2.setParams({
                    'orderId' : component.get("v.recordId"),
                    'priceBookId' : result.Pricebook2Id,
                    'searchStr' : component.get("v.searchText")
                });
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set('v.showSpinner', false);
                        
                        var result = response.getReturnValue();
                        component.set("v.data", result);
                        component.set("v.alldata", result);
                    }
                });
                $A.enqueueAction(action2);     
            }
        });
        $A.enqueueAction(action);                      
    },
    fillOrderItems: function (component, event, helper) {
        var orderItms = component.get("v.orderItems");
        console.log('orderItms');
        console.log(orderItms);
        component.set('v.showSpinner', true);
        var action = component.get("c.gtOrderIteams");
        action.setParams({
            'order' : component.get("v.order"),
            'products' : component.get("v.selectedData"),
            'orderItms' : orderItms
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.showSpinner', false);
                
                var result = response.getReturnValue();
                component.set("v.orderItems", result);
            }
        });
        $A.enqueueAction(action);                     
    }
})
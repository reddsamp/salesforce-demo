({
	helperInit : function(component, event, helper) {
		var action = component.get("c.getWorkOrder");
        action.setParams({
            'workOrderId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.workOrder', result);
                component.set('v.showComponent', true);
                console.log('result :: '+result);
            }
            
        });
        $A.enqueueAction(action);
	},
    helperInitOrderItems : function(component, event, helper) {
		var action = component.get("c.getWoPrItems");
        action.setParams({
            'workOrderId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                component.set('v.wrapWOPRItems', result);
                               
                var gross = 0;
                var discount = 0;
                var netvalue = 0;
                var tax = 0;
                 
                if(result.length > 0){
                    for(var i=0; i < result.length; i++){
                        if(result[i].grossVal != undefined)
                            gross += parseFloat(result[i].grossVal);
                        if(result[i].discount != undefined)
                            discount += parseFloat(result[i].discount);
                        if(result[i].netVal != undefined)
                            netvalue += parseFloat(result[i].netVal);
                        if(result[i].tax != undefined)
                            tax += parseFloat(result[i].tax);
                    }
                    
                    component.set('v.gross', gross);
                    component.set('v.discount', discount);
                    component.set('v.netvalue', netvalue);
                    component.set('v.tax', tax);
                }
                
                component.set('v.showComponent', true);
            }
        });
        $A.enqueueAction(action);
	}
})
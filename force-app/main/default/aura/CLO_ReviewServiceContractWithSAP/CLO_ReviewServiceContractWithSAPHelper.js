({
	helperInit : function(component, event, helper) {
		var action = component.get("c.getServiceContract");
        action.setParams({
            'serviceContractId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.serviceContract', result);
                component.set('v.showComponent', true);
                console.log('result :: '+result);
            }
            
        });
        $A.enqueueAction(action);
	},
    helperInitOrderItems : function(component, event, helper) {
		var action = component.get("c.getContractLineItems");
        action.setParams({
            'serviceContractId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                
                component.set('v.contractLineItems', result);
                
                var gross = 0;
                var discount = 0;
                var netvalue = 0;
                var tax = 0;
                	
                if(result.length > 0){
                    for(var i=0; i < result.length; i++){
                        if(result[i].CLO_List_Total__c != undefined)
                            gross += parseFloat(result[i].CLO_List_Total__c);
                        if(result[i].CLO_Total_Discount_Amount__c != undefined)
                            discount += parseFloat(result[i].CLO_Total_Discount_Amount__c);
                        if(result[i].TotalPrice != undefined)
                            netvalue += parseFloat(result[i].TotalPrice);
                        if(result[i].CLO_Tax__c != undefined)
                            tax += parseFloat(result[i].CLO_Tax__c);
                    }
                    
                    component.set('v.gross', gross);
                    component.set('v.discount', discount);
                    component.set('v.netvalue', netvalue);
                    component.set('v.tax', tax);
                }
                
                component.set('v.showComponent', true);
            }
        });
        $A.enqueueAction(action);
	}
})